<script type="text/javascript">
    var mainApp = angular.module("mainApp", []);

    mainApp.config(function ($httpProvider) {
        $httpProvider.defaults.headers.common['Access-Control-Allow-Headers'] = 'Content-Type';
    $httpProvider.defaults.headers.common['Access-Control-Allow-Methods']= 'GET, POST, OPTIONS';
    $httpProvider.defaults.headers.common['Access-Control-Allow-Origin']= '*';
    });

    mainApp.controller('affiliatesController', function($scope, $http) {
        $scope.affiliateList = [];
			
    $scope.map;
    $scope.geocoder;

    $scope.initlatitude = -23.5697431;
    $scope.initlongitude = -46.7033343;

    $scope.prodChek = 'TRE';
    $scope.address = '';

    $scope.markers =[];

    $scope.fristUperCase = function(str) {
        str = str.toLowerCase().replace(/(?:^|\s)\S/g, function(a) { return a.toUpperCase(); });
				return str;
    };

    $scope.clearphone = function(str) {
        str = str.replace('NULL', '').replace('.00000', '');
				return str;
    };


    $scope.clearMap = function() {
        $('#loadingform').show();
				$scope.markers =[];
				$scope.affiliateList =[];
				$scope.initMap(16);
    }

    $scope.initMap = function(zm) {	
				var mapOptions = {
			    	center: new google.maps.LatLng($scope.initlatitude, $scope.initlongitude),
    zoom: zm,
    mapTypeId: google.maps.MapTypeId.ROADMAP
    };

				$scope.map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);

				var marker = new google.maps.Marker({
				    position: new google.maps.LatLng($scope.initlatitude, $scope.initlongitude),
    map: $scope.map
				});

				$scope.geocoder = new google.maps.Geocoder();

    infoWindow = new google.maps.InfoWindow();

    google.maps.event.addListener($scope.map, 'click', function() {
			    	infoWindow.close();
    });
    }

    $scope.getLocation = function() {
        $scope.initMap(12);
				$scope.autoComplete();
				if(navigator.geolocation) {
				    navigator.geolocation.getCurrentPosition(function(position) {
            $scope.initlatitude = position.coords.latitude;
            $scope.initlongitude = position.coords.longitude;
            $scope.initMap(18);
				    },
            function(error) {
                alert('Erro ao obter localização!');
                console.log('Erro ao obter localização.', error);
            });
				} else {
        console.log('Navegador não suporta Geolocalização!');
				}
    }

    $scope.listMarker = function(markersData) {
				for (var i = 0; i < markersData.length; i++) {  
					var latlng = new google.maps.LatLng(markersData[i].geolocation.lat, markersData[i].geolocation.long);
    $scope.createMarker(latlng, $scope.fristUperCase(markersData[i].tradingName), $scope.fristUperCase(markersData[i].category), $scope.fristUperCase(markersData[i].address) +' - '+$scope.fristUperCase(markersData[i].district) +', '+$scope.fristUperCase(markersData[i].city) +' - '+$scope.fristUperCase(markersData[i].state), $scope.clearphone(markersData[i].telephone), markersData[i].programmes);
				}
    <!-- 				var markerCluster = new MarkerClusterer($scope.map, $scope.markers); -->
				$("#loadingform").hide();
    };


    $scope.createMarker = function(latlng, name, category, address, phone, programmes) {
				if($scope.prodChek == 'TKE') {
		   			$scope.prodChek = 'TK';
				} else if($scope.prodChek == 'TRE') {
					for(var i=0; i < programmes.length; i++) {
						if(programmes[i]== 'AVANTE') {
        $scope.prodChek = 'Avante';
    }
    }
				} if($scope.prodChek == 'TR') {
        $scope.prodChek = 'TRE';
				}
    var image = '/portal/ticket/img/pin-'+$scope.prodChek+'.png';
    var marker = new google.maps.Marker({
			    	map: $scope.map,
    position: latlng,
    title: name,
    icon: image,
    draggable: true,
    animation: google.maps.Animation.DROP
    });

				if($scope.prodChek == 'Avante') {
        google.maps.event.addListener(marker, 'click', function() {
            var iwContent = '<div id="iw_container">' +
                '<div class="iw_title"><strong>' + name + '</strong></div>' +
                '<div class="iw_content">' + category + '<br />' +
                address + '<br />Telefone: ' + phone +
                '<br /><br />Aqui você encontra uma rede diferenciada.<br />' +
                'Conheça o programa <a href="http://www.ticket.com.br/portal/servicos-diferenciais/programa-avante/" target="_blank" style="text-decoration: underline;font-weight: bold;">Avante da Ticket.</a></div></div>';

					      	infoWindow.setContent(iwContent);
            infoWindow.open($scope.map, marker);
        });
				} else {
        google.maps.event.addListener(marker, 'click', function() {
            var iwContent = '<div id="iw_container">' +
                '<div class="iw_title"><strong>' + name + '</strong></div>' +
                '<div class="iw_content">' + category + '<br />' +
                address + '<br />Telefone: ' + phone + '</div></div>';

					      	infoWindow.setContent(iwContent);
            infoWindow.open($scope.map, marker);
        });
				}

				$scope.markers.push(marker);
    };

    $scope.selectprod = function(id) {
        $scope.clearMap();
    if(id != null) {
			    	$scope.prodChek = id;
    $('#product').val(id);
    }
    if($("#buscaEnderecoNome").val() != "") {
		    		$scope.address = $("#buscaEnderecoNome").val();
    $scope.geocoder.geocode({'address': $scope.address}, function(results, status) {
						if (status == google.maps.GeocoderStatus.OK) {
        $("#lat").val(results[0].geometry.location.lat());
    $("#lon").val(results[0].geometry.location.lng());
    }
    });
    } else {
		    		$("#lat").val($scope.initlatitude);
    $("#lon").val($scope.initlongitude);
				}
				$http({
        method : 'GET',
    params: {
        buscaEnderecoNome : $("#buscaEnderecoNome").val(),
    lat: $("#lat").val(),
    lon: $("#lon").val(),
    limit: 500,
    offset: 1,
    product: $("#product").val(),
    radius: $("#radius").val(),
    token: 'MkM5RUUyNkYzRkQ5OEM1MTAxM0ZFMjg0QTdGNTBGNTItTW9uIEp1bCAxNSAxMDoyODowMCBCUlQgMjAxMw=='
    },
    url: '/portal-web/affiliatenetwork/localizacao/json'
				}).success(function(data, status) {
					if (data.status) {
        $scope.affiliateList = data.affiliate;
    $scope.listMarker(data.affiliate);
    analytcTicket('Rede Credenciada', 'submit', 'Busca - '+$scope.prodChek, 'sucesso');
    } else {
        $("#loadingform").hide();
    alert('Não foram encontrados estabelecimentos próximo a este endereço!');
    analytcTicket('Rede Credenciado', 'submit', 'Busca - '+$scope.prodChek, 'erro');
    }
				}).error(function(data, status) {
        $scope.data = data || "Request failed";
    $scope.status = status;
				});
				return $scope.affiliateList;
    };

    $scope.setCenterPos = function(lat, long) {
				var location = new google.maps.LatLng(lat, long);
    $scope.map.setCenter(location);
    $scope.map.setZoom(22);
    };

    $scope.autoComplete = function() {
        $("#buscaEnderecoNome").autocomplete({
            source: function(request, response) {
                $scope.geocoder.geocode({ 'address': request.term + ', Brasil', 'region': 'BR' }, function(results, status) {
                    response($.map(results, function(item) {
                        return {
                            label: item.formatted_address,
                            value: item.formatted_address,
                            latitude: item.geometry.location.lat(),
                            longitude: item.geometry.location.lng()
                        }
                    }));
                })
            },
            select: function(event, ui) {
                $("#lat").val(ui.item.latitude);
                $("#lon").val(ui.item.longitude);
                $scope.initlatitude = ui.item.latitude;
                $scope.initlongitude = ui.item.longitude;
                $scope.initMap(16);
            }
        });
    };

    $scope.getLocation();
    });
</script>