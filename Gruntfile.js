module.exports = function (grunt) {
	grunt.initConfig({
		clean: {
			dist: {
				src: 'dist'
			},
			configs:['src/app/app.js', 'src/app/config.js', 'src/index.htm', 'src/app/modules/beneficio-club/index.htm', 'src/app/modules/shared/scripts/controllers/headercontroller.js'],
			configsbase:['dist/src/app/_*.js', 'dist/src/_index.htm', 'dist/src/app/modules/beneficio-club/_index.htm', 'dist/src/app/modules/shared/scripts/controllers/_headercontroller.js'],
			arquivoNaoLazy:[
				'dist/src/app/shared/directives/*.js',
				'dist/src/app/modules/shared/scripts/controllers/maincontroller.js',
				'dist/src/app/modules/shared/scripts/controllers/menucontroller.js',
				'dist/src/app/modules/shared/scripts/factories/*.js',
				'!dist/src/app/modules/shared/scripts/factories/homefactory.js',
				'dist/src/app/modules/shared/scripts/services/*.js',
				'dist/src/app/modules/autenticacao/scripts/factories/autenticacaofactory.js',
				'dist/src/app/modules/beneficiario/scripts/factories/cadastrofactory.js',
				'dist/src/app/modules/beneficiario/scripts/factories/reemissaofactory.js',
				'dist/src/app/modules/beneficiario/scripts/factories/bloqueiofactory.js',
				'dist/src/app/modules/beneficiario/scripts/factories/desbloqueiofactory.js',
				'dist/src/app/modules/beneficiario/scripts/factories/cartaosenhafactory.js',
				'dist/src/app/modules/beneficiario/scripts/factories/alertafactory.js',
				'dist/src/app/modules/rede-credenciada/scripts/factories/redecredenciadafactory.js',
				'dist/src/app/modules/consulta-saldo/scripts/factories/consultasaldofactory.js',
				'dist/src/app/modules/beneficiario/scripts/factories/accentivfactory.js'
			],
			src: {
				src: 'dist/src'
			},
			souticket:{
				src: 'src/souticket'
			},
			index_souticket:{
				src: 'dist/src/souticket/_index.htm'
			}
		},

		copy: {
			public: {
				expand: true,
				// cwd: 'src',
				src: ['src/app/**/*.js'],
				dest: 'dist',
				filter: 'isFile',
			},
			app: {
				expand: false,
				// cwd: 'src',
				src: ['src/app/app.js'],
				dest: 'dist/src/app/app.js',
				filter: 'isFile',
			},
			config: {
				expand: false,
				// cwd: 'src',
				src: ['src/app/config.js'],
				dest: 'dist/src/app/config.js',
				filter: 'isFile',
			},
			index: {
				expand: false,
				// cwd: 'src',
				src: ['src/index.htm'],
				dest: 'dist/src/index.htm',
				filter: 'isFile',
			},
			indexBeneficioClub: {
				expand: false,
				// cwd: 'src',
				src: ['src/app/modules/beneficio-club/index.htm'],
				dest: 'dist/src/app/modules/beneficio-club/index.htm',
				filter: 'isFile',
			},
			headerController: {
				expand: false,
				// cwd: 'src',
				src: ['src/app/modules/shared/scripts/controllers/headercontroller.js'],
				dest: 'dist/src/app/modules/shared/scripts/controllers/_headercontroller.js',
				filter: 'isFile',
			},
			publish:{
				expand: true,
				// cwd: 'src',
				src: [
					'src/app/**',
					'src/assets/**',
					'src/templates/**',
					'src/*.htm',
				],
				dest: 'dist',
				filter: 'isFile',
			},
			build: {
				expand: true,
				cwd: 'dist/src/',
				src: ['**'],
				dest: 'dist/www',
				filter: 'isFile',
			},
			souticket: {
				expand: true,
				cwd: 'src/',
				src: ['**'],
				dest: 'src/souticket',
				filter: 'isFile',
			},
			index_principal:{
				expand: false,
				// cwd: 'src',
				src: ['src/_index.htm'],
				dest: 'src/souticket/_index.htm',
				filter: 'isFile',
			},
			dist_souticket: {
				expand: true,
				cwd: 'dist/src/',
				src: ['**'],
				dest: 'dist/src/souticket',
				filter: 'isFile',
			},
			dist_index_principal:{
				expand: false,
				// cwd: 'src',
				src: ['src/_index.htm'],
				dest: 'dist/src/souticket/_index.htm',
				filter: 'isFile',
			},
		},

		coffee: {
			roteirizacao: {
				expand: true,
				cwd: 'src/app/modules/roteirizacao/scripts/coffee',
				src: ['**/*.coffee'],
				dest: 'src/app/modules/roteirizacao/scripts',
				ext: '.js'
			}
		},

		sass: {
			dist: {
				// files: {
				// 'src/assets/css/style.css' : 'src/assets/sass/style.scss'
				// }
				files: [{
					expand: true,
					cwd: 'style',
					src: ['src/assets/sass/**/*.scss'],
					dest: 'src/assets/css/',
					ext: '.css'
				}]
			}
		},

		watch: {
			coffee: {
				options: {
					event: ['added', 'changed']
				},
				files: 'src/app/**/*.coffee',
				tasks: ['coffee:roteirizacao']
			},

			js: {
				options: {
					event: ['changed']
				},
				files: 'src/app/**/*.js',
				tasks: 'jshint:js'
			},

			css: {
				files: 'src/assets/sass/**/*.scss',
				tasks: ['sass']
			}
		},

		jshint: {
			js: {
				src: ['src/app/**/*.js']
			},
			options: {
				debug: true,
				reporter: require('jshint-stylish')
			}
		},
		test: {
			options: {
				server: {
					baseDir: "src/test"
				}
			}
		},
		browserSync: {
			dev: {
				bsFiles: {
					src: ['src/**/*']
				},
				options: {
					watchTask: true,
					server: {
						baseDir: "src",
						index: "index.htm"
					},
					port: 3000
				},
			},
			testeBuild: {
				bsFiles: {
					src: ['dist/src/**/*']
				},
				options: {
					watchTask: false,
					server: {
						baseDir: "dist/src",
						index: "index.htm"
					},
					port: 3000
				},
			},
			devThor: {
				bsFiles: {
					src: ['dist/src/**/*']
				},
				options: {
					watchTask: false,
					server: {
						baseDir: "dist/src",
						index: "index.htm"
					},
					port: 3000
				},
			},
		},
		karma: {
			unit: {
				configFile: 'src/test/karma.conf.js',
				singleRun: true
			}
		},

		uglify: {
			min_unico_arquivo: {
				files: {
					'dist/src/app/composite.all.min.js': [
						'src/app/shared/directives/*.js',
						'src/app/modules/shared/scripts/controllers/maincontroller.js',
						'src/app/modules/shared/scripts/controllers/menucontroller.js',
						'src/app/modules/shared/scripts/factories/*.js',
						'!src/app/modules/shared/scripts/factories/homefactory.js',
						'src/app/modules/shared/scripts/services/*.js',
						'src/app/modules/autenticacao/scripts/factories/autenticacaofactory.js',
						'src/app/modules/beneficiario/scripts/factories/cadastrofactory.js',
						'src/app/modules/beneficiario/scripts/factories/reemissaofactory.js',
						'src/app/modules/beneficiario/scripts/factories/bloqueiofactory.js',
						'src/app/modules/beneficiario/scripts/factories/desbloqueiofactory.js',
						'src/app/modules/beneficiario/scripts/factories/cartaosenhafactory.js',
						'src/app/modules/beneficiario/scripts/factories/alertafactory.js',
						'src/app/modules/rede-credenciada/scripts/factories/redecredenciadafactory.js',
						'src/app/modules/consulta-saldo/scripts/factories/consultasaldofactory.js',
						'src/app/modules/beneficiario/scripts/factories/accentivfactory.js'
					]
				}
			},
			min_lazy: {
				files: [{
					expand: true,
					src: ['src/app/**/*.js',
						'!src/app/app.js', 
						'!src/app/config.js',
						'!src/app/shared/directives/*.js',
						'!src/app/modules/shared/scripts/controllers/maincontroller.js',
						'!src/app/modules/shared/scripts/controllers/menucontroller.js',
						'!src/app/modules/shared/scripts/factories/criptofactory.js',
						'!src/app/modules/shared/scripts/factories/loginmodalsfactory.js',
						'!src/app/modules/shared/scripts/factories/menufactory.js',
						'!src/app/modules/shared/scripts/services/*.js',
						'!src/app/modules/autenticacao/scripts/factories/autenticacaofactory.js',
						'!src/app/modules/beneficiario/scripts/factories/cadastrofactory.js',
						'!src/app/modules/beneficiario/scripts/factories/reemissaofactory.js',
						'!src/app/modules/beneficiario/scripts/factories/bloqueiofactory.js',
						'!src/app/modules/beneficiario/scripts/factories/desbloqueiofactory.js',
						'!src/app/modules/beneficiario/scripts/factories/cartaosenhafactory.js',
						'!src/app/modules/beneficiario/scripts/factories/alertafactory.js',
						'!src/app/modules/rede-credenciada/scripts/factories/redecredenciadafactory.js',
						'!src/app/modules/consulta-saldo/scripts/factories/consultasaldofactory.js',
						'!src/app/modules/beneficiario/scripts/factories/accentivfactory.js'
					],
					dest: 'dist/'
				}]
			},
			min_configs:{
				files: [{
					expand: true,
					src: ['dist/src/app/app.js', 
						'dist/src/app/config.js'
					]
				}]
			}
		},

		processhtml: {
			dist: {
				files: {
					'dist/src/index.htm': ['dist/src/index.htm']
				}
			},
			dist_souticket:{
				files: {
					'dist/src/souticket/index.htm': ['dist/src/souticket/index.htm']
				}
			}
		},

		replace: {
			DESE: {
				options: {
					patterns: [
						{
							match: 'token',
							replacement: 'MkM5RUUyNkYzRjQ0QUNEOTAxM0Y1MUI5Mjk4OTZFN0UtTW9uIEp1biAxNyAwNzo0MDowMCBCUlQgMjAxMw=='
						},
						{
							match: 'chavesitegoogle',
							replacement: '6Lc2CiYTAAAAAGFrH0PL7yXMNFkLB802P2Hyr7my'//'6LfRPSgTAAAAAHpWuyNY1hah5cH3EXcm3_hE_FaY'//'6LfUBiYTAAAAAKK45fstthb5p4wUXUsrFZCmJT6o'
						},
						{
							match: 'baseUrlTicketMobile',
							replacement: 'https://api.ticket.com.br/mobile_qa' //''
						},
						{
							match: 'baseNewUrlTicketMobile',
							replacement: 'https://api.ticket.com.br/app/mobile_qa/v1' //''
						},
						{
							match: 'baseUrlPortalWeb',
							replacement: 'http://portalh.lanet.accorservices.net/portal-web/'
						},
						{
							match: 'baseUrlImagemPortalWeb',
							replacement: 'http://www.ticket.com.br/portal/data/'
						},
						{
							match: 'googlerecapctha',
							replacement: 'http://www.google.com/recaptcha/api/siteverify'
						},
						{
							match: 'baseBanners',
							replacement: 'http://ticketlumisportalqa.lanet.accorservices.net/'
						},
						{
							match: 'urlTicketPlus',
							replacement: 'https://portaldousuario.ecobeneficios.com.br/PortalUsuario/login'
						},
						{
							match: 'urlPlusRedeCredenciada',
							replacement: 'http://www.ecobeneficios.com.br/onde-comprar'
						},
						{
							match: 'urlTicketSecurity',
							replacement: 'http://testeapisecurity.lanet.accorservices.net/v1/tk'
						},
						{
							match: 'facebookId',
							replacement: '156050548214101'
						},
						{
							match: 'excecaoLumis',
							replacement: 'portalh.lanet.accorservices.net'
						},
						{
							match: 'scriptGoogleMaps',
							replacement: '//maps.googleapis.com/maps/api/js?sensor=false'
						},
						{
							match: 'base',
							replacement: '/'
						},
						{
							match: 'paginacaoJs',
							replacement: '../../../assets/components/angular-utils-pagination/dirPagination.js'
						},
						{
							match: 'paginacaoComponente',
							replacement: '../../../assets/components/angular-utils-pagination/dirPagination.tpl.html'
						},
						{
							match: 'headercontroller_home',
							replacement: '/'
						},
						{
							match: 'listDuoCard',
							replacement: [22,57,58,63,64,65,72,87,124,126]
						},
						{
							match: 'listPresenteKids',
							replacement: [26,43,44,45],
						},
						{
							match: 'listPresentePerfeito',
							replacement: [23,24,25,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,46,47,48,49,50,51,54,55,56,59,60,61,62,66,67,68,69,70,71,73,74,75,76,77,78,79,80,81,82,83,84,85,86,88,89,90,93,98,99,113,120,125]
						},
						{
							match: 'listPresenteGoodCard',
							replacement: [114]
						}
						
					]
				},
				files: [
					{ expand: false, flatten: true, src: ['src/app/_config.js'], dest: 'src/app/config.js'},
					{ expand: false, flatten: true, src: ['src/app/_app.js'], dest: 'src/app/app.js'},
					{ expand: false, flatten: true, src: ['src/_index.htm'], dest: 'src/index.htm'},
					{ expand: false, flatten: true, src: ['src/app/modules/beneficio-club/_index.htm'], dest: 'src/app/modules/beneficio-club/index.htm'},
					{ expand: false, flatten: true, src: ['src/app/modules/shared/scripts/controllers/_headercontroller.js'], dest: 'src/app/modules/shared/scripts/controllers/headercontroller.js'},
				]
			},
			DESE_SOUTICKET: {
				options: {
					patterns: [
						{
							match: 'scriptGoogleMaps',
							replacement: '//maps.googleapis.com/maps/api/js?sensor=false'
						},
						{
							match: 'base',
							replacement: '/souticket/'
						}
					]
				},
				files: [
					{ expand: false, flatten: true, src: ['src/souticket/_index.htm'], dest: 'src/souticket/index.htm'}
				]
			},
			QA: {
				options: {
					patterns: [
						{
							match: 'token',
							replacement: 'MkM5RUUyNkYzRjQ0QUNEOTAxM0Y1MUI5Mjk4OTZFN0UtTW9uIEp1biAxNyAwNzo0MDowMCBCUlQgMjAxMw=='
						},
						{
							match: 'chavesitegoogle',
							replacement: '6Lc2CiYTAAAAAGFrH0PL7yXMNFkLB802P2Hyr7my'
						},
						{
							match: 'baseUrlTicketMobile',
							replacement: 'https://api.ticket.com.br/mobile_qa'
						},
						{
							match: 'baseNewUrlTicketMobile',
							replacement: 'https://api.ticket.com.br/app/mobile_qa/v1' //''
						},
						{
							match: 'baseUrlPortalWeb',
							replacement: 'http://portalh.lanet.accorservices.net/portal-web/'
						},
						{
							match: 'baseUrlImagemPortalWeb',
							replacement: 'http://www.ticket.com.br/portal/data/'
						},
						{
							match: 'googlerecapctha',
							replacement: 'http://www.google.com/recaptcha/api/siteverify'
						},
						{
							match: 'baseBanners',
							replacement: 'http://ticketlumisportalqa.lanet.accorservices.net/'
						},
						{
							match: 'urlTicketPlus',
							replacement: 'https://portaldousuario.ecobeneficios.com.br/PortalUsuario/login'
						},
						{
							match: 'urlPlusRedeCredenciada',
							replacement: 'http://www.ecobeneficios.com.br/onde-comprar'
						},
						{
							match: 'urlTicketSecurity',
							replacement: 'http://testeapisecurity.lanet.accorservices.net/v1/tk'
						},
						{
							match: 'facebookId',
							replacement: '156050548214101'
						},
						{
							match: 'excecaoLumis',
							replacement: 'portalh.lanet.accorservices.net'
						},
						{
							match: 'scriptGoogleMaps',
							replacement: '//maps.googleapis.com/maps/api/js?sensor=false'
						},
						{
							match: 'base',
							replacement: '/lumis-theme/theme/minha-ticket/'
						},
						{
							match: 'paginacaoJs',
							replacement: 'assets/components/angular-utils-pagination/dirPagination.js'
						},
						{
							match: 'paginacaoComponente',
							replacement: 'assets/components/angular-utils-pagination/dirPagination.tpl.html'
						},
						{
							match: 'headercontroller_home',
							replacement: '/lumis-theme/theme/minha-ticket/'
						},
						{
							match: 'listDuoCard',
							replacement: [22,57,58,63,64,65,72,87,124,126]
						},
						{
							match: 'listPresenteKids',
							replacement: [26,43,44,45],
						},
						{
							match: 'listPresentePerfeito',
							replacement: [23,24,25,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,46,47,48,49,50,51,54,55,56,59,60,61,62,66,67,68,69,70,71,73,74,75,76,77,78,79,80,81,82,83,84,85,86,88,89,90,93,98,99,113,120,125]
						},
						{
							match: 'listPresenteGoodCard',
							replacement: [114]
						}
						
					]
				},
				files: [
					{ expand: false, flatten: true, src: ['src/app/_config.js'], dest: 'src/app/config.js'},
					{ expand: false, flatten: true, src: ['src/app/_app.js'], dest: 'src/app/app.js'},
					{ expand: false, flatten: true, src: ['src/_index.htm'], dest: 'src/index.htm'},
					{ expand: false, flatten: true, src: ['src/app/modules/beneficio-club/_index.htm'], dest: 'src/app/modules/beneficio-club/index.htm'},
					{ expand: false, flatten: true, src: ['src/app/modules/shared/scripts/controllers/_headercontroller.js'], dest: 'src/app/modules/shared/scripts/controllers/headercontroller.js'},
				]
			},
			QA_SOUTICKET:{
				options: {
					patterns: [
						{
							match: 'scriptGoogleMaps',
							replacement: '//maps.googleapis.com/maps/api/js?sensor=false'
						},
						{
							match: 'base',
							replacement: '/lumis-theme/theme/minha-ticket/souticket/'
						}
					]
				},
				files: [
					{ expand: false, flatten: true, src: ['dist/src/souticket/_index.htm'], dest: 'dist/src/souticket/index.htm'}
				]
			},
			HOMO: {
				options: {
					patterns: [
						{
							match: 'token',
							replacement: 'MkM5RUUyNkYzRjQ0QUNEOTAxM0Y1MUI5Mjk4OTZFN0UtTW9uIEp1biAxNyAwNzo0MDowMCBCUlQgMjAxMw=='
						},
						{
							match: 'chavesitegoogle',
							replacement: '6LdSWkUUAAAAAPng-eg_bfBtIpj76GVucrayUMgv'
						},
						{
							match: 'baseUrlTicketMobile',
							replacement: 'https://api.ticket.com.br/mobile_qa'
						},
						{
							match: 'baseNewUrlTicketMobile',
							replacement: 'https://api.ticket.com.br/app/mobile_qa/v1' //''
						},
						{
							match: 'baseUrlPortalWeb',
							replacement: 'https://www.ticket.com.br/portal-web/'
						},
						{
							match: 'baseUrlImagemPortalWeb',
							replacement: 'https://www.ticket.com.br/portal/data/'
						},
						{
							match: 'googlerecapctha',
							replacement: 'http://www.google.com/recaptcha/api/siteverify'
						},
						{
							match: 'baseBanners',
							replacement: 'http://ticketlumisportalqa.lanet.accorservices.net/'
						},
						{
							match: 'urlTicketPlus',
							replacement: 'https://portaldousuario.ecobeneficios.com.br/PortalUsuario/login'
						},
						{
							match: 'urlPlusRedeCredenciada',
							replacement: 'http://www.ecobeneficios.com.br/onde-comprar'
						},
						{
							match: 'urlTicketSecurity',
							replacement: 'http://testeapisecurity.lanet.accorservices.net/v1/tk'
						},
						{
							match: 'facebookId',
							replacement: '156050548214101'
						},
						{
							match: 'excecaoLumis',
							replacement: 'portalh.lanet.accorservices.net'
						},
						{
							match: 'scriptGoogleMaps',
							replacement: '//maps.googleapis.com/maps/api/js?sensor=false'		
						},
						{
							match: 'base',
							replacement: '/lumis-theme/theme/minha-ticket/'
						},
						{
							match: 'paginacaoJs',
							replacement: 'assets/components/angular-utils-pagination/dirPagination.js'
						},
						{
							match: 'paginacaoComponente',
							replacement: 'assets/components/angular-utils-pagination/dirPagination.tpl.html'
						},
						{
							match: 'headercontroller_home',
							replacement: '/lumis-theme/theme/minha-ticket/'
						},
						{
							match: 'listDuoCard',
							replacement: [22,57,58,63,64,65,72,87,124,126]
						},
						{
							match: 'listPresenteKids',
							replacement: [26,43,44,45],
						},
						{
							match: 'listPresentePerfeito',
							replacement: [23,24,25,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,46,47,48,49,50,51,54,55,56,59,60,61,62,66,67,68,69,70,71,73,74,75,76,77,78,79,80,81,82,83,84,85,86,88,89,90,93,98,99,113,120,125]
						},
						{
							match: 'listPresenteGoodCard',
							replacement: [114]
						}
						
					]
				},
				files: [
					{ expand: false, flatten: true, src: ['src/app/_config.js'], dest: 'src/app/config.js'},
					{ expand: false, flatten: true, src: ['src/app/_app.js'], dest: 'src/app/app.js'},
					{ expand: false, flatten: true, src: ['src/_index.htm'], dest: 'src/index.htm'},
					{ expand: false, flatten: true, src: ['src/app/modules/beneficio-club/_index.htm'], dest: 'src/app/modules/beneficio-club/index.htm'},
					{ expand: false, flatten: true, src: ['src/app/modules/shared/scripts/controllers/_headercontroller.js'], dest: 'src/app/modules/shared/scripts/controllers/headercontroller.js'},
				]
			},
			HOMO_SOUTICKET: {
				options: {
					patterns: [
						{
							match: 'scriptGoogleMaps',
							replacement: '//maps.googleapis.com/maps/api/js?sensor=false'		
						},
						{
							match: 'base',
							replacement: '/souticket/'
						}
					]
				},
				files: [
					{ expand: false, flatten: true, src: ['dist/src/souticket/_index.htm'], dest: 'dist/src/souticket/index.htm'}
				]
			},
			PROD: {
				options: {
					patterns: [
						{
							match: 'token',
							replacement: 'MkM5RUUyNkYzRjQ0QUNEOTAxM0Y1MUI5Mjk4OTZFN0UtTW9uIEp1biAxNyAwNzo0MDowMCBCUlQgMjAxMw=='
						},
						{
							match: 'chavesitegoogle',
							replacement: '6LfRPSgTAAAAAHpWuyNY1hah5cH3EXcm3_hE_FaY'
						},
						{
							match: 'baseUrlTicketMobile',
							replacement: 'https://api.ticket.com.br/mobile'
						},
						{
							match: 'baseNewUrlTicketMobile',
							replacement: 'https://api.ticket.com.br/app/mobile/v1' //''
						},
						{
							match: 'baseUrlPortalWeb',
							replacement: 'https://www.souticket.com.br/ticketportal/'
						},
						{
							match: 'baseUrlImagemPortalWeb',
							replacement: 'https://www.souticket.com.br/ticketportal/data/'
						},
						{
							match: 'googlerecapctha',
							replacement: 'http://www.google.com/recaptcha/api/siteverify'
						},
						{
							match: 'baseBanners',
							replacement: 'https://www.souticket.com.br/'
						},
						{
							match: 'urlTicketPlus',
							replacement: 'https://portaldousuario.ecobeneficios.com.br/PortalUsuario/login'
						},
						{
							match: 'urlPlusRedeCredenciada',
							replacement: 'https://maps.goodcard.com.br/ondecomprar/index.php?portal=ticket'
						},
						{
							match: 'urlTicketSecurity',
							replacement: 'http://testeapisecurity.lanet.accorservices.net/v1/tk'
						},
						{
							match: 'facebookId',
							replacement: '156050548214101'
						},
						{
							match: 'excecaoLumis',
							replacement: 'www.souticket.com.br'
						},
						{
							match: 'scriptGoogleMaps',
							replacement: '//maps.googleapis.com/maps/api/js?client=gme-edenredsas&v=3.25&sensor=false&channel=benefitsbr'
						},
						{
							match: 'base',
							replacement: '/'
						},
						{
							match: 'paginacaoJs',
							replacement: '../../../assets/components/angular-utils-pagination/dirPagination.js'
						},
						{
							match: 'paginacaoComponente',
							replacement: '../../../assets/components/angular-utils-pagination/dirPagination.tpl.html'
						},
						{
							match: 'headercontroller_home',
							replacement: '/'
						},
						{
							match: 'listDuoCard',
							replacement: [22,57,58,63,64,65,72,87,124,126]
						},
						{
							match: 'listPresenteKids',
							replacement: [26,43,44,45],
						},
						{
							match: 'listPresentePerfeito',
							replacement: [23,24,25,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,46,47,48,49,50,51,54,55,56,59,60,61,62,66,67,68,69,70,71,73,74,75,76,77,78,79,80,81,82,83,84,85,86,88,89,90,93,98,99,113,120,125]
						},
						{
							match: 'listPresenteGoodCard',
							replacement: [115]
						}
						
					]
				},
				files: [
					{ expand: false, flatten: true, src: ['src/app/_config.js'], dest: 'src/app/config.js'},
					{ expand: false, flatten: true, src: ['src/app/_app.js'], dest: 'src/app/app.js'},
					{ expand: false, flatten: true, src: ['src/_index.htm'], dest: 'src/index.htm'},
					{ expand: false, flatten: true, src: ['src/app/modules/beneficio-club/_index.htm'], dest: 'src/app/modules/beneficio-club/index.htm'},
					{ expand: false, flatten: true, src: ['src/app/modules/shared/scripts/controllers/_headercontroller.js'], dest: 'src/app/modules/shared/scripts/controllers/headercontroller.js'},
				]
			},
			PROD_SOUTICKET: {
				options: {
					patterns: [
						{
							match: 'scriptGoogleMaps',
							replacement: '//maps.googleapis.com/maps/api/js?client=gme-edenredsas&v=3.25&sensor=false&channel=benefitsbr'
						},
						{
							match: 'base',
							replacement: '/souticket/'
						}
					]
				},
				files: [
					{ expand: false, flatten: true, src: ['dist/src/souticket/_index.htm'], dest: 'dist/src/souticket/index.htm'}
				]
			}
		},
		zip: {
			qa:{
				cwd: 'dist/src/',
				src:'dist/src/**/*',
				dest:'dist/www.zip'
			},
			homo:{
				cwd: 'dist/src/',
				src:'dist/src/**/*',
				dest:'dist/www.zip'
			},
			prod:{
				cwd: 'dist/src/',
				src:'dist/src/**/*',
				dest:'dist/www.zip'
			}
      		
    	},
		

	});

	//registrando tasks
	grunt.registerTask('copy-files-replace', ['copy:app', 'copy:config', 'copy:index', 'copy:indexBeneficioClub', 'copy:headerController']);
	grunt.registerTask('serve', ['jshint', 'clean:configs', 'replace:DESE', 'clean:souticket', 'copy:souticket','copy:index_principal','replace:DESE_SOUTICKET', 'browserSync:dev', 'watch']);
	grunt.registerTask('test', ['karma']);

	/*
	1 = (jshint) avalia; 
	2 = (clean:dist) limpa pasta dist;
	3 = (clean:configs) apaga os arquivos app.js, config.js e index.htm que são gerados pelo replace.
		A não exclusão destes arquivos gera conflitos no jshint;
	4 = (copy:publish) copia todos os arquivos para a pasta dist/www
	5 = (replace) replaces do ambiente;
	6 = Copia os arquivos app.js, config.js, index e indexBeneficioClub que foram gerados na raiz.
	7 = (minificacao) minifica todos arquivos não lazy;
	8 = (minificacao) minifica todos arquivos lazy;
	9 = (minificacao) minifica app.js e config.js;
	10 = (clean:configsbase) apaga os arquivos de configuracao que são base para o replace
	11 = (clean:arquivoNaoLazy) apaga os arquivos não lazy que agora já estão minificados no "composite.all.js"
	12 = (processhtml) substitui todas as referencias pelo composite.all.min.js;
	13 = (zip) cria o pacote zipado para publicação
	*/
	grunt.registerTask('souticket', ['copy:dist_souticket','copy:dist_index_principal']);
	grunt.registerTask('minificacao', ['uglify:min_unico_arquivo', 'uglify:min_lazy','uglify:min_configs']);
	grunt.registerTask('build-qa', 	['jshint', 'clean:dist', 'clean:configs', 'copy:publish', 'replace:QA'  , 'copy-files-replace', 'minificacao', 'clean:configsbase', 'clean:arquivoNaoLazy', 'processhtml:dist','souticket','replace:QA_SOUTICKET','processhtml:dist_souticket', 'clean:index_souticket','zip:qa']);
	grunt.registerTask('build-homo', ['jshint', 'clean:dist','clean:configs', 'copy:publish', 'replace:HOMO', 'copy-files-replace', 'minificacao', 'clean:configsbase', 'clean:arquivoNaoLazy', 'processhtml:dist','souticket','replace:HOMO_SOUTICKET','processhtml:dist_souticket', 'clean:index_souticket','zip:homo']);
	grunt.registerTask('build-prod', ['jshint', 'clean:dist','clean:configs', 'copy:publish', 'replace:PROD', 'copy-files-replace', 'minificacao', 'clean:configsbase', 'clean:arquivoNaoLazy', 'processhtml:dist','souticket','replace:PROD_SOUTICKET','processhtml:dist_souticket', 'clean:index_souticket','zip:prod']);

	//carregando tasks
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-contrib-clean');
	grunt.loadNpmTasks('grunt-contrib-coffee');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-jshint');
	grunt.loadNpmTasks('grunt-contrib-sass');
	grunt.loadNpmTasks('grunt-browser-sync');
	grunt.loadNpmTasks('grunt-karma');
	grunt.loadNpmTasks('grunt-processhtml');
	grunt.loadNpmTasks('grunt-replace');
	grunt.loadNpmTasks('grunt-zip');
}
