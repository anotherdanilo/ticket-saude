﻿(function (gl) {
    "use strict";
    var token, site, origin;
    var chavesitegoogle;
    token = "@@token";
    site = "BC";
    origin = "offerBCReport";
    chavesitegoogle = "@@chavesitegoogle";
    var baseUrlTicketMobile = "@@baseUrlTicketMobile";
    var baseNewUrlTicketMobile = "@@baseNewUrlTicketMobile"; 
    var baseUrlPortalWeb = "@@baseUrlPortalWeb";
    var baseUrlImagemPortalWeb = "@@baseUrlImagemPortalWeb";
    var googlerecapctha = "@@googlerecapctha";
    var baseBanners = "@@baseBanners";
    var urlTicketPlus = "@@urlTicketPlus";
    var urlPlusRedeCredenciada = "@@urlPlusRedeCredenciada";
    var urlTicketSecurity = "@@urlTicketSecurity";
    var urlAccentiv = "https://api.ticket.com.br/iflex_homo/card/";
    gl.config = {
        urls: {
            buscaEstabelecimento: baseUrlTicketMobile + '/api/estabelecimentos',
            favoritaEstabelecimento: baseUrlTicketMobile + '/api/usuarios/:idUsuario/favoritos',
            loginAPI: baseUrlTicketMobile + '/usuarios/v3/autenticar',
            usuarioAPI: baseUrlTicketMobile + '/usuarios/v3/:id',
            validarEmailAPI: baseUrlTicketMobile + '/usuarios/v3/validacao-email',
            reenviarEmailAPI: baseUrlTicketMobile + '/usuarios/v3/reenvio-email-ativacao',
            reenviarSenhaAPI: baseUrlTicketMobile + '/usuarios/v3/reenviar-senha',
            redefinicaoSenha: baseUrlTicketMobile + '/usuarios/v3/redefinicao-senha',
            beneficiarioUsuarioAPI: baseUrlTicketMobile + '/usuarios/v3/:id',
            beneficiarioUsuarioAPIPostV3_1: baseUrlTicketMobile + '/usuarios/v3.1',
            cartaoAPI: baseUrlTicketMobile + '/usuarios/v3/:idUsuario/cartoes',
            newCartaoAPI: baseNewUrlTicketMobile + '/usuarios/:CdUsuario/cartoes',
            getSimplificados: baseNewUrlTicketMobile + '/usuarios/:CdUsuario/cartoes/simplificados',
            getExtrato: baseNewUrlTicketMobile + '/usuarios/:idUsuario/cartoes/:idCartao/extrato',
            getAllCards: baseUrlTicketMobile + '/usuarios/v3/:idUsuario/get-all-cards',
            rotaCartaoAPI: baseUrlTicketMobile + '/v3/cartao/consulta/:id',
            obterPorNumeroCartao: baseUrlTicketMobile + '/v3/cartao/:numeroCartao',
            autoServicoAPI: baseUrlTicketMobile + '/v3/cartao/:idUsuario/auto-servico',
            favoritarCartao: baseUrlTicketMobile + '/v3/cartao/:idUsuario/favoritar-cartao',
            excluirCartaoAPI: baseUrlTicketMobile + '/usuarios/v3/:idUsuario/cartoes',
            obterPorCep: baseUrlTicketMobile + '/usuarios/v3/obter-endereco/:id',
            obterUsuarioPorToken: baseUrlTicketMobile + '/usuarios/v3/obter-por-token/:id',
            ofertasAPI: baseUrlPortalWeb + 'ws/bc/offers/json',
            categoriasAPI: baseUrlPortalWeb + 'ws/bc/category/json',
            stateAPI: baseUrlPortalWeb + 'ws/bc/state/json',
            denunciaAPI: baseUrlPortalWeb + 'ws/bc/report/json',
            obterAceiteRecaptcha: googlerecapctha,
            recaptcha: baseUrlTicketMobile + '/v3/cartao/recaptcha',
            getBanners: baseBanners + "lumis/api/rest/home-bc/lumgetdata/list.json?lumReturnFields=name,introduction,logo,image,position,linkUrl",
            indicarParceiro: baseUrlTicketMobile + '/usuarios/v3/indicar-parceiro',
            ticketPlus: urlTicketPlus,
            plusRedeCredenciada : urlPlusRedeCredenciada,
            ticketSecurity: baseUrlTicketMobile + "/v3/cartao/:idUsuario/alterar-senha",
            consultaSaldo: baseUrlTicketMobile + "/v3/cartao/consultar-saldo",
            addCartaoAccentiv: urlAccentiv + 'account/:idusuario/cards',
            obterCartaoAccentiv: urlAccentiv + 'detail/',
            saldoCartaoAccentiv: urlAccentiv + 'balance/',
            extratoCartaoAccentiv: urlAccentiv + 'transactions/',
            ativarCartaoAccentiv: urlAccentiv + 'active/',
            bloquearCartaoAccentiv: urlAccentiv + 'block/',
            desbloquearCartaoAccentiv: urlAccentiv + 'unblock/',
            
        },
        nomeCookieSSO: 'Edenred',
        nomeCookieUsuario: 'EdenredPUA',
        horasExpiracaoCookie: 8,
        token: token,
        site: site,
        origin: origin,
        chavesitegoogle: chavesitegoogle,
        urlsAceitamParametroP: [
            "beneficio-club"
        ],
        baseUrlImagemPortalWeb: baseUrlImagemPortalWeb,
        facebookId: "@@facebookId",
        tokenTecladoVirtual: "Hepyj+owEI94llRey5gyDg==",
        ivTecladoVirtual: "R1Sh8KeaMH4J8sh5uJakww==",
        listDuoCard:"@@listDuoCard",
        listPresenteKids:"@@listPresenteKids",
        listPresentePerfeito:"@@listPresentePerfeito",
        listPresenteGoodCard:"@@listPresenteGoodCard"
    };
})(window);