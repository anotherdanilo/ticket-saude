(function() {
    angular.module('portalUsuarioApp').directive('invalidDate', ['toaster', function(toaster) {

    return {
            require: 'ngModel',
            restrict: 'A',
            link: function (scope, elem, attrs, ctrl) {

                scope.$watch(attrs.ngModel, function (newVal, oldVal, scope) {
                    if(newVal)
                    {
                        ctrl.$setValidity('invalidDate', validate(newVal));     
                    }
                });
           
                var validate = function (date) {
                    if ((date && Date.parse(date) === null) || date.length === 0) {
                        return false;
                    }
                    return true;
                };
            }
        };
    }
  ]);
}).call(this);