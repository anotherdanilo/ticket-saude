(function() {
    'use strict';
    angular.module('portalUsuarioApp').directive('validarForm', [
        'toaster', function (toaster) {

            

            return {
                restrict: 'A',
                require: '^form',
                scope: {
                    validarForm: "@"
                },
                link: function (scope, el, attrs, formCtrl) {

                    var element;

                    element = angular.element(el);
                    element.bind('click', function(e) {
                        var i, inputAng, inputEl, inputNgEl, inputs, j, label, ref, divRadio;

                        divRadio = $('#' + scope.validarForm).find('.grupo-radio');

                        divRadio.each(function() {
                            var inputsRadio = $(this).find('input[type=radio]:checked');
                            if (inputsRadio.length === 0) {
                                toaster.pop('error', 'Sexo é um campo obrigatório');
                                e.stopImmediatePropagation();
                            }
                        });

                        inputs = $('#' + scope.validarForm).find('input[type!="radio"], select, textarea');
                        for (i = j = 0, ref = inputs.length; 0 <= ref ? j < ref : j > ref; i = 0 <= ref ? ++j : --j) {
                            inputEl = inputs[i];
                            inputNgEl = angular.element(inputEl);
                            inputAng = formCtrl[inputNgEl.attr('name')];
                            if (inputAng) {
                                inputNgEl.parent().toggleClass('has-error', inputAng.$invalid);
                                label = inputNgEl.attr('placeholder') || $(inputEl).siblings("label").text();
                               

                                while ($(inputNgEl).parent().find('#mensagem').length > 0) {

                                    $(inputNgEl).parent().find('#mensagem').remove();
                                }

                                if ($(inputNgEl).is(':visible')) { //Aplica as validações somente para campos visiveis
                                    if (inputAng.$error.required) {

                                        //toaster.pop('error', inputNgEl.attr('required-error-message') ? inputNgEl.attr('required-error-message') : label + ' é um campo obrigatório');
                                        var div = $("<div id='mensagem' class='validation-msg'></div>");
                                        
                                        var mensagemLabel =  label + " é um campo obrigatório";

                                        
                                        if(inputNgEl.attr('required-error-message'))
                                        {
                                            div.append(inputNgEl.attr('required-error-message'));
                                        }
                                        else
                                        {
                                            div.append(mensagemLabel);
                                        }
                                        
                                        $(inputNgEl).parent().append(div);

                                        e.stopImmediatePropagation();
                                    }
                                    if (inputAng.$error.minlength) {

                                        $(inputNgEl).parent().append("<div id='mensagem' class='validation-msg'>O campo " + label + " deve conter no mínimo " + inputNgEl.attr("minlength") + " caracteres</div>");
                                        e.stopImmediatePropagation();
                                    }
                                    if (inputAng.$error.maxlength) {

                                        $(inputNgEl).parent().append("<div id='mensagem' class='validation-msg'>O campo " + label + " deve conter no máximo " + inputNgEl.attr("minlength") + " caracteres</div>");
                                        e.stopImmediatePropagation();
                                    }
                                    if (inputAng.$error.min) {
                                        toaster.pop('error', inputNgEl.attr('min-error-message') ? inputNgEl.attr('min-error-message') : 'O campo ' + label + ' deve conter um valor superior ou igual a ' + inputNgEl.attr("min"));
                                        e.stopImmediatePropagation();
                                    }
                                    if (inputAng.$error.max) {
                                        toaster.pop('error', inputNgEl.attr('max-error-message') ? inputNgEl.attr('max-error-message') : 'O campo ' + label + ' deve conter um valor inferior ou igual a ' + inputNgEl.attr("max"));
                                        e.stopImmediatePropagation();
                                    }
                                    if (inputAng.$error.url) {
                                        toaster.pop('error', inputNgEl.attr('url-error-message') ? inputNgEl.attr('required-error-message') : 'O valor do campo ' + label + ' não é uma URL válida');
                                        e.stopImmediatePropagation();
                                    }
                                    if (inputAng.$error.email) {

                                        $(inputNgEl).parent().append("<div id='mensagem' class='validation-msg'>O valor do campo " + label + " não é um endereço de e-mail válido</div>");
                                        e.stopImmediatePropagation();
                                    }
                                    if (inputAng.$error.thisEarlierThan) {
                                        label = $(inputNgEl).parents("[range-data]").attr("range-data");
                                        toaster.pop('error', 'O valor do campo ' + label + ' possui o range da data errado.');
                                        e.stopImmediatePropagation();
                                        $(inputNgEl).parents("[range-data]").find("input").css("border", "1px solid red");
                                    }
                                    if (inputAng.$error.invalidDate) {
                                        toaster.pop('error', inputNgEl.attr('invalid-error-message') ? inputNgEl.attr('invalid-error-message') : 'A ' + label + ' é inválida.');
                                        e.stopImmediatePropagation();
                                    }

                                    if (inputAng.$error.invalidCpf && inputAng.$viewValue) {
                                        
                                        $(inputNgEl).parent().append("<div id='mensagem' class='validation-msg'>Campo " + label + " inválido </div>");
                                        e.stopImmediatePropagation();
                                    }
                                    
                                    if (inputAng.$error.invalidCel) {
                                        $(inputNgEl).parent().append("<div id='mensagem' class='validation-msg'>Número de celular inválido </div>");
                                        e.stopImmediatePropagation();
                                    }
                                    if (inputAng.$error.cpf) {
                                        $(inputNgEl).parent().append("<div id='mensagem' class='validation-msg'>" + inputNgEl.attr('invalid-error-message') + "</div>");
                                        e.stopImmediatePropagation();
                                    }
                                    
                                    if (inputAng.$error.pattern) {

                                        $(inputNgEl).parent().append("<div id='mensagem' class='validation-msg'>" + inputNgEl.attr('invalid-error-message') + "</div>");

                                        e.stopImmediatePropagation();
                                    }
                           
                                }
                            }
                        }
                    });
                }
            };
        }
    ]);

}).call(this);

