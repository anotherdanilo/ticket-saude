(function() {
    angular.module('portalUsuarioApp').directive('invalidCel', ['toaster', function(toaster) {

        return {
            require: 'ngModel',
            restrict: 'A',
            link: function(scope, elem, attrs, ctrl) {

                scope.$watch(attrs.ngModel, function(newVal, oldVal, scope) {
                    if (newVal) {
                        if (newVal !== "") {
                            ctrl.$setValidity('invalidCel', validate(newVal));
                        }
                    }
                });

                var validate = function(value) {
                    value = value.replace("(", "");
                    value = value.replace(")", "");
                    value = value.replace("-", "");
                    value = value.replace(" ", "").trim();
                    if (value == '0000000000') {
                        return false;
                    } else if (value == '00000000000') {
                        return false;
                    }
                    if (["00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10"].indexOf(value.substring(0, 2)) != -1) {
                        return false;
                    }
                    if (value.length < 10 || value.length > 11) {
                        return false;
                    }
                    if (["6", "7", "8", "9"].indexOf(value.substring(2, 3)) == -1) {
                        return false;
                    }
                    return true;

                };
            }
        };
    }]);
}).call(this);