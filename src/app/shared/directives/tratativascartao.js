(function () {
  'use strict';
  angular.module('portalUsuarioApp').directive('tratativasCartao', ['AccentivFactory', function (AccentivFactory) {

    function link(scope, element, attrs) {

      function atualizaHtml(cartao) {
        if (cartao) {

          var cssAccentivEncontrado = false; //parametro usado apenas para cartões da accentiv
          var cssCartao = "card-ticket ";
          var bloqueado = false;
          var textoBloqueado = "";
          var textoBloqueadoMin = "";
          var bandeiraEsquerda = "";
          var bandeiraDireita = "";

          var balance = angular.fromJson(cartao);

          AccentivFactory.reset();

          //alteraer CSS cartao
          if (balance.bin == "ACT" && balance.personalizacaoId) {

            var detalheAccentiv = AccentivFactory.existePersonalizacaoId(balance.personalizacaoId);
            cssCartao += detalheAccentiv.css;
            bandeiraDireita = detalheAccentiv.bandeira;

          }
          else if(balance.bin == "TSE")
          {
            cssCartao += "-saude";
            bandeiraEsquerda = "valesaude";
            bandeiraDireita = "vidalink";
          }
          else {
            switch (balance.bin) {
              case "TRE":
                cssCartao += "-restaurante";
                break;
              case "TAE":
                cssCartao += "-alimentacao";
                break;
              case "TKE":
                cssCartao += "-cultura";
                break;
              case "TC":
                cssCartao += "-car";
                break;
              default:
                cssCartao += "-transporte";
            }
          }

          //bloquear cartao]

          switch (balance.situacao) {
            case 2: // 2: BloqueadoTicket
              cssCartao += " -blocked";
              bloqueado = true;
              textoBloqueado = "Cartão Bloqueado";
              textoBloqueadoMin = "Bloqueado Ticket";
              break;
            case 3: // 3: BloqueadoBeneficiario
              cssCartao += " -blocked";
              bloqueado = true;
              textoBloqueado = "Cartão Bloqueado";
              textoBloqueadoMin = "Bloqueado";
              break;
            case 4: // 4: Cancelado 
              cssCartao += " -blocked";
              bloqueado = true;
              textoBloqueado = "Cartão Cancelado";
              textoBloqueadoMin = "Cancelado";
              break;
            case 5: // 4: Inativo
              
              cssCartao += " -blocked";
              bloqueado = true;
              textoBloqueado = "Cartão Inativo";
              textoBloqueadoMin = "Inativo";
              break;
              
          }

          //funcao para exibir somente os 4 ultimos numeros do cartão
          var length = balance.number.length;
          var cardLast = "";

          if (length > 4)
            cardLast = balance.number.substring(length - 4);
          else
            cardLast = balance.number;

          //funcao para FAVORITO
          var a_cssEstrela = "favcard";
          var i_cssEstrela = "icon-star_border";

          //verifica se o cartao é o ativo ou não
          if (balance.favorito) {
            a_cssEstrela = "favcard active";
            i_cssEstrela = "icon-star";
          }

          cartao.cssCartao = cssCartao;
          cartao.bloqueado = bloqueado;
          cartao.textoBloqueado = textoBloqueado;
          cartao.textoBloqueadoMin = textoBloqueadoMin;
          cartao.cardLast = cardLast;
          cartao.a_cssEstrela = a_cssEstrela;
          cartao.i_cssEstrela = i_cssEstrela;
          cartao.bandeiraEsquerda = bandeiraEsquerda;
          cartao.bandeiraDireita = bandeiraDireita;

        }
      }

      scope.$watch(attrs.tratativasCartao, function (value) {
        atualizaHtml(value);

      });
    }

    return {
      restrict: 'A',
      link: link
    };
  }]);

}).call(this);
