(function() {
  'use strict';
  angular.module('portalUsuarioApp').directive('loadingMenu', function () {
    return {
      restrict: 'E',
      scope: {},
      templateUrl: 'templates/loading-menu.htm',
      replace: true,
      link: function(scope, element, attrs) {
        //element.mask(attrs.mask);
      },
    };
  });

}).call(this);
