(function() {
  'use strict';
  angular.module('portalUsuarioApp').directive('scrollPosition', ['$window', function($window) {
    return {
      scope: {
        scroll: '=scrollPosition'
      },
      restrict: 'A',
      link: function(scope, element, attrs) {
        var windowEl = angular.element($window);
        var handler = function() {
          scope.scroll = windowEl.scrollTop();
        };
        windowEl.on('scroll', scope.$apply.bind(scope, handler));
        handler();
      }
    };
  }
  
  ]);

}).call(this);
