(function() {
    'use strict';
    angular.module('portalUsuarioApp').directive('mask', function() {
        return {
            restrict: 'A',
            link: function(scope, element, attrs) {
                element.mask(attrs.mask);
            }
        };
    });

}).call(this);
