(function() {
  'use strict';
  angular.module('portalUsuarioApp').directive('loadingMask', function () {
    return {
      restrict: 'E',
      scope: {},
      templateUrl: 'templates/loading-mask.htm',
      replace: true,
      link: function(scope, element, attrs) {
        //element.mask(attrs.mask);
      },
    };
  });

}).call(this);
