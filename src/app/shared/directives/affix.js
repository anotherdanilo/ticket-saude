(function() {
  'use strict';
  angular.module('portalUsuarioApp').directive('affix', ['$window', function ($window) {
    return {
      restrict: 'A',
      link: function($scope, $element, $attributes) {
        var win = angular.element($window);
        var topDistance = $attributes.topDistance;
        var topOffset = $element[0].offsetTop + Number(topDistance);

        function affixElement() {
            if ($window.pageYOffset > topOffset) {
                $element.addClass('-fixed');
            } else {
                $element.removeClass('-fixed');
            }
        }

        $scope.$on('$routeChangeStart', function() {
            win.unbind('scroll', affixElement);
        });
        win.bind('scroll', affixElement);
      }
    };
  }
  ]);

}).call(this);
