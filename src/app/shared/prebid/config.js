var PREBID_TIMEOUT = 3000;

var bidsConfigs = [
    {
        bidder: 'rubicon',
        params: {
            accountId: '18046',
            siteId: '188110',
            zoneId: '917280'
        }
    }
];

var adUnits = [
    {
        code: 'rede-credenciada_lateral-esq-hor_320x50_fixo',
        mediaTypes: {
            banner: {
                sizes: [[320, 50]],
            }
        },
        bids: bidsConfigs
    },
    {
        code: 'rede-credenciada_footer-hor_728x90_fixo',
        mediaTypes: {
            banner: {
                sizes: [[728, 90]],
            }
        },
        bids: bidsConfigs
    },
    {
        code: 'consulta-saldo_header-hor_970x90_fixo',
        mediaTypes: {
            banner: {
                sizes: [[970, 90]],
            }
        },
        bids: bidsConfigs
    },
    {
        code: 'consulta-saldo_dashboard_footer-hor_970x90_fixo',
        mediaTypes: {
            banner: {
                sizes: [[970, 90]],
            }
        },
        bids: bidsConfigs
    },
    {
        code: 'beneficiario_dashboard_header-hor_970x90_fixo',
        mediaTypes: {
            banner: {
                sizes: [[970, 90]],
            }
        },
        bids: bidsConfigs
    },
    {
        code: 'beneficiario_atendimento_footer-hor_970x90_fixo',
        mediaTypes: {
            banner: {
                sizes: [[970, 90]],
            }
        },
        bids: bidsConfigs
    }
];

var pbjs = pbjs || {};
pbjs.que = pbjs.que || [];