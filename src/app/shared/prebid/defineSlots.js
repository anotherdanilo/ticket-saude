slots = {
    'rede-credenciada_lateral-esq-hor_320x50_fixo' : undefined,
    'rede-credenciada_footer-hor_728x90_fixo' : undefined,
    'consulta-saldo_header-hor_970x90_fixo': undefined,
    'consulta-saldo_dashboard_footer-hor_970x90_fixo': undefined,
    'beneficiario_dashboard_header-hor_970x90_fixo': undefined,
    'beneficiario_atendimento_footer-hor_970x90_fixo': undefined
};

googletag.cmd.push(function () {
    // Rede Credenciada
    slots['rede-credenciada_lateral-esq-hor_320x50_fixo'] = 
        googletag.defineSlot(
            '/135709868/rede-credenciada_lateral-esq-hor_320x50_fixo',
            [[320, 50]],
            'rede-credenciada_lateral-esq-hor_320x50_fixo')
        .addService(googletag.pubads());

    slots['rede-credenciada_footer-hor_728x90_fixo'] = 
        googletag.defineSlot(
            '/135709868/rede-credenciada_footer-hor_728x90_fixo',
            [[728, 90]],
            'rede-credenciada_footer-hor_728x90_fixo')
        .addService(googletag.pubads());

    // consulta-saldo
    slots['consulta-saldo_header-hor_970x90_fixo'] =
        googletag.defineSlot(
            '/135709868/consulta-saldo_header-hor_970x90_fixo',
            [[970, 90]],
            'consulta-saldo_header-hor_970x90_fixo')
            .addService(googletag.pubads());

    slots['consulta-saldo_dashboard_footer-hor_970x90_fixo'] =
        googletag.defineSlot(
            '/135709868/consulta-saldo_dashboard_footer-hor_970x90_fixo',
            [[970, 90]],
            'consulta-saldo_dashboard_footer-hor_970x90_fixo')
            .addService(googletag.pubads());

    // beneficiario
    slots['beneficiario_dashboard_header-hor_970x90_fixo'] =
        googletag.defineSlot(
            '/135709868/beneficiario_dashboard_header-hor_970x90_fixo',
            [[970, 90]],
            'beneficiario_dashboard_header-hor_970x90_fixo')
            .addService(googletag.pubads());

    slots['beneficiario_atendimento_footer-hor_970x90_fixo'] =
        googletag.defineSlot(
            '/135709868/beneficiario_atendimento_footer-hor_970x90_fixo',
            [[970, 90]],
            'beneficiario_atendimento_footer-hor_970x90_fixo')
            .addService(googletag.pubads());


    googletag.pubads().enableSingleRequest();
    googletag.enableServices();
});