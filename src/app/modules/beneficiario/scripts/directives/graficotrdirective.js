(function() {
  'use strict';

  angular.module('portalUsuarioApp').directiveLazy('tktGraficoTr', [
    'GraficoConsumoFactory',
    function(GraficoConsumoFactory) {

      var directive = {
        restrict: 'EA',
        scope: {},
        templateUrl: 'app/modules/beneficiario/dashboard/graficotr.htm',
        replace: true,
        link: link
      };

      return directive;

      function link(scope, element, attrs) {

        var trData = GraficoConsumoFactory.bindTrChartData();

        var dashboardTrChartId = '#dashboard-tr-chart';
        var dashboardTrChartMargin = { top: 20, right: 1, bottom: 1, left: 1 };
        var dashboardTrMeanBox = { width: 184, height: 20, r: 2, margin: 4 };

        initTrChart();
        updateTrChart(trData);

        function initTrChart() {

            var svg = d3.select(dashboardTrChartId).append('svg');
            svg.attr('width', '100%').attr('height', '100%');

            var margin = dashboardTrChartMargin;

            var width = +svg.style('width').replace('px', '') - margin.left - margin.right;
            var height = +svg.style('height').replace('px', '') - margin.top - margin.bottom;

            // append axis, groups and line

            svg.append('text')
              .attr('class', 'about-text')
              .attr('y', 12)
              .text('Consumo no período');

            svg.append('g')
              .attr('class', 'y-axis')
              .attr('transform', 'translate(' + (margin.left + width) + ',' + margin.top + ')');

            svg.append('text')
              .attr('class', 'y-tick y-tick-max-text')
              .attr('y', 32)
              .text('R$ 123,45 (máx.)');

            svg.append('text')
              .attr('class', 'y-tick')
              .attr('y', margin.top + height - 4)
              .text('R$ 0,00');

            var chart = svg.append('g')
                .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')')
                .attr('class', 'chart');

            chart.append('path')
              .attr('class', 'line');

            chart.append('line')
              .attr('class', 'mean-line')
              .attr('x1', 0)
              .attr('y1', height)
              .attr('x2', width)
              .attr('y2', height);

            chart.append('rect')
              .attr('class', 'mean-box')
              .attr('x', 0)
              .attr('y', height - dashboardTrMeanBox.height - dashboardTrMeanBox.margin)
              .attr('rx', dashboardTrMeanBox.r)
              .attr('ry', dashboardTrMeanBox.r)
              .attr('width', dashboardTrMeanBox.width)
              .attr('height', dashboardTrMeanBox.height);

            chart.append('text')
              .attr('class', 'mean-text')
              .attr('x', dashboardTrMeanBox.width/2)
              .attr('y', height - dashboardTrMeanBox.margin - 7)
              .attr('line-height', dashboardTrMeanBox.height);
        }

        function updateTrChart(data) {

            var svg = d3.select(dashboardTrChartId + ' svg');

            var margin = dashboardTrChartMargin;

            var width = +svg.style('width').replace('px', '') - margin.left - margin.right;
            var height = +svg.style('height').replace('px', '') - margin.top - margin.bottom;

            // SCALES

            // x
            var x = d3.scale.ordinal()
              .domain(data.map(function (d, i) { return i; }))
              .rangePoints([0, width], 0);

            // y
            var yMax = d3.max(data, function (d) { return d.value; });
            var yMean = d3.mean(data, function (d) { return d.value; });
            var y = d3.scale.linear().domain([0, yMax]).range([height, 0]);

            svg.select('text.y-tick-max-text')
              .text('R$ ' + d3.format(',.2f')(yMax) + ' (máx.)');

            var yAxis = d3.svg.axis()
              .scale(y)
              .orient('left')
              .tickValues([0, yMax])
              .tickSize(width)
              .tickPadding(8);

            svg.select('g.y-axis')
              .call(yAxis);

            // DRAW

            var chart = svg.select('g.chart');

            // enter / update

            svg.select('.mean-line')
              .attr('y1', y(yMean))
              .attr('y2', y(yMean));

            svg.select('.mean-box')
              .attr('y', y(yMean) - dashboardTrMeanBox.height - dashboardTrMeanBox.margin);

            svg.select('.mean-text')
              .attr('y', y(yMean) - dashboardTrMeanBox.margin - 7)
              .text('R$ ' + d3.format(',.2f')(yMean) + ' (ticket médio no período)');

            var line = d3.svg.line()
              .x(function (d, i) { return x(i); })
              .y(function (d) { return y(d.value); });

            chart.select('path.line')
              .datum(data)
              .attr('d', line);
        }
      }
    }
  ]);

})();
