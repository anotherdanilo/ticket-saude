(function() {
    'use strict';

    angular.module('portalUsuarioApp').directiveLazy('tktGraficoRosca', [
        '$filter', 'CartaoFactory',
        function($filter, CartaoFactory) {

          var directive = {
                restrict: 'E',
                scope: {
                    objectToInject: '=',
                },
              templateUrl: 'app/modules/beneficiario/dashboard/graficorosca.htm',
              replace: true,
              link: link
          };

            return directive;

            function link($scope, element, attrs) {
                $scope.hasData = true;
                //var chartData = CartaoFactory.bindDonutChartData();

                // if (chartData.total === 0) {
                //     $scope.hasData = false;
                // }
                // else {
                //     $scope.hasData = true;
                //     var chartEl = element[0];
                //     var $chart = $(chartEl);
                //     var chartMargin = { top: 20, right: 0, bottom: 0, left: 0 };
                //     var chartText = { labelSize: 10, valueSize: 20 };

                //     // chart variables
                //     var outerR, innerR, r, arc;

                //     initChart(chartData);
                //     updateChart(chartData);
                // }

                var chartEl;
                var $chart;
                    var chartMargin = { top: 20, right: 0, bottom: 0, left: 0 };
                    var chartText = { labelSize: 10, valueSize: 20 };

                    // chart variables
                    var outerR, innerR, r, arc;

                function initChart(data) {
                    
                    var svg = d3.select(chartEl).append('svg');
                    svg.attr('width', '100%').attr('height', '100%');

                    var margin = chartMargin;

                    var width = $chart.find('svg').width() - margin.left - margin.right;
                    var height = $chart.find('svg').height() - margin.top - margin.bottom;

                    outerR = width > height ? height / 2 : width / 2;
                    innerR = outerR - 16;

                    // SCALES

                    // r
                    r = d3.scale.linear()
                        .domain([0, data.total])
                        .range([0, Math.PI * 1.5]);

                    // D3 PATHS

                    // arc
                    arc = d3.svg.arc()
                        .innerRadius(innerR)
                        .outerRadius(outerR)
                        .cornerRadius((outerR - innerR) / 2)
                        .startAngle(0)
                        .endAngle(function(d) { return r(d); });

                    // append axis, groups and line

                    svg.append('text')
                        .attr('class', 'about-text')
                        .attr('y', 12)
                    .text('Gráfico de saldo - Mês de referência');

                    var arcs = svg.append('g')
                        .attr('transform', 'translate(' + (width / 2) + ',' + ((height / 2) + margin.top) + ') rotate(-135)')
                        .attr('class', 'arcs');

                    arcs.append('path')
                        .attr('class', 'arc-bg')
                        .datum(data.total)
                        .attr('d', arc);

                    arcs.append('path')
                        .attr('class', 'arc')
                        .datum(0)
                        .attr('d', arc);

                    var chart = svg.append('g')
                        .attr('transform', 'translate(' + (width / 2) + ',' + ((height / 2) + margin.top) + ')')
                        .attr('class', 'chart');

                    chart.append('line')
                        .attr('x1', -20)
                        .attr('x2', 20)
                        .attr('y1', 0)
                        .attr('y2', 0);

                    var usedText = chart.append('g')
                        .attr('transform', 'translate(0,-' + (chartText.labelSize + chartText.valueSize) + ')')
                        .attr('class', 'used-text');

                    usedText.append('text')
                        .attr('class', 'value')
                        .attr('style', 'font-size:' + chartText.valueSize + 'px');

                    usedText.append('text')
                        .attr('class', 'label')
                        .attr('y', chartText.labelSize + 4)
                        .attr('style', 'font-size:' + chartText.labelSize + 'px')
                        .text('Utilizado');

                    var availableText = chart.append('g')
                        .attr('transform', 'translate(0,' + (chartText.labelSize + chartText.valueSize) + ')')
                        .attr('class', 'available-text');

                    availableText.append('text')
                        .attr('class', 'value')
                        .attr('style', 'font-size:' + chartText.valueSize + 'px');

                    availableText.append('text')
                        .attr('class', 'label')
                        .attr('y', chartText.labelSize + 4)
                        .attr('style', 'font-size:' + chartText.labelSize + 'px')
                        .text('Disponível');
                }

                function updateChart(data) {
                    
                    $scope.hasData = true;
                    if (data.length === 0) {
                        $scope.hasData = false;
                        return;
                    }

                    var svg = d3.select(chartEl).select('svg');

                    var margin = chartMargin;

                    var width = $chart.find('svg').width() - margin.left - margin.right;
                    var height = $chart.find('svg').height() - margin.top - margin.bottom;

                    // DRAW

                    var arcs = svg.select('g.arcs');

                    // update

                    arcs.select('path.arc')
                        .datum(data.used)
                        // .transition()
                        .attr('d', arc);

                    var usedValueFormat = $filter('currency')(data.used, "R$ ", 2);
                    var deposit = $filter('currency')(data.total - data.used, "R$ ", 2);
                    
                    

                    svg.select('.used-text .value')
                        .text(usedValueFormat);

                    svg.select('.available-text .value')
                        .text(deposit);
                }

                /*This method will be called whet the 'objectToInject' value is changes*/
                $scope.$watch('objectToInject', function(value) {
                    /*Checking if the given value is not undefined*/
                    
                    if (value) {
                        var chartData = CartaoFactory.bindDonutChartData();

                        if (chartData.length === 0) {
                            $scope.hasData = false;
                        }
                        else {
                            if (chartEl) {
                                $(d3.select(chartEl)[0]).find("svg").remove();
                            }
                            else {
                                chartEl = element[0];
                                $chart = $(chartEl);
                            }
                            initChart(chartData);
                            updateChart(chartData);
                        }

                        // $scope.Obj = value;
                        //     /*Injecting the Method*/
                        //     $scope.Obj.invoke = function(){
                        //         alert("Testee");
                        //     };
                        //d3.select(chartEl) = [];

                        //initChart(CartaoFactory.bindLineChartData());
                }
                });

                // $scope.$watch(attrs.ngModel, function(newVal, oldVal, scope) {
                //     if (newVal) {
                //         updateChart(CartaoFactory.bindLineChartData());
                //     }
                // });
            }
        }
    ]);

})();
