(function() {
    'use strict';

    angular.module('portalUsuarioApp').directiveLazy('tktGraficoLinha', [
        'CartaoFactory',
        function(CartaoFactory) {

            var directive = {
                restrict: 'E',
                scope: {
                    objectToInject: '=',
                },
                templateUrl: 'app/modules/beneficiario/dashboard/graficolinha.htm',
                replace: true,
                link: link
            };

            return directive;

            function link($scope, element, attrs) {
                $scope.hasData = true;

                // var chartData = CartaoFactory.bindLineChartData();

                // if (chartData.length === 0) {
                //     $scope.hasData = false;
                // }
                // else {
                //     var chartEl = element[0];
                //     var $chart = $(chartEl);
                //     var chartMargin = { top: 20, right: 1, bottom: 1, left: 1 };
                //     var meanBox = { width: 184, height: 20, r: 2, margin: 4 };

                //     // chart variables
                //     var width, height, x, yMax, yMean, y;

                //      //initChart(chartData);
                //      //updateChart(chartData);
                // }


                var chartEl;
                var $chart;
                var chartMargin = { top: 20, right: 1, bottom: 1, left: 1 };
                var meanBox = { width: 184, height: 20, r: 2, margin: 4 };

                // chart variables
                var width, height, x, yMax, yMean, y;

                function initChart(data) {
                    // if(d3.select(chartEl).length > 0)
                    // {
                    //     d3.select(chartEl) = [];
                    // };
                    var svg = d3.select(chartEl).append('svg');
                    svg.attr('width', '100%').attr('height', '100%');

                    width = $chart.find('svg').width() - chartMargin.left - chartMargin.right;
                    height = $chart.find('svg').height() - chartMargin.top - chartMargin.bottom;

                    // SCALES

                    // x
                    x = d3.scale.ordinal()
                        .domain(data.map(function(d, i) { return i; }))
                        .rangePoints([0, width], 0);

                    // y
                    yMax = d3.max(data, function(d) { return d.value; });
                    yMean = d3.mean(data, function(d) { return d.value; });
                    y = d3.scale.linear().domain([0, yMax]).range([height, 0]);

                    // append axis, groups and line
                    svg.append('text')
                        .attr('class', 'about-text')
                        .attr('y', 12)
                        .text('Consumo no período');

                    svg.append('g')
                        .attr('class', 'y-axis')
                        .attr('transform', 'translate(' + (chartMargin.left + width) + ',' + chartMargin.top + ')');

                    svg.append('text')
                        .attr('class', 'y-tick y-tick-max-text')
                        .attr('y', 32)
                        .text('R$ 123,45 (máx.)');

                    svg.append('text')
                        .attr('class', 'y-tick')
                        .attr('y', chartMargin.top + height - 4)
                        .text('R$ 0,00');

                    var chart = svg.append('g')
                        .attr('transform', 'translate(' + chartMargin.left + ',' + chartMargin.top + ')')
                        .attr('class', 'chart');

                    chart.append('path')
                        .attr('class', 'line');

                    chart.append('line')
                        .attr('class', 'mean-line')
                        .attr('x1', 0)
                        .attr('y1', height)
                        .attr('x2', width)
                        .attr('y2', height);

                    chart.append('rect')
                        .attr('class', 'mean-box')
                        .attr('width', meanBox.width)
                        .attr('height', meanBox.height)
                        .attr('rx', meanBox.r)
                        .attr('ry', meanBox.r)
                        .attr('x', 0)
                        .attr('y', height - meanBox.height - meanBox.margin);

                    chart.append('text')
                        .attr('class', 'mean-text')
                        .attr('x', meanBox.width / 2)
                        .attr('y', height - meanBox.margin - 7);
                }

                function updateChart(data) {

                    $scope.hasData = true;
                    if (data.length === 0) {
                        $scope.hasData = false;
                        return;
                    }

                    var svg = d3.select(chartEl).select('svg');

                    svg.select('text.y-tick-max-text')
                        .text('R$ ' + d3.format(',.2f')(yMax) + ' (máx.)');

                    var yAxis = d3.svg.axis()
                        .scale(y)
                        .orient('left')
                        .tickValues([0, yMax])
                        .tickSize(width)
                        .tickPadding(8);

                    svg.select('g.y-axis')
                        .call(yAxis);

                    // DRAW

                    var chart = svg.select('g.chart');
                    
                    // enter / update

                    var line = d3.svg.line()
                        .x(function(d, i) { return x(i); })
                        .y(function (d) { return y(d.value); });

                    chart.select('path.line')
                        .datum(data)
                        // .transition()
                        .attr('d', line);

                    svg.select('.mean-line')
                        // .transition()
                        .attr('y1', y(yMean))
                        .attr('y2', y(yMean));

                    svg.select('.mean-box')
                        // .transition()
                        .attr('y', y(yMean) - meanBox.height - meanBox.margin);

                    svg.select('.mean-text')
                        .text('R$ ' + d3.format(',.2f')(yMean) + ' (ticket médio no período)')
                        // .transition()
                        .attr('y', y(yMean) - meanBox.margin - 7);
                }

                /*This method will be called whet the 'objectToInject' value is changes*/
                $scope.$watch('objectToInject', function(value) {
                    /*Checking if the given value is not undefined*/
                    if (value) {
                        var chartData = CartaoFactory.bindLineChartData();

                        if (chartData.length === 0) {
                            $scope.hasData = false;
                        }
                        else {
                            if (chartEl) {
                                $(d3.select(chartEl)[0]).find("svg").remove();
                            }
                            else {
                                chartEl = element[0];
                                $chart = $(chartEl);
                            }
                            initChart(chartData);
                            updateChart(chartData);
                        }

                        // $scope.Obj = value;
                        //     /*Injecting the Method*/
                        //     $scope.Obj.invoke = function(){
                        //         alert("Testee");
                        //     };
                        //d3.select(chartEl) = [];

                        //initChart(CartaoFactory.bindLineChartData());
                    }
                });

                // $scope.$watch(attrs.ngModel, function(newVal, oldVal, scope) {
                //     if (newVal) {
                //         updateChart(CartaoFactory.bindLineChartData());
                //     }
                // });
            }
        }
    ]);

})();
