(function() {
  'use strict';

  angular.module('portalUsuarioApp').factory('RedeCredenciadaFactory', [
    function() {

      // set variables
      var redeFormData = {};
      resetRedeFormData();

      // set service
      var service = {
        bindRedeFormData: bindRedeFormData,
        submitRedeFormData: submitRedeFormData,
        //closeModal: closeModal
      };

      return service;

      // public
      // used to bind redeFormData to controller
      function bindRedeFormData() {
          return redeFormData;
      }

      // public
      // used to submit redeFormData
      function submitRedeFormData() {
        console.log(redeFormData);
      }

      // private
      // reset redeFormData
      function resetRedeFormData() {

        var iconTr = {
          url: '../assets/images/tmaps-marker-tr.png',
          size: new google.maps.Size(48, 56),
          origin: new google.maps.Point(0, 0),
          scaledSize: new google.maps.Size(48, 56)
        };
        var iconTrAC = {
          url: '../assets/images/tmaps-marker-tr-ac.png',
          size: new google.maps.Size(48, 56),
          origin: new google.maps.Point(0, 0),
          scaledSize: new google.maps.Size(48, 56)
        };
        var iconTrA = {
          url: '../assets/images/tmaps-marker-tr-a.png',
          size: new google.maps.Size(48, 56),
          origin: new google.maps.Point(0, 0),
          scaledSize: new google.maps.Size(48, 56)
        };
        var iconTrC = {
          url: '../assets/images/tmaps-marker-tr-c.png',
          size: new google.maps.Size(48, 56),
          origin: new google.maps.Point(0, 0),
          scaledSize: new google.maps.Size(48, 56)
        };
        var iconTa = {
          url: '../assets/images/tmaps-marker-ta.png',
          size: new google.maps.Size(48, 56),
          origin: new google.maps.Point(0, 0),
          scaledSize: new google.maps.Size(48, 56)
        };
        var iconTaA = {
          url: '../assets/images/tmaps-marker-ta-a.png',
          size: new google.maps.Size(48, 56),
          origin: new google.maps.Point(0, 0),
          scaledSize: new google.maps.Size(48, 56)
        };
        var iconFav = {
          url: '../assets/images/tmaps-marker-fav.png',
          size: new google.maps.Size(48, 56),
          origin: new google.maps.Point(0, 0),
          scaledSize: new google.maps.Size(48, 56)
        };
        var iconFavC = {
          url: '../assets/images/tmaps-marker-fav-c.png',
          size: new google.maps.Size(48, 56),
          origin: new google.maps.Point(0, 0),
          scaledSize: new google.maps.Size(48, 56)
        };
        var iconFavA = {
          url: '../assets/images/tmaps-marker-fav-a.png',
          size: new google.maps.Size(48, 56),
          origin: new google.maps.Point(0, 0),
          scaledSize: new google.maps.Size(48, 56)
        };
        var iconFavAC = {
          url: '../assets/images/tmaps-marker-fav-ac.png',
          size: new google.maps.Size(48, 56),
          origin: new google.maps.Point(0, 0),
          scaledSize: new google.maps.Size(48, 56)
        };
        var iconTk = {
          url: '../assets/images/tmaps-marker-tk.png',
          size: new google.maps.Size(48, 56),
          origin: new google.maps.Point(0, 0),
          scaledSize: new google.maps.Size(48, 56)
        };

        redeFormData.address = '';
        redeFormData.radius = 1;
        redeFormData.radiusOpts = [
          {
            name: '500 m',
            value: 0
          },
          {
            name: '1 km',
            value: 1
          },
          {
            name: '2 km',
            value: 2
          },
          {
            name: '3 km',
            value: 3
          },
          {
            name: '5 km',
            value: 4
          },
          {
            name: '8 km',
            value: 5
          },
          {
            name: '10 km',
            value: 6
          },
        ];
        redeFormData.cardtype = 'ta';
        redeFormData.listFav = '';
        redeFormData.listResults = [];
      }

    }
  ]);

})();
