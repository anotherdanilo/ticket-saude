
  angular.module('portalUsuarioApp').factoryLazy('AccentivResource', [
    '$resource', '$http', function($resource, $http) {

	  var self;
	  self = this;

      return $resource(config.urls.addCartaoAccentiv, null, {
        'update': {
          method: 'PUT'
        }
      });

		
    }
  ]);

