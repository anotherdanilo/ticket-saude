(function () {
    'use strict';

    angular.module('portalUsuarioApp').factory('AccentivFactory', [

        function () {

            var retorno = {
                sucesso: false,
                css: '',
                bandeira: ''
            };
            var service = {
                existePersonalizacaoId: existePersonalizacaoId,
                reset: reset
            };

            return service;

            function reset() {
                retorno = {
                    sucesso: false,
                    css: '',
                    bandeira: ''
                };
            }

            function existePersonalizacaoId(id) {

                var listPresenteGoodCard = JSON.parse(config.listPresenteGoodCard);
                var listDuoCard = JSON.parse(config.listDuoCard);
                var listPresenteKids = JSON.parse(config.listPresenteKids);
                var listPresentePerfeito = JSON.parse(config.listPresentePerfeito);


                for (var x = 0; x < listPresenteGoodCard.length; x++) {
                    if (listPresenteGoodCard[x] == id) {
                        retorno.sucesso = true;
                        retorno.css = '-presente';
                        retorno.bandeira = 'goodcard';
                        return retorno;
                    }
                }

                for (x = 0; x < listDuoCard.length; x++) {
                    if (listDuoCard[x] == id) {
                        retorno.sucesso = true;
                        retorno.css = '-duocard';
                        retorno.bandeira = '';
                        return retorno;
                    }
                }

                for (x = 0; x < listPresenteKids.length; x++) {
                    if (listPresenteKids[x] == id) {
                        retorno.sucesso = true;
                        retorno.css = '-presentekids';
                        retorno.bandeira = '';
                        return retorno;
                    }
                }

                for (x = 0; x < listPresentePerfeito.length; x++) {
                    if (listPresentePerfeito[x] == id) {
                        retorno.sucesso = true;
                        retorno.css = '-presente';
                        retorno.bandeira = 'mastercard';
                        return retorno;
                    }
                }

                return retorno;

            }


        }
    ]);

})();
