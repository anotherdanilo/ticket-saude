(function() {
  'use strict';

  angular.module('portalUsuarioApp').factoryLazy('GraficoConsumoFactory', [
    function() {

      // set variables
      var fakeTrChartData = [
        { day: 1,  value: 10 },
        { day: 2,  value: 20 },
        { day: 3,  value: 0 },
        { day: 4,  value: 40 },
        { day: 5,  value: 50 },
        { day: 6,  value: 134 },
        { day: 7,  value: 36 },
        { day: 8,  value: 74 },
        { day: 9,  value: 0 },
        { day: 10, value: 0 },
        { day: 11, value: 27 },
        { day: 12, value: 8 },
        { day: 13, value: 10 },
        { day: 14, value: 33 },
        { day: 15, value: 20 },
      ];

      // set service
      var service = {
        bindTrChartData: bindTrChartData
      };

      return service;

      // public
      // used to bind signUpFormData to controller
      function bindTrChartData() {
        return fakeTrChartData;
      }
    }
  ]);

})();
