(function() {
  'use strict';
  angular.module('portalUsuarioApp').factoryLazy('AtendimentoModalsFactory', [
    '$uibModal',
    function($uibModal) {

      var serviceData = {};
      resetServiceData();

      var selectCardModalInstance;
      var telModalInstance;
      var msgModalInstance;

      var service = {
        bindServiceData: bindServiceData,
        openSelect: openSelect,
        closeSelect: closeSelect,
        openTel: openTel,
        closeTel: closeTel,
        openMsg: openMsg,
        closeMsg: closeMsg,
      };

      return service;

      // private
      // reset service data
      function resetServiceData() {
        serviceData.title = '';
        serviceData.label = '';
        serviceData.service = '';
      }

      // private
      // set service data
      function setServiceData(action) {
        switch (action) {
          case 'unblock':
            serviceData.title = 'Desbloquear cartão';
            serviceData.label = 'desbloquear';
            serviceData.service = 'unblock';
            break;
          case 'block':
            serviceData.title = 'Bloquear cartão';
            serviceData.label = 'bloquear';
            serviceData.service = 'block';
            break;
          case 'changePwd':
            serviceData.title = 'Alterar senha';
            serviceData.label = 'alterar a senha';
            serviceData.service = 'changePwd';
            break;
          case 'reissue':
            serviceData.title = 'Reemitir cartão';
            serviceData.label = 'reemitir';
            serviceData.service = 'reissue';
            break;
        }
      }

      // public
      // used to bind serviceData to controller
      function bindServiceData() {
        return serviceData;
      }

      //open modal to select a card
      function openSelect(action) {
        setServiceData(action);
        selectCardModalInstance = $uibModal.open({
          animation: true,
          templateUrl: 'app/modules/beneficiario/modal/selecionarcartao.htm',
          windowClass: '-ticket -selecionar',
          size: 'sm'
        });
      }

      function closeSelect() {
        selectCardModalInstance.dismiss();
      }

      //open modal to select a card
      function openTel() {
        telModalInstance = $uibModal.open({
          animation: true,
          templateUrl: 'app/modules/beneficiario/modal/contatotel.htm',
          windowClass: '-ticket -contatotel',
          size: 'sm'
        });
      }

      function closeTel() {
        telModalInstance.dismiss();
      }

      //open modal to select a card
      function openMsg() {
        msgModalInstance = $uibModal.open({
          animation: true,
          templateUrl: 'app/modules/beneficiario/modal/contatomsg.htm',
          windowClass: '-ticket -contatomsg',
          size: 'sm'
        });
      }

      function closeMsg() {
        msgModalInstance.dismiss();
      }
    }
  ]);

})();
