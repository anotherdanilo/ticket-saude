(function() {
  angular.module('portalUsuarioApp').factoryLazy('UsuarioResource', [
    '$resource', function($resource) {
        return $resource(config.urls.beneficiarioUsuarioAPI, null, {
        'update': {method: 'PUT'},
        'save': {method: 'POST', url: config.urls.beneficiarioUsuarioAPIPostV3_1}
      });
    }
  ]);

}).call(this);
