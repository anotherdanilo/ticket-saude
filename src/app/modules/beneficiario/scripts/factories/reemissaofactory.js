(function() {
  'use strict';

  angular.module('portalUsuarioApp').factory('ReemissaoFactory', [
    
    function() {

      // set variables
      var reissueFormData = {};
      resetReissueFormData();

      var cartao;

      // set service
      var service = {
        bindReissueFormData: bindReissueFormData,
        submitReissueFormData: submitReissueFormData,
        getCartao: getCartao,
        setCartao: setCartao
        //closeModal: closeModal
      };

      return service;

      // public
      // used to bind reissueFormData to controller
      function bindReissueFormData() {
        return reissueFormData;
      }
      
      function setCartao(detalheCartao) {
          cartao = detalheCartao;
      }
      
      function getCartao() {
          return cartao;
      }

      // public
      // used to submit reissueFormData
      function submitReissueFormData() {
        console.log(reissueFormData);
      }

      // private
      // reset reissueFormData
      function resetReissueFormData() {
        reissueFormData.name = '';
        reissueFormData.cpf = '';
        reissueFormData.record = '';
        reissueFormData.birthDateDay = 1;
        reissueFormData.birthDateDayOpts = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31];
        reissueFormData.birthDateMonth = 1;
        reissueFormData.birthDateMonthOpts = [
          {
            name: 'Janeiro',
            value: 1
          },
          {
            name: 'Fevereiro',
            value: 2
          },
          {
            name: 'Março',
            value: 3
          },
          {
            name: 'Abril',
            value: 4
          },
          {
            name: 'Maio',
            value: 5
          },
          {
            name: 'Junho',
            value: 6
          },
          {
            name: 'Julho',
            value: 7
          },
          {
            name: 'Agosto',
            value: 8
          },
          {
            name: 'Setembro',
            value: 9
          },
          {
            name: 'Outubro',
            value: 10
          },
          {
            name: 'Novembro',
            value: 11
          },
          {
            name: 'Dezembro',
            value: 12
          }
        ];
        reissueFormData.birthDateYear = 1980;
        reissueFormData.birthDateYearOpts = [
              1916, 1917, 1918, 1919, 1920, 1921, 1922, 1923, 1924, 1925, 1926, 1927, 1928, 1929, 1930, 1931, 1932, 1933, 1934, 1935, 1936, 1937,
              1938, 1939, 1940, 1941, 1942, 1943, 1944, 1945, 1946, 1947, 1948, 1949, 1950, 1951, 1952, 1953, 1954, 1955, 1956, 1957, 1958, 1959,
              1960, 1961, 1962, 1963, 1964, 1965, 1966, 1967, 1968, 1969, 1970, 1971, 1972, 1973, 1974, 1975, 1976, 1977, 1978, 1979, 1980, 1981,
              1982, 1983, 1984, 1985, 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993, 1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003,
              2004, 2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016
        ];
        reissueFormData.motive = 1;
        reissueFormData.motiveOpts = [
          {
            name: 'Perdi meu cartão',
            value: 1
          },
          {
            name: 'Meu cartão foi roubado',
            value: 2
          },
          {
            name: 'Tarja ou chip do cartão não funciona',
            value: 7
          },
          {
            name: 'Meu cartão quebrou',
            value: 9
          },
        ];
      }

      // function closeModal() {
      //   LoginModalsFactory.closeConfirm();
      // }
    }
  ]);

})();
