(function() {
  angular.module('portalUsuarioApp').factoryLazy('RedeCredenciadaResource', [
    '$resource', function($resource) {
        return $resource(config.urls.buscaEstabelecimento, null, {
        'update': {
          method: 'PUT'
        }
      });
    }
  ]);

}).call(this);
