(function() {
  'use strict';
  angular.module('portalUsuarioApp').factoryLazy('AutoModalsFactory', [
    '$uibModal',
    function($uibModal) {
        
      var addModalInstance,
          editModalInstance,
          changePwdModalInstance,
          blockModalInstance,
          unblockModalInstance,
          reissueModalInstance,
          viewData;

      var service = {
        openAdd: openAdd,
        closeAdd: closeAdd,
        openEdit: openEdit,
        closeEdit: closeEdit,
        openChangePwd: openChangePwd,
        closeChangePwd: closeChangePwd,
        openBlock: openBlock,
        closeBlock: closeBlock,
        openUnblock: openUnblock,
        closeUnblock: closeUnblock,
        openReissue: openReissue,
        closeReissue: closeReissue
      };

      return service;
    
      function openAdd() {
        addModalInstance = $uibModal.open({
          animation: true,
          templateUrl: 'app/modules/beneficiario/modal/adicionarcartao.htm',
          windowClass: '-ticket -adicionar',
          size: 'sm'
        });
      }

      function closeAdd() {
        addModalInstance.dismiss();
      }

      function openEdit(id) {

        editModalInstance = $uibModal.open({
          animation: true,
          templateUrl: 'app/modules/beneficiario/modal/editarcartao.htm',
          windowClass: '-ticket -editar',
          size: 'sm'
          
        });
      }

      function closeEdit() {
        editModalInstance.dismiss();
      }

      function openUnblock(valid) {
        if (valid) {
          unblockModalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'app/modules/beneficiario/modal/desbloqueiocartao.htm',
            windowClass: '-ticket -desbloqueio',
            size: 'sm'
          });
        }
        else {
          unblockModalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'app/modules/beneficiario/modal/desbloqueiocartao.htm',
            windowClass: '-ticket -desbloqueio -confirmadados',
            size: 'sm'
          });
        }
      }

      function closeUnblock() {
        unblockModalInstance.dismiss();
      }

      function openBlock(valid) {
        if (valid) {
          blockModalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'app/modules/beneficiario/modal/bloqueiocartao.htm',
            windowClass: '-ticket -bloqueio',
            size: 'sm'
          });
        }
        else {
          blockModalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'app/modules/beneficiario/modal/bloqueiocartao.htm',
            windowClass: '-ticket -bloqueio -confirmadados',
            size: 'sm'
          });
        }
      }

      function closeBlock() {
        blockModalInstance.dismiss();
      }

      function openReissue(valid) {
        if (valid) {
          reissueModalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'app/modules/beneficiario/modal/reemissaocartao.htm',
            windowClass: '-ticket -reemissao',
            size: 'sm'
          });
        }
        else {
          reissueModalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'app/modules/beneficiario/modal/reemissaocartao.htm',
            windowClass: '-ticket -reemissao -confirmadados',
            size: 'sm'
          });
        }
      }

      function closeReissue() {
        reissueModalInstance.dismiss();
      }

      function openChangePwd() {
        changePwdModalInstance = $uibModal.open({
          animation: true,
          templateUrl: 'app/modules/beneficiario/modal/alterarsenhacartao.htm',
          windowClass: '-ticket -alterar',
          size: 'sm'
        });
      }

      function closeChangePwd() {
        changePwdModalInstance.dismiss();
      }
    }
  ]);

})();
