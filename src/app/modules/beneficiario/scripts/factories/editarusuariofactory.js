(function() {
  'use strict';

  angular.module('portalUsuarioApp').factoryLazy('EditarUsuarioFactory', [
    function() {

      // set variables
      var usuarioFormData = {};
      resetUsuarioFormData();

      // set service
      var service = {
        bindUsuarioFormData: bindUsuarioFormData,
      };

      return service;

      // public
      // used to bind usuarioFormData to controller
      function bindUsuarioFormData() {
        return usuarioFormData;
      }

      // private
      // reset usuarioFormData
      function resetUsuarioFormData() {
        usuarioFormData.name = 'Fulano de tal';
        usuarioFormData.cpf = '123.456.789-00';
        usuarioFormData.record = '12345678900';
        usuarioFormData.cellphone = '(11) 91234-5678';
        usuarioFormData.email = 'fulano.tal@gmail.com';
        usuarioFormData.gender = 'masc';
        usuarioFormData.birthDateDay = 1;
       // usuarioFormData.birthDateDayOpts = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31];
        usuarioFormData.birthDateMonth = 1;
        usuarioFormData.birthDateMonthOpts = [
          {
            name: 'Janeiro',
            value: 1
          },
          {
            name: 'Fevereiro',
            value: 2
          },
          {
            name: 'Março',
            value: 3
          },
          {
            name: 'Abril',
            value: 4
          },
          {
            name: 'Maio',
            value: 5
          },
          {
            name: 'Junho',
            value: 6
          },
          {
            name: 'Julho',
            value: 7
          },
          {
            name: 'Agosto',
            value: 8
          },
          {
            name: 'Setembro',
            value: 9
          },
          {
            name: 'Outubro',
            value: 10
          },
          {
            name: 'Novembro',
            value: 11
          },
          {
            name: 'Dezembro',
            value: 12
          }
        ];
        usuarioFormData.birthDateYear = 1980;
        usuarioFormData.birthDateYearOpts = [
              1916, 1917, 1918, 1919, 1920, 1921, 1922, 1923, 1924, 1925, 1926, 1927, 1928, 1929, 1930, 1931, 1932, 1933, 1934, 1935, 1936, 1937,
              1938, 1939, 1940, 1941, 1942, 1943, 1944, 1945, 1946, 1947, 1948, 1949, 1950, 1951, 1952, 1953, 1954, 1955, 1956, 1957, 1958, 1959,
              1960, 1961, 1962, 1963, 1964, 1965, 1966, 1967, 1968, 1969, 1970, 1971, 1972, 1973, 1974, 1975, 1976, 1977, 1978, 1979, 1980, 1981,
              1982, 1983, 1984, 1985, 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993, 1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003,
              2004, 2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016
        ];
        usuarioFormData.profilePic = '../assets/images/profile-pic-01.jpg';
        usuarioFormData.cep = '01452-001';
        usuarioFormData.address1 = 'Av. Faria Lima';
        usuarioFormData.address2 = 'Cj. 21';
        usuarioFormData.addressNum = '1853';
        usuarioFormData.state = 'SP';
        usuarioFormData.city = 'São Paulo';
        usuarioFormData.bairro = 'Jardim Paulistano';
        usuarioFormData.interests = [
          {
            name: 'Bem-estar e Saúde',
            value: false,
          },
          {
            name: 'Cultura e Lazer',
            value: true,
          },
          {
            name: 'Cursos',
            value: false,
          },
          {
            name: 'Esporte e Aventura',
            value: false,
          },
          {
            name: 'Gastronomia',
            value: true,
          },
          {
            name: 'Hotéis e Viagens',
            value: true,
          },
          {
            name: 'Produtos',
            value: false,
          },
          {
            name: 'Serviços',
            value: true,
          },
        ];
        usuarioFormData.optIn = true;
      }
    }
  ]);

})();
