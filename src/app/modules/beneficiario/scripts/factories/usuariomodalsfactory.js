(function() {
    'use strict';
    angular.module('portalUsuarioApp').factoryLazy('UsuarioModalsFactory', [
        '$uibModal',
        function($uibModal) {

            var changePwdInstance;
            var forgetPwdInstance;
            var escopo = {
                step: 0
            };

            var service = {
                openChangePwd: openChangePwd,
                openForgetPwd: openForgetPwd,
                closeChangePwd: closeChangePwd,
                closeForgetPwd: closeForgetPwd,
                getEscopo: getEscopo,
                setEscopo: setEscopo
            };

            return service;

            function setEscopo(step) {
                escopo.step = step;
            }

            function getEscopo() {
                return escopo;
            }

            function openChangePwd() {
                //escopo.step = 3;
                changePwdInstance = $uibModal.open({
                    animation: true,
                    templateUrl: 'app/modules/beneficiario/modal/alterarsenhausuario.htm',
                    windowClass: '-ticket -changepwd',
                    size: 'sm'
                });
            }
            
            function openForgetPwd() {
                //escopo.step = 3;
                forgetPwdInstance = $uibModal.open({
                    animation: true,
                    templateUrl: 'app/modules/autenticacao/modal/login.htm',
                    windowClass: '-ticket -changepwd',
                    size: 'sm'
                });
            }
            
            // function uploadFoto() {
            //     changePwdInstance = $uibModal.open({
            //     animation: true,
            //     templateUrl: 'app/modules/beneficiario/usuario/dropzone.htm',
            //     windowClass: '-ticket ',
            //     size: 'sm'
            // });
            // }


            function closeChangePwd() {
                changePwdInstance.dismiss();
            }
            
            function closeForgetPwd() {
                forgetPwdInstance.dismiss();
            }
        }
    ]);


})();
