﻿(function () {
    'use strict';

    angular.module('portalUsuarioApp').factory('AlertaFactory', [
        '$uibModal',
        function ($uibModal) {

            var loginModalInstance;

            var parametros = {
                titulo: '',
                mensagem: '',
                icone: '',
                callback: null,
                textoBotaoOk: ''
            };

            var service = {
                openAlerta: openAlerta,
                closeAlerta: closeAlerta,
                parametros: parametros,
                close: close,
                init: init
            };

            return service;

            function init() {
                parametros = {
                    titulo: '',
                    mensagem: '',
                    icone: '',
                    callback: null,
                    textoBotaoOk: ''
                };
            }

            function openAlerta(textBotaoOk, callback) {

                
                parametros.textoBotaoOk = "Ok";

                if (textBotaoOk) {
                    parametros.textoBotaoOk = textBotaoOk;
                }
                
                parametros.callback = null;
                
                if(callback){
                    parametros.callback = callback;
                }

                loginModalInstance = $uibModal.open({
                    animation: true,
                    templateUrl: 'app/modules/beneficiario/modal/alerta.htm',
                    windowClass: '-ticket -login',
                    size: 'sm'
                });

            }

            function closeAlerta() {

                if (parametros.callback) {
                    parametros.callback();

                    parametros.callback = null;
                }

                loginModalInstance.dismiss();

            }
            
            function close() {

                parametros.callback = null;

                loginModalInstance.dismiss();

            }

        }
    ]);

})();
