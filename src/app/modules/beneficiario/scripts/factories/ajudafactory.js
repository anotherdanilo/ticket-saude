(function() {
  'use strict';

  angular.module('portalUsuarioApp').factory('AjudaFactory', [
    function() {

      // set variables
      var faqData = {};
      populateFaqData();

      // set service
      var service = {
        bindFaqData: bindFaqData,
      };

      return service;

      // public
      // used to bind faqData to controller
      function bindFaqData() {
        return faqData;
      }

      // private
      // populate FaqData
      function populateFaqData() {
        faqData.categories = [
          {
            id: 'acesso',
            label: 'Acesso'
          },
          {
            id: 'cartao',
            label: 'Cartão'
          },
          {
            id: 'bc',
            label: 'Benefício Club'
          }
        ];
        faqData.questions = [
          {
            id: 'q01',
            category: 'Acesso',
            title: 'Lorem acesso sit dolar amet',
            answer: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras auctor velit quis ex cursus, a consectetur purus imperdiet. Nulla eu convallis massa. Nunc malesuada massa vitae elementum efficitur. Fusce nec convallis felis. Vestibulum quam purus, pretium et felis a, gravida convallis metus. Vivamus eros sem, auctor vel ultricies ut, tincidunt vel ligula. Integer eu viverra erat. Cras posuere est eu ligula rhoncus, faucibus vulputate turpis dictum. Phasellus dictum mattis est et tincidunt.',
          },
          {
            id: 'q02',
            category: 'Acesso',
            title: 'Lorem acesso sit dolar amet',
            answer: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras auctor velit quis ex cursus, a consectetur purus imperdiet. Nulla eu convallis massa. Nunc malesuada massa vitae elementum efficitur. Fusce nec convallis felis. Vestibulum quam purus, pretium et felis a, gravida convallis metus. Vivamus eros sem, auctor vel ultricies ut, tincidunt vel ligula. Integer eu viverra erat. Cras posuere est eu ligula rhoncus, faucibus vulputate turpis dictum. Phasellus dictum mattis est et tincidunt.',
          },
          {
            id: 'q03',
            category: 'Acesso',
            title: 'Lorem acesso sit dolar amet',
            answer: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras auctor velit quis ex cursus, a consectetur purus imperdiet. Nulla eu convallis massa. Nunc malesuada massa vitae elementum efficitur. Fusce nec convallis felis. Vestibulum quam purus, pretium et felis a, gravida convallis metus. Vivamus eros sem, auctor vel ultricies ut, tincidunt vel ligula. Integer eu viverra erat. Cras posuere est eu ligula rhoncus, faucibus vulputate turpis dictum. Phasellus dictum mattis est et tincidunt.',
          },
          {
            id: 'q04',
            category: 'Cartão',
            title: 'Lorem cartao sit dolar amet',
            answer: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras auctor velit quis ex cursus, a consectetur purus imperdiet. Nulla eu convallis massa. Nunc malesuada massa vitae elementum efficitur. Fusce nec convallis felis. Vestibulum quam purus, pretium et felis a, gravida convallis metus. Vivamus eros sem, auctor vel ultricies ut, tincidunt vel ligula. Integer eu viverra erat. Cras posuere est eu ligula rhoncus, faucibus vulputate turpis dictum. Phasellus dictum mattis est et tincidunt.',
          },
          {
            id: 'q05',
            category: 'Cartão',
            title: 'Lorem cartao sit dolar amet',
            answer: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras auctor velit quis ex cursus, a consectetur purus imperdiet. Nulla eu convallis massa. Nunc malesuada massa vitae elementum efficitur. Fusce nec convallis felis. Vestibulum quam purus, pretium et felis a, gravida convallis metus. Vivamus eros sem, auctor vel ultricies ut, tincidunt vel ligula. Integer eu viverra erat. Cras posuere est eu ligula rhoncus, faucibus vulputate turpis dictum. Phasellus dictum mattis est et tincidunt.',
          },
          {
            id: 'q06',
            category: 'Cartão',
            title: 'Lorem cartao sit dolar amet',
            answer: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras auctor velit quis ex cursus, a consectetur purus imperdiet. Nulla eu convallis massa. Nunc malesuada massa vitae elementum efficitur. Fusce nec convallis felis. Vestibulum quam purus, pretium et felis a, gravida convallis metus. Vivamus eros sem, auctor vel ultricies ut, tincidunt vel ligula. Integer eu viverra erat. Cras posuere est eu ligula rhoncus, faucibus vulputate turpis dictum. Phasellus dictum mattis est et tincidunt.',
          },
          {
            id: 'q07',
            category: 'Cartão',
            title: 'Lorem cartao sit dolar amet',
            answer: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras auctor velit quis ex cursus, a consectetur purus imperdiet. Nulla eu convallis massa. Nunc malesuada massa vitae elementum efficitur. Fusce nec convallis felis. Vestibulum quam purus, pretium et felis a, gravida convallis metus. Vivamus eros sem, auctor vel ultricies ut, tincidunt vel ligula. Integer eu viverra erat. Cras posuere est eu ligula rhoncus, faucibus vulputate turpis dictum. Phasellus dictum mattis est et tincidunt.',
          },
          {
            id: 'q08',
            category: 'Cartão',
            title: 'Lorem cartao sit dolar amet',
            answer: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras auctor velit quis ex cursus, a consectetur purus imperdiet. Nulla eu convallis massa. Nunc malesuada massa vitae elementum efficitur. Fusce nec convallis felis. Vestibulum quam purus, pretium et felis a, gravida convallis metus. Vivamus eros sem, auctor vel ultricies ut, tincidunt vel ligula. Integer eu viverra erat. Cras posuere est eu ligula rhoncus, faucibus vulputate turpis dictum. Phasellus dictum mattis est et tincidunt.',
          },
          {
            id: 'q09',
            category: 'Benefício Club',
            title: 'Lorem bc sit dolar amet',
            answer: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras auctor velit quis ex cursus, a consectetur purus imperdiet. Nulla eu convallis massa. Nunc malesuada massa vitae elementum efficitur. Fusce nec convallis felis. Vestibulum quam purus, pretium et felis a, gravida convallis metus. Vivamus eros sem, auctor vel ultricies ut, tincidunt vel ligula. Integer eu viverra erat. Cras posuere est eu ligula rhoncus, faucibus vulputate turpis dictum. Phasellus dictum mattis est et tincidunt.',
          },
          {
            id: 'q10',
            category: 'Benefício Club',
            title: 'Lorem bc sit dolar amet',
            answer: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras auctor velit quis ex cursus, a consectetur purus imperdiet. Nulla eu convallis massa. Nunc malesuada massa vitae elementum efficitur. Fusce nec convallis felis. Vestibulum quam purus, pretium et felis a, gravida convallis metus. Vivamus eros sem, auctor vel ultricies ut, tincidunt vel ligula. Integer eu viverra erat. Cras posuere est eu ligula rhoncus, faucibus vulputate turpis dictum. Phasellus dictum mattis est et tincidunt.',
          },
          {
            id: 'q11',
            category: 'Benefício Club',
            title: 'Lorem bc sit dolar amet',
            answer: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras auctor velit quis ex cursus, a consectetur purus imperdiet. Nulla eu convallis massa. Nunc malesuada massa vitae elementum efficitur. Fusce nec convallis felis. Vestibulum quam purus, pretium et felis a, gravida convallis metus. Vivamus eros sem, auctor vel ultricies ut, tincidunt vel ligula. Integer eu viverra erat. Cras posuere est eu ligula rhoncus, faucibus vulputate turpis dictum. Phasellus dictum mattis est et tincidunt.',
          },
          {
            id: 'q12',
            category: 'Benefício Club',
            title: 'Lorem bc sit dolar amet',
            answer: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras auctor velit quis ex cursus, a consectetur purus imperdiet. Nulla eu convallis massa. Nunc malesuada massa vitae elementum efficitur. Fusce nec convallis felis. Vestibulum quam purus, pretium et felis a, gravida convallis metus. Vivamus eros sem, auctor vel ultricies ut, tincidunt vel ligula. Integer eu viverra erat. Cras posuere est eu ligula rhoncus, faucibus vulputate turpis dictum. Phasellus dictum mattis est et tincidunt.',
          },
        ];
      }
    }
  ]);

})();
