
  angular.module('portalUsuarioApp').factoryLazy('CartaoResource', [
    '$resource', '$http', function($resource, $http) {

	  var self;
	  self = this;

      return $resource(config.urls.cartaoAPI, null, {
        'update': {
          method: 'PUT'
        }
      });

		
    }
  ]);

