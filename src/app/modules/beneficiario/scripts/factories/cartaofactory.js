(function() {
    'use strict';

    angular.module('portalUsuarioApp').factoryLazy('CartaoFactory', [
        function() {

            // set variables
            var fakeLineChartData = [];

            var fakeDonutChartData;

            // set service
            var service = {
                bindLineChartData: bindLineChartData,
                bindDonutChartData: bindDonutChartData,
                setDataDonutChart: setDataDonutChart,
                setDataLineChart: setDataLineChart
            };

            return service;

            function setDataDonutChart(obj) {
                fakeDonutChartData = null;
                fakeDonutChartData = {
                    total: obj.total,
                    used: obj.used
                };
            }

            function setDataLineChart(obj) {

                fakeLineChartData = [];
                angular.forEach(obj, function(valor, key) {
                    var dia = moment(valor.date, "DD/MM/YYYY").format('DD');
                    fakeLineChartData.push({
                        day: parseInt(dia),
                        value: parseInt(valor.value)
                    });
                });
            }

            // public
            // used to bind chart data to controller
            function bindLineChartData() {
                return fakeLineChartData;
            }

            // used to bind chart data to controller
            function bindDonutChartData() {
                return fakeDonutChartData;
            }
        }
    ]);

})();
