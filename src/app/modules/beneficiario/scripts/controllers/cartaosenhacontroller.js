(function () {
    'use strict';

    angular.module('portalUsuarioApp').controllerLazy('CartaoSenhaController', [
        'CartaoSenhaFactory', 'AutoModalsFactory', '$scope', '$rootScope', '$window', 'CriptoFactory', 'AutenticacaoService', 'CartaoSenhaService', 'CartaoService',
        function (CartaoSenhaFactory, AutoModalsFactory, $scope, $rootScope, $window, CriptoFactory, AutenticacaoService, CartaoSenhaService, CartaoService) {


            CartaoSenhaFactory.resetCartaoSenhaFormData();

            var senha = this;
            senha.mensagemShow = false;
            senha.mensagem = "";
            senha.promise = null;
            senha.post = {
                IdCartao: 0,
                Nome: "",
                Matricula: "",
                Cpf: "",
                DataNasc: "",
                TipoOperacaoAutoServico: 5,
                Guid: "",
                Chave: "",
                OrigemWeb: true //Valor padrão
            };
            

            senha.step = 0;
            senha.toToken = toToken;
            $scope.toNewPwd = toNewPwd;
            $scope.toSuccess = toSuccess;
            senha.formData = CartaoSenhaFactory.bindCartaoSenhaFormData();
            senha.submit = CartaoSenhaFactory.submitCartaoSenhaFormData;
            //senha.close = AutoModalsFactory.closeChangePwd;
            senha.campo = 'atual';

            senha.senhasDeVerdade = {
                atual: {
                    deVerdade: "",
                    deMentira: ""
                },
                nova: {
                    deVerdade: "",
                    deMentira: ""
                },
                confirma: {
                    deVerdade: "",
                    deMentira: ""
                }
            };

            var usuario = AutenticacaoService.obterUsuarioLogado();

            senha.idUsuario = usuario.id;
            senha.emailUsuario = usuario.email;

            //JQUERY/////////////////////////////////////////////

            var scope = angular.element($("#senha")).scope();                        

            //inicializa o teclado virtual
            $window.jQuery('#inlineKeypad').keypad(
                {
                    onKeypress: function (key, value) {

                        var scope = angular.element($("#senha")).scope();            
                        scope.$apply(scope.teclado(key));

                        value = "";

                    },
                    randomiseNumeric: true
                });

            ///////////////////////////////////////////////////
            senha.pageLoad = function () {
                
                var detalheCartao = null;
                if ($scope.detalheCartao) {
                    detalheCartao = $scope.detalheCartao;
                }
                else {
                    detalheCartao = CartaoSenhaFactory.getCartao();
                }
                if (detalheCartao) {

                    var balance = detalheCartao;//response.value.balance;

                    senha.cartao = {
                        id: balance.id,
                        number: balance.number,
                        name: 'card' + balance.id,
                        bin: balance.bin,
                        pushCode: balance.bin,
                        cssCartao: "",
                        a_cssEstrela: "",
                        i_cssEstrela: "",
                        cardLast: "",
                        apelido: balance.apelido,
                        nickName: balance.apelido,
                        favorito: balance.favorito,
                        situacao: balance.situacao,
                        validado: balance.validado,
                        value: balance.value,
                        OrigemWeb: true //Valor padrão
                    };
                   
                }
            };

            function S4() {
                return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
            }
            function toToken() {
                
                senha.step = 1;
            }
            function toNewPwd() {

                if (!senha.formData.remember)
                    senha.campo = 'nova';

                senha.step = 2;
            }
            function toSuccess() {
                
                //verifica se o CVT está sendo exibido, se estiver, verifica se está preenchido
                if (!senha.formData.remember) {
                    senha.post.TipoOperacaoAutoServico = 6;

                    if (!senha.formData.cardCVT) {
                        senha.mensagem = "Código verificador é um campo obrigatório.";
                        senha.mensagemShow = true;
                        return false;
                    }
                    else if (senha.formData.cardCVT.length < 3) {
                        senha.mensagem = "O campo código verificador deve conter 3 caracteres";
                        senha.mensagemShow = true;
                        return false;
                    }

                }

                senha.mensagemShow = false;

                if(senha.senhasDeVerdade.nova.deVerdade !== senha.senhasDeVerdade.confirma.deVerdade){
                    senha.mensagem = "Dados Inválidos. Insira os dados novamente";
                    senha.mensagemShow = true;
                    senha.formData.cardCVT = "";
                    senha.toClear('atual');
                    senha.toClear('nova');
                    senha.toClear('confirma');
                    return false;
                }

                senha.post.IdCartao = senha.cartao.id;

                senha.post.DataNasc = senha.formData.birthDateMonth + "/" +
                    senha.formData.birthDateDay + "/" +
                    senha.formData.birthDateYear;

                var numerocartao = senha.cartao.number;
                senha.promise = CartaoSenhaService.getKeyIvCript(senha.idUsuario);
                var senhaAtual = "";
                var novasenha = "";
                var confirmacaonovasenha = "";
                angular.forEach(senha.senhasDeVerdade.atual.deVerdade.split(";"), function (value, key) {
                    senhaAtual += CriptoFactory.descriptografar(value, config.tokenTecladoVirtual, config.ivTecladoVirtual);
                });
                angular.forEach(senha.senhasDeVerdade.nova.deVerdade.split(";"), function (value, key) {
                    novasenha += CriptoFactory.descriptografar(value, config.tokenTecladoVirtual, config.ivTecladoVirtual);
                });
                angular.forEach(senha.senhasDeVerdade.confirma.deVerdade.split(";"), function (value, key) {
                    confirmacaonovasenha += CriptoFactory.descriptografar(value, config.tokenTecladoVirtual, config.ivTecladoVirtual);
                });

                var cipheredcvt = "";
                var cipheredsenhaatual = "";
                var cipherednovasenha = "";
                var cipheredconfirmacaonovasenha = "";

                senha.promise.then(function (response) {

                    var cipherednumerocartao = CriptoFactory.criptografar(numerocartao, response.key, response.iv);

                    angular.forEach(senha.formData.cardCVT.split(''), function (value, key) {
                        cipheredcvt += CriptoFactory.criptografar(value, response.key, response.iv) + ";";
                    });

                    angular.forEach(senhaAtual.split(''), function (value, key) {
                        cipheredsenhaatual += CriptoFactory.criptografar(value, response.key, response.iv) + ";";
                    });
                    angular.forEach(novasenha.split(''), function (value, key) {
                        cipherednovasenha += CriptoFactory.criptografar(value, response.key, response.iv) + ";";
                    });
                    angular.forEach(confirmacaonovasenha.split(''), function (value, key) {
                        cipheredconfirmacaonovasenha += CriptoFactory.criptografar(value, response.key, response.iv) + ";";
                    });

                    var json = null;
                    if (senha.formData.remember) {
                        json = {
                            "numero_cartao": cipherednumerocartao,
                            "senha_atual": CriptoFactory.criptografar(cipheredsenhaatual, response.key, response.iv),
                            "nova_senha": CriptoFactory.criptografar(cipherednovasenha, response.key, response.iv),
                            "confirmacao_nova_senha": CriptoFactory.criptografar(cipheredconfirmacaonovasenha, response.key, response.iv)
                        };
                    }
                    else {
                        json = {
                            "numero_cartao": cipherednumerocartao,
                            "cvt": CriptoFactory.criptografar(cipheredcvt, response.key, response.iv),
                            "nova_senha": CriptoFactory.criptografar(cipherednovasenha, response.key, response.iv),
                            "confirmacao_nova_senha": CriptoFactory.criptografar(cipheredconfirmacaonovasenha, response.key, response.iv)
                        };
                    }

                    
                    var finalcipheredtext = CriptoFactory.criptografar(JSON.stringify(json), response.key, response.iv);
                    
                    senha.post.Guid = response.guid;
                    senha.post.Chave = finalcipheredtext;

                    senha.promise = CartaoService.autoServicoCartao(senha.idUsuario, senha.post);

                    senha.promise.then(function (response1) {
                        
                        senha.protocolo = response1.value.protocolo;
                        senha.step = 3;
                        console.log(response1);

                    }, function (error1) {
                        
                        if (error1.status == 400) {

                            try {

                                var parse = JSON.parse(error1.data.message);

                                 if (parse.message) {
                                    senha.mensagem = parse.message;
                                }
                                else{
                                    senha.step = 4;
                                }

                            } catch (e) {

                                senha.mensagem = error1.data.message.replace(/"/g, '');
                            }

                            senha.formData.cardCVT = "";
                            senha.toClear('atual');
                            senha.toClear('nova');
                            senha.toClear('confirma');

                            senha.mensagemShow = true;

                        }
                        else if (error1.status == 500) {
                            senha.step = 4;
                        }

                        console.log(error1);

                    });
                }, function (error) {
                    
                    if (error.status == 400) {
                        senha.mensagem = error.data.message.replace(/"/g, '');
                        senha.mensagemShow = true;
                    }
                    else if (error.status == 500) {
                        senha.step = 4;
                    }
                });
            }

            $scope.teclado = function (key) {

                
                senha.senhasDeVerdade[senha.campo].deVerdade += CriptoFactory.criptografar(key, config.tokenTecladoVirtual, config.ivTecladoVirtual) + ";";
                senha.senhasDeVerdade[senha.campo].deMentira = senha.senhasDeVerdade[senha.campo].deMentira + "*";

                if (senha.campo == 'atual') {
                    senha.formData.currentPwd = senha.senhasDeVerdade[senha.campo].deMentira;
                }
                else if (senha.campo == 'nova') {
                    senha.formData.newPwd = senha.senhasDeVerdade[senha.campo].deMentira;
                }
                else if (senha.campo == 'confirma') {
                    senha.formData.confirmNewPwd = senha.senhasDeVerdade[senha.campo].deMentira;
                }

            };

            senha.focusCampo = function (campo) {
                
                senha.campo = campo;
            };

            senha.toClear = function (campo) {

                if (campo == 'atual') {

                    senha.senhasDeVerdade.atual.deVerdade = "";
                    senha.senhasDeVerdade.atual.deMentira = "";
                    senha.formData.currentPwd = "";

                }
                else if (campo == 'nova') {
                    senha.senhasDeVerdade.nova.deVerdade = "";
                    senha.senhasDeVerdade.nova.deMentira = "";
                    senha.formData.newPwd = "";

                }
                else if (campo == 'confirma') {
                    senha.senhasDeVerdade.confirma.deVerdade = "";
                    senha.senhasDeVerdade.confirma.deMentira = "";
                    senha.formData.confirmNewPwd = "";
                }

                senha.focusCampo(campo);

            };

            senha.close = function () {
                $scope.modal.dismiss();
            };


            senha.pageLoad();

        }
    ]);

})();
