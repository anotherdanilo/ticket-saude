(function () {
    'use strict';

    angular.module('portalUsuarioApp').controllerLazy('AdicionarController', [
        'AutoModalsFactory', 'CartaoService', '$scope', 'AutenticacaoService', 'AlertaFactory', '$window', 'AccentivService', 'AccentivFactory',
        function (AutoModalsFactory, CartaoService, $scope, AutenticacaoService, AlertaFactory, $window, AccentivService, AccentivFactory) {

            var adicionar = this;

            adicionar.myPromise = null;

            if ($scope.$parent.$parent) {
                adicionar.parentDashboard = $scope.$parent.$parent.dashboard;
                adicionar.parent = $scope.$parent.$parent.editModalInstance;
            }

            adicionar.step = 0;
            adicionar.cardtype = '';
            adicionar.setType = setType;
            adicionar.isSelected = isSelected;
            adicionar.selectOpen = true;
            adicionar.cartao = null;
            adicionar.cardLast = "0000";

            adicionar.validacaoAccentiv = null;

            var usuario = AutenticacaoService.obterUsuarioLogado();
            adicionar.idUsuario = usuario.id;

            adicionar.erro = false;
            adicionar.msgerro = "";

            AccentivFactory.reset();

            adicionar.cadastrarCartao = function () {

                if (adicionar.isSelected('tt')) {
                    adicionar.cartao.Number = adicionar.cartao.CPF;
                }

                //verifica se o cartão selecionado é o cartão da accentiv, 
                //se sim, consulta a api da accentiv antes de prcessguir

                if (adicionar.isSelected('dc') || adicionar.isSelected('pp')) {
                    var portadorAccentiv;
                    
                    adicionar.myPromise = AccentivService.obterCartao(adicionar.cartao.Number.replace(/ /g, ''));

                    adicionar.myPromise.then(

                        function (response) {

                            if (response.IdentificacaoPortadorResult) {
                                
                                portadorAccentiv = response.IdentificacaoPortadorResult;

                                adicionar.validacaoAccentiv = AccentivFactory.existePersonalizacaoId(portadorAccentiv.PersonalizaoId);

                                if (adicionar.validacaoAccentiv.sucesso) {

                                    adicionar.cartao.personalizacaoId = portadorAccentiv.PersonalizaoId;

                                    cadastrar();
                                } else {

                                    adicionar.step = 3;
                                    adicionar.erro = true;
                                    adicionar.msgerro = "Esse tipo de cartão não pode ser cadastrado.";

                                }

                            }

                        },

                        function (error) {
                            
                            if(error.status == 404 && error.statusText == "Not Found")
                            {
                                switch (adicionar.cardtype){
                                    case 'dc':
                                        adicionar.msgerro = "Este cartão não é um Duo Card. Por favor selecione o cartão correto.";
                                    break;
                                    case 'pp':
                                        adicionar.msgerro = "Este cartão não é um Presente Perfeito. Por favor selecione o cartão correto.";
                                    break;
                                    default:
                                        adicionar.msgerro = "Cartão não encontrado.";
                                }
                                adicionar.step = 3;
                                adicionar.erro = true;
                                
                            }

                            console.log(error);
                        });
                }
                else {

                    cadastrar();
                }

            };

            function cadastrar() {

                var numeroCartao = adicionar.cartao.Number.replace(/ /g, '');
                var inicioCartao = numeroCartao.substring(0, 6);

                if(inicioCartao == "308512")
                {
                    adicionar.step = 3;
                    adicionar.msgerro = "Para consulta de saldo dos cartões Ticket Log, baixe o app Ticket Log ou acesse “Meu Saldo” em <a href='https://www.ticketlog.com.br' target='_blank'><u>www.ticketlog.com.br</u></a>";
                    adicionar.erro = true;
                    return false;
                }

                var length = adicionar.cartao.Number.length;

                adicionar.cardLast = adicionar.cartao.Number.substring(length - 4);

                
                adicionar.myPromise = CartaoService.cadastrarCartao(adicionar);

                adicionar.myPromise.then(function (responsePost) {

                    
                    if (responsePost.success === true) {

                        var paramGet = responsePost;
                        adicionar.myPromise = CartaoService.obterCartaoPorIdService(paramGet.value.id);
                        adicionar.myPromise.then(function (response) {

                            //chama o serviço externo da AccentivService

                            if (response.success === true) {
                                var cssCartao = "card-ticket ";
                                var length = response.value.balance.number.length;

                                adicionar.cartao.cardLast = response.value.balance.number.substring(length - 4);
                                adicionar.cartao.NickName = response.value.balance.nickName;
                                adicionar.cartao.value = response.value.balance.valueParsed === null ? 0 : response.value.balance.valueParsed;

                                if (adicionar.validacaoAccentiv) {
                                    cssCartao += adicionar.validacaoAccentiv.css;
                                }
                                else {
                                    switch (response.value.balance.bin) {
                                        case "TRE":
                                            cssCartao += "-restaurante";
                                            break;
                                        case "TAE":
                                            cssCartao += "-alimentacao";
                                            break;
                                        case "TKE":
                                            cssCartao += "-cultura";
                                            break;
                                        case "TC":
                                            cssCartao += "-car";
                                            break;
                                        case "TSE":
                                            cssCartao += "-saude";
                                            break;
                                        default:
                                            cssCartao += "-transporte";
                                    }
                                }

                                adicionar.cartao.cssCartao = cssCartao;
                                adicionar.step = 1;

                                $scope.carregarCartoes();

                                //adicionar.parentDashboard.blank = false;
                            }
                            else {
                                throw response.message;
                            }

                        })["catch"](function (error) {

                            adicionar.step = 3;
                            adicionar.msgerro = "Não foi possível adicionar este cartão. Por favor tente novamente mais tarde.";
                            console.log(error);

                        });

                    }
                    else {

                        throw responsePost.message;
                    }

                })["catch"](function (error) {
                    adicionar.step = 3;
                    adicionar.msgerro = "Não foi possível adicionar este cartão. Por favor tente novamente mais tarde.";
                    console.log(error);
                });


            }

            function setType(card) {
                if (adicionar.selectOpen) {
                    adicionar.cardtype = card;
                    adicionar.selectOpen = false;
                }
                else {
                    adicionar.cardtype = '';
                    adicionar.selectOpen = true;
                }
            }

            function isSelected(card) {
                return adicionar.cardtype === card;
            }

            adicionar.Success = function () {
                adicionar.parentDashboard.carregarCartoes();
                adicionar.close();
            };

            adicionar.close = function () {
                adicionar.parent.dismiss();
            };

            adicionar.favoritarCartao = function () {

                var cartao = {
                    number: adicionar.cartao.Number.replace(/ /g, '')
                };

                adicionar.parentDashboard.favoritarCartao(cartao);

                adicionar.close();


            };

            adicionar.openAlerta = AlertaFactory.openAlerta;

            adicionar.changeInput = function () {

                if (adicionar.cartao.Number) {
                    var bin = adicionar.cartao.Number.replace(" ", "").slice(0, 6);

                    if (bin === "603574" || bin === "605674" || bin === "605681") {

                        adicionar.cartao.Number = "";

                        AlertaFactory.parametros.titulo = "Cartão Ticket Plus";
                        AlertaFactory.parametros.mensagem = "Você está digitando um cartão Ticket Plus, para consultar o limite do seu cartão clique no botão abaixo.";
                        AlertaFactory.parametros.icone = "icon-done";
                        // AlertaFactory.parametros.callback = callback;

                        adicionar.openAlerta("Ver limite Ticket Plus", callback);

                    }

                }
            };

            function callback() {
                $window.open(config.urls.ticketPlus);
            }

        }
    ]);

})();
