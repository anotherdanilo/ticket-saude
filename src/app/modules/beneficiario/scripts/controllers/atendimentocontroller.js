(function() {
    'use strict';

    angular.module('portalUsuarioApp').controllerLazy('AtendimentoController', [
        'HomeFactory','AutenticacaoService', 'AtendimentoModalsFactory', '$location',
        '$rootScope', 'prebidService','$browser',
        function(HomeFactory, AutenticacaoService, AtendimentoModalsFactory, $location,
            $rootScope, prebidService, $browser) {

            prebidService.getBeneficiarioAds();

            var usuario = AutenticacaoService.obterUsuarioLogado();
            if (usuario.isLogged === false) {
                $location.path($browser.baseHref());
            }

            $rootScope.menuSelecionado = 3;

            var atendimento = this;

            atendimento.openSelect = AtendimentoModalsFactory.openSelect;
            atendimento.openTel = AtendimentoModalsFactory.openTel;
            atendimento.openMsg = AtendimentoModalsFactory.openMsg;
            atendimento.toHelp = toHelp;

            function toHelp() {
                $location.path('/beneficiario/ajuda');
            }
            
            atendimento.aplicativo = function(){
                HomeFactory.parametros.baixaApp = true;
                $rootScope.menuSelecionado = 0;
                $location.path($browser.baseHref());  
            };
        }
    ]);

})();
