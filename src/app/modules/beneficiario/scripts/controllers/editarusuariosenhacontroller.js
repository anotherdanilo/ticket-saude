(function() {
    'use strict';

    angular.module('portalUsuarioApp').controllerLazy('EditarUsuarioSenhaController', [
        'UsuarioService', '$rootScope', 'UsuarioModalsFactory', 'AutenticacaoService',
        function(UsuarioService, $rootScope, UsuarioModalsFactory, AutenticacaoService) {
            var senha = this;

            var usuarioAutenticado = AutenticacaoService.obterUsuarioLogado();

            senha.step = 0;
            senha.toSuccess = toSuccess;
            senha.toError = toError;
            senha.close = UsuarioModalsFactory.closeChangePwd;
            senha.myPromise = null;

            function toSuccess() {
                if (senha.formData.newPwd !== senha.formData.confirmPwd) {
                    senha.error = true;
                    senha.mensagem = "Nova senha e confirmação de senha não conferem! Por favor, digite as senhas novamente.";
                    limparCampos();
                    return false;
                }

                var id = usuarioAutenticado.id;
                var senhaAtual = senha.formData.currentPwd;
                var novaSenha = senha.formData.newPwd;

                var model = {
                    Id: id,
                    Password: senhaAtual,
                    NewPassword: novaSenha
                };
                
                senha.myPromise = UsuarioService.alterarCadastroUsuario(model);
                senha.myPromise.then(function(retorno) {
                    senha.step = 1;
                    $rootScope.bodyClass = "";
                }, function(error) {
                    senha.error = true;
                    limparCampos();
                    senha.mensagem = error;
                    $rootScope.bodyClass = "";
                });
            }
            function toError() {
                senha.step = 2;
            }

            function limparCampos() {
                senha.formData.currentPwd = "";
                senha.formData.newPwd = "";
                senha.formData.confirmPwd = "";
            }

        }
    ]);

})();
