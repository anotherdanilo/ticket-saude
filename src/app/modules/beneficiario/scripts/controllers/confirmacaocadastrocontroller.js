(function() {
  'use strict';

  angular.module('portalUsuarioApp').controllerLazy('ConfirmacaoCadastroController', [
    'CadastroFactory',
    function(CadastroFactory) {
      var confirmacaoCadastro = this;

      confirmacaoCadastro.step = 0;
      confirmacaoCadastro.changeEmail = false;
      confirmacaoCadastro.toChangeEmail = toChangeEmail;
      confirmacaoCadastro.changeEmailBtn = 'Alterar endereço de e-mail';
      confirmacaoCadastro.sendEmailBtn = 'Reenviar';
      confirmacaoCadastro.toEmailConfirm = toEmailConfirm;
      confirmacaoCadastro.getSentEmail = CadastroFactory.getSentEmail();
      confirmacaoCadastro.closeModal = CadastroFactory.closeModal;

      function toEmailConfirm() {
        confirmacaoCadastro.step = 1;
      }
      function toChangeEmail() {
        if (!confirmacaoCadastro.changeEmail) {
          confirmacaoCadastro.changeEmailBtn = 'Cancelar alteração';
          confirmacaoCadastro.sendEmailBtn = 'Salvar alteração e Reenviar';
        }
        else {
          confirmacaoCadastro.changeEmailBtn = 'Alterar endereço de e-mail';
          confirmacaoCadastro.sendEmailBtn = 'Reenviar';
        }

        confirmacaoCadastro.changeEmail = !confirmacaoCadastro.changeEmail;
      }
    }
  ]);

})();
