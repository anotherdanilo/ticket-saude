(function () {
    'use strict';

    angular.module('portalUsuarioApp').controllerLazy('IndicarParceiroController', [
      'BeneficioModalsFactory', 'UsuarioService', 'AutenticacaoService',
      function (BeneficioModalsFactory, UsuarioService, AutenticacaoService) {
          var parceiro = this;

          parceiro.indicar = true;
          parceiro.step = 0;
          parceiro.toMotive = toMotive;
          parceiro.toSuccess = toSuccess;
          parceiro.close = BeneficioModalsFactory.closePartnerForm;
          parceiro.toConfirm = toConfirm;

          parceiro.promise = null;

          var usuario = AutenticacaoService.obterUsuarioLogado();
          parceiro.idUsuario = usuario.id;
          parceiro.indicar = !usuario.isLogged;
          
          parceiro.dados = {
              Id: parceiro.idUsuario,
              NomeParceiro: "",
              EmailParceiro:"",
              Telefone: "",
              SiteParceiro: "",
              Motivo: "",
              Endereco: {
                  Logradouro: "",
                  Numero: "",
                  Complemento: "",
                  Bairro: "",
                  Cidade: "",
                  Uf: "",
                  Cep: ""
              },
              NomeIndicador: "",
              EmailIndicador: ""
          };

          function toMotive() {

              parceiro.step = 1;
          }
          function toSuccess() {

              parceiro.step = 2;
          }

          function toConfirm() {
            
            if(usuario.isLogged)
            {
                parceiro.dados.NomeIndicador = usuario.nome;
                parceiro.dados.EmailIndicador = usuario.email;
            }
              parceiro.promise = UsuarioService.indicarParceiro(parceiro.dados, parceiro.idUsuario);

              parceiro.promise.then(function (response) {

                  parceiro.step = 2;

              }, function (error) {

                    parceiro.step = 3;

              });

          }

          parceiro.obterCep = function () {

              parceiro.myPromise = UsuarioService.obterPorCep(parceiro.dados.Endereco.Cep);

              parceiro.myPromise.then(function (response) {

                  var enderecoRetornado = response.value;
                  if (enderecoRetornado !== undefined && enderecoRetornado !== null) {
                      parceiro.dados.Endereco = {};
                      parceiro.dados.Endereco = {
                          Cep: enderecoRetornado.cep,
                          Logradouro: enderecoRetornado.logradouro,
                          Bairro: enderecoRetornado.bairro,
                          Cidade: enderecoRetornado.cidade,
                          Uf: enderecoRetornado.uf
                      };

                      $('#txtnumero').focus();
                  }

              }, function (error) {

                  parceiro.dados.Endereco.Logradouro = "";
                  parceiro.dados.Endereco.Bairro = "";
                  parceiro.dados.Endereco.Cidade = "";
                  parceiro.dados.Endereco.Uf = "";
                  parceiro.dados.Endereco.Complemento = "";

              });

          };
      }
    ]);

})();
