(function() {
    'use strict';
    angular.module('portalUsuarioApp').controllerLazy('ReemissaoController', [
        'AutoModalsFactory', 'ReemissaoFactory', 'CartaoService', '$scope', '$http', 'AutenticacaoService',
        function(AutoModalsFactory, ReemissaoFactory, CartaoService, $scope, $http, AutenticacaoService) {
            var self = this;

            self.step = 0;

            self.mensagemShow = false;
            self.mensagem = "Ops! Ocorreu um erro ao tentar reemitir este cartão, por favor, tente novamente mais tarde!";
        self.post = {
                IdCartao: 0,
                Nome: "",
                Matricula: "",
                Cpf: "",
                DataNasc: "",
                TipoOperacaoAutoServico: 0,
                MotivoOperacaoReemissao: 0,
                OrigemWeb: true //Valor padrão
            };

            var usuario = AutenticacaoService.obterUsuarioLogado();
            self.idUsuario = usuario.id;

            self.textoConfirmacaoReemissao = "Deseja reemitir seu cartão?";

            self.protocolo = 0;

            self.promise = null;

            self.pageLoad = function() {
                var detalheCartao = null;
                if ($scope.detalheCartao) {
                    detalheCartao = $scope.detalheCartao;
                }
                else {
                    detalheCartao = ReemissaoFactory.getCartao();
                }
                
                if (detalheCartao) {
                    var balance = detalheCartao;//response.value.balance;

                    self.cartao = {
                        id: balance.id,
                        number: balance.number,
                        name: 'card' + balance.id,
                        bin: balance.bin,
                        pushCode: balance.bin,
                        cssCartao: "",
                        a_cssEstrela: "",
                        i_cssEstrela: "",
                        cardLast: "",
                        apelido: balance.apelido,
                        nickName: balance.apelido,
                        favorito: balance.favorito,
                        situacao: balance.situacao,
                        validado: balance.validado,
                        value: balance.value,
                        OrigemWeb: true //Valor padrão
                    };

                    if (balance.validado) {
                        self.textoConfirmacaoReemissao = "Deseja reemitir este cartão?";
                    }
                }
            };

            self.reemitirCartao = function() {

                self.post.IdCartao = self.cartao.id;
                self.post.DataNasc = self.formData.birthDateMonth + "/" +
                    self.formData.birthDateDay + "/" +
                    self.formData.birthDateYear;
                self.post.TipoOperacaoAutoServico = 3; //3 = Reemissao
                self.post.MotivoOperacaoReemissao = self.formData.motive;

                self.promise = CartaoService.autoServicoCartao(self.idUsuario, self.post);

                self.promise.then(function (response) {

                    self.protocolo = response.value.protocolo;
                    self.step = 2;

                }, function(error) {

                    self.step = 3;

                    if (error.status == 400) {
                        self.mensagem = error.data.message.replace(/"/g, '');
                    }

                });

            };


            self.toConfirm = toConfirm;
            self.toSuccess = toSuccess;
            self.formData = ReemissaoFactory.bindReissueFormData();
            self.submit = ReemissaoFactory.submitReissueFormData;

            self.close = function() {
                if ($scope.modal) {
                    $scope.modal.dismiss();
                }
                else {
                    AutoModalsFactory.closeReissue();
                }
            };

            function toConfirm() {
                self.step = 1;
            }
            function toSuccess() {
                self.step = 2;
            }

            self.favoritarCartao = function() {
                $scope.favoritarCartao(self.cartao);
                $scope.modal.dismiss();
            };

            self.pageLoad();
        }
    ]);

})();
