(function() {
  'use strict';

  angular.module('portalUsuarioApp').controllerLazy('AtendimentoContatoController', [
    'AtendimentoModalsFactory', '$rootScope',
    function(AtendimentoModalsFactory, $rootScope) {
      
      var contato = this;



      contato.step = 0;
      contato.toSuccess = toSuccess;
      contato.toError = toError;
      contato.closeTel = AtendimentoModalsFactory.closeTel;
      contato.closeMsg = AtendimentoModalsFactory.closeMsg;

      function toSuccess() {
        contato.step = 1;
      }
      function toError() {
        contato.step = 2;
      }
    }
  ]);

})();
