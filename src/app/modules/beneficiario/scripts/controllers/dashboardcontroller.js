(function () {
    'use strict';

    angular.module('portalUsuarioApp').controllerLazy('DashboardController', [
        '$filter',
        '$rootElement',
        '$document',
        '$timeout',
        '$scope',
        'AutoModalsFactory',
        '$uibModal',
        'CartaoService',
        'CartaoFactory',
        'AutenticacaoService',
        '$rootScope',
        '$location',
        '$window',
        'AccentivService',
        'AccentivFactory',
        'prebidService',
        '$browser',
        function ($filter, $rootElement, $document, $timeout, $scope, AutoModalsFactory,
            $uibModal, CartaoService, CartaoFactory, AutenticacaoService, $rootScope, $location,
            $window, AccentivService, AccentivFactory, prebidService, $browser) {

            $scope.$emit('newPageLoaded', {
                title: "Meus Cartões",
                description: "Bem-vindo ao portal Sou Ticket.",
                path: $location.absUrl(),
                imageurl: ''
            });

            $rootScope.menuSelecionado = 0;

            moment.locale("pt-br");

            var dashboard = this;
            dashboard.senhaDeVerdade = "";
            dashboard.senhaMentirosa = "";

            dashboard.myPromise = null;
            dashboard.isNotKarma = true;

            var usuario = AutenticacaoService.obterUsuarioLogado();
            if (usuario.isLogged === false) {
                $location.path($browser.baseHref());
            }

            dashboard.idUsuario = usuario.id;
            dashboard.listaCartoes = [];
            dashboard.content = "";
            dashboard.injectedObject = {};
            dashboard.nomeUsuarioLogado = usuario.nome;
            // if (usuario.nome !== undefined) {
            //     dashboard.nomeUsuarioLogado = usuario.nome.split(' ')[0];
            //  }


            dashboard.blank = true;
            //   dashboard.getContent()
            //   {
            //   }

            if (!$rootScope.jaAcessouADashboard) {
                dashboard.descricaoDashboard = "Bem-vindo ao portal Sou Ticket";
                $rootScope.jaAcessouADashboard = true;
            }
            else {
                dashboard.descricaoDashboard = "Bem-vindo de volta!";
            }

            dashboard.setContent = function (valor) {
                dashboard.content = valor;
            };

            dashboard.FormataData = function (data) {
                var ano, dataFormatada, dia, mes, splitData;
                splitData = data.split('/');
                dia = splitData[0];
                mes = splitData[1] - 1;
                ano = splitData[2];
                dataFormatada = new Date(ano, mes, dia);
                return dataFormatada;
            };

            $scope.formatDateExtrato = function (date) {
                return moment(date, "DD/MM/YYYY").format("DD/MM");
            };

            dashboard.carregarCartoes = function () {

                debugger;
                prebidService.getBeneficiarioAds();

                if (!$rootScope.jaAcessouADashboard) {
                    $rootScope.jaAcessouADashboard = true;
                }

                $window.scrollTo(0, 0);
                $(".loading-mask-position").css("top", document.body.scrollTop);
                $rootScope.bodyClass = "menu-opened";

                dashboard.blank = true;

                dashboard.listaCartoes = [];

                dashboard.myPromise = CartaoService.carregarCartoesPorUsuarioService(dashboard.idUsuario);

                dashboard.myPromise.then(function (response) {
                    if (response.success) {

                        console.log(response);
                        var balances = response.value;

                        var i = 0;

                        angular.forEach(balances, function (value, key) {

                            var balance = value.balance;

                            if (balance.bin === "TT") {
                                angular.forEach(value.ttCard, function (cartaott, key) {

                                    dashboard.listaCartoes.push({
                                        id: balance.id,
                                        cpf: balance.number,
                                        number: cartaott.numero,
                                        name: balance.id,
                                        apelido: balance.nickName,
                                        favorito: balance.favorito,
                                        situacao: balance.situacao,
                                        bin: balance.bin,
                                        diaProximoCredito: null,
                                        validado: balance.validado,
                                        mesProximoCredito: null,
                                        valorProximoCredito: null,
                                        extrato: value.scheduling,
                                        deposit: balance.deposit,
                                        value: (balance.valueParsed === null ? 0 : balance.valueParsed),
                                        mediaPorDia: balance.dailyAverage,
                                        beneficiario: cartaott.beneficiario,
                                        operadora: cartaott.operadora
                                    });
                                });

                            }
                            else if (balance.bin == "ACT") {

                                dashboard.listaCartoes.push({

                                    id: balance.id,
                                    number: balance.number,
                                    name: balance.id,
                                    apelido: balance.nickName,
                                    favorito: balance.favorito,
                                    bin: balance.bin,
                                    classExtrato: "card-details -carregando",
                                    validado: true,
                                    accentiv: true,
                                    carregando: true
                                });

                            }
                            else {
                                var month = "-";
                                var day = "-";
                                if (balance.dateNextDeposit !== null) {
                                    month = moment(balance.dateNextDeposit, "DD/MM/YYYY").format("MMM");
                                    day = moment(balance.dateNextDeposit, "DD/MM/YYYY").format("DD");
                                }

                                var valorProximoCredito = "-";
                                if (JSLINQ(value.scheduling).First() !== null) {
                                    valorProximoCredito = JSLINQ(value.scheduling).First().value;
                                }

                                dashboard.listaCartoes.push({
                                    id: balance.id,
                                    number: balance.number,
                                    name: balance.id,
                                    apelido: balance.nickName,
                                    favorito: balance.favorito,
                                    situacao: balance.situacao,
                                    bin: balance.bin,
                                    diaProximoCredito: day,
                                    validado: balance.validado,
                                    mesProximoCredito: month.toUpperCase(),
                                    valorProximoCredito: valorProximoCredito,
                                    extrato: [],
                                    deposit: balance.deposit,
                                    value: (balance.valueParsed === null ? 0 : balance.valueParsed),
                                    mediaPorDia: balance.dailyAverage
                                });

                            }

                            i++;

                        });

                        carregarCartoesAccentiv();

                        dashboard.blank = false;
                        
                        if (CartaoService.IdCartaoSelecionado === null && dashboard.listaCartoes.length > 0) {
                            dashboard.setCard(dashboard.listaCartoes[0].id);
                        } else {
                            dashboard.setCard(CartaoService.IdCartaoSelecionado);
                        }

                        //$scope.listaCartoes = dashboard.listaCartoes;
                        $rootScope.bodyClass = "";
                        dashboard.myPromise = null;
                    }

                }, function (response) {

                    $rootScope.bodyClass = "";
                    dashboard.myPromise = null;
                });

            };

            dashboard.formatarCartaoSaude = function(numeroCartao){
                var parte1, parte2, parte3, parte4;

                parte1 = numeroCartao.substring(0, 4);
                parte2 = numeroCartao.substring(4, 8);
                parte3 = numeroCartao.substring(8, 12);
                parte4 = numeroCartao.substring(12);

                return parte1 + " " + parte2 + " " + parte3 + " " + parte4;
            }

            function carregarCartoesAccentiv() {

                angular.forEach(dashboard.listaCartoes, function (value, key) {

                    var balance = value;

                    if (balance.bin === 'ACT') {

                        dashboard.myPromiseAccentiv = AccentivService.obterCartao(balance.number);

                        dashboard.myPromiseAccentiv.then(function (response) {

                            var dados = response.IdentificacaoPortadorResult;

                            //busca o status do cartão
                            switch (dados.CodigoRetornoProcessadora) {
                                case 67:
                                    dados.situacao = 4;
                                    break;
                                case 20:
                                    dados.situacao = 3;
                                    break;
                                case 2:
                                    dados.situacao = 5;
                                    break;
                                default:
                                    dados.situacao = 1;
                            }

                            var classExtrato = "card-details ";
                            
                            classExtrato += AccentivFactory.existePersonalizacaoId(dados.PersonalizaoId).css;

                            dashboard.myPromiseExtrato = AccentivService.extratoCartao(balance.number);

                            var depositado = 0;
                            dashboard.myPromiseExtrato.then(function (extrato) {
                            
                                //trativa do extrato
                                var novoExtrato = [];
                                
                                if (extrato.ExtratoLinhas && extrato.ExtratoLinhas.ExtratoLinha) {

                                    angular.forEach(extrato.ExtratoLinhas.ExtratoLinha, function (extratolinha, key) {

                                        var dateParsed = moment(extratolinha.Data, 'DD-MM-YYYY').format("YYYY-MM-DD");

                                        var valueParsed = extratolinha.Valor.replace('.', '@').replace(',', '.').replace('@', ',');

                                        var descricao = extratolinha.TipoTransacao;
                                        if (extratolinha.Estabelecimento) {
                                            descricao += ' ' + extratolinha.Estabelecimento;
                                        }

                                        if (descricao.toString().indexOf('CREDIT VOUCHER') > -1) {
                                            depositado += parseFloat(valueParsed);
                                        }

                                        novoExtrato.push({
                                            date: extratolinha.Data,
                                            dateParsed: dateParsed,
                                            value: extratolinha.Valor,
                                            valueParsed: parseFloat(valueParsed),
                                            description: descricao,
                                            deposit: depositado,
                                        });

                                    });

                                }

                                value.situacao = dados.situacao;
                                value.extrato = novoExtrato;
                                value.value = dados.SaldoCartao;
                                value.classExtrato = classExtrato;
                                value.personalizacaoId = dados.PersonalizaoId;
                                value.carregando = false;
                                value.deposit = depositado;


                            }, function (errorExtrato) {

                            });

                        }, function (response) {

                            $rootScope.bodyClass = "";
                            dashboard.myPromise = null;
                        });
                    }

                });


            }

            $scope.carregarCartoes = dashboard.carregarCartoes;

            dashboard.setCard = setCard;
            dashboard.isActive = isActive;
            dashboard.filtrarPeriodoExtrato = filtrarPeriodoExtrato;
            dashboard.checkActive = checkActive;
            dashboard.openEdit = AutoModalsFactory.openEdit;
            dashboard.openChangePwd = AutoModalsFactory.openChangePwd;
            dashboard.modelDateFrom = "04/01/2016";
            dashboard.modelDateTo = "04/30/2016";
            dashboard.extratoFiltrado = null;

            function setCard(card) {

                $window.scrollTo(0, 0);
                $(".loading-mask-position").css("top", document.body.scrollTop);
                $rootScope.bodyClass = "menu-opened";

                var cartao;
                
                if(card)
                {
                    dashboard.myPromise = CartaoService.carregarExtratoCartaoService(dashboard.idUsuario, card);

                    dashboard.myPromise.then(function (response) {

                        debugger;
                        var value = response.value;
                        
                        cartao = JSLINQ(dashboard.listaCartoes)
                            .Where(function (item) { return item.id == value.balance.id; })
                            .First();
                        
                        if(cartao)
                        {
                            
                        
                            cartao.extrato = value.release;

                            //fim:atualiza a data e valor do proximo credito///////////////////////////////
                            var month = "-";
                            var day = "-";
                            if (value.balance.dateNextDeposit !== null) {
                                month = moment(value.balance.dateNextDeposit, "DD/MM/YYYY").format("MMM");
                                day = moment(value.balance.dateNextDeposit, "DD/MM/YYYY").format("DD");
                            }
                        
                            var valorProximoCredito = "-";
                            if (JSLINQ(value.scheduling).First() !== null) {
                                valorProximoCredito = JSLINQ(value.scheduling).First().value;
                            }

                            cartao.diaProximoCredito = day;
                        
                            cartao.mesProximoCredito = month.toUpperCase();
                            cartao.valorProximoCredito = valorProximoCredito;

                            //fim///////////////////////////////////////////////////////////////////////////
                        
                            if (CartaoService.IdCartaoSelecionado) {
                                CartaoService.IdCartaoSelecionado = null;
                            }
        
                            if (card !== undefined && card !== null) {
                                dashboard.content = card;
                                    
                                dashboard.detalheCartao = cartao;
            
                                if (cartao.bin === "TRE" || cartao.bin === "TAE" || cartao.bin === "TC") {
                                    dashboard.periodo = "30";
                                } else if (cartao.bin === "TKE") {
                                    dashboard.periodo = "90";
                                } else {
                                    dashboard.periodo = "30";
                                }
            
                                dashboard.filtrarPeriodoExtrato();
                                dashboard.detalheCartao = cartao;
                                $scope.detalheCartao = dashboard.detalheCartao;
                                
                            }
                            else {
                                dashboard.blank = true;
                            }
                        }
                            $rootScope.bodyClass = "";
                            dashboard.myPromise = null;

                    

                    }, function (response) {

                        $rootScope.bodyClass = "";
                        dashboard.myPromise = null;

                    });
                }
                
            }

            $rootScope.setCard = dashboard.setCard;

            function filtrarPeriodoExtrato() {

                var dataInicial = moment().subtract(dashboard.periodo, "days");
                var dataFinal = moment();

                var extratoFiltrado = Enumerable.From(dashboard.detalheCartao.extrato)
                    .Where(function (item) {
                        return moment(item.date, "DD/MM/YYYY") >= dataInicial &&
                            moment(item.date, "DD/MM/YYYY") <= dataFinal;
                    })
                    .OrderByDescending(function (item) { return item.dateParsed; })
                    .ThenByDescending(function (item) { return item.valueParsed; })
                    .ToArray();

                //logica criada apenas para o grafico
                var extratoFiltradoGrafico = Enumerable.From(extratoFiltrado)
                    .Where(function (item) { return item.description !== "DISPONIB. DE CREDITO"; })
                    .Where(function (item) { return item.description !== "CREDIT VOUCHER -"; })
                    .Where(function (item) { return item.description !== "REVERSAL SAQUE -"; })
                    .Where(function (item) { return item.description !== "REVERSAO TARIFA PGTO BOLETO -"; })
                    .Where(function (item) { return item.description !== "EST TARIFA DE SAQUE -"; })
                    .Where(function (item) { return item.description !== "REVERSAO TARIFA RECARGA CELULAR -"; })
                    .Where(function (item) { return item.description !== "REVERSAO TARIFA DE MENSALIDADE -"; })
                    .OrderBy(function (item) { return item.dateParsed; })
                    .ThenBy(function (item) { return item.valueParsed; })
                    .ToArray();

                var result = Enumerable.From(extratoFiltradoGrafico).GroupBy("$.dateParsed", null,
                    function (key, g) {
                        var result = {
                            date: key,
                            value: g.Sum("$.valueParsed")
                        };
                        return result;
                    }).ToArray();

                CartaoFactory.setDataLineChart(result);

                // var usedValueFormat = $filter('currency')((usedValue >= 0 ? usedValue : usedValue * -1), "", 2);
                // var deposit = $filter('currency')(dashboard.detalheCartao.deposit, "", 2);
                if (!dashboard.detalheCartao.value)
                    dashboard.detalheCartao.value = 0;

                if (!dashboard.detalheCartao.deposit)
                    dashboard.detalheCartao.deposit = 0;

                var usedValue = dashboard.detalheCartao.value - dashboard.detalheCartao.deposit;

                var dataDonutChartData = {
                    total: dashboard.detalheCartao.deposit,
                    used: (usedValue >= 0 ? usedValue : usedValue * -1)
                };

                CartaoFactory.setDataDonutChart(dataDonutChartData);

                dashboard.injectedObject = dashboard.periodo;

                angular.forEach(extratoFiltrado, function (value, key) {

                    var obj = value;

                    extratoFiltrado[key].description = obj.description.replace('COMPRAS - ', "").replace('COMPRA - ', "");

                });

                dashboard.extratoFiltrado = extratoFiltrado;
            }

            function isActive(card) {

                return dashboard.content === card;
            }

            function checkActive(card) {

                if (dashboard.content != card) {
                    dashboard.setCard(card);
                }
                $timeout(function () {
                    dashboard.injectedObject = "=";
                    //angular.element("tkt-grafico-linha").trigger('change');
                }, 0);
            }

            dashboard.adicionarCartao = function () {

                $scope.editModalInstance = $uibModal.open({
                    animation: true,
                    templateUrl: 'app/modules/beneficiario/modal/adicionarcartao.htm',
                    windowClass: '-ticket -adicionar',
                    size: 'sm',
                    scope: $scope
                });

            };
            dashboard.editarCartao = function (id) {

                $scope.idCartao = id;

                $scope.modal = $uibModal.open({
                    animation: true,
                    templateUrl: 'app/modules/beneficiario/modal/editarcartao.htm',
                    windowClass: '-ticket -editar',
                    size: 'sm',
                    scope: $scope
                });
            };
            dashboard.reemitirCartao = function (id) {

                $scope.idCartao = id;

                if (dashboard.detalheCartao.validado) {
                    $scope.modal = $uibModal.open({

                        animation: true,
                        templateUrl: 'app/modules/beneficiario/modal/reemissaocartao.htm',
                        windowClass: '-ticket -reemissao',
                        size: 'sm',
                        scope: $scope
                    });
                }
                else {
                    $scope.modal = $uibModal.open({

                        animation: true,
                        templateUrl: 'app/modules/beneficiario/modal/reemissaocartao.htm',
                        windowClass: '-ticket -reemissao -confirmadados',
                        size: 'sm',
                        scope: $scope
                    });
                }
            };


            function openChangePwd() {
                changePwdModalInstance = $uibModal.open({
                    animation: true,
                    templateUrl: 'app/modules/beneficiario/modal/alterarsenhacartao.htm',
                    windowClass: '-ticket -alterar',
                    size: 'sm'
                });
            }

            dashboard.ativarCartao = function (id) {

                $scope.idCartao = id;

                $scope.modal = $uibModal.open({

                    animation: true,
                    templateUrl: 'app/modules/beneficiario/modal/ativarcartao.htm',
                    windowClass: '-ticket -desbloqueio',
                    size: 'sm',
                    scope: $scope
                });

            };

            dashboard.alterarSenha = function (id) {

                $scope.idCartao = id;
                if (dashboard.detalheCartao.validado) {
                    $scope.modal = $uibModal.open({

                        animation: true,
                        templateUrl: 'app/modules/beneficiario/modal/alterarsenhacartao.htm',
                        windowClass: '-ticket -alterar',
                        size: 'sm',
                        scope: $scope

                    });
                }
                else {
                    $scope.modal = $uibModal.open({

                        animation: true,
                        templateUrl: 'app/modules/beneficiario/modal/alterarsenhacartao.htm',
                        windowClass: '-ticket -alterar -confirmadados',
                        //windowClass: '-ticket -alterar',
                        size: 'sm',
                        scope: $scope

                    });
                }



            };

            dashboard.desbloquearCartao = function (id) {

                $scope.idCartao = id;

                if (dashboard.detalheCartao.validado) {
                    $scope.modal = $uibModal.open({

                        animation: true,
                        templateUrl: 'app/modules/beneficiario/modal/desbloqueiocartao.htm',
                        windowClass: '-ticket -desbloqueio',
                        size: 'sm',
                        scope: $scope

                    });
                }
                else {
                    $scope.modal = $uibModal.open({

                        animation: true,
                        templateUrl: 'app/modules/beneficiario/modal/desbloqueiocartao.htm',
                        windowClass: '-ticket -desbloqueio -confirmadados',
                        size: 'sm',
                        scope: $scope

                    });
                }

            };

            dashboard.bloquearCartao = function (id) {

                $scope.idCartao = id;

                if (dashboard.detalheCartao.validado) {
                    $scope.modal = $uibModal.open({
                        animation: true,
                        templateUrl: 'app/modules/beneficiario/modal/bloqueiocartao.htm',
                        windowClass: '-ticket -bloqueio',
                        size: 'sm',
                        scope: $scope
                    });
                }
                else {
                    $scope.modal = $uibModal.open({
                        animation: true,
                        templateUrl: 'app/modules/beneficiario/modal/bloqueiocartao.htm',
                        windowClass: '-ticket -bloqueio -confirmadados',
                        size: 'sm',
                        scope: $scope
                    });
                }

            };

            dashboard.favoritarCartao = function (cartao) {

                var promise;

                $window.scrollTo(0, 0);
                $(".loading-mask-position").css("top", document.body.scrollTop);
                $rootScope.bodyClass = "menu-opened";

                promise = CartaoService.favoritarCartao(dashboard.idUsuario, cartao.number);

                promise.then(function (response) {

                    $scope.titulo = 'Sucesso';

                    if (dashboard.isNotKarma) {

                        debugger;
                        dashboard.carregarCartoes();
                    }

                }, function (response) {
                    $scope.titulo = 'Ops!';
                   
                    $rootScope.bodyClass = "";
                    dashboard.myPromise = null;
                });
            };

            $scope.favoritarCartao = dashboard.favoritarCartao;

            dashboard.beneficioClub = function () {

                $location.path('/beneficio-club');

            };

            dashboard.atendimentoDigital = function () {

                $location.path('/beneficiario/atendimento');

            };

            dashboard.redeCredenciada = function () {

                $location.path('/rede-credenciada');

            };


        }]);
}).call(this);

