(function() {
    'use strict';
    angular.module('portalUsuarioApp').controllerLazy('EdicaoController', [
        '$routeParams', '$scope', '$http', 'CartaoService', 'AutenticacaoService',
        function($routeParams, $scope, $http, CartaoService, AutenticacaoService) {

            var edicao;
            edicao = this;

            edicao.step = 0;

            var usuario = AutenticacaoService.obterUsuarioLogado();
            edicao.idUsuario = usuario.id;
            edicao.cardLast = "0000";
            edicao.protocolo = "";
            edicao.MensagemAlterarOuExcluir = 1;
            edicao.corCartao = "";
            edicao.cartao = "";

            edicao.myPromise = null;

            edicao.carregarCartao = function() {

                var promise;

                if ($scope.detalheCartao) {
                    var balance = $scope.detalheCartao;
                    
                    edicao.cartao = {
                        id: balance.id,
                        number: balance.bin == "TT" ? balance.cpf : balance.number,
                        name: 'card' + balance.id,
                        bin: balance.bin,
                        pushCode: balance.bin,
                        cssCartao: "",
                        a_cssEstrela: "",
                        i_cssEstrela: "",
                        cardLast: "",
                        apelido: balance.apelido,
                        nickName: balance.apelido,
                        favorito: balance.favorito,
                        situacao: balance.situacao,
                        validado: balance.validado,
                        value: balance.value,
                        OrigemWeb: true,
                        personalizacaoId: balance.personalizacaoId //Valor padrão
                    };

                    //funcao para exibir somente os 4 ultimos numeros do cartão
                    var length = edicao.cartao.number.length;

                    if (length > 4)
                        edicao.cardLast = edicao.cartao.number.substring(length - 4);
                    else
                        edicao.cardLast = edicao.cartao.number;
                }

            };

            edicao.alterarCartao = function() {

                edicao.myPromise = CartaoService.alterarCartaoService(edicao);

                edicao.myPromise.then(function (response) {
                    if (response.success) {

                        $scope.carregarCartoes();

                        edicao.MensagemAlterarOuExcluir = 1;

                        edicao.toSuccess();
                    }
                    else {

                        edicao.toError();

                    }
                }, function(response) {
                    edicao.toError();
                });

            };

            edicao.desejaRemover = function() {
                edicao.step = 1;
            };

            edicao.excluirCartao = function() {

                edicao.myPromise = CartaoService.excluirCartaoService(edicao.idUsuario, edicao.cartao);

                edicao.myPromise.then(function (response) {

                    if (response.data.success) {

                        $scope.carregarCartoes();
                        edicao.MensagemAlterarOuExcluir = 2;
                        edicao.toSuccess();

                    } else {
                        edicao.toError();
                    }
                }, function(response) {
                    edicao.toError();
                });

            };


            edicao.toError = function() {
                edicao.step = 2;
            };

            edicao.toSuccess = function() {
                edicao.step = 3;
            };

            edicao.ok = function() {
                $scope.modal.dismiss();
            };

            edicao.close = function() {
                $scope.modal.dismiss();
            };

            edicao.favoritarCartao = function() {
                $scope.favoritarCartao(edicao.cartao);
                $scope.modal.dismiss();
            };

            //PAGE LOAD
            edicao.carregarCartao();

            edicao.formatarCartaoSaude = function(numeroCartao){
                var parte1, parte2, parte3, parte4;

                parte1 = numeroCartao.substring(0, 4);
                parte2 = numeroCartao.substring(4, 8);
                parte3 = numeroCartao.substring(8, 12);
                parte4 = numeroCartao.substring(12);

                return parte1 + " " + parte2 + " " + parte3 + " " + parte4;
            }

        }


    ]);

}).call(this);
