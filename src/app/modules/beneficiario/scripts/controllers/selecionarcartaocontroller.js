(function () {
    'use strict';

    angular.module('portalUsuarioApp').controllerLazy('SelecionarCartaoController', [
        'ReemissaoFactory', 'BloqueioFactory', 'DesbloqueioFactory', '$uibModal', '$scope', 'CartaoService', 'AutenticacaoService', 'AtendimentoModalsFactory', 'AutoModalsFactory',
        function (ReemissaoFactory, BloqueioFactory, DesbloqueioFactory, $uibModal, $scope, CartaoService, AutenticacaoService, AtendimentoModalsFactory, AutoModalsFactory) {
            $scope.modalDesbloqueio = null;
            var cartao = this;
            var usuario = AutenticacaoService.obterUsuarioLogado();
            var unblockModalInstance;

            cartao.serviceData = AtendimentoModalsFactory.bindServiceData();
            cartao.content = '';
            cartao.setCard = setCard;
            cartao.isActive = isActive;
            cartao.checkActive = checkActive;
            cartao.nextModal = nextModal;
            cartao.close = AtendimentoModalsFactory.closeSelect;

            cartao.listaCartoes = [];
            cartao.myPromise = null;
            cartao.carregarCartoes = carregarCartoes;
            cartao.msgerro = "";

            function setCard(card) {
                cartao.content = card;
                var detalheCartao = JSLINQ(cartao.listaCartoes)
                    .Where(function (item) { return item.id == cartao.content; })
                    .First();
                $scope.detalheCartao = detalheCartao;
            }

            function isActive(card) {
                return cartao.content === card;
            }

            function checkActive(card) {
                if (cartao.content != card) {
                    cartao.setCard(card);
                }
            }

            function nextModal() {
                if (!$scope.detalheCartao) {
                    cartao.msgerro = "Selecione um cartão.";
                    return false;
                }
                switch (cartao.serviceData.service) {
                    case 'block':
                        BloqueioFactory.setCartao($scope.detalheCartao);
                        AutoModalsFactory.openBlock($scope.detalheCartao.validado);
                        break;
                    case 'unblock':
                        DesbloqueioFactory.setCartao($scope.detalheCartao);
                        AutoModalsFactory.openUnblock($scope.detalheCartao.validado);
                        break;
                    case 'changePwd':
                        AutoModalsFactory.openChangePwd();
                        break;
                    case 'reissue':
                        ReemissaoFactory.setCartao($scope.detalheCartao);
                        AutoModalsFactory.openReissue($scope.detalheCartao.validado);
                        break;
                }
                cartao.close();
            }

            function carregarCartoes() {

                cartao.myPromise = CartaoService.carregarCartoesPorUsuarioService(usuario.id);

                cartao.myPromise.then(function (response) {

                    if (response.success) {

                        var balances = response.value;

                        var i = 0;

                        angular.forEach(balances, function (value, key) {

                            var balance = value.balance;

                            var continuar = false;

                            switch (cartao.serviceData.service) {
                                case 'block':
                                    if (balance.situacao === 1)
                                        continuar = true;
                                    break;
                                case 'unblock':
                                    if (balance.situacao === 3)
                                        continuar = true;
                                    break;
                                case 'changePwd':
                                    break;
                                case 'reissue':
                                    if (balance.situacao === 1)
                                        continuar = true;
                                    break;
                            }

                            if (continuar) {
                                if (balance.bin === "TT") {

                                    angular.forEach(value.ttCard, function (cartaott, key) {
                                        cartao.listaCartoes.push({
                                            id: balance.id,
                                            number: cartaott.numero,
                                            name: balance.id,
                                            apelido: balance.nickName,
                                            favorito: balance.favorito,
                                            situacao: balance.situacao,
                                            bin: balance.bin,
                                            diaProximoCredito: null,
                                            validado: balance.validado,
                                            mesProximoCredito: null,
                                            valorProximoCredito: null,
                                            extrato: value.scheduling,
                                            deposit: balance.deposit,
                                            value: (balance.valueParsed === null ? 0 : balance.valueParsed),
                                            mediaPorDia: balance.dailyAverage,
                                            beneficiario: cartaott.beneficiario,
                                            operadora: cartaott.operadora
                                        });
                                    });

                                } else {
                                    
                                    if (balance.bin !== "TC") {
                                        var month = "-";
                                        var day = "-";
                                        if (balance.dateNextDeposit !== null) {
                                            month = moment(balance.dateNextDeposit, "DD/MM/YYYY").format("MMM");
                                            day = moment(balance.dateNextDeposit, "DD/MM/YYYY").format("DD");
                                        }

                                        var valorProximoCredito = "-";
                                        if (JSLINQ(value.scheduling).First() !== null) {
                                            valorProximoCredito = JSLINQ(value.scheduling).First().value;
                                        }

                                        cartao.listaCartoes.push({
                                            id: balance.id,
                                            number: balance.number,
                                            name: balance.id,
                                            apelido: balance.nickName,
                                            favorito: balance.favorito,
                                            situacao: balance.situacao,
                                            bin: balance.bin,
                                            diaProximoCredito: day,
                                            validado: balance.validado,
                                            mesProximoCredito: month.toUpperCase(),
                                            valorProximoCredito: valorProximoCredito,
                                            extrato: value.release,
                                            deposit: balance.deposit,
                                            value: (balance.valueParsed === null ? 0 : balance.valueParsed),
                                            mediaPorDia: balance.dailyAverage
                                        });
                                    }
                                }
                            }

                            i++;

                        });

                        // if (CartaoService.IdCartaoSelecionado === null && dashboard.listaCartoes.length > 0) {
                        //     dashboard.setCard(dashboard.listaCartoes[0].id);
                        // } else {
                        //     dashboard.setCard(CartaoService.IdCartaoSelecionado);
                        // }

                    }

                    if (!cartao.listaCartoes.length) {
                        cartao.msgerro = "Você não possui cartões cadastrados que permitam a utilização desse serviço de auto atendimento.";
                    }

                }, function (response) {
                    console.log(response);
                });
            }
        }
    ]);

})();
