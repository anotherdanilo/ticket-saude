(function () {
    'use strict';
    angular.module('portalUsuarioApp').controllerLazy('EditarUsuarioController',
        ['$location', '$scope', '$http', 'UsuarioModalsFactory', '$uibModal', 'UsuarioService', 'AutenticacaoService', 'toaster', '$rootScope', 'AlertaFactory','$browser',
            function ($location, $scope, $http, UsuarioModalsFactory, $uibModal, UsuarioService, AutenticacaoService, toaster, $rootScope, AlertaFactory, $browser) {
                $rootScope.menuSelecionado = null;

                var editar = this;


                var usuarioAutenticado = AutenticacaoService.obterUsuarioLogado();

                AutenticacaoService.setRedirecionando(false);

                if (usuarioAutenticado.isLogged === false) {
                    $location.path($browser.baseHref());
                }


                editar.myPromise = null;

                editar.usuariologado = {
                    nome: usuarioAutenticado.nome.split(' ')[0],
                    id: usuarioAutenticado.id,
                    logado: usuarioAutenticado.isLogged,
                    isFacebook: usuarioAutenticado.isFacebook
                };

                //UsuarioModalsFactory.setEscopo(2);
                editar.openChangePwd = UsuarioModalsFactory.openChangePwd;
                editar.closeChangePwd = UsuarioModalsFactory.closeChangePwd;
                editar.openAlerta = AlertaFactory.openAlerta;
                editar.exibirMensagemSexoNaoPreenchido = false;
                //  editar.uploadFoto = UsuarioModalsFactory.uploadFoto;

                editar.FormataData = function (data) {
                    var ano, dataFormatada, dia, mes, splitData;
                    splitData = data.split('/');
                    dia = splitData[0];
                    mes = splitData[1] - 1;
                    ano = splitData[2];
                    dataFormatada = new Date(ano, mes, dia);
                    return dataFormatada;
                };

                editar.carregarDadosUsuario = function () {

                    $(".loading-mask-position").css("top", document.body.scrollTop);
                    $rootScope.bodyClass = "menu-opened";

                    editar.myPromise = UsuarioService.carregarUsuarioPorId(editar.usuariologado.id);

                    editar.myPromise.then(function (response) {

                        var usuarioCadastrado = response.value;
                        var sobrenome = "";
                        var nome = "";
                        var nomeCompleto = "";

                        if (usuarioCadastrado.lastName !== undefined && usuarioCadastrado.lastName !== null) {
                            sobrenome = usuarioCadastrado.lastName;

                        }
                        if (usuarioCadastrado.name !== undefined && usuarioCadastrado.name !== null) {
                            nome = usuarioCadastrado.name;
                        }
                        
                        debugger;
                        if(usuarioCadastrado.sexo != 'F' && usuarioCadastrado.sexo != 'M')
                        {
                            usuarioCadastrado.sexo = null;
                        }

                        editar.usuario = {};
                        editar.usuario = {
                            id: usuarioCadastrado.id,
                            nome: nome + ' ' + sobrenome,
                            readOnlyNome: (nome ? true : false),
                            cpf: usuarioCadastrado.cpf,
                            readOnlyCpf: (usuarioCadastrado.cpf ? true : false),
                            matricula: usuarioCadastrado.matricula,
                            cellPhone: usuarioCadastrado.cellPhone,
                            possuiFoto: usuarioCadastrado.hasPicture,
                            email: usuarioCadastrado.email,
                            readOnlyEmail: (usuarioCadastrado.email ? true : false),
                            foto: usuarioCadastrado.foto,
                            sexo: usuarioCadastrado.sexo,

                            interests: usuarioCadastrado.checkboxPreferencia,
                            aceitoMarketing: usuarioCadastrado.aceitoMarketing

                        };


                        if (usuarioCadastrado.endereco !== null) {
                            editar.usuario.endereco = {};
                            editar.usuario.endereco = {
                                cep: usuarioCadastrado.endereco.cep,
                                logradouro: usuarioCadastrado.endereco.logradouro,
                                numero: usuarioCadastrado.endereco.numero,
                                complemento: usuarioCadastrado.endereco.complemento,
                                bairro: usuarioCadastrado.endereco.bairro,
                                cidade: usuarioCadastrado.endereco.cidade,
                                uf: usuarioCadastrado.endereco.uf
                            };
                        }


                        var dia = 1, mes = 1, ano = 1980;

                        if (usuarioCadastrado.nascimento) {
                            dia = parseInt(usuarioCadastrado.nascimento.split('-')[2].split('T')[0]);
                            mes = parseInt(usuarioCadastrado.nascimento.split('-')[1].split('T')[0]);
                            ano = parseInt(usuarioCadastrado.nascimento.split('-')[0].split('T')[0]);
                        }

                        editar.usuario.formData = {
                            birthDateDay: dia,
                            birthDateDayOpts: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31],
                            birthDateMonth: mes,
                            birthDateMonthOpts: [{ name: 'Janeiro', value: 1 }, { name: 'Fevereiro', value: 2 }, { name: 'Março', value: 3 }, { name: 'Abril', value: 4 }, { name: 'Maio', value: 5 }, { name: 'Junho', value: 6 }, { name: 'Julho', value: 7 }, { name: 'Agosto', value: 8 }, { name: 'Setembro', value: 9 }, { name: 'Outubro', value: 10 }, { name: 'Novembro', value: 11 }, { name: 'Dezembro', value: 12 }],
                            birthDateYear: ano,
                            birthDateYearOpts: [
                                1916, 1917, 1918, 1919, 1920, 1921, 1922, 1923, 1924, 1925, 1926, 1927, 1928, 1929, 1930, 1931, 1932, 1933, 1934, 1935, 1936, 1937,
                                1938, 1939, 1940, 1941, 1942, 1943, 1944, 1945, 1946, 1947, 1948, 1949, 1950, 1951, 1952, 1953, 1954, 1955, 1956, 1957, 1958, 1959,
                                1960, 1961, 1962, 1963, 1964, 1965, 1966, 1967, 1968, 1969, 1970, 1971, 1972, 1973, 1974, 1975, 1976, 1977, 1978, 1979, 1980, 1981,
                                1982, 1983, 1984, 1985, 1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993, 1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003,
                                2004, 2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016
                            ]

                        };

                        $rootScope.bodyClass = "";
                    });
                };

                editar.obterCep = function () {

                    if (editar.usuario.endereco && editar.usuario.endereco.cep) {

                        editar.myPromise = UsuarioService.obterPorCep(editar.usuario.endereco.cep);

                        editar.myPromise.then(function (response) {

                            var enderecoRetornado = response.value;
                            if (enderecoRetornado !== undefined && enderecoRetornado !== null) {
                                editar.usuario.endereco = {};
                                editar.usuario.endereco = {
                                    cep: enderecoRetornado.cep,
                                    logradouro: enderecoRetornado.logradouro,
                                    bairro: enderecoRetornado.bairro,
                                    cidade: enderecoRetornado.cidade,
                                    uf: enderecoRetornado.uf
                                };

                                $('#txtnumero').focus();
                            }

                        }, function (error) {

                            editar.usuario.endereco.logradouro = "";
                            editar.usuario.endereco.bairro = "";
                            editar.usuario.endereco.cidade = "";
                            editar.usuario.endereco.uf = "";
                            editar.usuario.endereco.numero = "";
                            editar.usuario.endereco.complemento = "";

                        });
                    }
                };

                //   editar.uploadFoto = function (){

                //   };


                editar.alterarDadosusuarioCadastrado = function () {

                    if (editar.usuario.nome !== undefined) {
                        editar.usuario.name = editar.usuario.nome;
                    }

                    if (!editar.usuario.sexo) {
                        editar.exibirMensagemSexoNaoPreenchido = true;
                        return false;
                    }

                    editar.usuario.CellPhone = editar.usuario.cellPhone;

                    editar.usuario.nascimento = editar.FormataData(editar.usuario.formData.birthDateDay + "/" + editar.usuario.formData.birthDateMonth + "/" + editar.usuario.formData.birthDateYear);

                    editar.usuario.Preferencias = [];

                    if (editar.usuario.interests !== undefined) {

                        editar.usuario.interests.forEach(function (element) {
                            if (element.value === true) {
                                editar.usuario.Preferencias.push(element.id);
                            }
                        }, this);

                        $(".loading-mask-position").css("top", document.body.scrollTop);
                        $rootScope.bodyClass = "menu-opened";

                        debugger;
 
                        var request = {
                            "id": editar.usuario.id,
                            "Email": editar.usuario.email,
                            "Name": editar.usuario.name,
                            "Cpf": editar.usuario.cpf,
                            "CellPhone": editar.usuario.cellPhone,
                            "HasPicture": editar.usuario.possuiFoto,
                            "nascimento": editar.usuario.nascimento,
                            "matricula": editar.usuario.matricula,
                            "aceitoMarketing": editar.usuario.aceitoMarketing,
                            "Sexo": editar.usuario.sexo,
                            "Foto": editar.usuario.foto,
                            "Endereco": editar.usuario.endereco,
                            "Preferencias": editar.usuario.Preferencias                            
                        };

                        editar.myPromise = UsuarioService.alterarCadastroUsuario(request);
                        editar.myPromise.then(function (retorno) {


                            //if (editar.usuario.nome.split(' ') > 0)
                            //    AutenticacaoService.obterUsuarioLogado().nome = editar.usuario.nome.split(' ')[0];
                            //else
                            //    AutenticacaoService.obterUsuarioLogado().nome = editar.usuario.nome;
                            if (retorno.success) {
                                AutenticacaoService.setCadastroCompleto(true);
                                AlertaFactory.parametros.titulo = "Atualização";
                                AlertaFactory.parametros.mensagem = "Seus dados foram atualizados com sucesso!";
                                AlertaFactory.parametros.icone = "icon-done";
                                AlertaFactory.parametros.callback = callback;
                                editar.openAlerta();
                            }
                            else {
                                AlertaFactory.parametros.titulo = "Erro";
                                AlertaFactory.parametros.mensagem = retorno.message;
                                AlertaFactory.parametros.icone = "icon-error";
                                editar.openAlerta();
                            }

                            $rootScope.bodyClass = "";

                        }, function (error) {

                            $rootScope.bodyClass = "";
                            AlertaFactory.parametros.titulo = "Erro";
                            AlertaFactory.parametros.mensagem = "Não possível editar o cadastro, por favor tente novamente mais tarde.";
                            AlertaFactory.parametros.icone = "icon-error";
                            editar.openAlerta();
                            //$rootScope.bodyClass = "menu-opened";
                        });
                    }
                };

                function callback() {
                    $location.path('/beneficiario/dashboard');
                }

            }
        ]);

}).call(this);
