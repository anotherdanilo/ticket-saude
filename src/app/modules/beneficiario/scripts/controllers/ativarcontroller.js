(function () {
    'use strict';

    angular.module('portalUsuarioApp').controllerLazy('AtivarController', [
        'AutoModalsFactory',
        'AccentivService',
        '$scope',
        '$http',
        'AutenticacaoService',
        '$rootScope',
        '$window',
        function (AutoModalsFactory, AccentivService, $scope, $http, AutenticacaoService, $rootScope, $window) {

            var self = this;
            self.promise = null;
            self.mensagemShow = false;
            self.mensagem = "Ops! Ocorreu um erro ao tentar ativar este cartão, por favor, tente novamente mais tarde!";
            self.post = {
                IdCartao: 0,
                Nome: "",
                Matricula: "",
                Cpf: "",
                DataNasc: "",
                TipoOperacaoAutoServico: 0,
                OrigemWeb: true //Valor padrão
            };

            var usuario = AutenticacaoService.obterUsuarioLogado();
            self.idUsuario = usuario.id;

            self.textoConfirmacaoBloqueio = "Deseja ativar seu cartão?";

            self.protocolo = 0;

            self.pageLoad = function () {

                var detalheCartao = null;
                if ($scope.detalheCartao) {
                    detalheCartao = $scope.detalheCartao;
                }
                else {

                }
                if (detalheCartao) {

                    var balance = detalheCartao;//response.value.balance;

                    self.cartao = {
                        id: balance.id,
                        number: balance.number,
                        name: 'card' + balance.id,
                        bin: balance.bin,
                        pushCode: balance.bin,
                        cssCartao: "",
                        a_cssEstrela: "",
                        i_cssEstrela: "",
                        cardLast: "",
                        apelido: balance.apelido,
                        nickName: balance.apelido,
                        favorito: balance.favorito,
                        situacao: balance.situacao,
                        validado: balance.validado,
                        value: balance.value,
                        OrigemWeb: true, //Valor padrão
                        personalizacaoId: balance.personalizacaoId,
                        bandeira: balance.bandeira
                    };

                    console.log(self.cartao);

                }
            };

            self.ativarCartao = function () {

                $window.scrollTo(0, 0);
                $rootScope.bodyClass = "menu-opened";

                self.post.IdCartao = self.cartao.id;

                self.promise = AccentivService.ativarCartao(self.cartao.number, self.cartao.pin);

                self.promise.then(function (response) {

                    debugger;
                    self.step = 1;

                    $rootScope.bodyClass = "";

                }, function (error) {

                    debugger;
                    self.mensagem = "Ops! Ocorreu um erro ao tentar ativar este cartão, por favor, tente novamente mais tarde!";

                    if(error.status == 400)
                    {
                        if(error.data.message == "Invalid Pin")
                        {
                            self.mensagem = "Número de PIN inválido.";
                        }
                    }
                    self.step = 2;
                    self.mensagemShow = true;
                    console.log(error.data.message);

                    $rootScope.bodyClass = "";
                });

            };

            self.step = 0;
            self.toSuccess = toSuccess;

            self.close = function () {
                if ($scope.modal) {
                    $scope.modal.dismiss();
                }
                else {
                    AutoModalsFactory.closeBlock();
                }
            };

            function toSuccess() {
                if ($scope.modal) {
                    $scope.carregarCartoes();
                    $scope.modal.dismiss();
                }
                else {
                    AutoModalsFactory.closeBlock();
                }
            }

            self.favoritarCartao = function () {
                $scope.favoritarCartao(self.cartao);
                $scope.modal.dismiss();
            };

            self.pageLoad();

        }
    ]);

})();
