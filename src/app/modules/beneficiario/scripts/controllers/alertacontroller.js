(function () {
    'use strict';

    angular.module('portalUsuarioApp').controllerLazy('AlertaController', [
      '$scope', 'AlertaFactory', function ($scope, AlertaFactory) {
          var self = this;

          self.close = AlertaFactory.close;
          self.closeAlerta = AlertaFactory.closeAlerta;
          self.titulo = AlertaFactory.parametros.titulo;
          self.mensagem = AlertaFactory.parametros.mensagem;
          self.icone = AlertaFactory.parametros.icone;
          self.textoBotaoOk = (AlertaFactory.parametros.textoBotaoOk === '' ? 'Ok': AlertaFactory.parametros.textoBotaoOk);
      }
    ]);

})();
