(function () {
    'use strict';

    angular.module('portalUsuarioApp').controllerLazy('BloqueioController', [
        'AutoModalsFactory', 'CartaoService', '$scope', '$http', 'BloqueioFactory', 'AutenticacaoService', '$rootScope', '$window', 'AccentivService',
        function (AutoModalsFactory, CartaoService, $scope, $http, BloqueioFactory, AutenticacaoService, $rootScope, $window, AccentivService) {

            BloqueioFactory.resetBlockFormData();

            var self = this;
            self.promise = null;
            self.mensagemShow = false;
            self.mensagem = "O status desse cartão não permite bloqueio, entre em contato com a central de atendimento Ticket ou com seu RH.";
            self.post = {
                IdCartao: 0,
                Nome: "",
                Matricula: "",
                Cpf: "",
                DataNasc: "",
                TipoOperacaoAutoServico: 0,
                OrigemWeb: true //Valor padrão
            };

            self.exibeProtocolo = true;

            var usuario = AutenticacaoService.obterUsuarioLogado();
            self.idUsuario = usuario.id;

            self.textoConfirmacaoBloqueio = "Deseja bloquear seu cartão?";

            self.protocolo = 0;

            self.pageLoad = function () {


                var detalheCartao = null;
                if ($scope.detalheCartao) {
                    detalheCartao = $scope.detalheCartao;
                }
                else {
                    detalheCartao = BloqueioFactory.getCartao();
                }
                if (detalheCartao) {

                    var balance = detalheCartao;//response.value.balance;

                    self.cartao = {
                        id: balance.id,
                        number: balance.number,
                        name: 'card' + balance.id,
                        bin: balance.bin,
                        pushCode: balance.bin,
                        cssCartao: "",
                        a_cssEstrela: "",
                        i_cssEstrela: "",
                        cardLast: "",
                        apelido: balance.apelido,
                        nickName: balance.apelido,
                        favorito: balance.favorito,
                        situacao: balance.situacao,
                        validado: balance.validado,
                        value: balance.value,
                        OrigemWeb: true, //Valor padrão
                        personalizacaoId: balance.personalizacaoId //Valor padrão
                    };

                    if (balance.validado) {
                        self.textoConfirmacaoBloqueio = "Deseja bloquear este cartão?";
                    }
                }
            };

            self.bloquearCartao = function () {

                $window.scrollTo(0, 0);
                $rootScope.bodyClass = "menu-opened";

                //Cartao Accentiv

                if ($scope.detalheCartao.accentiv) {

                    self.exibeProtocolo = false;

                    self.promise = AccentivService.bloqueioCartao(self.cartao.number);

                    self.promise.then(function (response) {

                        if (response.success) {

                            self.step = 1;

                            $rootScope.bodyClass = "";

                        }
                        else {
                            self.step = 2;
                        }

                    }, function (error) {

                        if (error.status == 400) {

                            if (error.data.message == "Invalid Pin") {
                                self.mensagem = "Número de PIN inválido.";
                            }
                            else {
                                self.mensagem = error.data.message.replace(/"/g, '');
                            }

                            self.mensagemShow = true;
                        }
                        else if (error.status == 500) {
                            self.step = 2;
                        }

                        $rootScope.bodyClass = "";

                    });
                }
                else {

                    self.post.IdCartao = self.cartao.id;

                    self.post.DataNasc = self.formData.birthDateMonth + "/" +
                        self.formData.birthDateDay + "/" +
                        self.formData.birthDateYear;

                    self.post.TipoOperacaoAutoServico = 1;


                    self.promise = CartaoService.autoServicoCartao(self.idUsuario, self.post);

                    self.promise.then(function (response) {

                        self.protocolo = response.value.protocolo;
                        self.step = 1;

                        $rootScope.bodyClass = "";

                    }, function (error) {

                        if (error.status == 400) {

                            self.mensagem = error.data.message.replace(/"/g, '');
                            self.mensagemShow = true;
                        }
                        else if (error.status == 500) {
                            self.step = 2;
                        }

                        $rootScope.bodyClass = "";
                    });
                }
            };

            self.step = 0;
            self.toSuccess = toSuccess;
            self.formData = BloqueioFactory.bindBlockFormData();
            self.submit = BloqueioFactory.submitBlockFormData;

            self.close = function () {
                if ($scope.modal) {
                    $scope.modal.dismiss();
                }
                else {
                    AutoModalsFactory.closeBlock();
                }
            };

            function toSuccess() {
                if ($scope.modal) {
                    $scope.carregarCartoes();
                    $scope.modal.dismiss();
                }
                else {
                    AutoModalsFactory.closeBlock();
                }
            }

            self.favoritarCartao = function () {
                $scope.favoritarCartao(self.cartao);
                $scope.modal.dismiss();
            };

            self.pageLoad();

        }
    ]);

})();
