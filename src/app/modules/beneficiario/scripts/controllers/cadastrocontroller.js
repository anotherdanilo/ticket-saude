(function () {
    'use strict';

    angular.module('portalUsuarioApp')
        .controllerLazy('CadastroController', [
            '$location', '$scope', 'CadastroFactory', 'LoginModalsFactory', 'UsuarioService', 'toaster', '$sce', 'Facebook', 'UsuarioModalsFactory', '$compile', 'AutenticacaoService', 'AlertaFactory',
            function ($location, $scope, CadastroFactory, LoginModalsFactory, UsuarioService, toaster, $sce, Facebook, UsuarioModalsFactory, $compile, AutenticacaoService, AlertaFactory) {

                // Define user empty data :/
                $scope.user = {};

                // Defining user logged status
                $scope.logged = false;

                // And some fancy flags to display messages upon user status change
                $scope.byebye = false;
                $scope.salutation = false;

                var userIsConnected = false;

                CadastroFactory.resetSignUpFormData();

                Facebook.getLoginStatus(function (response) {
                    if (response.status == 'connected') {
                        userIsConnected = true;
                    }
                });

                /**
                 * IntentLogin
                 */
                $scope.IntentLogin = function () {
                    $scope.logged = true;
                    $scope.login();
                };


                /**
                 * Login
                 */
                $scope.login = function () {
                    
                    Facebook.login(function () {
                        // nÃ£o faz nada, deixa a resposta a cargo da promise
                    }, { scope: 'public_profile,email' }).then(function (response) {
                        if (response.status == 'connected') {
                            cadastro.formData.token = response.authResponse.accessToken;
                            $scope.logged = true;
                            $scope.me();
                        }

                    });
                };

                /**
                 * me 
                 */
                $scope.me = function () {
                    Facebook.api('/me', function () {
                        // nÃ£o faz nada, deixa a resposta a cargo da promise
                    }, { "fields": "email,age_range,birthday,first_name,last_name,gender,locale,updated_time,picture" }).then(function (response) {
                        
                        // cadastro.promise = UsuarioService.obterUsuarioPorToken(response.email);

                        // cadastro.promise.then(function(response2) {
                        //     if (response2.success) {
                        cadastro.promise = AutenticacaoService.autenticar(
                            response.email,
                            null,
                            cadastro.formData.token,
                            null,
                            null);

                        cadastro.promise.then(function (response3) {
                            cadastro.close();
                            $location.path("/beneficiario/dashboard");
                        }, function (error) {
                            

                            if (error.status == 500) {
                                AlertaFactory.parametros.titulo = "Erro";
                                AlertaFactory.parametros.mensagem = "Ops! Ocorreu um erro ao tentar cadastrar o seu usuário, por favor, tente novamente mais tarde!";
                                AlertaFactory.parametros.icone = "icon-error";
                                AlertaFactory.openAlerta();
                                cadastro.close();
                                return;
                            }

                            cadastro.formData.NomeCompleto = response.first_name + " " + response.last_name;
                            cadastro.formData.Email = response.email;

                            if (response.gender == "male") {
                                cadastro.formData.Sexo = "M";
                            }
                            else {
                                cadastro.formData.Sexo = "F";
                            }

                            cadastro.step = 1;
                        });
                        //}
                        // }, function(error) {
                        //     //cadastro.formData.token = response.id;

                        //     cadastro.formData.NomeCompleto = response.first_name + " " + response.last_name;
                        //     cadastro.formData.Email = response.email;

                        //     if (response.gender == "male") {
                        //         cadastro.formData.Sexo = "M";
                        //     }
                        //     else {
                        //         cadastro.formData.Sexo = "F";
                        //     }

                        //     cadastro.step = 1;
                        // });
                    });
                };

                /**
                 * Logout
                 */
                $scope.logout = function () {
                    Facebook.logout(function () {
                        $scope.$apply(function () {
                            $scope.user = {};
                            $scope.logged = false;
                        });
                    });
                };

                var cadastro = this;


                cadastro.error = false;
                cadastro.errorCPF = false;
                //cadastro.mensagem = "";

                cadastro.typeSenha = 'password';

                cadastro.promise = "";

                cadastro.parametros = LoginModalsFactory.parametros;

                cadastro.openChangePwd = UsuarioModalsFactory.openChangePwd;

                cadastro.toRecoverPassword = function () {

                    LoginModalsFactory.parametros.esqueciMinhaSenha = true;

                    LoginModalsFactory.openLogin();

                    cadastro.close();

                };

                cadastro.teste = function () {
                    return $sce.trustAsHtml('<a href="#">clique aqui</a>');
                };

                cadastro.cadastrarUsuario = function () {


                    $(".loading-mask-position").css("top", 0);

                    cadastro.formData.Name = cadastro.formData.NomeCompleto;

                    cadastro.formData.nascimento = cadastro.formData.birthDateMonth + "/" +
                        cadastro.formData.birthDateDay + "/" +
                        cadastro.formData.birthDateYear;

                    debugger;
                    var request = {
                        "Name": cadastro.formData.Name,
                        "Cpf": cadastro.formData.Cpf,
                        "CellPhone": cadastro.formData.Cellphone,
                        "Email": cadastro.formData.Email,
                        "nascimento": cadastro.formData.nascimento,
                        "Sexo": cadastro.formData.Sexo,
                        "Password": cadastro.formData.Password,
                        "OrigemWeb": true
                    };

                    cadastro.promise = UsuarioService.cadastrarUsuario(request);

                    cadastro.promise.then(function (response) {

                        cadastro.step = 2;

                    }, function (error) {


                        var mensagem1 = "CPF já informado para outro usuário.";
                        var mensagem2 = "Este Email já possui um cadastro, efetue login ou caso não lembre sua senha";

                        var contemMensagemCpf = error.data.message.indexOf(mensagem1);
                        var contemMensagemEmail = error.data.message.indexOf(mensagem2);

                        if (contemMensagemCpf > -1 || contemMensagemEmail > -1) {
                            cadastro.errorCPF = false;
                            cadastro.error = true;
                            cadastro.mensagem = "Usuário já cadastrado na Ticket, faça seu  <a ng-click='cadastro.login()'>login.</a>";
                            return;
                        }

                        cadastro.errorCPF = false;
                        cadastro.error = true;
                        cadastro.mensagem = error.data.message.replace('\n', '<br/>');

                    });

                };

                cadastro.login = function () {

                    cadastro.changeToLogin();

                };

                // cadastro.gotoFace = function() {
                //     Facebook.login(function(response) {
                //         if (response.status == 'connected') {
                //             cadastro.logged = true;
                //             cadastro.me();
                //         }
                //         // Do something with response.
                //         // Facebook.api('/me', { "fields": "email,age_range,birthday,first_name,last_name,gender,locale,updated_time,picture" }, function(response) {
                //         //     cadastro.formData.NomeCompleto = response.first_name + " " + response.last_name;
                //         // });
                //     });

                //     cadastro.step = 1;
                // };

                cadastro.toForm = toForm;
                cadastro.formData = CadastroFactory.bindSignUpFormData();
                cadastro.submit = CadastroFactory.submitSignUpFormData;
                cadastro.changeToLogin = LoginModalsFactory.changeToLogin;
                cadastro.changeToConfirm = LoginModalsFactory.changeToConfirm;
                cadastro.close = LoginModalsFactory.closeSignUp;

                var escopo = LoginModalsFactory.getEscopo();

                cadastro.step = escopo.step;
                if (escopo.objUsuario) {
                    
                    cadastro.formData.token = escopo.objUsuario.token;
                    cadastro.formData.NomeCompleto = escopo.objUsuario.NomeCompleto;
                    cadastro.formData.Email = escopo.objUsuario.Email;
                    cadastro.formData.Sexo = escopo.objUsuario.Sexo;

                    $scope.logged = true;
                }

                function toForm() {
                    cadastro.step = 1;
                    $scope.logged = false;
                    cadastro.formData.token = null;
                }

                cadastro.alterarType = function () {
                    cadastro.typeSenha = cadastro.typeSenha === 'password' ? 'text' : 'password';
                };

                cadastro.alterarSenha = function () {
                    UsuarioModalsFactory.setEscopo(2);
                    cadastro.openChangePwd();
                    cadastro.close();
                };
            }
        ]);

})();
