(function () {
    'use strict';

    angular.module('portalUsuarioApp').controllerLazy('DesbloqueioController', [
        'AutoModalsFactory', 'DesbloqueioFactory', 'CartaoService', '$scope', '$http', 'AutenticacaoService', 'AccentivService',
        function (AutoModalsFactory, DesbloqueioFactory, CartaoService, $scope, $http, AutenticacaoService, AccentivService) {

            DesbloqueioFactory.resetUnblockFormData();

            var self = this;

            self.post = {
                IdCartao: 0,
                Nome: "",
                Matricula: "",
                Cpf: "",
                DataNasc: "",
                TipoOperacaoAutoServico: 0,
                OrigemWeb: true //Valor padrão
            };

            self.mensagem = "";
            self.mensagemShow = false;
            self.exibeProtocolo = true;

            self.textoConfirmacaoDesbloqueio = "Deseja desbloquear seu cartão?";
            var usuario = AutenticacaoService.obterUsuarioLogado();
            self.idUsuario = usuario.id;
            self.protocolo = 0;

            self.promise = null;

            self.pageLoad = function () {

                var detalheCartao = null;
                if ($scope.detalheCartao) {
                    detalheCartao = $scope.detalheCartao;
                }
                else {
                    detalheCartao = DesbloqueioFactory.getCartao();
                }

                if (detalheCartao) {
                    var balance = detalheCartao; //response.value.balance;
                    debugger;
                    self.cartao = {
                        id: balance.id,
                        number: balance.number,
                        name: 'card' + balance.id,
                        bin: balance.bin,
                        pushCode: balance.bin,
                        cssCartao: "",
                        a_cssEstrela: "",
                        i_cssEstrela: "",
                        cardLast: "",
                        apelido: balance.apelido,
                        nickName: balance.apelido,
                        favorito: balance.favorito,
                        situacao: balance.situacao,
                        validado: balance.validado,
                        value: balance.value,
                        OrigemWeb: true, //Valor padrão
                        personalizacaoId: balance.personalizacaoId, //Valor padrão
                        accentiv: balance.accentiv,
                        bandeira: balance.bandeira
                    };

                    if (balance.validado) {
                        self.textoConfirmacaoDesbloqueio = "Deseja desbloquear este cartão?";
                    }
                }
            };

            self.desbloquearCartao = function () {

                if ($scope.detalheCartao.accentiv) {

                    self.exibeProtocolo = false;

                    self.promise = AccentivService.desbloqueioCartao(self.cartao.number, self.cartao.pin);

                    self.promise.then(function (response) {

                        debugger;
                        if (response.success) {

                            self.step = 1;

                            $rootScope.bodyClass = "";

                        }
                        else {
                            self.step = 2;
                        }

                    }, function (error) {

                        toError(error);

                    });
                }
                else {
                    self.post.IdCartao = self.cartao.id;
                    self.post.DataNasc = self.formData.birthDateMonth + "/" +
                        self.formData.birthDateDay + "/" +
                        self.formData.birthDateYear;

                    self.post.TipoOperacaoAutoServico = 2;

                    self.promise = CartaoService.autoServicoCartao(self.idUsuario, self.post);

                    self.promise.then(function (response) {

                        self.protocolo = response.value.protocolo;
                        self.step = 1;

                    }, function (error) {

                        toError(error);

                    });
                }
            };

            self.step = 0;
            self.toSuccess = toSuccess;
            self.formData = DesbloqueioFactory.bindUnblockFormData();
            self.submit = DesbloqueioFactory.submitUnblockFormData;

            self.close = function () {
                if ($scope.modal) {
                    $scope.modal.dismiss();
                }
                else {
                    AutoModalsFactory.closeUnblock();
                }
            };

            function toSuccess() {
                if ($scope.modal) {
                    $scope.carregarCartoes();
                    $scope.modal.dismiss();
                }
                else {
                    AutoModalsFactory.closeUnblock();
                }
            }

            function toError(error) {
                if (error.status == 400) {

                    if (error.data.message == "Invalid Pin") {
                        self.mensagem = "Número de PIN inválido.";
                    }
                    else {
                        self.mensagem = error.data.message.replace(/"/g, '');
                    }

                    self.mensagemShow = true;
                }
                else if (error.status == 500) {
                    self.step = 2;
                }
            }

            self.favoritarCartao = function () {
                $scope.favoritarCartao(self.cartao);
                $scope.modal.dismiss();
            };

            self.pageLoad();
        }
    ]);
})();
