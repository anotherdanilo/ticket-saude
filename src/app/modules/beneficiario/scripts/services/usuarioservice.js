(function() {
  'use strict';
  angular.module('portalUsuarioApp').serviceLazy("UsuarioService", [
        'UsuarioResource', '$q', '$resource', function(UsuarioResource, $q, $resource) {
        
     	this.cadastrarUsuario = function(usuario) {

        return $q(function(resolve, reject) {

                    UsuarioResource.save(usuario, function(response) {

                        if (response.success) {
                            resolve(response);
                        } else {
                            response.data.message = "Ops! Ocorreu um problema ao cadastrar. Por favor tente novamente mais tarde.";
                            reject(error);
                        }

            }, function(error) {

              reject(error);

            });
        });      
      };      
      
    this.alterarCadastroUsuario = function(usuario) {

        usuario.OrigemWeb = true;
        
        return $q(function(resolve, reject) {
      
            UsuarioResource.update(usuario, function(response) {
                if (response.success) {
                    resolve(response);
                } else {
                    reject(response.message);
                }          
            }, function(error) {
              reject(error);
            });
         });
      }; 
      
            this.obterPorCep = function(cep) {
                return $q(function(resolve, reject) {

                    var resource = $resource(config.urls.obterPorCep);

                    resource.get({ id: cep }, function(response) {

                        if (response.success) {
           
             resolve(response);              
           } else {
                            response.message = "Cep não encontrado, complete seu endereço";
                            reject(response);
                        }
                    }, function(error) {
                        reject(error);
                    });
                });
            };

            this.carregarUsuarioPorId = function(idUsuario) {
         return $q(function(resolve, reject) {
         
                    UsuarioResource.get({ id: idUsuario }, function(response) {
                        if (response.success) {
             resolve(response);              
           } else {
              response.data.message = "UsuÃ¡rio nÃ£o encontrado"; 
              reject(error);
           }          
                    }, function(error) {
                        reject(error);
                    });
                });
            };

            this.obterUsuarioPorToken = function(token) {
                return $q(function(resolve, reject) {

                    var resource = $resource(config.urls.obterUsuarioPorToken);

                    resource.get({ id: token }, function(response) {
                        resolve(response);
                    }, function(error) {
                        reject(error);
                    });
                });
            };

            this.indicarParceiro = function (dados, idUsuario) {

                return $q(function(resolve, reject) {

                    var resource = $resource(config.urls.indicarParceiro);

                    resource.save(null, dados, function (response) {
                        
                        return resolve(response);
                    }, function (error) {
                        
                        return reject(error);
                    });

                });

            };

      }
    ]);

}).call(this);