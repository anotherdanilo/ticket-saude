(function() {
  'use strict';
  angular.module('portalUsuarioApp').serviceLazy("DashboardService", [
    'CartaoResource', '$q','$resource','$http', function(CartaoResource, $q,$resource,$http) {
      
	    var self;
	    self = this;

	    self.carregarCartoesService = function(idUsuario){

	    	return $q(function(resolve, reject){
	    		var resource = $resource(config.urls.cartaoAPI);

	    		resource.get({idUsuario: idUsuario}, function(response){
	    			resolve(response);
	    		}, function(error){
	    			reject(error);
	    		});
	    	});

	    };

	}

  ]);

}).call(this);

