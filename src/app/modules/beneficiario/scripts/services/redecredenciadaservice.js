(function() {
    'use strict';
    angular.module('portalUsuarioApp').serviceLazy("RedeCredenciadaService", [
        '$http', 'RedeCredenciadaResource', '$q', '$resource', function($http, RedeCredenciadaResource, $q, $resource) {

            this.buscaEstabelecimentos = function (filtro) {
                filtro.OrigemWeb = true;
                return $q(function(resolve, reject) {

                    RedeCredenciadaResource.save(filtro, function(response) {
                        if (response.success) {
                            resolve(response);
                        } else {
                            
                            response.data.message = "Ops! Ocorreu um problema ao cadastrar. Por favor tente novamente mais tarde.";
                            reject(error);
                        }

                    }, function(error) {

                        reject(error);

                    });
                });
            };

            this.favoritarCartao = function(idUsuario, obj) {

                return $q(function(resolve, reject) {

                    var resource = $resource(config.urls.favoritaEstabelecimento);

                    resource.save({ idUsuario: idUsuario }, obj, function(response) {
                        resolve(response);
                    }, function(error) {
                        reject(error);
                    });
                });
            };
            
            this.excluirFavorito = function(idUsuario, obj) {

                return $q(function(resolve, reject) {
                    $http({
                        method: 'DELETE',
                        url: config.urls.favoritaEstabelecimento.replace(':idUsuario', idUsuario),
                        data: obj,
                        headers: { 
                            'content-type': 'application/json;charset=utf-8',
                            'code': obj.Code
                         }
                    }).then(function successCallback(response) {
                        resolve(response);
                    }, function errorCallback(response) {
                        reject(response);
                    });
                });
            };
        }
    ]);

}).call(this);