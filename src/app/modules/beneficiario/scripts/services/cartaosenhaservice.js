(function () {
    'use strict';
    angular.module('portalUsuarioApp').serviceLazy("CartaoSenhaService", [
        '$q', '$resource', '$http', function ($q, $resource, $http) {

            var self;
            self = this;

            self.getKeyIvCript = function (idUsuario) {

                return $q(function (resolve, reject) {
                    
                    var resource = $resource(config.urls.ticketSecurity);

                    resource.get({ idUsuario: idUsuario }, function (response) {
                        resolve(response.value);
                    }, function (response) {
                        reject(response);
                    });

                });

            };
            
        }

    ]);

}).call(this);