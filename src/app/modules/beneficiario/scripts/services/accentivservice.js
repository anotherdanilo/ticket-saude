(function () {
    'use strict';
    angular.module('portalUsuarioApp').serviceLazy("AccentivService", [
        'AccentivResource', '$q', '$resource', '$http', function (AccentivResource, $q, $resource, $http) {

            var self;
            self = this;

            self.cadastrarCartao = function (obj) {
                
                return $q(function (resolve, reject) {

                    AccentivResource.save({
                        id: obj.idUsuario
                    }, obj.cartao, function (response) {
                        resolve(response);
                    }, function (error) {
                        var mensagemErro;
                        mensagemErro = "Falha de conexão com o servidor.";
                        if (error.data !== null) {
                            mensagemErro = error.data.message;
                        }
                        reject(mensagemErro);
                    });

                });

            };

            self.obterCartao = function (nrCartao) {

                return $q(function (resolve, reject) {

                    var resource = $resource(config.urls.obterCartaoAccentiv);

                    resource.save({ cardNumber: nrCartao }, function (response) {
                        
                        resolve(response);
                    }, function (response) {
                        
                        reject(response);
                    });

                });

            };

            self.ativarCartao = function (nrCartao, pin) {

                return $q(function (resolve, reject) {

                    var resource = $resource(config.urls.ativarCartaoAccentiv);

                    resource.save({ cardNumber: nrCartao, cardPin: pin }, function (response) {
                        resolve(response);
                    }, function (response) {
                        reject(response);
                    });

                });

            };

            self.bloqueioCartao = function (number) {

                return $q(function (resolve, reject) {

                    var resource = $resource(config.urls.bloquearCartaoAccentiv);

                    resource.save({ cardNumber: number }, function (response) {
                        resolve(response);
                    }, function (response) {
                        reject(response);
                    });

                });

            };

            self.desbloqueioCartao = function (nrCartao, pin) {

                return $q(function (resolve, reject) {

                    var resource = $resource(config.urls.desbloquearCartaoAccentiv);

                    resource.save({ "cardNumber": nrCartao, "cardPin": pin }, function (response) {
                        resolve(response);
                    }, function (response) {
                        reject(response);
                    });

                });

            };

            self.getToken = function () {

                return $q(function (resolve, reject) {

                    var resource = $resource(config.urls.desbloquearCartaoAccentiv);

                    resource.save({ idusuario: obj.idUsuario, idcartao: obj.idCartao }, obj, 
                    function (response) {
                        resolve(response);
                    }, function (response) {
                        reject(response);
                    });

                });

            };

            self.saldoCartao = function (nrCartao) {

                return $q(function (resolve, reject) {

                    var resource = $resource(config.urls.saldoCartaoAccentiv);

                    resource.save({ cardNumber: nrCartao }, function (response) {
                        resolve(response);
                    }, function (response) {
                        reject(response);
                    });

                });

            };

            self.extratoCartao = function (nrCartao) {

                
                var obj = {
                    "cardNumber": nrCartao,
                    "dateFrom": moment().add('days', -90).format('YYYY-MM-DD'),
                    "dateTo": moment().format('YYYY-MM-DD'),
                    "page": 1,
                    "max": 10000
                };

                return $q(function (resolve, reject) {

                    var resource = $resource(config.urls.extratoCartaoAccentiv);

                    resource.save(obj, function (response) {
                        resolve(response);
                    }, function (response) {
                        reject(response);
                    });

                });

            };

        }

    ]);

}).call(this);

