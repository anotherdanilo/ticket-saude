(function () {
    'use strict';
    angular.module('portalUsuarioApp').serviceLazy("CartaoService", [
        'CartaoResource', '$q', '$resource', '$http', function (CartaoResource, $q, $resource, $http) {

            var self;
            self = this;

            self.IdCartaoSelecionado = null;

            self.obterCartaoPorUsuario = function (idUsuario) {
                return $q(function (resolve, reject) {
                    CartaoResource.get({
                        idUsuario: idUsuario
                    }, function (response) {
                        resolve(response.value);
                    }, function (error) {
                        var mensagemErro;
                        mensagemErro = "Falha de conexão com o servidor.";
                        if (error.data !== null) {
                            mensagemErro = error.data.Message;
                        }
                        reject(mensagemErro);
                    });
                });
            };

            self.obterCartaoPorIdService = function (idCartao) {

                return $q(function (resolve, reject) {

                    var resource = $resource(config.urls.rotaCartaoAPI);

                    resource.get({ id: idCartao }, function (response) {
                        resolve(response);
                    }, function (response) {
                        reject(response);
                    });

                });
            };


            // self.obterCartaoPorNumeroService = function(numeroCartao) {

            //     return $q(function(resolve, reject) {

            //         var resource = $resource(config.urls.obterPorNumeroCartao);

            //         resource.get({ numeroCartao: numeroCartao }, function(response) {
            //             resolve(response);
            //         }, function(response) {
            //             reject(response);
            //         });

            //     });
            // };

            self.alterarCartaoService = function (obj) {

                return $q(function (resolve, reject) {
                    CartaoResource.update({
                        idUsuario: obj.idUsuario
                    }, obj.cartao, function (response) {
                        resolve(response);
                    }, function (error) {
                        reject(error);
                    });
                });
            };

            self.excluirCartaoService = function (idUsuario, cartao) {

                return $q(function (resolve, reject) {
                    $http({
                        method: 'DELETE',
                        url: config.urls.excluirCartaoAPI.replace(':idUsuario', idUsuario),
                        data: cartao,
                        headers: {
                            'content-type': 'application/json;charset=utf-8',
                            'number': cartao.number,
                            'origemWeb': cartao.OrigemWeb
                        }
                    }).then(function successCallback(response) {
                        resolve(response);
                    }, function errorCallback(response) {
                        reject(response);
                    });
                });
            };

            //self.verificarRecaptchaService = function (responseRecaptcha) {

            //    return $q(function(resolve, reject) {

            //        var resource = $resource(config.urls.recaptcha);

            //        resource.save("\"" + responseRecaptcha + "\"", function(response) {


            //            if (response.success) {

            //                resolve(response);

            //            }
            //            else { //success = false
            //                reject(response);
            //            }

            //        }, function(error) {

            //            reject(error);

            //        });

            //    });
            //};


            self.cadastrarCartao = function (obj) {
                obj.cartao.OrigemWeb = true;
                return $q(function (resolve, reject) {
                    debugger;

                    var baseUri = config.urls.newCartaoAPI.replace(":CdUsuario", obj.idUsuario);
                    var resource = $resource(baseUri);

                    resource.save(obj.cartao, function (response) {
                        resolve(response);
                    }, function (error) {
                        var mensagemErro;
                        mensagemErro = "Falha de conexão com o servidor.";
                        if (error.data !== null) {
                            mensagemErro = error.data.message;
                        }
                        reject(mensagemErro);
                    });
                });
            };

            self.carregarCartoesPorUsuarioService = function (idUsuario) {

                return $q(function (resolve, reject) {

                    var resource = $resource(config.urls.getSimplificados);

                    resource.get({ CdUsuario: idUsuario, origemWeb: true }, function (response) {
                        resolve(response);
                    }, function (error) {
                        reject(error);
                    });
                });

            };

            self.carregarExtratoCartaoService = function(idUsuario, idCartao){

                return $q(function (resolve, reject) {

                    var resource = $resource(config.urls.getExtrato);

                    resource.get({ idUsuario: idUsuario, idCartao: idCartao, origemWeb: true }, function (response) {
                        resolve(response);
                    }, function (error) {
                        reject(error);
                    });
                });

            };

            self.autoServicoCartao = function (idUsuario, obj) {

                return $q(function (resolve, reject) {

                    var resource = $resource(config.urls.autoServicoAPI);

                    resource.save({ idUsuario: idUsuario }, obj, function (response) {
                        
                        resolve(response);
                    }, function (error) {
                        
                        try {

                            var erro = JSON.parse(error.data.message);

                            if (erro.IsSuccess) {
                                
                                if (erro.links.length === 3) {

                                    var ultimaUrl = erro.links[1].uri;

                                    var response = {
                                        value: {
                                            protocolo: ultimaUrl.substring(ultimaUrl.lastIndexOf("/") + 1)
                                        }
                                    };

                                    resolve(response);
                                } else {
                                    reject(error);
                                }

                            } else {
                                reject(error);
                            }

                        } catch (e) {

                            reject(error);
                        }

                    });

                });

            };

            self.favoritarCartao = function (idUsuario, numeroCartao) {
                return $q(function (resolve, reject) {

                    var resource = $resource(config.urls.favoritarCartao);

                    resource.save({ idUsuario: idUsuario }, numeroCartao, function (response) {

                        if (response.success) {
                            resolve(response);
                        }
                        else { //success = false
                            reject(response);
                        }

                    }, function (error) {

                        reject(error);

                    });

                });
            };

            self.consultarSaldo = function (numeroCartao, recaptcha) {
                return $q(function (resolve, reject) {

                    var resource = $resource(config.urls.consultaSaldo);

                    var param = {
                        "numeroCartao": numeroCartao,
                        "tokenRecaptcha": recaptcha,
                        "origemWeb": true
                    };

                    resource.save(null, param, function (response) {

                        if (response.success) {
                            resolve(response);
                        }
                        else { //success = false
                            reject(response);
                        }

                    }, function (error) {

                        reject(error);

                    });

                });
            };

            self.carregarCartaoPorNumero = function (numeroCartao) {
                return $q(function (resolve, reject) {

                    var resource = $resource(config.urls.obterPorNumeroCartao);
                    resource.get({ numeroCartao: numeroCartao }, function (response) {


                        if (response.success) {
                            resolve(response);
                        } else {
                            reject(response);

                        }
                    }, function (error) {
                        var mensagemErro;

                        mensagemErro = "Falha de conexão com o servidor.";
                        if (error.data !== null) {

                            mensagemErro = error.message;
                        }
                        reject(mensagemErro);
                    });

                });
            };

        }

    ]);

}).call(this);

