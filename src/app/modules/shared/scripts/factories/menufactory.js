(function() {
    'use strict';

    angular.module('portalUsuarioApp').factory('MenuFactory', [
        '$rootScope',
        function($rootScope) {
            var service = {
                open: open,
                close: close
            };

            return service;


            function open() {
                $rootScope.$broadcast('tkt-open-menu');
            }

            function close() {
                $rootScope.$broadcast('tkt-close-menu');
            }
        }
    ]);

})();
