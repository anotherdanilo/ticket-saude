﻿(function () {
    'use strict';

    angular.module('portalUsuarioApp').factoryLazy('HomeFactory', [
      function () {

          var parametros = {
            baixaApp: false
          };

          // set service
          var service = {
              parametros: parametros
          };

          return service;

      }
    ]);

})();
