(function() {
  'use strict';

  angular.module('portalUsuarioApp').factory('CriptoFactory', [
    function() {

      var service = {
        criptografar: criptografar,
        descriptografar: descriptografar
      };

      return service;
      
      function criptografar(texto, key, iv){
        
        var base64 = CryptoJS.enc.Base64.parse(key);
        var iv64 = CryptoJS.enc.Base64.parse(iv);
        
        var encrypted = CryptoJS.AES.encrypt(
            texto,
            base64,
            {
              iv: iv64,
              keySize: 256,
              mode: CryptoJS.mode.CBC,
              padding: CryptoJS.pad.Pkcs7
            });
        
        return encrypted.toString();
        
      }
      
      function descriptografar(cripto, key, iv){
        
        var base64 = CryptoJS.enc.Base64.parse(key);
        var iv64 = CryptoJS.enc.Base64.parse(iv);
        
        var cipherParams = CryptoJS.lib.CipherParams.create(
            {
              ciphertext: CryptoJS.enc.Base64.parse(cripto)
            });
          
        var decrypted = CryptoJS.AES.decrypt(
            cipherParams,
            base64,
            {
              iv: iv64,
              mode: CryptoJS.mode.CBC,
              padding: CryptoJS.pad.Pkcs7
            });
            
        return decrypted.toString(CryptoJS.enc.Utf8); 
        
      }
      
    }
  ]);

})();
