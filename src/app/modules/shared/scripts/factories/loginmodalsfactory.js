(function() {
    'use strict';

    angular.module('portalUsuarioApp').factory('LoginModalsFactory', [
        '$uibModal',
        function($uibModal) {

            var loginModalInstance,
                signUpModalInstance,
                confirmModalInstance;

            var escopo = {
                step: 0,
                objUsuario: null
                // {
                //     token: 0,
                //     NomeCompleto: "",
                //     Email: "",
                //     Sexo: ""
                // }
            };

            var parametros = {
                urlRetorno: '',
                urlRetornoParam: '',
                email: '',
                d: null, //parametro usado na tela beneficioclubdetalhe para abrir as denuncias
                reload: false,
                mensagem: '',
                esqueciMinhaSenha: false
            };

            function setEscopo(step, obj) {
                escopo.step = step;
                if (obj) {
                    escopo.objUsuario = obj;
                }
            }

            function getEscopo() {
                return escopo;
            }

            var service = {
                openLogin: openLogin,
                openSignUp: openSignUp,
                changeToSignUp: changeToSignUp,
                closeSignUp: closeSignUp,
                changeToLogin: changeToLogin,
                closeLogin: closeLogin,
                changeToConfirm: changeToConfirm,
                closeConfirm: closeConfirm,
                setParametrosSignUp: setParametrosSignUp,
                parametros: parametros,
                getEscopo: getEscopo,
                setEscopo: setEscopo
            };

            return service;

            function openLogin() {
                loginModalInstance = $uibModal.open({
                    animation: true,
                    templateUrl: 'app/modules/autenticacao/modal/login.htm',
                    windowClass: '-ticket -login',
                    size: 'sm'
                });
            }

            function closeLogin() {
                loginModalInstance.dismiss();
            }

            //CADASTRO.HTM///////////////////////////////////////////////////////////////////
            function setParametrosSignUp(urlRetorno, urlRetornoParam) {
                parametros.urlRetorno = urlRetorno;
                parametros.urlRetornoParam = urlRetornoParam;
            }

            function openSignUp() {
                signUpModalInstance = $uibModal.open({
                    animation: true,
                    templateUrl: 'app/modules/beneficiario/modal/cadastro.htm',
                    windowClass: '-ticket -sign-up',
                    size: 'sm'
                });
            }

            function closeSignUp() {
                signUpModalInstance.dismiss();
            }
            ///////////////////////////////////////////////////////////////////////////////

            function openConfirm() {
                confirmModalInstance = $uibModal.open({
                    animation: true,
                    templateUrl: 'app/modules/beneficiario/modal/confirmacaocadastro.htm',
                    windowClass: '-ticket -confirm',
                    size: 'sm'
                });
            }

            function closeConfirm() {
                confirmModalInstance.dismiss();
            }

            function changeToSignUp() {
                loginModalInstance.dismiss();
                openSignUp();
            }

            function changeToLogin() {
                signUpModalInstance.dismiss();
                openLogin();
            }

            function changeToConfirm() {
                signUpModalInstance.dismiss();
                openConfirm();
            }
        }
    ]);

})();
