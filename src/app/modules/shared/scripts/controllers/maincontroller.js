(function() {
    'use strict';
    angular.module('portalUsuarioApp').controller('MainController', [
        '$scope', '$rootScope',
        function($scope, $rootScope) {

            var main = this;
            //$rootScope.bodyClass = 'menu-opened';

            $rootScope.jaAcessouADashboard = false;

            $scope.metadata = {
                'title': '',
                'description': '',
                'path': '',
                'imageurl': ''
            };

            $scope.$on('newPageLoaded', function(event, metadata) {
                $scope.metadata = metadata;
            });


            $scope.$on('tkt-open-menu', function(event, args) {
                $rootScope.bodyClass = 'menu-opened';
            });

            $scope.$on('tkt-close-menu', function(event, args) {
                $rootScope.bodyClass = '';
            });

            $scope.$on('$viewContentLoaded', function() {
                window.prerenderReady = false;
            });

        }

    ]);

}).call(this);
