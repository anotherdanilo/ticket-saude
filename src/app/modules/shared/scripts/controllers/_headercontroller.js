(function () {
    'use strict';

    angular.module('portalUsuarioApp').controllerLazy('HeaderController', [
        'HomeFactory', '$document', '$scope', 'LoginModalsFactory', 'MenuFactory', '$location', 'AutenticacaoService', '$rootScope', '$uibModal', 'UsuarioModalsFactory', 'AlertaFactory', '$window','$browser',
        function (HomeFactory, $document, $scope, LoginModalsFactory, MenuFactory, $location, AutenticacaoService, $rootScope, $uibModal, UsuarioModalsFactory, AlertaFactory, $window, $browser) {

            var header = this;

            //header.isLogged = true;
            $rootScope.menuSelecionado = 10;

            header.openLogin = LoginModalsFactory.openLogin;
            header.openSignUp = LoginModalsFactory.openSignUp;
            header.openMenu = MenuFactory.open;
            header.parametrosLogin = LoginModalsFactory.parametros;
            header.openAlerta = AlertaFactory.openAlerta;
            header.openChangePwd = UsuarioModalsFactory.openForgetPwd;
            header.closeChangePwd = UsuarioModalsFactory.closeChangePwd;

            //var usuarioLogado = AutenticacaoService.obterUsuarioLogado();
            // if (AutenticacaoService.usuarioLogado) {
            //     header.userName = AutenticacaoService.usuarioLogado.value.name;
            //     
            // }

            // $scope.$on('tkt-close-menu-logout', function(event, args) {
            //     header.usuarioLogado.isLogged = false;
            // });


            //Guarda a referencia da memória.

            header.usuarioLogado = AutenticacaoService.obterUsuarioLogado();




            var location = $location.search();
            
            if (location.t) //verifica o parametro de primeiro acesso
            {
                var promise;
                var isOnlyValidateToken = false;

                if (location.rs) {
                    isOnlyValidateToken = true;
                }

                
                promise = AutenticacaoService.validarEmail(location.t, isOnlyValidateToken);

                promise.then(function (response) {

                    header.parametrosLogin.email = response.email;

                    if (location.rs) {
                        UsuarioModalsFactory.setEscopo(3);
                        header.openChangePwd();
                    }
                    else {

                        header.openLogin();

                        AlertaFactory.parametros.titulo = "Ativação de cadastro";
                        AlertaFactory.parametros.mensagem = "Cadastro ativado com sucesso.";
                        AlertaFactory.parametros.icone = "icon-done";

                        header.openAlerta();
                    }

                }, function (error) {
                    AlertaFactory.parametros.titulo = "Ativação de cadastro";

                    if (error.indexOf('token expirado') >= 0) {
                        AlertaFactory.parametros.mensagem = "Por questões de segurança, o link que enviamos para você trocar sua senha expirou, mas fique tranquilo, enviamos outro e-mail contendo um novo link para que você realize a operação.";
                    } else {
                        AlertaFactory.parametros.mensagem = error;
                    }

                    AlertaFactory.parametros.icone = "icon-error";

                    header.openAlerta();
                });
            }

            header.home = function () {
                $rootScope.menuSelecionado = 0;
                window.location.href = $browser.baseHref();
                window.scrollTo(0,0);
            };

            header.toDashboard = function () {
                $location.path('/beneficiario/dashboard');
            };

            header.toRedeCredenciada = function () {
                $rootScope.menuSelecionado = 1;
                $location.path("/rede-credenciada");
            };

            header.toBaixeApp = function() {

                
                if (window.location.pathname === $browser.baseHref()) {
                    var goToCover = angular.element(document.getElementById("cover-app"));
                    $document.scrollToElementAnimated(goToCover, 120);
                } else {
                    HomeFactory.parametros.baixaApp = true;
                    $rootScope.menuSelecionado = 0;
                    $location.path($browser.baseHref());
                }

                

            };

            header.beneficioClub = function () {
                $rootScope.menuSelecionado = 2;
                $location.path('/beneficio-club');
                //$window.location.href = 'beneficio-club';
                //window.location.href = "#/beneficio-club";
            };

            header.toAtendimento = function () {
                $rootScope.menuSelecionado = 3;
                $location.path('/beneficiario/atendimento');
            };

            header.toConsultaSaldo = function () {
                $rootScope.menuSelecionado = 4;
                //$window.location.href = '/consulta-saldo';
                $location.path('/consulta-saldo');
            };

            header.openLogin = function () {
                UsuarioModalsFactory.setEscopo(0);
                LoginModalsFactory.openLogin();

            };

            header.goToTicket = function () {
                $rootScope.menuSelecionado = 0;
                $location.path($browser.baseHref());
            };
        }
    ]);

})();
