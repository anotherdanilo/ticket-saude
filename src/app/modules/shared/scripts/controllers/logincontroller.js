(function() {
  'use strict';
  angular.module('portalUsuarioApp').controllerLazy('LoginController', [
    function() {
      var login = this;
      login.content = "recover";

      login.setContent = function(setContent) {
        login.content = setContent;
      };

      login.isSet = function(checkContent) {
        return login.content === checkContent;
      };
    }
  ]);

}).call(this);
