(function () {
    'use strict';
    angular.module('portalUsuarioApp').controller('MenuController', [
        'LoginModalsFactory', 'UsuarioModalsFactory', '$document', 'HomeFactory', '$cookies', 'MenuFactory', '$scope', 'AutenticacaoService', '$location', 'CartaoService', '$rootScope', '$window', 'AccentivService', 'AccentivFactory','$browser',
        function (LoginModalsFactory, UsuarioModalsFactory, $document, HomeFactory, $cookies, MenuFactory, $scope, AutenticacaoService, $location, CartaoService, $rootScope, $window, AccentivService, AccentivFactory, $browser) {

            var menu = this;

            menu.isOpen = false;
            menu.close = MenuFactory.close;
            menu.myPromise = null;
            menu.listaCartoes = [];

            menu.usuario = AutenticacaoService.obterUsuarioLogado();

            // if (menu.usuario.nome !== undefined) {
            //     menu.usuario.nome = menu.usuario.nome.split(' ')[0];
            // }

            $scope.$on('tkt-open-menu', function (event, args) {
                menu.isOpen = true;
                menu.carregarCartoes();
            });

            $scope.$on('tkt-close-menu', function (event, args) {
                menu.isOpen = false;
                menu.listaCartoes = [];
            });

            menu.toDash = function (obj) {
                menu.close();
                $location.path('/beneficiario/dashboard');
            };

            menu.toDashboard = function (obj) {
                menu.close();

                if ($location.$$url.indexOf('/beneficiario/dashboard') > -1) {
                    $rootScope.setCard(obj);
                } else {
                    $location.path('/beneficiario/dashboard');
                    CartaoService.IdCartaoSelecionado = obj;
                }
            };

            menu.toRedeCredenciada = function (obj) {
                menu.close();
                $window.scrollTo(0, 0);
                $location.path('/rede-credenciada');
            };

            menu.toAtendimentoDigital = function (obj) {
                menu.close();
                $window.scrollTo(0, 0);
                $rootScope.menuSelecionado = 3;
                $location.path('/beneficiario/atendimento');
            };

            menu.toMeuCadastro = function () {
                menu.close();
                $location.path('/beneficiario/usuario/editarusuario');
            };

            menu.toBeneficioClub = function () {

                menu.isOpen = false;
                $rootScope.menuSelecionado = 2;
                $location.path('/beneficio-club');
            };

            menu.toConsultaSaldo = function () {
                menu.close();
                $window.scrollTo(0, 0);
                $rootScope.menuSelecionado = 4;
                $location.path('/consulta-saldo');
            };

            menu.toBaixeApp = function () {
                menu.close();

                if (window.location.hash === "#/") {
                    var goToCover = angular.element(document.getElementById("cover-app"));
                    $document.scrollToElementAnimated(goToCover, 120);
                } else {
                    HomeFactory.parametros.baixaApp = true;
                }

                $rootScope.menuSelecionado = 0;
                window.location.href = "#/";
            };

            menu.toHome = function () {
                menu.close();
                $window.scrollTo(0, 0);
                $rootScope.menuSelecionado = 0;
                window.location.href = $browser.baseHref();
            };

            menu.carregarCartoes = function () {

                if (menu.usuario.isLogged) {

                    menu.listaCartoes = [];

                    menu.myPromise = CartaoService.carregarCartoesPorUsuarioService(menu.usuario.id);

                    menu.myPromise.then(function (response) {

                        //wait ten seconds before continuing
                        if (response.success) {

                            var balances = response.value;

                            angular.forEach(balances, function (value, key) {
                                var balance = value.balance;

                                if (balance.bin === "ACT") {
                                    debugger;
                                    menu.listaCartoes.push({

                                        id: balance.id,
                                        number: balance.number,
                                        name: balance.id,
                                        apelido: balance.nickName,
                                        favorito: balance.favorito,
                                        bin: balance.bin,
                                        classExtrato: "card-details -carregando",
                                        validado: true,
                                        accentiv: true,
                                        carregando: true
                                    });

                                }
                                else {

                                    var month = "-";
                                    var day = "-";
                                    if (balance.dateNextDeposit !== null) {
                                        month = moment(balance.dateNextDeposit, "DD/MM/YYYY").format("MMM");
                                        day = moment(balance.dateNextDeposit, "DD/MM/YYYY").format("DD");
                                    }

                                    var valorProximoCredito = "-";
                                    if (JSLINQ(value.scheduling).First() !== null) {
                                        valorProximoCredito = JSLINQ(value.scheduling).First().value;
                                    }

                                    menu.listaCartoes.push({
                                        id: balance.id,
                                        number: balance.number,
                                        name: balance.id,
                                        apelido: balance.nickName,
                                        favorito: balance.favorito,
                                        situacao: balance.situacao,
                                        bin: balance.bin,
                                        diaProximoCredito: day,
                                        validado: balance.validado,
                                        mesProximoCredito: month.toUpperCase(),
                                        valorProximoCredito: valorProximoCredito,
                                        extrato: value.release,
                                        deposit: balance.deposit,
                                        value: (balance.valueParsed === null ? 0 : balance.valueParsed)
                                    });
                                }
                            });

                            carregarCartoesAccentiv();
                        }

                        menu.myPromise = null;

                    }, function (error) {
                        menu.myPromise = null;
                        //$rootScope.bodyClass = "";

                    });

                }

            };

            function carregarCartoesAccentiv() {

                angular.forEach(menu.listaCartoes, function (value, key) {

                    debugger;
                    var balance = value;

                    if (balance.bin === 'ACT') {

                        menu.myPromiseAccentiv = AccentivService.obterCartao(balance.number);

                        menu.myPromiseAccentiv.then(function (response) {

                            debugger;
                            var dados = response.IdentificacaoPortadorResult;

                            //busca o status do cartão
                            switch (dados.CodigoRetornoProcessadora) {
                                case 67:
                                    dados.situacao = 4;
                                    break;
                                case 20:
                                    dados.situacao = 3;
                                    break;
                                case 2:
                                    dados.situacao = 5;
                                    break;
                                default:
                                    dados.situacao = 1;
                            }

                            var classExtrato = "card-details ";

                            classExtrato += AccentivFactory.existePersonalizacaoId(dados.PersonalizaoId).css;

                            menu.myPromiseExtrato = AccentivService.extratoCartao(balance.number);

                            menu.myPromiseExtrato.then(function (extrato) {

                                //trativa do extrato
                                var novoExtrato = [];
                                if (extrato.ExtratoLinhas && extrato.ExtratoLinhas.ExtratoLinha) {
                                    angular.forEach(extrato.ExtratoLinhas.ExtratoLinha, function (value, key) {

                                        var dateParsed = moment(value.Data).format("YYYY-MM-DD");

                                        var valueParsed = value.Valor.replace('.', '@').replace(',', '.').replace('@', ',');

                                        novoExtrato.push({
                                            date: value.Data,
                                            dateParsed: dateParsed,
                                            value: value.Valor,
                                            valueParsed: parseFloat(valueParsed),
                                            description: "Compra - " + value.Estabelecimento
                                        });

                                    });
                                }
                                value.situacao = dados.situacao;
                                value.extrato = novoExtrato;
                                value.value = dados.SaldoCartao;
                                value.classExtrato = classExtrato;
                                value.personalizacaoId = dados.PersonalizaoId;
                                value.carregando = false;


                            }, function (errorExtrato) {

                            });

                        }, function (response) {

                            $rootScope.bodyClass = "";

                        });
                    }

                });


            }

            menu.sair = function () {
                debugger;
                $rootScope.bodyClass = "";
                $cookies.remove("Edenred");
                $cookies.remove("EdenredPUA");
                AutenticacaoService.setNomeUsuarioLogado();
                $location.search({});
                $location.path("/");
                menu.isOpen = false;
            };

            menu.openLogin = function () {
                menu.close();
                UsuarioModalsFactory.setEscopo(0);
                LoginModalsFactory.openLogin();

            };

            menu.openSignUp = function () {
                menu.close();
                LoginModalsFactory.openSignUp();
            };

        }
    ]);

}).call(this);
