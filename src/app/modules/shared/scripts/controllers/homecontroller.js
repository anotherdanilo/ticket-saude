(function() {
    'use strict';
    angular.module('portalUsuarioApp').controllerLazy('HomeController', [
        'HomeFactory', '$rootScope', '$scope', '$location', '$document', '$window', 'AutenticacaoService', 'BeneficioClubFactory', 'LoginModalsFactory', 'Facebook', 'UsuarioService',
        function(HomeFactory, $rootScope, $scope, $location, $document, $window, AutenticacaoService, BeneficioClubFactory, LoginModalsFactory, Facebook, UsuarioService) {
            $rootScope.menuSelecionado = 0;

            $scope.$emit('newPageLoaded', {
                'title': 'Sou Ticket | Consulta de Saldo, Rede Credenciada e Mais!',
                'description': 'Sou Ticket, o portal para usuários Ticket, para consultar saldo, ver rede credenciada, bloquear e desbloquear cartões, pedir 2ª via, e mais. Conheça!',
                'path': $location.absUrl(),
                'imageurl': ''
            });

            // Define user empty data :/
            $scope.user = {};

            // Defining user logged status
            $scope.logged = false;

            // And some fancy flags to display messages upon user status change
            $scope.byebye = false;
            $scope.salutation = false;

            var userIsConnected = false;
            var tokenFacebook = "";
            
            Facebook.getLoginStatus(function(response) {
                if (response.status == 'connected') {
                    userIsConnected = true;
                }
            });

            /**
             * IntentLogin
             */
            $scope.IntentLogin = function() {
                $scope.logged = true;
                if (!userIsConnected) {
                    $scope.login();
                }
                else {
                    $scope.me();
                }
            };


            /**
             * Login
             */
            $scope.login = function() {
                Facebook.login(function() {
                    // não faz nada, deixa a resposta a cargo da promise
                }).then(function(response) {
                    if (response.status == 'connected') {
                        tokenFacebook = response.authResponse.accessToken;
                        $scope.logged = true;
                        $scope.me();
                    }

                });
            };

            /**
             * me 
             */
            $scope.me = function() {
                Facebook.api('/me', function() {
                    // não faz nada, deixa a resposta a cargo da promise
                }, { "fields": "email,age_range,birthday,first_name,last_name,gender,locale,updated_time,picture" }).then(function(response) {

                    /**
                     * Using $scope.$apply since this happens outside angular framework.
                     */
                    // $scope.$apply(function() {
                    //     $scope.user = response;
                    // });

                    var promise = AutenticacaoService.autenticar(
                        response.email,
                        null,
                        tokenFacebook,
                        null,
                        null);

                    promise.then(function(response3) {
                        $location.path("/beneficiario/dashboard");
                    }, function(error) {
                        
                        var obj = {
                            token: response.id,
                            NomeCompleto: response.first_name + " " + response.last_name,
                            Email: response.email,
                            Sexo: ""
                        };

                        if (response.gender == "male") {
                            obj.Sexo = "M";
                        }
                        else {
                            obj.Sexo = "F";
                        }

                        home.openCadastroEmail(obj);
                    });
                });
            };

            /**
             * Logout
             */
            $scope.logout = function() {
                Facebook.logout(function() {
                    $scope.$apply(function() {
                        $scope.user = {};
                        $scope.logged = false;
                    });
                });
            };

            var home = this;
            home.usuarioLogado = AutenticacaoService.obterUsuarioLogado();
            home.toConsultaSaldo = function() {
                if (!home.usuarioLogado.isLogged) {
                    //$location.path('/consulta-saldo/');
                    $location.path("/consulta-saldo");

                } else {

                    $location.path('/beneficiario/dashboard');
                }

            };

            home.isNotKarma = true;

            home.openSignUp = LoginModalsFactory.openSignUp;

            home.content = "cover-bemvindo";
            home.setActive = setActive;
            home.isActive = isActive;
            home.checkActive = checkActive;
            home.window = $(window);
            home.covers = $('.tkt-cover');
            home.coversPos = [];

            home.showVideo = false;
            home.loadVideo = loadVideo();
            home.startVideo = startVideo;
            home.playVideo = playVideo;
            home.stopVideo = stopVideo;
            //home.player = player;

            home.scrollTo = scrollTo;
            home.getMiddlePosition = getMiddlePosition();

            //animação dos blocos no scroll
            home.checkVisibility = checkVisibility;

            $document.on('scroll resize', home.checkVisibility);

            function setActive(cover) {
                home.content = cover;
            }

            function isActive(cover) {
                return home.content === cover;
            }

            function checkActive(cover) {
                if (home.content != cover) {
                    home.setActive(cover);
                }
                //console.log(home.section02);
                //home.goToCover = angular.element(document.getElementById(cover));
                //$document.scrollToElementAnimated(home.goToCover);
            }

            //Função para animação no scroll
            function checkVisibility() {
                var w_height = home.window.height();
                var w_top = home.window.scrollTop();
                var w_bottom = (w_top + w_height);

                $.each(home.covers, function() {
                    var el = $(this);
                    var el_height = el.outerHeight();
                    // var el_top = el.offset().top + 200;
                    // var el_bottom = ((el_top-200) + el_height);
                    var el_top = el.offset().top;
                    var el_bottom = el_top + el_height;

                    if ((el_bottom - 240 >= w_top) &&
                        (el_top + 240 <= w_bottom)) {
                        el.addClass('visible');
                    } else {
                        //if enabled, runs animation every time the sections is visible again
                        el.removeClass('visible');
                    }

                    if ((w_bottom >= 0) && (w_bottom < home.coversPos[1])) {
                        home.setActive('cover-bemvindo');
                    }
                    else if ((w_bottom >= home.coversPos[1]) && (w_bottom < home.coversPos[2])) {
                        home.setActive('cover-autosservico');
                    }
                    else if ((w_bottom >= home.coversPos[2]) && (w_bottom < home.coversPos[3] + 200)) {
                        home.setActive('cover-estabelecimentos');
                    }
                    else if ((w_bottom >= home.coversPos[3] + 200) && (w_bottom < home.coversPos[4])) {
                        home.setActive('cover-bc');
                    }
                    else if ((w_bottom >= home.coversPos[4]) && (w_bottom < home.coversPos[5] + 200)) {
                        home.setActive('cover-depoimentos');
                    }
                    else if ((w_bottom >= home.coversPos[5] + 200) && (w_bottom < home.coversPos[6])) {
                        home.setActive('cover-app');
                    }
                    // else if ((w_bottom >= home.coversPos[6]) && (w_bottom < home.coversPos[7])) {
                    //     home.setActive('cover-cadastro');
                    // }
                    else if (w_bottom >= home.coversPos[6]) {
                        home.setActive('cover-cadastro');
                    }
                });
            }

            function getMiddlePosition() {
                $.each(home.covers, function() {
                    var el = $(this);
                    var el_height = el.outerHeight();
                    var el_top = el.offset().top;
                    var el_bottom = el_top + el_height;
                    var el_middle = el_top + ((el_bottom - el_top) / 2);
                    home.coversPos.push(el_middle);
                    //console.log(el.attr('id') + '/' + el_height + '/' + el_middle);
                });
                //Fix for slick height problem
                for (var i = 5; i < 8; i++) {
                    home.coversPos[i] = home.coversPos[i] - 500;
                }
            }

            function scrollTo(cover, offset) {
                var goToCover = angular.element(document.getElementById(cover));
                $document.scrollToElementAnimated(goToCover, offset);
            }

            function loadVideo() {
                //home.showVideo = true;
                //Youtube API load video
                var tag = document.createElement('script');
                tag.src = "https://www.youtube.com/iframe_api";

                var firstScriptTag = document.getElementsByTagName('script')[0];
                firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
            }

            $window.onYouTubeIframeAPIReady = function() {
                home.player = new YT.Player(document.getElementById('player'), {
                    videoId: 'Nxv3tb02yFE',
                    events: {
                        'onReady': home.startVideo
                    }
                });
            };

            function startVideo() {

            }
            function playVideo() {
                home.showVideo = true;
                home.player.playVideo();
            }
            function stopVideo() {
                home.showVideo = false;
                home.player.stopVideo();
            }

            home.goToBenefioClub = function(categoria) {

                $window.scrollTo(0, 0);

                if (categoria) {
                    $location.path("/beneficio-club/p/" + categoria);
                } else {
                    $location.path("/beneficio-club");
                }

            };

            home.goToRedeCredenciada = function() {

                $location.path("/rede-credenciada");
            };

            home.goToAtendimento = function() {

                if (!home.usuarioLogado.isLogged) {

                    LoginModalsFactory.parametros.mensagem = 'Para gerenciar seus cartões, você precisa estar cadastrado ou fazer seu login clicando em ‘Entrar’.';

                    home.openSignUp();
                } else {

                    $location.path("/beneficiario/atendimento");

                }
            };

            home.openCadastroEmail = function(obj) {

                LoginModalsFactory.setEscopo(1, obj);

                home.openSignUp();

            };


            if (HomeFactory.parametros.baixaApp) {
                window.scrollTo(0, 0);
                setTimeout(function() { home.scrollTo('cover-app', 120); }, 1000);
                HomeFactory.parametros.baixaApp = false;
            }

        }
    ]);

}).call(this);
