(function() {
    'use strict';
    angular.module('portalUsuarioApp')
        .service("prebidService", [ '$q',
            function ($q) {
                var pbjs;

                this.getBid = function(slotName) {
                    var slot = slots[slotName];

                    pbjs.que.push(function () {
                        pbjs.requestBids({
                            timeout: PREBID_TIMEOUT,
                            adUnitCodes: [slotName],
                            bidsBackHandler: function () {
                                pbjs.setTargetingForGPTAsync([slotName]);
                                googletag.pubads().refresh([slot]);
                            }
                        });
                    });
                };

                this.getRedeCredenciadaAds = function() {
                    if (window['pbjs']) {
                        pbjs = window['pbjs'];

                        this.getBid('rede-credenciada_lateral-esq-hor_320x50_fixo');
                        this.getBid('rede-credenciada_footer-hor_728x90_fixo');

                    } else {
                        console.error('OOPS, pbjs não carregado');
                    }
                };

                this.getConsultaSaldoAds = function () {
                    if (window['pbjs']) {
                        pbjs = window['pbjs'];

                        this.getBid('consulta-saldo_header-hor_970x90_fixo');
                        this.getBid('consulta-saldo_dashboard_footer-hor_970x90_fixo');

                    } else {
                        console.error('OOPS, pbjs não carregado');
                    }
                };

                this.getBeneficiarioAds = function () {
                    if (window['pbjs']) {
                        pbjs = window['pbjs'];

                        this.getBid('beneficiario_dashboard_header-hor_970x90_fixo');
                        this.getBid('beneficiario_atendimento_footer-hor_970x90_fixo');

                    } else {
                        console.error('OOPS, pbjs não carregado');
                    }
                };
            }
        ]);
}).call(this);