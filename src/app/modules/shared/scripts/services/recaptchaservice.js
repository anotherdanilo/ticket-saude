﻿(function () {
    'use strict';
    angular.module('portalUsuarioApp').service("RecaptchaService", [
        '$q', '$resource', function ($q, $resource) {
           
            var self;
            self = this;

            self.verificarRecaptchaService = function (responseRecaptcha) {

                return $q(function (resolve, reject) {

                    var resource = $resource(config.urls.recaptcha);
                    var model = {
                        "ResponseRecaptcha": responseRecaptcha
                    };
                    resource.save(model, function (response) {

                        if (response.success) {

                            resolve(response);

                        }
                        else { //success = false
                            reject(response);
                        }

                    }, function (error) {

                        reject(error);

                    });

                });
            };

        }

    ]);

}).call(this);

