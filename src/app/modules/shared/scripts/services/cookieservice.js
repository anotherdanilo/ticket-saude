(function() {
    'use strict';
    angular.module('portalUsuarioApp').service("cookieService", [
        '$cookies', '$base64', function($cookies, $base64) {
            this.criarCookieAutenticacao = function (token, manterconectado) {
                var cookie;
                cookie = config.nomeCookieSSO + '=' + JSON.stringify(token);
                if (manterconectado) {
                    cookie = cookie + ';expires=' + config.horasExpiracaoCookie.hours().fromNow().toString();
                }
                document.cookie = cookie;
            };
            this.criarCookieUsuario = function(usuario) {
                var cookie;
                cookie = config.nomeCookieUsuario + '=' + $base64.encode(JSON.stringify(usuario));
                if (usuario.manterconectado) {
                    cookie = cookie + ';expires=' + config.horasExpiracaoCookie.hours().fromNow().toString();
                }

                document.cookie = cookie;
            };
            this.obterValorCookieAutenticacao = function(nomeCookieSSO) {
                var cookie;
                if (nomeCookieSSO) {
                    cookie = $cookies.get(nomeCookieSSO);
                }
                else {
                    cookie = $cookies.get(config.nomeCookieSSO);
                }

                if (cookie) {
                    return JSON.parse(cookie);
                }
            };
            this.obterValorCookieUsuario = function(nomeCookieUsuario) {
                var cookie;
                if (nomeCookieUsuario) {
                    cookie = $cookies.get(nomeCookieUsuario);
                }
                else {
                    cookie = $cookies.get(config.nomeCookieUsuario);
                }

                if (cookie) {
                    return JSON.parse($base64.decode(cookie));
                }
            };
            this.alterarManterConectado = function(valor){

                var cookie;

                cookie = $cookies.get(config.nomeCookieUsuario);
                var usuarioCookie = JSON.parse($base64.decode(cookie));

                usuarioCookie.cadastroCompleto = valor;

                cookie = config.nomeCookieUsuario + '=' + $base64.encode(JSON.stringify(usuarioCookie));

                if (usuarioCookie.manterconectado) {
                    cookie = cookie + ';expires=' + config.horasExpiracaoCookie.hours().fromNow().toString();
                }
                
                document.cookie = cookie;
            };
            this.alterarRedirecionando = function(valor){

                var cookie;

                cookie = $cookies.get(config.nomeCookieUsuario);
                var usuarioCookie = JSON.parse($base64.decode(cookie));

                usuarioCookie.redirecionando = valor;

                cookie = config.nomeCookieUsuario + '=' + $base64.encode(JSON.stringify(usuarioCookie));

                if (usuarioCookie.manterconectado) {
                    cookie = cookie + ';expires=' + config.horasExpiracaoCookie.hours().fromNow().toString();
                }
                
                document.cookie = cookie;
            };
            
        }
    ]);
}).call(this);
