﻿(function () {
    'use strict';

    angular.module('portalUsuarioApp').controllerLazy('BeneficioClubDetalheController', [
      'Socialshare', '$compile', 'BeneficioClubDetalheFactory', '$location', 'LoginModalsFactory', 'BeneficioService', '$rootScope', '$uibModal', '$scope', 'AutenticacaoService', '$window',
      function (Socialshare, $compile, BeneficioClubDetalheFactory, $location, LoginModalsFactory, BeneficioService, $rootScope, $uibModal, $scope, AutenticacaoService, $window) {

          
          $rootScope.menuSelecionado = 2;

          var oferta = this;

          oferta.promise = null;

          oferta.filtros = {

              category: null,
              countView: null,
              type: null,
              id: null,
              qtd: 2,
              offerNotId: null

          };

          oferta.item = null;
          oferta.isLogged = true;

          oferta.resultadoState = null;
          oferta.cidades = null;
          oferta.bairros = null;

          oferta.title = "Cheque Teatro";
          oferta.offerData = BeneficioClubDetalheFactory.bindOfferData();
          //oferta.estados = oferta.offerData.locationStateOpts;

          oferta.usuarioLogado = AutenticacaoService.obterUsuarioLogado();

          oferta.unidadesParticipantes = [];
          oferta.enderecos = [];
          oferta.resultadoEnderecos = [];

          var url = $location.search();


          oferta.qtdPromisesEsperados = 4;
          oferta.qtdPromisesConcluidos = 0;

          oferta.maxIntroduction = 200;

          oferta.pageLoad = function () {
              $window.scrollTo(0, 0);

              $(".loading-mask-position").css("top", document.body.scrollTop);
              $rootScope.bodyClass = "menu-opened";

              oferta.offerData.imagens = [];

              oferta.promise = BeneficioService.listarPromocoes(oferta.filtros);

              oferta.promise.then(function (response) {
                  
                  oferta.item = response.data.offers[0];

                  oferta.item.logoPrincipal = oferta.item.logo1;

                  if (oferta.item.publishEndDateTime)
                      oferta.item.EndDateTimeParse = Date.parse(oferta.item.publishEndDateTime);

                  var imagemUrl;

                  if (!oferta.item.logo1) {

                      oferta.item.logoPrincipal = config.baseUrlImagemPortalWeb + oferta.item.partners.image;

                      if (oferta.item.image5) {

                          oferta.offerData.imagens.push({
                              title: oferta.item.title + 'Imagem 5',
                              src: config.baseUrlImagemPortalWeb + oferta.item.image5
                          });

                          imagemUrl = config.baseUrlImagemPortalWeb + oferta.item.image5;
                      }

                      if (oferta.item.image4) {
                          oferta.offerData.imagens.push({
                              title: oferta.item.title + 'Imagem 4',
                              src: config.baseUrlImagemPortalWeb + oferta.item.image4
                          });

                          imagemUrl = config.baseUrlImagemPortalWeb + oferta.item.image4;
                      }

                      if (oferta.item.image3) {
                          oferta.offerData.imagens.push({
                              title: oferta.item.title + 'Imagem 3',
                              src: config.baseUrlImagemPortalWeb + oferta.item.image3
                          });

                          imagemUrl = config.baseUrlImagemPortalWeb + oferta.item.image3;
                      }

                      if (oferta.item.image2) {
                          oferta.offerData.imagens.push({
                              title: oferta.item.title + 'Imagem 2',
                              src: config.baseUrlImagemPortalWeb + oferta.item.image2
                          });

                          imagemUrl = config.baseUrlImagemPortalWeb + oferta.item.image2;
                      }

                      if (oferta.item.image1) {
                          oferta.offerData.imagens.push({
                              title: oferta.item.title + 'Imagem 1',
                              src: config.baseUrlImagemPortalWeb + oferta.item.image1
                          });

                          imagemUrl = config.baseUrlImagemPortalWeb + oferta.item.image1;
                      }

                  }

                  $scope.$emit('newPageLoaded', {
                      'title': 'BENEFICIO CLUB - ' + oferta.item.title,
                      'description': oferta.item.title,
                      'path': $location.absUrl(),
                      'imageurl': imagemUrl
                  });

                  if (oferta.usuarioLogado.isLogged) {
                      oferta.maxIntroduction = oferta.item.introduction.length;
                  }

                  oferta.filtros.category = oferta.item.category[0].name;
                  oferta.estadoSelecionado = null;//oferta.item.partners.address[0].state;
                  oferta.cidadeSelecionada = null;//oferta.item.partners.address[0].city;
                  oferta.bairroSelecionado = null;//oferta.item.partners.address[0].neighborhood;

                  oferta.UnidadesParticipantes();

                  oferta.ofertasRelacionadas();

                  oferta.promise = null;

              }, function (error) {

                  $rootScope.bodyClass = "";

                  oferta.promise = null;

              });

          };

          oferta.UnidadesParticipantes = function () {

              var param = {
                  state: oferta.estadoSelecionado,
                  city: oferta.cidadeSelecionada,
                  offer: oferta.filtros.id
              };

              oferta.promise = BeneficioService.States(param);

              oferta.promise.then(function (response) {

                  oferta.resultadoGeral = response.data.state;

                  if (oferta.resultadoGeral) {

                      angular.forEach(JSLINQ(oferta.resultadoGeral).items[0].address, function (value, key) {

                          oferta.resultadoEnderecos.push(value);

                      });

                      //monta combo de estados
                      oferta.estados = JSLINQ(oferta.resultadoEnderecos).Distinct(function(el) { return el.state; }).items;

                      oferta.FiltroUnidades(0);

                  }

              }, function (error) {

                  $rootScope.bodyClass = "";
              });
          };

          oferta.FiltroUnidades = function (filtro) {

              var resultado;

              if (filtro === 2) { //cidade selecionada
                  resultado = JSLINQ(oferta.cidades).Where(function (el) { return el.city === oferta.cidadeSelecionada; });

                  if (resultado.items[0].neighborhood) {
                      oferta.bairros = resultado.items[0].neighborhood;
                  }

                  oferta.enderecos = JSLINQ(oferta.resultadoEnderecos).Where(function (el) { return el.state === oferta.estadoSelecionado && el.city === oferta.cidadeSelecionada; }).items;

              }
              else if (filtro === 1) { //estado selecionado
                  resultado = JSLINQ(oferta.resultadoGeral).Where(function (el) { return el.state === oferta.estadoSelecionado; });

                  oferta.bairros = [];
                  oferta.cidades = [];

                  if (resultado.items[0])
                      oferta.cidades = resultado.items[0].city;

                  oferta.enderecos = JSLINQ(oferta.resultadoEnderecos).Where(function (el) { return el.state === oferta.estadoSelecionado; }).items;

              }

              resultado = JSLINQ(oferta.resultadoEnderecos).items;

              if (oferta.estadoSelecionado) {
                  resultado = JSLINQ(resultado).Where(function (el) { return el.state === oferta.estadoSelecionado; }).items;
              }
              if (oferta.cidadeSelecionada) {
                  resultado = JSLINQ(resultado).Where(function (el) { return el.city === oferta.cidadeSelecionada; }).items;
              }
              if (oferta.bairroSelecionado) {
                  resultado = JSLINQ(resultado).Where(function (el) { return el.neighborhood === oferta.bairroSelecionado; }).items;
              }

              oferta.enderecos = resultado;

          };

          oferta.ofertasRelacionadas = function () {

              oferta.filtros.offerNotId = oferta.filtros.id;
              oferta.filtros.id = null;

              oferta.promise = BeneficioService.listarPromocoes(oferta.filtros);

              oferta.promise.then(function (response) {

                  oferta.ofertas = response.data.offers;


                  if ($location.$$search.d) {
                      oferta.openReport();
                  }

                  $rootScope.bodyClass = "";

              }, function (error) {

                  $rootScope.bodyClass = "";

              });
          };

          oferta.goToSite = function () {
              if (!oferta.usuarioLogado.isLogged) {

                  LoginModalsFactory.parametros.mensagem = "Para aproveitar essa oferta você precisa fazer um rápido cadastro ou entrar com seu login clicando em ‘Entrar’.";
                  
                  oferta.openLogin();

                  return false;
              }
              var urlWebSite = "";
              urlWebSite = "http://" + oferta.item.website.replace('https://', '').replace('http://', '');
              $window.open(urlWebSite);
          };

          oferta.goToDetail = function (title, id) {
              $location.search('p', id);
              $location.path('/beneficio-club/detalhe/' + title);
          };

          oferta.openLogin = function (origem) {

              LoginModalsFactory.parametros.urlRetorno = $location.$$path;
              LoginModalsFactory.parametros.urlRetornoParam = $location.$$search.p;

              LoginModalsFactory.openSignUp();
          };

          oferta.openVerMaisOferta = function (origem) {

              if (!oferta.usuarioLogado.isLogged) {
                  if (origem === 1) {
                      LoginModalsFactory.parametros.mensagem = 'Para ver mais detalhes e aproveitar essa oferta você precisa fazer um rápido cadastro ou entrar com seu login clicando em ‘Entrar’.';
                  } else if (origem === 2) {
                      LoginModalsFactory.parametros.mensagem = 'Para ver o regulamento e aproveitar essa oferta você precisa fazer um rápido cadastro ou entrar com seu login clicando em ‘Entrar’.';
                  } else if (origem === 3) {
                      LoginModalsFactory.parametros.mensagem = 'Para descobrir as unidades participantes e aproveitar essa oferta você precisa fazer um rápido cadastro ou entrar com seu login clicando em ‘Entrar’.';
                  }
                  oferta.openLogin();
              }
          };

          oferta.openReport = function () {

              if (!oferta.usuarioLogado.isLogged) {

                  LoginModalsFactory.parametros.d = 1;
                  LoginModalsFactory.parametros.mensagem = 'Para denunciar essa oferta você precisa fazer um cadastro rápido ou entrar com seu login clicando em ‘Entrar’.';
                  oferta.openLogin();

              } else {

                  LoginModalsFactory.parametros.d = null;

                  $scope.offer = {
                      parceiro: oferta.item.partners.fantasy,
                      oferta: oferta.item.title,
                      partners: oferta.item.partners.id,
                      offer: oferta.item.id,
                      register: oferta.usuarioLogado.id,
                      registerName: oferta.usuarioLogado.nome,
                      registerEmail: oferta.usuarioLogado.email
                  };

                  $scope.modal = $uibModal.open({
                      animation: true,
                      templateUrl: 'app/modules/beneficio-club/modal/denunciaroferta.htm',
                      windowClass: '-ticket -report',
                      size: 'sm',
                      scope: $scope
                  });
              }


          };

          if (!url.p) {
              $location.search('p', null);
              $location.path('/beneficio-club');
          } else {

              oferta.filtros.id = url.p;
              oferta.pageLoad();
          }

          oferta.back = backToBc;
          oferta.changeTab = changeTab;
          oferta.openSignUp = LoginModalsFactory.openSignUp;


          function backToBc() {
              $location.search('p', null);
              $location.path('/beneficio-club');
          }
          function changeTab($event) {

              if (!oferta.usuarioLogado.isLogged) {

                  oferta.openLogin();

              }
          }

          oferta.facebook = function () {
              
              Socialshare.share({
                  'provider': 'facebook',
                  'attrs': {
                      'socialshareUrl': $location.$$absUrl,
                      'socialshareType': 'feed',
                      'socialshareVia': config.facebookId,
                      'socialshareDisplay': 'popup',
                      'socialshareText': 'BENEFICIO CLUB - ' + oferta.item.title,
                      'socialshareDescription': oferta.item.introduction,
                      'socialshareCaption': 'https://www.souticket.com.br',
                      'socialshareMedia': oferta.item.logoPrincipal.replace('https', 'http')
                  }
              });

          };

          oferta.twiter = function () {

              Socialshare.share({
                  'provider': 'twitter',
                  'attrs': {
                      'socialshareUrl': $location.$$absUrl,
                      'socialshareText': 'BENEFICIO CLUB - ' + oferta.item.title,
                  }
              });

          };

          oferta.google = function () {

              Socialshare.share({
                  'provider': 'google',
                  'attrs': {
                      'socialshareUrl': $location.$$absUrl
                  }
              });

          };

          oferta.pinterest = function () {

              Socialshare.share({
                  'provider': 'pinterest',
                  'attrs': {
                      'socialshareUrl': $location.$$absUrl,
                      'socialshareText': 'BENEFICIO CLUB - ' + oferta.item.title,
                      'socialshareMedia': oferta.item.logoPrincipal.replace('https', 'http')
                  }
              });

          };

          oferta.urlConcatenada = function (texto) {

              return config.baseUrlImagemPortalWeb + texto;

          };
      }
    ]);

})();
