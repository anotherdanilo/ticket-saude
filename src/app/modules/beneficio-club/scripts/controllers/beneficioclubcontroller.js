﻿(function () {
    'use strict';

    angular.module('portalUsuarioApp').controllerLazy('BeneficioClubController', [
      '$route', 'BeneficioClubFactory', 'BeneficioModalsFactory', '$location', 'BeneficioService', '$sce', '$rootScope', '$window', '$scope',
      function ($route, BeneficioClubFactory, BeneficioModalsFactory, $location, BeneficioService, $sce, $rootScope, $window, $scope) {
          $scope.$emit('newPageLoaded', {
              title: 'Benefício Club Ticket | Descontos e Ofertas Exclusivos',
              description: "O Benefício Club é o canal de ofertas exclusivas para usuários dos cartões Ticket. Descubra descontos e promoções dos parceiros. Acesse e Aproveite!",
              path: $location.absUrl(),
              imageurl: ''
          });

          //menu selecionado no header
          //setTimeout(function () { $rootScope.menuSelecionado = 2; }, 300);

          var beneficio = this;

          beneficio.currentPage = 1;
          beneficio.pageSize = 12;

          beneficio.search = "";

          beneficio.filterData = BeneficioClubFactory.bindBeneficioFilterData();

          beneficio.contadores = beneficio.filterData.contadores;

          beneficio.filtrarInput = function (row) {
              return !!((row.title.toUpperCase().indexOf(beneficio.search.toUpperCase() || '') !== -1 || row.introduction.toUpperCase().indexOf(beneficio.search.toUpperCase() || '') !== -1));
          };

          beneficio.posicionamento = function () {
              if (beneficio.filterData.filtros.category) {
                  var top = $('#txtFiltro').offset().top;
                  $window.scrollTo(0, top - 70);
              } else {
                  $window.scrollTo(0, 0);
              }
          };

          beneficio.pageLoad = function () {
              
              var url = $location.search();
              
              

              if(url.ip == "1")
              {
                  $location.search('ip', null);
                  beneficio.openPartnerForm();
              }

              setTimeout(function () {
                  $rootScope.menuSelecionado = 2;
              }, 1);
              $rootScope.bodyClass = "menu-opened";
              $(".loading-mask-position").css("top", document.body.scrollTop);

              beneficio.filterData.promise = BeneficioService.listarPromocoes(beneficio.filterData.filtros);

              beneficio.filterData.promise.then(function (response) {

                  beneficio.filterData.ofertas = [];
                  beneficio.filterData.ofertas = response.data.offers;
                  
                  if (beneficio.filterData.primeiroCarremento) {

                      var lista = JSLINQ(beneficio.filterData.ofertas);

                      beneficio.contadores.todos = lista.Count();
                      beneficio.contadores.online = lista.Where(function (el) { return el.typeOffer === 1; }).Count();
                      beneficio.contadores.lojaFisica = lista.Where(function (el) { return el.typeOffer === 0; }).Count();


                      //carrega as categorias
                      beneficio.carregarCategorias();

                      beneficio.filterData.primeiroCarremento = false;

                  } else {

                      beneficio.posicionamento();
                      $rootScope.bodyClass = "";
                      beneficio.filterData.promise = null;

                  }
              }, function (error) {

                  $rootScope.bodyClass = "";
                  beneficio.filterData.promise = null;
              });


          };

          beneficio.carregarBanners = function () {
              
              beneficio.filterData.promise = BeneficioService.listarBanners();

              beneficio.filterData.promise.then(function (response) {

                  console.log(response);

                  beneficio.banners = [];

                  angular.forEach(response.data.rows, function (value, key) {
                      
                      beneficio.banners.push({
                          image: {
                              href: value.image.href.replace('http', 'https').replace('ticketlumisportal.lanet.accorservices.net', 'www.souticket.com.br'),
                              mediaLegend: value.image.mediaLegend
                          },
                          logo: {
                              href: value.logo.href.replace('http', 'https').replace('ticketlumisportal.lanet.accorservices.net', 'www.souticket.com.br'),

                          },
                          introduction: value.introduction,
                          linkUrl: value.linkUrl
                      });

                  });


              }, function (error) {
                  console.log(error);
              });

          };

          beneficio.filtrar = function (valor) {


              beneficio.filterData.filtros.category = valor;

              beneficio.pageLoad();
          };

          beneficio.filtrarSemLoad = function (valor) {

              beneficio.filterData.filtros.category = valor;

          };

          beneficio.ordernar = function (valor) {

              beneficio.filterData.filtros.countView = valor;

              beneficio.pageLoad();
          };

          beneficio.ordernarSemLoad = function (valor) {

              beneficio.filterData.filtros.countView = valor;

          };

          beneficio.tipoOferta = function (valor) {

              beneficio.filterData.filtros.type = valor;

              beneficio.pageLoad();
          };

          beneficio.tipoOfertaSemLoad = function (valor) {

              beneficio.filterData.filtros.type = valor;
          };

          beneficio.carregarCategorias = function () {
              
              beneficio.filterData.categorias = [];

              beneficio.filterData.promise = BeneficioService.listarCategorias();

              beneficio.filterData.promise.then(function (response) {

                  var retorno = JSLINQ(beneficio.filterData.ofertas).Select(function (el) { return el.category; });

                  var cat = $route.current.params.parametros;

                  

                  angular.forEach(response.data.category, function (value, key) {

                      //var cat = $route.current.params.parametros;

                      var category = value;

                      //var cat = decodeURIComponent($.urlParam('cat'));

                      if (cat) {
                          if (cat === category.name) {
                              category.selecionado = true;
                          }
                      }

                      beneficio.filterData.categorias.push(category);

                  });

                  //var cat = decodeURIComponent($.urlParam('cat'));
                  if (cat) {

                      beneficio.filterData.filtros.category = cat;
                      BeneficioClubFactory.parametros.cat = null;
                      beneficio.pageLoad();

                  } else {

                      beneficio.posicionamento();
                      $rootScope.bodyClass = "";
                      beneficio.filterData.promise = null;

                  }

              }, function (error) {

                  $rootScope.bodyClass = "";
                  beneficio.filterData.promise = null;
              });
          };



          beneficio.bannerData = BeneficioClubFactory.bindBeneficioBannerData();
          beneficio.openFilter = BeneficioModalsFactory.openFilter;
          beneficio.closeFilter = BeneficioModalsFactory.closeFilter;
          beneficio.openPartnerForm = BeneficioModalsFactory.openPartnerForm;
          beneficio.closePartnerForm = BeneficioModalsFactory.closePartnerForm;

          beneficio.orderBy = orderBy;
          //beneficio.goToDetail = goToDetail;

          function orderBy(order) {
              beneficio.filterData.orderBy = order;
          }

          beneficio.goToDetail = function (title, id) {
              

              var find = ' ';
              var re = new RegExp(find, 'g');

              title = title.replace(re, '-');

              $location.search('p', id);
              $location.path('/beneficio-club/detalhe/' + title);

              //$window.location.href = window.location.origin + ;
          };


          beneficio.urlConcatenada = function (texto) {

              return config.baseUrlImagemPortalWeb + texto;

          };

          beneficio.pageLoadCloseFilter = function () {

              beneficio.closeFilter();

              beneficio.pageLoad();

          };

          beneficio.carregarBanners();

      }
    ]);

})();
