(function () {
    'use strict';

    angular.module('portalUsuarioApp').controllerLazy('BeneficioClubDenunciaController', [
      'BeneficioModalsFactory', '$scope', 'BeneficioService', 'toaster',
      function (BeneficioModalsFactory, $scope, BeneficioService, toaster) {
          var denuncia = this;
          
          denuncia.step = 0;
          denuncia.toSuccess = toSuccess;
          denuncia.toError = toError;

          denuncia.close = function() {
              $scope.modal.dismiss();
          };

          denuncia.promise = null;

          denuncia.pageLoad = function (){
              denuncia.parceiro = $scope.offer.parceiro;
              denuncia.oferta = $scope.offer.oferta;
          };

          denuncia.enviar = function () {
              var param = {
                  partners: $scope.offer.partners,
                  offer: $scope.offer.offer,
                  register: $scope.offer.register,
                  description: denuncia.motivo + ' - ' + denuncia.descricao,
                  registerName: $scope.offer.registerName,
                  registerEmail: $scope.offer.registerEmail,
              };

              denuncia.promise = BeneficioService.denunciarOferta(param);

              denuncia.promise.then(function (response) {
                  denuncia.toSuccess();

              }, function (error) {

                  denuncia.toError();

              });

          };

          function toSuccess() {
              denuncia.step = 1;
          }
          function toError() {
              denuncia.step = 2;
          }
      }
    ]);

})();
