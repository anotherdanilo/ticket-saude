﻿(function () {
    'use strict';
    angular.module('portalUsuarioApp').serviceLazy("BeneficioService", [
      'BeneficioResource', '$q', '$resource', '$http', '$rootScope', function (BeneficioResource, $q, $resource, $http, $rootScope) {

          var self;
          self = this;

          self.listarPromocoes = function (parametros) {
              
              //trativa do parametro ID (não pode ser passado como nulo para a API
              var param;

              param = {
                  site: config.site,
                  token: config.token,
                  category: parametros.category,
                  countView: parametros.countView,
                  type: parametros.type,
                  id: parametros.id,
                  qtd: parametros.qtd,
                  offerNotId: parametros.offerNotId
              };

              if (!param.id) {
                  delete param.id;
              }

              if (!param.offerNotId) {
                  delete param.offerNotId;
              }

              return $q(function (resolve, reject) {
                  
                  $http({
                      url: config.urls.ofertasAPI,
                      method: 'POST',
                      headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                      data: $.param(param),
                      withCredentials: false

                  }).then(function successCallback(response) {
                      if (response.statusText === "OK") {
                          resolve(response);
                      } else {
                          reject(response);
                      }
                      resolve(response);
                  }, function errorCallback(response) {
                      reject(response);
                  });

              });

          };

          self.listarCategorias = function () {

              return $q(function (resolve, reject) {

                  $http({
                      url: config.urls.categoriasAPI,
                      method: 'POST',
                      headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                      data: $.param({
                          token: config.token,
                          site: config.site
                      }),
                      withCredentials: false

                  }).then(function successCallback(response) {
                      if (response.statusText === "OK") {
                          resolve(response);
                      } else {
                          reject(response);
                      }
                  }, function errorCallback(response) {
                      reject(response);
                  });

              });
          };

          self.listarBanners = function () {
              return $q(function (resolve, reject) {

                  $http({
                      method: 'GET',
                      url: config.urls.getBanners
                  }).then(function successCallback(response) {
                      resolve(response);
                  }, function errorCallback(response) {
                      reject(response);
                  });

              });
          };

            self.States = function (parametros) {

              var param;

              param = {
                  site: config.site,
                  token: config.token,
                  state: parametros.state,
                  city: parametros.city,
                  offer: parametros.offer
              };

              if (!param.state) {
                  delete param.state;
              }

              if (!param.city) {
                  delete param.city;
              }

              return $q(function (resolve, reject) {

                  $http({
                      url: config.urls.stateAPI,
                      method: 'POST',
                      headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                      data: $.param(param),
                      withCredentials: false

                  }).then(function successCallback(response) {
                      if (response.status) {
                          resolve(response);
                      } else {
                          reject(response);
                      }
                  }, function errorCallback(response) {
                      reject(response);
                  });

              });

          };

          self.denunciarOferta = function (parametros) {

              var param;

              param = {
                  token: config.token,
                  partners: parametros.partners,
                  offer: parametros.offer,
                  register: parametros.register,
                  description: parametros.description,
                  registerName: parametros.registerName,
                  registerEmail: parametros.registerEmail,
                  origin: config.origin
              };

              return $q(function (resolve, reject) {

                  $http({
                      url: config.urls.denunciaAPI,
                      method: 'POST',
                      headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                      data: $.param(param),
                      withCredentials: false

                  }).then(function successCallback(response) {

                      if (response.status === 200 && response.statusText === "OK") {
                          resolve(response);
                      } else {
                          reject(response);
                      }
                  }, function errorCallback(response) {
                      reject(response);
                  });

              });

          };

          self.isImage = function (src) {

              var deferred = $q.defer();

              var image = new Image();
              image.onerror = function () {
                  deferred.resolve(false);
              };
              image.onload = function () {
                  deferred.resolve(true);
              };
              image.src = src;

              return deferred.promise;

          };

      }
    ]);

}).call(this);