﻿(function () {
    angular.module('portalUsuarioApp').factoryLazy('BeneficioResource', [
      '$resource', function ($resource) {
          return $resource(config.urls.pesquisaAPI, null, {
              'update': {
                  method: 'PUT'
              }
          });
      }
    ]);

}).call(this);
