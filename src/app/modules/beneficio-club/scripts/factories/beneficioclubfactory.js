(function () {
    'use strict';

    angular.module('portalUsuarioApp').factoryLazy('BeneficioClubFactory', [
      function () {

          // set variables
          var beneficioFilterData = {};
          resetBeneficioFilterData();
          var beneficioBannerData = {};
          resetBeneficioBannerData();

          var parametros = {
              p: '',
              cat: ''
          };

          // set service
          var service = {
              bindBeneficioFilterData: bindBeneficioFilterData,
              bindBeneficioBannerData: bindBeneficioBannerData,
              parametros: parametros
          };

          return service;

          // public
          // used to bind beneficioFilterData to controller
          function bindBeneficioFilterData() {
              return beneficioFilterData;
          }
          function bindBeneficioBannerData() {
              return beneficioBannerData;
          }

          // private
          // reset beneficioFilterData
          function resetBeneficioFilterData() {
              beneficioFilterData.orderBy = 'recent';
              beneficioFilterData.type = {
                  todas: null,
                  online: 1,
                  lojaFisica: 0
              };

              beneficioFilterData.contadores = {
                  todos: 0,
                  online: 0,
                  lojaFisica: 0,
                  culturaLazer: 0,
                  produtos: 0,
                  servicos: 0,
                  gastronomia: 0
              };

              beneficioFilterData.categorias = [];

              beneficioFilterData.primeiroCarremento = true;

              beneficioFilterData.filtros = {

                  category: null,
                  countView: null,
                  type: null,
                  id: null,
                  offerNotId: null

              };

              beneficioFilterData.cat = 'all';

              beneficioFilterData.ofertas = [];

              beneficioFilterData.promise = null;

          }
          function resetBeneficioBannerData() {
              beneficioBannerData.slides = [
                {
                    id: '1',
                    img: '../assets/images/banner-img-01.jpg',
                    logo: '',
                    text: 'Seja o herói da sua SUPER MÃE'
                },
                {
                    id: '2',
                    img: '../assets/images/banner-img-01.jpg',
                    logo: '',
                    text: 'Cursos Presenciais em Marketing Digital com 10% de desconto na SPDS'
                },
                {
                    id: '3',
                    img: '../assets/images/banner-img-01.jpg',
                    logo: '',
                    text: 'Centauro e Ticket trazem até 12% de desconto em todo o site'
                },
                {
                    id: '4',
                    img: '../assets/images/banner-img-01.jpg',
                    logo: '',
                    text: 'Associado do Benefício Club compra o Kit Cheque Teatro por R$ 70,00'
                },
              ];
          }
      }
    ]);

})();
