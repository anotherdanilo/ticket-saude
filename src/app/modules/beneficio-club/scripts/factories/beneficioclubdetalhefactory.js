(function() {
  'use strict';

  angular.module('portalUsuarioApp').factoryLazy('BeneficioClubDetalheFactory', [
    function() {

      // set variables
      var offerData = {};
      resetOfferData();

      // set service
      var service = {
        bindOfferData: bindOfferData,
      };

      return service;

      // public
      // used to bind offerData to controller
      function bindOfferData() {
        return offerData;
      }

      // private
      // reset offerData
      function resetOfferData() {
        offerData.company = 'Cheque Teatro';
        offerData.logo = '../assets/images/logo-cheque-teatro.jpg';
        offerData.title = 'Associado do Benefício Club compra o Kit Cheque Teatro por R$ 70,00';
        offerData.date = '04/06/2016';
        offerData.shortdesc = 'Entradas Vips para o teatro e preço especial para acompanhantes, além de desconto para parques, cinemas e muito mais.';
        offerData.desc = "";
        offerData.link = "";
        offerData.conditions = "";
        offerData.images = [
          {
            title: 'Cheque Teatro',
            src: '../assets/images/offer-img-teatro-01.jpg',
            caption: '',
            width: '',
            height: '',
          },
          {
            title: 'Cheque Teatro',
            src: '../assets/images/offer-img-teatro-02.jpg',
            caption: '',
            width: '',
            height: '',
          },
          {
            title: 'Cheque Teatro',
            src: '../assets/images/offer-img-teatro-03.jpg',
            caption: '',
            width: '',
            height: '',
          },
        ];
        offerData.locationState = 'Todos';
        offerData.locationStateOpts = ['Todos', "AC", "AL", "AM", "AP", "BA", "CE", "DF", "ES", "GO", "MA", "MT", "MS", "MG", "PA", "PB", "PR", "PE", "PI", "RJ", "RN", "RO", "RS", "RR", "SC", "SE", "SP", "TO"];
        offerData.locationCity = 'Todas';
        offerData.locationCityOpts = ['Todas','São Paulo','Rio de Janeiro','Belo Horizonte'];
        offerData.location = 'Todos';
        offerData.locationOpts = ['Todos','Vila Mariana','Pinheiros','Vila Madalena'];
        offerData.locations = [
          {
            title: 'Unidade Alameda Itu',
            address1: 'Alameda Itu, 1310',
            address2: 'Jd. Paulista - São Paulo - SP',
            cep: '01421-004',
            tel1: '(11) 2640-3805',
            tel2: '(11) 2776-3805',
          },
          {
            title: '',
            address1: 'Alameda Itu, 1310',
            address2: 'Jd. Paulista - São Paulo - SP',
            cep: '01421-004',
            tel1: '(11) 2640-3805',
            tel2: '(11) 2776-3805',
          },
          {
            title: 'Unidade Alameda Itu',
            address1: 'Alameda Itu, 1310',
            address2: 'Jd. Paulista - São Paulo - SP',
            cep: '01421-004',
            tel1: '(11) 2640-3805',
            tel2: '(11) 2776-3805',
          },
        ];
        offerData.related = [
          {
            name: 'Space Tennis',
            logo: '../assets/images/logo-space-tennis.jpg',
            cat: '-cat02',
            title: '',
            desc: 'Ofertas exclusivas da Space Tennis para você',
            link: 'detalhe',
          },
          {
            name: 'Walmart',
            logo: '../assets/images/logo-walmart.jpg',
            cat: '-cat03',
            title: '',
            desc: 'Walmart oferece desconto de 31% no Boneco Mickey Ovo',
            link: 'detalhe',
          },
        ];
      }
    }
  ]);

})();
