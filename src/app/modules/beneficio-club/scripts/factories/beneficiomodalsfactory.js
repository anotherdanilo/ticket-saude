(function() {
  'use strict';
  angular.module('portalUsuarioApp').factoryLazy('BeneficioModalsFactory', [
    '$uibModal',
    function($uibModal) {

      var filterModalInstance;
      var reportModalInstance;

      var service = {
        openFilter: openFilter,
        closeFilter: closeFilter,
        openReport: openReport,
        closeReport: closeReport,
        openPartnerForm: openPartnerForm,
        closePartnerForm: closePartnerForm,
      };

      return service;

      function openFilter() {
        filterModalInstance = $uibModal.open({
          animation: true,
          templateUrl: 'app/modules/beneficiario/modal/beneficioclubfiltro.htm',
          windowClass: '-ticket -filter',
          size: 'sm'
        });
      }

      function closeFilter() {
        filterModalInstance.dismiss();
      }

      function openReport() {
        reportModalInstance = $uibModal.open({
          animation: true,
          templateUrl: 'app/modules/beneficiario/modal/denunciaroferta.htm',
          windowClass: '-ticket -report',
          size: 'sm'
        });
      }

      function closeReport() {
        reportModalInstance.dismiss();
      }

      function openPartnerForm() {
        reportModalInstance = $uibModal.open({
          animation: true,
          templateUrl: 'app/modules/beneficiario/modal/indicarparceiro.htm',
          windowClass: '-ticket -partner',
          size: 'sm'
        });
      }

      function closePartnerForm() {
        reportModalInstance.dismiss();
      }
    }
  ]);

})();
