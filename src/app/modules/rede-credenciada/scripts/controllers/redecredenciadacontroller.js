(function() {
    'use strict';

    angular.module('portalUsuarioApp').controllerLazy('RedeCredenciadaController', [
        '$window', 'AlertaFactory', '$compile', 'AutenticacaoService', 'RedeCredenciadaService',
        'RedeCredenciadaFactory', 'RedeCredenciadaModalsFactory', '$scope', '$rootScope',
        '$location', 'LoginModalsFactory', '$timeout', 'CartaoService', '$uibModal', 'prebidService',
        function($window, AlertaFactory, $compile, AutenticacaoService, RedeCredenciadaService,
            RedeCredenciadaFactory, RedeCredenciadaModalsFactory, $scope, $rootScope,
            $location, LoginModalsFactory, $timeout, CartaoService, $uibModal, prebidService) {

            $location.search({});

            $scope.$emit('newPageLoaded', {
                title: 'Rede Credenciada Ticket | Veja aonde aceita seus cartões',
                description: 'Confira toda a rede credenciada da Ticket. Saiba quais estabelecimentos aceitam nossos cartões para poder utilizar seu benefício.',
                path: $location.absUrl(),
                imageurl: ''
            });


            // if (navigator.geolocation) {

            //     navigator.geolocation.getCurrentPosition(function(position){
            //       $scope.$apply(function(){
            //         $scope.position = position;
            //       });
            //     });
            //   }

            // var options = {
            //     enableHighAccuracy: true,
            //     timeout: 5000,
            //     maximumAge: 0
            // };

            // navigator.geolocation.getCurrentPosition(function(location) {
            //     console.log(location.coords.latitude);
            //     console.log(location.coords.longitude);
            //     console.log(location.coords.accuracy);
            // });

            // if (!$window.navigator.geolocation) {
            //     deferred.reject('Geolocation not supported.');
            // } else {
            //     $window.navigator.geolocation.getCurrentPosition(
            //         function(position) {
            //             console.log(position);
            //         },
            //         function(err) {
            //             console.log(err);
            //         });
            // }
            setTimeout(function() { $rootScope.menuSelecionado = 1; }, 1);

            var rede = this;
            rede.usuario = AutenticacaoService.obterUsuarioLogado();

            //rede.formData = null; 
            rede.openAlerta = AlertaFactory.openAlerta;
            rede.formData = RedeCredenciadaFactory.bindRedeFormData();
            //rede.listaFiltrada = rede.formData.listResults;
            rede.submit = RedeCredenciadaFactory.submitRedeFormData;
            rede.showFav = false;
            rede.changeToFav = changeToFav;
            rede.goToEstabelecimentos = goToEstabelecimentos;
            rede.setType = setType;
            rede.isTypeSelected = isTypeSelected;
            rede.cardtype = 'TRE';
            rede.selectOpen = false;
            rede.openSearch = openSearch;
            rede.closeSearch = closeSearch;

            rede.atualizarBusca = AtualizarBusca;
            
            rede.checkFavoritos = checkFavoritos;
            rede.favcheched = false;
            rede.promise = null;
            rede.idUsuario = rede.usuario.id;
            rede.formData.idUsuario = rede.usuario.id;
            rede.map = '';
            // rede.mapOptions = {
            //     zoom: 17,
            //     center: new google.maps.LatLng(-23.5742787, -46.6918861),
            //     mapTypeId: google.maps.MapTypeId.ROADMAP
            // };
            rede.infoWindow = RedeCredenciadaFactory.infoWindow;
            rede.openInfoWindow = openInfoWindow;
            rede.toggleFav = toggleFav;
            rede.lat = null;
            rede.long = null;
            rede.getLocation = getLocation;
            rede.markers = [];
            //rede.formData.modalFiltro = {
            //    Latitude: 0,
            //    Longitude: 0,
            //    CardBin: "",
            //    Radius: 0,
            //    UserId: "0",
            //    Offset: 0,
            //    Limit: 0
            //};
            rede.toProgamAvante = toProgamAvante;
            rede.erro = false;

            //pageLoad();
            rede.pageLoad = pageLoad;
            autoComplete();
            rede.verificaProduto = verificaProduto;

            //getLocation();

            var searchModalInstance;

            function openSearch() {
                
                searchModalInstance = $uibModal.open({
                    animation: true,
                    templateUrl: 'app/modules/beneficiario/modal/redecredenciadafiltro.htm',
                    windowClass: '-ticket -search',
                    size: 'sm',
                    scope: $scope
                });
            }

            function closeSearch() {
                if (searchModalInstance) {
                    searchModalInstance.dismiss();
                }
            }

            function verificaProduto(produto) {
                switch (produto) {
                    case "TRE":
                        return "-restaurante";
                    case "TAE":
                        return "-alimentacao";
                    case "TKE":
                        return "-cultura";
                    case "TC":
                        return "-car";
                    default:
                        return "-transporte";
                }
            }

            function toProgamAvante() {
                window.open('http://www.ticket.com.br/meus-servicos/ticket-fit/', '_blank');
            }

            function getAddress(latitude, longitude) {
                var lat = parseFloat(latitude);
                var lng = parseFloat(longitude);
                var latlng = new google.maps.LatLng(lat, lng);
                var geocoder = new google.maps.Geocoder();
                geocoder.geocode({ 'latLng': latlng }, function(results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        if (results[0]) {
                            //alert("Location: " + results[1].formatted_address);
                            rede.formData.address = results[0].formatted_address;
                        }
                    }
                });
            }

            function getLocation() {
                //$scope.initMap(12);
                //$scope.autoComplete();
                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(function(position) {
                        rede.lat = position.coords.latitude;
                        rede.long = position.coords.longitude;
                        //rede.formData.address =  position.coords.longitude;
                        getAddress(rede.lat, rede.long);
                    },
                        function(error) {
                            console.log('Erro ao obter localização.', error);
                        });
                } else {
                    console.log('Navegador não suporta Geolocalização!');
                }
            }

            function pageLoad() {
                prebidService.getRedeCredenciadaAds();
                
                if (rede.usuario.isLogged) {

                    $window.scrollTo(0, 0);
                    $(".loading-mask-position").css("top", document.body.scrollTop);

                    $rootScope.bodyClass = "menu-opened";

                    rede.promise = CartaoService.carregarCartoesPorUsuarioService(rede.usuario.id);

                    rede.promise.then(function(response) {

                        var cartaoFavorito = JSLINQ(response.value)
                            .First(function(item) { return item.balance.favorito === true; });

                        if (cartaoFavorito) {

                            rede.cardtype = cartaoFavorito.balance.bin;
                        }

                        if (navigator.geolocation) {
                            navigator.geolocation.getCurrentPosition(function(position) {
                                var lat = position.coords.latitude;
                                var long = position.coords.longitude;
                                //rede.formData.address =  position.coords.longitude;
                                getAddress(lat, long);
                                //var lat = results[0].geometry.location.lat();
                                //var long = results[0].geometry.location.lng();
                                rede.formData.modalFiltro = {
                                    Latitude: lat,
                                    Longitude: long,
                                    CardBin: rede.cardtype,
                                    Radius: rede.formData.radius === '0' ? 500 : parseInt(rede.formData.radius) * 1000,
                                    UserId: rede.usuario.id,
                                    Offset: 1,
                                    Limit: 500
                                };

                                buscaEstabelecimentosBD();
                            },
                                function(error) {
                                    //alert('Erro ao obter localização!');
                                    console.log('Erro ao obter localização.', error);
                                    initMapSemPin();
                                });
                        } else {
                            console.log('Navegador não suporta Geolocalização!');
                            initMapSemPin();
                        }

                        $rootScope.bodyClass = "";

                    }, function(response) {

                        $rootScope.bodyClass = "";
                    });
                }
                else {
                    if (navigator.geolocation) {
                        navigator.geolocation.getCurrentPosition(function(position) {
                            var lat = position.coords.latitude;
                            var long = position.coords.longitude;
                            //rede.formData.address =  position.coords.longitude;
                            getAddress(lat, long);
                            //var lat = results[0].geometry.location.lat();
                            //var long = results[0].geometry.location.lng();
                            rede.formData.modalFiltro = {
                                Latitude: lat,
                                Longitude: long,
                                CardBin: "TRE",
                                Radius: rede.formData.radius === '0' ? 500 : parseInt(rede.formData.radius) * 1000,
                                UserId: rede.usuario.id,
                                Offset: 1,
                                Limit: 500
                            };

                            buscaEstabelecimentosBD();
                        },
                            function(error) {
                                //alert('Erro ao obter localização!');
                                console.log('Erro ao obter localização.', error);
                                initMapSemPin();
                            });
                    } else {
                        console.log('Navegador não suporta Geolocalização!');
                        initMapSemPin();
                    }
                }


                // var endereco = rede.formData.address;
                // var geocoder = new google.maps.Geocoder();
                // var address = endereco;
                // geocoder.geocode({ 'address': address }, function(results, status) {
                //     if (status == google.maps.GeocoderStatus.OK) {
                //         var lat = results[0].geometry.location.lat();
                //         var long = results[0].geometry.location.lng();
                //         var modalFiltro = {
                //             Latitude: lat,
                //             Longitude: long,
                //             CardBin: "TRE",
                //             Radius: parseInt(rede.formData.radius) * 1000,
                //             UserId: rede.idUsuario,
                //             Offset: 1,
                //             Limit: 500
                //         };

                //         buscaEstabelecimentosBD(modalFiltro);
                //     }
                // });
                // rede.mapOptions = {
                //     zoom: 10,
                //     center: new google.maps.LatLng(-23.5742787, -46.6918861),
                //     mapTypeId: google.maps.MapTypeId.ROADMAP
                // };

                // initMap();

            }

            function initMapSemPin() {
                // var endereco = "Brasil";
                // var geocoder = new google.maps.Geocoder();
                // var address = endereco;
                rede.formData.listaFiltrada = [];
                rede.formData.listResults = [];
                rede.formData.address = "";
                // geocoder.geocode({ 'address': address }, function(results, status) {
                //     if (status == google.maps.GeocoderStatus.OK) {
                //         var lat = results[0].geometry.location.lat();
                //         var long = results[0].geometry.location.lng();
                //         rede.mapOptions = {
                //             zoom: 10,
                //             center: new google.maps.LatLng(lat, long),
                //             mapTypeId: google.maps.MapTypeId.ROADMAP
                //         };
                //         initMap();
                //     }
                // });
                rede.mapOptions = {
                    zoom: 10,
                    center: new google.maps.LatLng(-23.5697431, -46.7033343),
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                };
                initMap();
            }

            function goToEstabelecimentos(){
                window.open('http://www.ticket.com.br/portal/servicos-diferenciais/indique-estabelecimento/', '_blank');
            }

            function changeToFav() {

                if (rede.usuario.isLogged) {


                    if (!rede.formData.address) {

                        $('#buscaEnderecoNome').focus();

                        AlertaFactory.parametros.titulo = "VER FAVORITOS";
                        AlertaFactory.parametros.mensagem = "Para ver seus estabelecimentos favoritos, por favor insira um endereço próximo, que atualizamos a busca.";
                        AlertaFactory.parametros.icone = "icon-error";

                        rede.openAlerta();

                        return;
                    }
                    else {

                        rede.showFav = !rede.showFav;
                        rede.favcheched = rede.showFav;

                        if (rede.favcheched) {
                            checkFavoritos();
                        }
                        else {
                            //AtualizarBusca();
                            buscaEstabelecimentosBD();
                        }

                        return;

                    }
                }

                LoginModalsFactory.parametros.mensagem = 'Para ver e gerenciar seus estabelecimentos favoritos você precisa estar cadastrado ou fazer seu login clicando em ‘Entrar’.';
                LoginModalsFactory.parametros.reload = true;
                LoginModalsFactory.openSignUp();


            }

            function setType(card) {
                
                if (rede.selectOpen) {
                    
                    if(card == "PLUS"){
                        $window.open(config.urls.plusRedeCredenciada);
                    }
                    else{
                    
                        rede.cardtype = card;
                        rede.selectOpen = false;
                        if (card == "TC") {
                            window.open('https://www.ticketlog.com.br/localizar', '_blank');
                        }
                   
                    }
                }
                else {
                    rede.cardtype = '';
                    rede.selectOpen = true;
                }

            }
            
            function isTypeSelected(card) {

                return rede.cardtype === card;
            }

            function initMap() {
                
                var mapDiv = document.getElementById('gmaps-ticket');
                $(mapDiv).removeAttr("style");
                $(mapDiv).empty();
                var markers = rede.formData.listaFiltrada;
                rede.map = new google.maps.Map(mapDiv, rede.mapOptions);

                for (var i = 0; i < markers.length; i++) {
                    createMarkers(markers[i]);
                    //console.log(markers[i]);
                }
            }

            function createMarkers(place) {
                var marker = new google.maps.Marker({
                    map: rede.map,
                    position: new google.maps.LatLng(place.lat, place.long),
                    icon: place.icon,
                    animation: google.maps.Animation.DROP,
                });

                //rede.markers.push(marker);
                place.marker = marker;

                var headerRightSideContent = '';
                if (place.avante) {
                    headerRightSideContent += '<i class="iconavante"></i>';
                }
                if (place.collact) {
                    headerRightSideContent += '<i class="iconcollact"></i>';
                }
                var classFav = '';
                if (place.fav) {
                    classFav = "icon-star";
                }
                if (!place.fav) {
                    classFav = "icon-star_border";
                }

                var json = "{'fav': " + place.fav + ", 'codEstabelecimento': '" + place.codEstabelecimento + "'}";

                //var json = JSON.stringify(objeto);

                google.maps.event.addListener(marker, 'click', function() {
                       
                    var content = '<div class="infoWindowContent">' +
                        '<h6>' +
                        '<i class="icon-star-favorite ' + classFav + '" ng-click="rede.toggleFav(' + json + ')"></i>' +
                        '<span class="place-name">' + place.name + '</span>' + headerRightSideContent +
                        '</h6>' +
                        '<p>' + place.address1 + '</p>' +
                        '<p>' + place.address2 + '</p>' +
                        '<p>' + place.phone + '</p>' +
                        '<div class="cardmini-holder">' +
                        '</div>' +
                        '</div>';
                    var cartoes = content;
                    var products = place.products;
                    angular.forEach(products, function(product, key) {
                        switch (product.description) {
                            case "TRE":
                                content = $(content).find(".cardmini-holder").append('<div class="cardmini -restaurante"><div class="card-logo"></div></div>').parent();
                                break;
                            case "TAE":
                                content = $(content).find(".cardmini-holder").append('<div class="cardmini -alimentacao"><div class="card-logo"></div></div>').parent();
                                break;
                            case "TKE":
                                content = $(content).find(".cardmini-holder").append('<div class="cardmini -cultura"><div class="card-logo"></div></div>').parent();
                                break;
                            case "TC":
                                content = $(content).find(".cardmini-holder").append('<div class="cardmini -car"><div class="card-logo"></div></div>').parent();
                                break;
                            default:
                                content = $(content).find(".cardmini-holder").append('<div class="cardmini -transporte"><div class="card-logo"></div></div>').parent();
                        }
                    });
                    content = $(content)[0].outerHTML;

                    if (place.avante && place.collact) { content = content.replace('<h6>', '<h6><i class="icon-avante"></i><i class="icon-collact"></i>'); }
                    else if (place.avante) { content = content.replace('<h6>', '<h6><i class="icon-avante"></i>'); }
                    else if (place.collact) { content = content.replace('<h6>', '<h6><i class="icon-collact"></i>'); }

                    var template = angular.element(content);
                    var linkFn = $compile(template);
                    var element = linkFn($scope);
                    var infowindow = rede.infoWindow();
                    infowindow.setContent(element[0]);
                    infowindow.open(rede.map, marker);
                });


            }

            function openInfoWindow(e, selectedMarker) {
                //e.preventDefault();
                //var marker = new google.maps.Marker(selectedMarker);
                
                new google.maps.event.trigger(selectedMarker, 'click');
                //google.maps.event.trigger(selectedMarker.marker, 'click');
            }

            function toggleFav(place) {

                if (rede.usuario.isLogged) {
                    place.fav = !place.fav;
                    var itemListaFiltrada = JSLINQ(rede.formData.listaFiltrada).First(function(item) { return item.codEstabelecimento === place.codEstabelecimento; });
                    itemListaFiltrada.fav = place.fav;

                    var modal = {
                        Code: place.codEstabelecimento,
                        Favoritar: place.fav
                    };
                    favoritarCartao(modal);

                    return;
                }

                LoginModalsFactory.parametros.mensagem = 'Para marcar estabelecimentos como seus favoritos você precisa estar cadastrado ou fazer seu login clicando em ‘Entrar’.';
                LoginModalsFactory.parametros.reload = true;
                LoginModalsFactory.openSignUp();
            }

            function AtualizarBusca() {

                //RedeCredenciadaFactory.atualizarBusca();
                var geocoder = new google.maps.Geocoder();
                var address = rede.formData.address;
                geocoder.geocode({ 'address': address }, function(results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        var lat = results[0].geometry.location.lat();
                        var long = results[0].geometry.location.lng();
                        rede.formData.modalFiltro = {
                            Latitude: lat,
                            Longitude: long,
                            CardBin: rede.cardtype,
                            Radius: rede.formData.radius === "0" ? 500 : parseInt(rede.formData.radius) * 1000,
                            UserId: rede.usuario.id,
                            Offset: 1,
                            Limit: 500
                        };

                        buscaEstabelecimentosBD();
                    }
                });
            }
            
            $scope.atualizarBusca = rede.atualizarBusca;
            $scope.fecharModal = rede.closeSearch;

            function buscaLatLong(endereco) {
                var geocoder = new google.maps.Geocoder();
                var address = endereco;
                geocoder.geocode({ 'address': address }, function(results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        var lat = results[0].geometry.location.lat();
                        var long = results[0].geometry.location.lng();
                        rede.lat = lat;
                        rede.long = long;
                        // var modalFiltro = {
                        //     Latitude: lat,
                        //     Longitude: long,
                        //     CardBin: "TRE",
                        //     Radius: 1,
                        //     UserId: rede.idUsuario
                        // };

                        // buscaEstabelecimentosBD(modalFiltro);

                        // rede.promise = RedeCredenciadaService.buscaEstabelecimentos(modalFiltro);

                        // rede.promise.then(function(response) {
                        //     rede.listaFiltrada = [];
                        //     rede.formData.listResults = [];

                        // }, function(error) {

                        // });
                    }
                });
            }

            function checkFavoritos() {
                if (rede.usuario.isLogged) {

                    if (rede.favcheched) {
                        var lista = JSLINQ(rede.formData.listResults)
                            .Where(function(item) { return item.fav === true; });

                        rede.formData.listaFiltrada = lista.items;
                    } else {
                        rede.formData.listaFiltrada = rede.formData.listResults;
                    }
                    initMap();

                    return;
                }

                LoginModalsFactory.parametros.mensagem = 'Para ver e gerenciar seus estabelecimentos favoritos você precisa estar cadastrado ou fazer seu login clicando em ‘Entrar’.';
                LoginModalsFactory.parametros.reload = true;
                LoginModalsFactory.openSignUp();

                rede.favcheched = false;
            }

            function buscaEstabelecimentosBD() {
                $window.scrollTo(0, 0);
                $(".loading-mask-position").css("top", document.body.scrollTop);
                $rootScope.bodyClass = "menu-opened";

                rede.promise = RedeCredenciadaService.buscaEstabelecimentos(rede.formData.modalFiltro);

                rede.promise.then(function(response) {

                    var estabelecimentos = response.value;
                    rede.formData.listaFiltrada = [];
                    rede.formData.listResults = [];
                    var listaResult = [];

                    //   var iconPinCurrentLocation = {
                    //     url: "assets/images/tmaps-marker-current-location.png",
                    //     size: new google.maps.Size(48, 56),
                    //     origin: new google.maps.Point(0, 0),
                    //     scaledSize: new google.maps.Size(48, 56)
                    // };

                    // listaResult.push(
                    //     {
                    //         name: "",
                    //         fav: "",
                    //         address1: "",
                    //         address2: "",
                    //         lat: rede.modalFiltro.Latitude,
                    //         long: rede.modalFiltro.Longitude,
                    //         phone: "",
                    //         type: "",
                    //         icon: iconPinCurrentLocation,
                    //         avante: null,
                    //         collact: false,
                    //         marker: null,
                    //         codEstabelecimento: 0,
                    //         products: 0
                    //     });

                    // var marker = new google.maps.Marker({
                    //     map: rede.map,
                    //     position: new google.maps.LatLng(rede.modalFiltro.Latitude, rede.modalFiltro.Longitude),
                    //     icon: iconPinCurrentLocation,
                    //     animation: google.maps.Animation.DROP,
                    // });



                    var zoom = 0;

                    var radius = (rede.formData.modalFiltro.Radius / 1000);
                    switch (radius) {
                        case 0.5:
                        case 1:
                        case 2:
                            zoom = 17;
                            break;
                        case 3:
                        case 4:
                            zoom = 16;
                            break;
                        case 5:
                        case 6:
                            zoom = 15;
                            break;
                        case 7:
                        case 8:
                        case 9:
                        case 10:
                            zoom = 14;
                            break;
                        default:
                            zoom = 15;
                    }

                    rede.mapOptions = {
                        zoom: zoom,
                        center: new google.maps.LatLng(rede.formData.modalFiltro.Latitude, rede.formData.modalFiltro.Longitude),
                        mapTypeId: google.maps.MapTypeId.ROADMAP
                    };
                    angular.forEach(estabelecimentos, function(estabelecimento, key) {
                        var iconPin = {};
                        var urlPin = "";
                        if (estabelecimento.favorite && estabelecimento.programmes && estabelecimento.programmes.length > 0) {
                            urlPin = 'assets/images/tmaps-marker-fav-a.png';
                        }
                        else if (estabelecimento.favorite) {
                            urlPin = 'assets/images/tmaps-marker-fav.png';
                        }
                        else {
                            switch (rede.formData.modalFiltro.CardBin) {
                                case "TRE":
                                    {
                                        urlPin = 'assets/images/tmaps-marker-tr.png';
                                        if (estabelecimento.programmes && estabelecimento.programmes.length > 0) {
                                            urlPin = 'assets/images/tmaps-marker-tr-a.png';
                                        }
                                        break;
                                    }
                                case "TAE":
                                    {
                                        urlPin = 'assets/images/tmaps-marker-ta.png';
                                        if (estabelecimento.programmes && estabelecimento.programmes.length > 0) {
                                            urlPin = 'assets/images/tmaps-marker-ta-a.png';
                                        }
                                        break;
                                    }
                                case "TKE":
                                    urlPin = 'assets/images/tmaps-marker-tk.png';
                                    break;
                                case "TC":
                                    urlPin = '';
                                    break;
                                default:
                                    urlPin = '';
                            }
                        }

                        iconPin = {
                            url: urlPin,
                            size: new google.maps.Size(48, 56),
                            origin: new google.maps.Point(0, 0),
                            scaledSize: new google.maps.Size(48, 56)
                        };

                        listaResult.push(
                            {
                                name: estabelecimento.tradingName,
                                fav: estabelecimento.favorite,
                                address1: estabelecimento.address,
                                address2: estabelecimento.district + ", " + estabelecimento.city + " - " + estabelecimento.state + ", " + estabelecimento.zipcode,
                                lat: estabelecimento.geolocation.lat,
                                long: estabelecimento.geolocation.long,
                                phone: estabelecimento.telephone,
                                type: rede.cardtype,
                                icon: iconPin,
                                avante: estabelecimento.programmes && estabelecimento.programmes.length > 0,
                                collact: false,
                                marker: null,
                                codEstabelecimento: estabelecimento.id,
                                products: estabelecimento.products
                            });
                    });

                    //rede.listaFiltrada = listaResult;
                    rede.formData.listResults = listaResult;
                    rede.formData.listaFiltrada = rede.formData.listResults;
                    initMap();

                    $rootScope.bodyClass = "";
                    rede.closeSearch();

                }, function(error) {
                    rede.erro = true;
                    $rootScope.bodyClass = "";
                });
            }

            function favoritarCartao(modalEstabelecimento) {
                $window.scrollTo(0, 0);
                $(".loading-mask-position").css("top", document.body.scrollTop);
                if (modalEstabelecimento.Favoritar) {
                    rede.promise = RedeCredenciadaService.favoritarCartao(rede.usuario.id, modalEstabelecimento);
                }
                else {
                    rede.promise = RedeCredenciadaService.excluirFavorito(rede.usuario.id, modalEstabelecimento);
                }
                rede.promise.then(function(response) {
                    rede.formData.modalFiltro.UserId = rede.usuario.id;
                    //rede.cardtype =  rede.formData.modalFiltro.CardBin;
                    if (!rede.showFav) {
                        buscaEstabelecimentosBD();
                    }
                    else {
                        checkFavoritos();
                    }

                }, function(error) {

                });
            }

            function autoComplete() {
                $timeout(
                    $("#buscaEnderecoNome").autocomplete({
                        source: function(request, response) {

                            var geocoder = new google.maps.Geocoder();
                            geocoder.geocode({ 'address': request.term + ', Brasil', 'region': 'BR' }, function(results, status) {
                                response($.map(results.slice(0, 5), function(item) {

                                    return {
                                        label: item.formatted_address,
                                        value: item.formatted_address,
                                        latitude: item.geometry.location.lat(),
                                        longitude: item.geometry.location.lng()
                                    };
                                }));
                            });
                        },
                        select: function(event, ui) {
                            rede.formData.address = ui.item.value;
                        },
                        autoFocus: true,
                        minLength: 1,
                    }).autocomplete("widget").addClass('tkt-autocomplete'), 1000);
            }

            jQuery.ui.autocomplete.prototype._resizeMenu = function() {
                var ul = this.menu.element;
                ul.outerWidth(this.element.outerWidth());
            };
        }
    ]);

})();
