(function() {
    'use strict';
    angular.module('portalUsuarioApp').factoryLazy('RedeCredenciadaModalsFactory', [
        '$uibModal',
        function($uibModal) {

            var searchModalInstance;

            var service = {
                openSearch: openSearch,
                closeSearch: closeSearch,
            };

            return service;

            function openSearch() {
                
                searchModalInstance = $uibModal.open({
                    animation: true,
                    templateUrl: 'app/modules/beneficiario/modal/redecredenciadafiltro.htm',
                    windowClass: '-ticket -search',
                    size: 'sm'
                });
            }

            function closeSearch() {
                
                if (searchModalInstance) {
                    searchModalInstance.dismiss();
                }
            }
        }
    ]);

})();
