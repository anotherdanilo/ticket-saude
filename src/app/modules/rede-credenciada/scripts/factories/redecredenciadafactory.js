(function() {
    'use strict';

    angular.module('portalUsuarioApp').factory('RedeCredenciadaFactory', ['RedeCredenciadaService',
        function(RedeCredenciadaService) {
            var infoWindow;
            // set variables
            var redeFormData = {
                modalFiltro:{
                    Latitude: 0,
                    Longitude: 0,
                    CardBin: "",
                    Radius: 0,
                    UserId: "0",
                    Offset: 0,
                    Limit: 0
                }
            };
            resetRedeFormData();

            // set service
            var service = {
                bindRedeFormData: bindRedeFormData,
                submitRedeFormData: submitRedeFormData,
                //atualizarBusca: atualizarBusca,
                //closeModal: closeModal
                infoWindow: infoWindowMethod
            };

            return service;

            // public
            // used to bind redeFormData to controller
            function bindRedeFormData() {
                return redeFormData;
            }

            // public
            // used to submit redeFormData
            function submitRedeFormData() {
                console.log(redeFormData);
            }

            // private
            // reset redeFormData
            function resetRedeFormData() {

                var iconTr = {
                    url: '../assets/images/tmaps-marker-tr.png',
                    size: new google.maps.Size(48, 56),
                    origin: new google.maps.Point(0, 0),
                    scaledSize: new google.maps.Size(48, 56)
                };
                var iconTrAC = {
                    url: '../assets/images/tmaps-marker-tr-ac.png',
                    size: new google.maps.Size(48, 56),
                    origin: new google.maps.Point(0, 0),
                    scaledSize: new google.maps.Size(48, 56)
                };
                var iconTrA = {
                    url: '../assets/images/tmaps-marker-tr-a.png',
                    size: new google.maps.Size(48, 56),
                    origin: new google.maps.Point(0, 0),
                    scaledSize: new google.maps.Size(48, 56)
                };
                var iconTrC = {
                    url: '../assets/images/tmaps-marker-tr-c.png',
                    size: new google.maps.Size(48, 56),
                    origin: new google.maps.Point(0, 0),
                    scaledSize: new google.maps.Size(48, 56)
                };
                var iconTa = {
                    url: '../assets/images/tmaps-marker-ta.png',
                    size: new google.maps.Size(48, 56),
                    origin: new google.maps.Point(0, 0),
                    scaledSize: new google.maps.Size(48, 56)
                };
                var iconTaA = {
                    url: '../assets/images/tmaps-marker-ta-a.png',
                    size: new google.maps.Size(48, 56),
                    origin: new google.maps.Point(0, 0),
                    scaledSize: new google.maps.Size(48, 56)
                };
                var iconFav = {
                    url: '../assets/images/tmaps-marker-fav.png',
                    size: new google.maps.Size(48, 56),
                    origin: new google.maps.Point(0, 0),
                    scaledSize: new google.maps.Size(48, 56)
                };
                var iconFavC = {
                    url: '../assets/images/tmaps-marker-fav-c.png',
                    size: new google.maps.Size(48, 56),
                    origin: new google.maps.Point(0, 0),
                    scaledSize: new google.maps.Size(48, 56)
                };
                var iconFavA = {
                    url: '../assets/images/tmaps-marker-fav-a.png',
                    size: new google.maps.Size(48, 56),
                    origin: new google.maps.Point(0, 0),
                    scaledSize: new google.maps.Size(48, 56)
                };
                var iconFavAC = {
                    url: '../assets/images/tmaps-marker-fav-ac.png',
                    size: new google.maps.Size(48, 56),
                    origin: new google.maps.Point(0, 0),
                    scaledSize: new google.maps.Size(48, 56)
                };
                var iconTk = {
                    url: '../assets/images/tmaps-marker-tk.png',
                    size: new google.maps.Size(48, 56),
                    origin: new google.maps.Point(0, 0),
                    scaledSize: new google.maps.Size(48, 56)
                };

                redeFormData.address = '';
                redeFormData.radius = '0';
                redeFormData.cardtype = 'ta';
                redeFormData.listFav = '';
                redeFormData.listResults = [];
                redeFormData.listaFiltrada = [];
                redeFormData.idUsuario = '';
            }

            function atualizarBusca(modalFiltro) {

                var geocoder = new google.maps.Geocoder();
                var address = redeFormData.address;
                geocoder.geocode({ 'address': address }, function(results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        var lat = results[0].geometry.location.lat();
                        var long = results[0].geometry.location.lng();
                        modalFiltro = {
                            Latitude: lat,
                            Longitude: long,
                            CardBin: rede.cardtype,
                            Radius: redeFormData.radius === "0" ? 500 : parseInt(redeFormData.radius) * 1000,
                            UserId: redeFormData.idUsuario,
                            Offset: 1,
                            Limit: 500
                        };

                        buscaEstabelecimentosBD();
                    }
                });
            }
            
            function buscaEstabelecimentosBD(modalFiltro) {
                $(".loading-mask-position").css("top", document.body.scrollTop);
                $rootScope.bodyClass = "menu-opened";

                var promise = RedeCredenciadaService.buscaEstabelecimentos(modalFiltro);

                promise.then(function(response) {
                    var estabelecimentos = response.value;
                    rede.formData.listaFiltrada = [];
                    rede.formData.listResults = [];
                    var listaResult = [];
                    var zoom = 0;
                    var radius = (modalFiltro.Radius/1000);
                    switch (radius) {
                        case 0.5:
                        case 1:
                        case 2:
                            zoom = 17;
                            break;
                        case 3:
                        case 4:
                            zoom = 16;
                            break;
                        case 5:
                        case 6:
                            zoom = 15;
                            break;
                        case 7:
                        case 8:
                        case 9:
                        case 10:
                            zoom = 14;
                            break;
                        default:
                            zoom = 15;
                    }

                    rede.mapOptions = {
                        zoom: zoom,
                        center: new google.maps.LatLng(modalFiltro.Latitude, modalFiltro.Longitude),
                        mapTypeId: google.maps.MapTypeId.ROADMAP
                    };
                    angular.forEach(estabelecimentos, function(estabelecimento, key) {
                        var iconPin = {};
                        var urlPin = "";
                        if (estabelecimento.favorite && estabelecimento.programmes && estabelecimento.programmes.length > 0) {
                            urlPin = 'assets/images/tmaps-marker-fav-a.png';
                        }
                        else if (estabelecimento.favorite) {
                            urlPin = 'assets/images/tmaps-marker-fav.png';
                        }
                        else {
                            switch (rede.cardtype) {
                                case "TRE":
                                    {
                                        urlPin = 'assets/images/tmaps-marker-tr.png';
                                        if (estabelecimento.programmes && estabelecimento.programmes.length > 0) {
                                            urlPin = 'assets/images/tmaps-marker-tr-a.png';
                                        }
                                        break;
                                    }
                                case "TAE":
                                    {
                                        urlPin = 'assets/images/tmaps-marker-ta.png';
                                        if (estabelecimento.programmes && estabelecimento.programmes.length > 0) {
                                            urlPin = 'assets/images/tmaps-marker-ta-a.png';
                                        }
                                        break;
                                    }
                                case "TKE":
                                    urlPin = 'assets/images/tmaps-marker-tk.png';
                                    break;
                                case "TC":
                                    urlPin = '';
                                    break;
                                default:
                                    urlPin = '';
                            }
                        }

                        iconPin = {
                            url: urlPin,
                            size: new google.maps.Size(48, 56),
                            origin: new google.maps.Point(0, 0),
                            scaledSize: new google.maps.Size(48, 56)
                        };

                        listaResult.push(
                            {
                                name: estabelecimento.tradingName,
                                fav: estabelecimento.favorite,
                                address1: estabelecimento.address,
                                address2: estabelecimento.district + ", " + estabelecimento.city + " - " + estabelecimento.state + ", " + estabelecimento.zipcode,
                                lat: estabelecimento.geolocation.lat,
                                long: estabelecimento.geolocation.long,
                                phone: estabelecimento.telephone,
                                type: rede.cardtype,
                                icon: iconPin,
                                avante: estabelecimento.programmes && estabelecimento.programmes.length > 0,
                                collact: false,
                                marker: null,
                                codEstabelecimento: estabelecimento.id,
                                products: estabelecimento.products
                            });
                    });

                    //rede.listaFiltrada = listaResult;
                    redeFormData.listResults = listaResult;
                    redeFormData.listaFiltrada = rede.formData.listResults;
                    initMap();

                    //$rootScope.bodyClass = "";
                    //rede.closeSearch();
                    
                    return resolve(response);

                }, function(error) {
                    //rede.erro = true;
                    //$rootScope.bodyClass = "";
                    return reject(error);
                });
            }
            
            function infoWindowMethod()
            {
                if(!infoWindow)
                {
                    infoWindow = new google.maps.InfoWindow();
                }
                
                return infoWindow;
            }
            
        }
    ]);

})();
