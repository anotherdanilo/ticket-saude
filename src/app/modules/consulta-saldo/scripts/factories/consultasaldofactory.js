(function () {
    'use strict';

    angular.module('portalUsuarioApp').factory('ConsultaSaldoFactory', [
        '$uibModal',
        function ($uibModal) {

            var parametros = {
                recaptcha: '',
                dadocartao: null
            };

            var service = {
                parametros: parametros,
                init: init
            };

            return service;

            function init() {
                parametros = {
                    recaptcha: '',
                    dadocartao: null
                };
            }
        }
    ]);

})();
