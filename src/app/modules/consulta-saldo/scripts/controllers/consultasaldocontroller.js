(function () {
    'use strict';


    angular.module('portalUsuarioApp').controllerLazy('ConsultaSaldoController', [
        '$rootScope', '$scope', '$http', 'LoginModalsFactory', '$location', 'CartaoService',
        'AutenticacaoService', 'vcRecaptchaService', '$window', 'RecaptchaService',
        'AlertaFactory', 'ConsultaSaldoFactory', 'prebidService',
        function ($rootScope, $scope, $http, LoginModalsFactory, $location, CartaoService,
            AutenticacaoService, vcRecaptchaService, $window, RecaptchaService, AlertaFactory,
            ConsultaSaldoFactory, prebidService) {

            $location.search({});
            setTimeout(function () { $rootScope.menuSelecionado = 4; }, 1);
            $scope.$emit('newPageLoaded', {
                title: 'Consulta de Saldo de Cartões Ticket | Sou Ticket',
                description: "Consulta de saldo dos cartões Ticket, confira o extrato de utilização e gerencie seu benefício.",
                path: $location.absUrl(),
                imageurl: ''
            });

            var consulta = this;

            
            ConsultaSaldoFactory.init();

            consulta.myPromise = null;
            consulta.isMobileBrowser = true;
            consulta.cardType = "none";
            consulta.openSignUp = LoginModalsFactory.openSignUp;
            consulta.mensagem = null;
            $scope.response = null;
            $scope.widgetId = null;
            consulta.model = {
                key: config.chavesitegoogle
            };

            $scope.setResponse = function (response) {
                $scope.response = response;
            };

            $scope.setWidgetId = function (widgetId) {
                $scope.widgetId = widgetId;
            };

            $scope.cbExpiration = function() {                 
                vcRecaptchaService.reload($scope.widgetId);
                $scope.response = null;
            };

            consulta.usuarioLogado = AutenticacaoService.obterUsuarioLogado();

            consulta.verificaLogado = function () {
                if (consulta.usuarioLogado.isLogged) {
                    $location.path('/beneficiario/dashboard');
                } else {
                    prebidService.getConsultaSaldoAds();
                }
            };
            consulta.toDashboardCartao = function () {

                consulta.mensagem = null;
                consulta.mensagem_campo = null;

                var numeroCartao = consulta.numeroCartao.replace(/ /g, '');
                var inicioCartao = numeroCartao.substring(0, 6);

                if(inicioCartao == "308512" || inicioCartao == "605680")
                {
                    consulta.mensagem_campo = "Para consulta de saldo dos cartões Ticket Log, baixe o app Ticket Log ou acesse “Meu Saldo” em <a href='https://www.ticketlog.com.br' target='_blank'><u>www.ticketlog.com.br</u></a>";
                    consulta.numeroCartao = "";
                    return false;
                }

                var pattern = /^[0-9 ]{19}$/;
                var isValid = pattern.test(consulta.numeroCartao);

                if (!isValid) {
                    consulta.mensagem_campo = "Campo Número do Cartão inválido.";//$("#numero-cartao").attr('invalid-error-message');
                    //$("#numero-cartao").parent().append("<div id='mensagem' class='validation-msg'>" +  + "</div>");
                    return false;
                }

                $window.sessionStorage.removeItem("numeroDoCartao");

                if (consulta.numeroCartao !== undefined) {

                    consulta.numeroCartaoTratado = consulta.numeroCartao;

                    consulta.numeroCartaoTratado = consulta.numeroCartaoTratado.replace(' ', '').replace(' ', '').replace(' ', '');
                    $window.sessionStorage.numeroDoCartao = consulta.numeroCartaoTratado;
                }
                if ($window.sessionStorage.numeroDoCartao !== undefined) {
                    consulta.validarRecaptcha();

                }

            };

            consulta.toDashboardCpf = function () {
                consulta.mensagemtt = null;
                $window.sessionStorage.removeItem("numeroDoCartao");
                
                if (consulta.cpf) {
                    consulta.cpfTratado = consulta.cpf;
                    consulta.cpfTratado = consulta.cpfTratado.replace('.', '').replace('.', '').replace('-', '');
                    $window.sessionStorage.numeroDoCartao = consulta.cpfTratado;
                }

                if ($window.sessionStorage.numeroDoCartao !== undefined) {

                    consulta.verificarcartaott($window.sessionStorage.numeroDoCartao);

                }
                else {
                    consulta.mensagemtt = "Campo CPF inválido.";
                    consulta.cpf = "";
                }
            };

            consulta.verificarNumeroCartao = function (numero, recaptcha) {

                 
                $rootScope.bodyClass = "menu-opened";
                $(".loading-mask-position").css("top", document.body.scrollTop);
                consulta.mensagem = null;
                consulta.myPromise = CartaoService.consultarSaldo(numero, recaptcha);
                
                consulta.myPromise.then(function (response) {

                    ConsultaSaldoFactory.parametros.dadocartao = response;
                    $window.sessionStorage.numeroDoCartao = numero;

                    if ($window.location.href.indexOf("consulta-saldo-teste") > 0) {
                        $window.location.href = window.location.origin + '/consulta-saldo/dashboard';
                    } else {
                        $location.path('/consulta-saldo/dashboard');
                    }
                    $rootScope.bodyClass = "";
                },
                    function (error) {
                        
                        vcRecaptchaService.reload($scope.widgetId);
                        consulta.mensagem = error.message;
                        consulta.numeroCartao = "";
                        $rootScope.bodyClass = "";
                    });

            };

            consulta.verificarcartaott = function (numero) {

                consulta.mensagemtt = null;
                consulta.myPromise = CartaoService.consultarSaldo(numero);
                consulta.myPromise.then(function (response) {

                    $window.sessionStorage.numeroDoCartao = numero;
                    ConsultaSaldoFactory.parametros.dadocartao = response;

                    if (response.value.ttCard.length > 0) {
                        $location.path('/consulta-saldo/dashboard');
                    }
                    else {
                        consulta.mensagemtt = "Esse CPF não possui benefícios gerenciados pela Ticket.";
                        consulta.cpf = "";
                    }

                },
                    function (error) {
                        consulta.mensagemtt = "Campo CPF inválido.";
                        consulta.cpf = "";

                    });

            };

            consulta.validarRecaptcha = function () {

                consulta.mensagem = null;
                
                var valorRetorno = vcRecaptchaService.getResponse($scope.widgetId);

                if (valorRetorno === "" || valorRetorno === undefined) {
                    consulta.mensagem = "Preencha o desafio do Não sou robô";

                } else {
                    
                    ConsultaSaldoFactory.parametros.recaptcha = valorRetorno;

                    // $(".loading-mask-position").css("top", document.body.scrollTop);
                    // $rootScope.bodyClass = "menu-opened";
                    //consulta.myPromise = RecaptchaService.verificarRecaptchaService(valorRetorno);
                    // consulta.myPromise.then(function (response) {
                    //     if (response.success) {

                    consulta.verificarNumeroCartao($window.sessionStorage.numeroDoCartao, valorRetorno);

                    //     }
                    //     else {
                    //         consulta.mensagem = "Preencha o desafio do Não sou robô, corretamente";
                    //     }

                    //     $rootScope.bodyClass = "";

                    // },
                    //     function (error) {

                    //         consulta.mensagem = "Preencha o desafio do Não sou robô, corretamente";
                    //         vcRecaptchaService.reload($scope.widgetId);

                    //         $rootScope.bodyClass = "";

                    //     });

                }
            };

            consulta.openAlerta = AlertaFactory.openAlerta;

            consulta.changeInput = function () {
                if (consulta.numeroCartao) {
                    var bin = consulta.numeroCartao.replace(" ", "").slice(0, 6);

                    if (bin === "602651") {
                        consulta.cardType = '-alimentacao';
                    }
                    else if (bin === "603342") {
                        consulta.cardType = '-restaurante';
                    }
                    else if (bin === "308513") {
                        consulta.cardType = '-cultura';
                    }
                    else if (bin === "308512") {
                        consulta.cardType = '-car';
                    }
                    else if (bin === "603574" || bin === "605674" || bin === "605681") {

                        consulta.numeroCartao = "";
                        
                        AlertaFactory.parametros.titulo = "Cartão Ticket Plus";
                        AlertaFactory.parametros.mensagem = "Você está digitando um cartão Ticket Plus, para consultar o limite do seu cartão clique no botão abaixo.";
                        AlertaFactory.parametros.icone = "icon-done";
                        // AlertaFactory.parametros.callback = callback;

                        consulta.openAlerta("Ver limite Ticket Plus", callback);

                    }
                    else {
                        consulta.cardType = 'none';
                    }
                }
            };

            function callback() {
                $window.open(config.urls.ticketPlus);
            }


        }]);

})();
