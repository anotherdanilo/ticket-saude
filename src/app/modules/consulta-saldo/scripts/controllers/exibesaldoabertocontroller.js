﻿(function () {
    'use strict';

    angular.module('portalUsuarioApp').controllerLazy('ExibeSaldoAbertoController', [
        '$scope', '$http', 'LoginModalsFactory', '$location', '$window', 'CartaoService',
        'AutenticacaoService', '$rootScope', 'ConsultaSaldoFactory', 'prebidService',
        function ($scope, $http, LoginModalsFactory, $location, $window, CartaoService,
            AutenticacaoService, $rootScope, ConsultaSaldoFactory, prebidService) {

            var consulta = this;
            consulta.myPromise = null;
            consulta.isMobileBrowser = true;
            consulta.maximo = 5;
            consulta.usuarioLogado = AutenticacaoService.obterUsuarioLogado();

            //consulta.openMobileBrowser = AppModalsFactory.openDownload;
            //startModal();

            $scope.formatDateExtrato = function (date) {
                return moment(date, "DD/MM/YYYY").format("DD/MM");
            };


            $scope.formatDateExtratoTT = function (date) {
                return moment(date, "DD/MM/YYYY").format("DD/MM/YYYY");
            };
            
            $scope.retiraOperadora = function(descricao){
                return descricao.substring(descricao.indexOf("-") + 1);
            };


            function startModal() {
                if (consulta.isMobileBrowser) {
                    //consulta.openMobileBrowser();
                }
            }


            consulta.openSignUp = function (origem) {

                if (!consulta.usuarioLogado.isLogged) {
                    consulta.openCadastro(origem);

                } else {

                    $location.path('/beneficiario/dashboard');
                }

            };

            consulta.openCadastro = function (origem) {

                if (origem === 1) {
                    LoginModalsFactory.parametros.mensagem = 'Para consultar outro cartão Ticket você precisa fazer um rápido cadastro ou entrar com seu login clicando em ‘Entrar’.';
                }
                else if (origem === 2) {
                    LoginModalsFactory.parametros.mensagem = 'Para ter informações sobreo próximo crédito, acessar gráficos de consumo e adicionar outros cartões, você precisa fazer um rápido cadastro ou entrar com seu login clicando em ‘Entrar’.';
                }
                else if (origem === 3) {
                    LoginModalsFactory.parametros.mensagem = 'Para ver detalhes do extrato de uso do seu cartão Ticket você precisa fazer um rápido cadastro ou entrar com seu login clicando em ‘Entrar’.';
                }
                else if (origem === 4) {
                    LoginModalsFactory.parametros.mensagem = 'Para editar, bloquear, desbloquear ou reemitir cartões Ticket, você precisa fazer um rápido cadastro ou entrar com seu login clicando em ‘Entrar’.';
                }

                LoginModalsFactory.parametros.urlRetorno = $location.$$path;
                LoginModalsFactory.parametros.urlRetornoParam = $location.$$search.p;

                LoginModalsFactory.openSignUp();
            };
            
            
            
            
            consulta.verificarNumeroCartao = function (numero) {
                consulta.myPromise = CartaoService.carregarCartaoPorNumero(numero);
                consulta.myPromise.then(function (response) {


                    if (response.value.ttCard !== undefined && response.value.ttCard.count > 0) {
                        var ttCard = response.value.ttCard;

                        consulta.usuario.beneficiario = ttCard[0].beneficiario;
                        consulta.usuario.cartao.operadora = ttCard[0].operadora;
                        consulta.usuario.cartao.conducao = ttCard[0].conducao;
                        consulta.usuario.cartao.numero = ttCard[0].numero;

                    }

                },
                    function (error) {
                        console.log('Ops, ocorreu um erro inesperado. Por favor, tente novamente mais tarde.' + error);


                    });

            };

            consulta.obterInformacoesCartao = function () {
                prebidService.getConsultaSaldoAds();
                
                consulta.cssCartao = "card-ticket";
                consulta.details = "card-details";

                //var numero = $location.$$search.numero;

                var numero = $window.sessionStorage.numeroDoCartao;
                var recaptcha = ConsultaSaldoFactory.parametros.recaptcha;
                var response = ConsultaSaldoFactory.parametros.dadocartao;

                if(response){
                // $window.scrollTo(0, 0);
                // $(".loading-mask-position").css("top", document.body.scrollTop);

                // consulta.myPromise = CartaoService.consultarSaldo(numero, recaptcha);
                // consulta.myPromise.then(function (response) {

                    
                    consulta.usuario = {};
                    consulta.usuario.cartao = {};
                    
                    consulta.usuario.cartao = response.value.balance;
                    consulta.usuario.cartao.creditos = [];
                    consulta.usuario.cartao.creditos = response.value.scheduling;
                    var length = null;
                    if (response.value.ttCard.length > 0) {
                        var ttCard = [];
                        ttCard = response.value.ttCard;
                        consulta.usuario.beneficiario = ttCard[0].beneficiario;
                        consulta.usuario.cartao.operadora = ttCard[0].operadora;
                        consulta.usuario.cartao.conducao = ttCard[0].conducao;
                        consulta.usuario.cartao.numero = ttCard[0].numero;

                        if (ttCard[0].numero) {
                            length = ttCard[0].numero.length;
                            consulta.usuario.cartao.cardlast = ttCard[0].numero.substring(length - 4);
                        }
                        
                  // consulta.usuario.cartao.dataEntrega = ttCard[0].dateParsed;

                    }

                    consulta.usuario.cartao.extrato = response.value.release;

                    if (response.value.balance.bin !== "TT") {
                        length = response.value.balance.number.length;
                        consulta.usuario.cartao.cardlast = response.value.balance.number.substring(length - 4);
                    }
                    consulta.usuario.cartao.saldo = response.value.balance.valueParsed;

                    

                  switch (response.value.balance.bin) {
                      case "TRE":
                          consulta.cssCartao = "card-ticket -restaurante";
                          consulta.details = "card-details -restaurante";
                          break;
                      case "TAE":
                          consulta.cssCartao = "card-ticket -alimentacao";
                          consulta.details = "card-details -alimentacao";
                          break;
                      case "TKE":
                          consulta.details = "card-details -cultura";
                          consulta.cssCartao = "card-ticket -cultura";
                          break;
                      case "TC":
                          consulta.cssCartao = "card-ticket -car";
                          consulta.details = "card-details -car";

                          break;
                      default:
                          consulta.isTT = true;
                          consulta.cssCartao = "card-ticket -transporte";
                          consulta.cssCartao = "card-ticket -transporte";
                  }


                    switch (consulta.usuario.cartao.situacao) {
                        case 2: // 2: BloqueadoTicket
                            consulta.details += " -blocked";
                            consulta.textoBloqueado = "Cartão Bloqueado";
                            consulta.textoBloqueadoMin = "Bloqueado Ticket";
                            break;
                        case 3: // 3: BloqueadoBeneficiario
                            consulta.details += " -blocked";
                            consulta.textoBloqueado = "Cartão Bloqueado";
                            consulta.textoBloqueadoMin = "Bloqueado";
                            break;
                        case 4: // 4: Cancelado 
                            consulta.details += " -blocked";
                            consulta.textoBloqueado = "Cartão Cancelado";
                            consulta.textoBloqueadoMin = "Cancelado";
                            break;
                    }

                    if (consulta.usuario.cartao.situacao !== 1){ //
                        consulta.details += ' -blocked';
                    }

                    consulta.carregado = true;

                }
                else{
                     consulta.carregado = false;
                }
                // },
                // function (error) {
                //     
                //     consulta.carregado = false;

                // });
                
            };
      }

    ]);

})();
