(function() {
'use strict';

angular.module('portalUsuarioApp')

.config(['$httpProvider', function($httpProvider) {
    $httpProvider.interceptors.push('OauthService');
}])

.factoryLazy('OauthService', [
  '$cookieStore','$location', function OauthService($cookieStore,$location){
  var token = null;
  var idUsuario = null;

  var setToken = function setToken(someToken) {
      token = someToken.AccessToken;
      idUsuario = someToken.IdUsuario;
  };

  var getToken = function getToken() {
      return token;
  };

  var redirecionarParaLogin = function(){
    $location.path('/autenticacao/login');
  };

  var request = function request(config) {
      if (token) {
          // jqXHR.setRequestHeader('Authorization','Token token="' + app.user.api_key.access_token + '"');
            //config.headers["Authorization"] = "Bearer " + token;
            //config.headers["IdUsuario"] = idUsuario;
            //console.log($cookieStore.get('AccessToken'));
        }
        //else
          //redirecionarParaLogin();

        return config;
  };



  return {
      setToken: setToken,
      getToken: getToken,
      request: request
  };

}]);

}).call(this);