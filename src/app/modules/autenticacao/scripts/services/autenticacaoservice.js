(function () {
    'use strict';
    angular.module('portalUsuarioApp').serviceLazy("AutenticacaoService", [
        '$location', '$q', 'LoginResource', 'UsuarioResource', 'cookieService', '$resource','AlertaFactory','$cookies','$base64',
        function ($location, $q, LoginResource, UsuarioResource, cookieService, $resource, AlertaFactory,$cookies,$base64) {

            moment.locale("pt-br");
            var self = this;
            var usuario = { nome: "", isLogged: false, id: 0, isFacebook: false,  cadastroCompleto: false, redirecionando: false };

            self.autenticar = function (login, senha, token, manterconectado, refreshtoken, isFacebook) {

                return $q(function (resolve, reject) {

                    LoginResource.save({
                        Email: login,
                        Password: senha,
                        Token: token,
                        origemWeb: true,
                        RefreshToken: refreshtoken
                    }, (function (response) {

                        var segundos = moment().add(response.expires_in, 'seconds');
                        var horaFinal = moment(segundos, "DD/MM/YYYY").format("DD/MM/YYYY HH:mm:ss");
                        var oauth = {
                            access_token: response.access_token,
                            expires_in: horaFinal,
                            refresh_token: response.refresh_token
                        };
                        cookieService.criarCookieAutenticacao(oauth, manterconectado);
                        return UsuarioResource.get({ id: response.id_user }, function (usuario) {
                            
                            //valida se o cadastro está completo
                            var u =  usuario.value;
                            var e = u.endereco;

                            usuario.value.cadastroCompleto = false;
                            usuario.value.redirecionando = false;

                            var enderecoOk = false;
                            if(e)
                            {
                                if((e.logradouro) && (e.numero) && (e.bairro) && (e.cidade) && (e.cep) && (e.uf))
                                {
                                    enderecoOk = true;
                                }
                            }
                                
                            if((u.name) && (u.cpf) && (u.email) && (u.nascimento) && (u.cellPhone) && (u.sexo.trim()) && enderecoOk)
                            {
                                usuario.value.cadastroCompleto = true;
                            }
                                 
                            //fim
                            usuario.value.manterconectado = manterconectado;
                            usuario.value.name = usuario.value.name.split(' ')[0];
                            usuario.value.isFacebook = isFacebook;
                            
                            cookieService.criarCookieUsuario(usuario.value);
                            return resolve(null);
                        }, function (error) {
                            var mensagemErro;
                            mensagemErro = "Não foi possível obter os dados do usuário";
                            return reject(mensagemErro);
                        });
                    }), function (error) {

                        // console.log(error);
                        // var mensagemErro;
                        // mensagemErro = "Login/Senha inválidos";
                        
                        return reject(error);
                    });
                });
            };

            // this.setNomeUsuarioLogado = function()
            // {
            //     var cookieAutenticacao, cookieUsuario;
            //     cookieAutenticacao = cookieService.obterValorCookieAutenticacao();
            //     cookieUsuario = cookieService.obterValorCookieUsuario();
            //     if (!cookieAutenticacao || !cookieUsuario) {
            //         return null;
            //     }
            //     nomeUsuario = cookieUsuario.name;
            // };

            self.setNomeUsuarioLogado = function () {
                
                var cookieAutenticacao, cookieUsuario;
                cookieAutenticacao = cookieService.obterValorCookieAutenticacao();
                cookieUsuario = cookieService.obterValorCookieUsuario();
                if (!cookieAutenticacao || !cookieUsuario) {
                    usuario.id = 0;
                    usuario.nome = "";
                    usuario.isLogged = false;
                    return null;
                }
                
                usuario.nome = cookieUsuario.name;
                usuario.isLogged = true;
                usuario.id = cookieUsuario.id;
                usuario.isFacebook = cookieUsuario.isFacebook;
                usuario.cadastroCompleto = cookieUsuario.cadastroCompleto;
                usuario.email = cookieUsuario.email;
				usuario.celular = cookieUsuario.cellPhone;
                usuario.dataNascimento = cookieUsuario.nascimento;
                usuario.cpf = cookieUsuario.cpf;
            };

            self.obterUsuarioLogado = function () {

                
                if (usuario.id <= 0) {
                    var cookieAutenticacao, cookieUsuario;
                    cookieAutenticacao = cookieService.obterValorCookieAutenticacao();
                    cookieUsuario = cookieService.obterValorCookieUsuario();
                    if (cookieUsuario) {
                        usuario.nome = cookieUsuario.name;
                        usuario.isLogged = true;
                        usuario.id = cookieUsuario.id;
                        usuario.email = cookieUsuario.email;
                        usuario.isFacebook = cookieUsuario.isFacebook;
                        usuario.cadastroCompleto = cookieUsuario.cadastroCompleto;
                        usuario.telaCadastroAberta = cookieUsuario.telaCadastroAberta;
                    }
                }

                if(usuario.id && !usuario.cadastroCompleto && !usuario.redirecionando)
                {
                    self.setRedirecionando(true);
                    AlertaFactory.parametros.titulo = "Cadastro incompleto";
                    AlertaFactory.parametros.mensagem = "Seu cadastro precisa de algumas informações importantes.";
                    AlertaFactory.parametros.icone = "icon-warning";
                    AlertaFactory.openAlerta();
                    $location.path('/beneficiario/usuario/editarusuario');
                }

                return usuario;
            };

            self.setRedirecionando = function(valor){
                usuario.redirecionando = valor;
                cookieService.alterarRedirecionando(valor);
            };

            self.setCadastroCompleto = function(valor){

                usuario.cadastroCompleto = valor;
                cookieService.alterarManterConectado(valor);

            };

            self.validarEmail = function (parametro, isOnlyValidateToken) {

                return $q(function (resolve, reject) {

                    var resource = $resource(config.urls.validarEmailAPI);

                    var r = resource.get({ t: parametro, isOnlyValidateToken: isOnlyValidateToken, origemWeb: true },
                        function (response) {
                            resolve(response);
                        }, function (error) {
                            
                            if (error.status === 410) {
                                if (error.data.erro === "código expirado") {
                                    reject("Código expirado. \n\n Um novo código foi encaminhado para o seu email.");
                                } else {
                                    reject(error.data.erro);
                                }
                            }
                            else if (error.status === 400) {
                                reject("Código inválido");
                            }
                            else {
                                reject('Não foi possivel ativar o cadastro.');
                            }
                        });
                });
            };

            self.reenviarEmail = function (email) {

                return $q(function (resolve, reject) {

                    var resource = $resource(config.urls.reenviarEmailAPI);

                    var r = resource.save({ email: email },
                        function (response) {
                            resolve(response);
                        }, function (error) {
                            
                            reject(error.data.message);
                        });
                });
            };

            self.reenviarSenha = function (email) {

                return $q(function (resolve, reject) {

                    var resource = $resource(config.urls.reenviarSenhaAPI);

                    var r = resource.save({ email: email },
                        function (response) {
                            resolve(response);
                        }, function (error) {
                            reject(error.data.message);
                        });
                });
            };

            self.redefinirSenha = function (email, senha) {

                var model = {
                    email: email,
                    senha: senha,
                    systemtoken: "6785ea7b3c734b32a1f893f06834660a"
                };

                return $q(function (resolve, reject) {

                    //var resource = $resource(config.urls.redefinicaoSenha);

                    var resource = $resource(config.urls.redefinicaoSenha, null, {
                        'update': {
                            method: 'PUT'
                        }
                    });

                    var r = resource.update(model,
                        function (response) {
                            resolve(response);
                        }, function (error) {
                            reject(error.data.message);
                        });
                });
            };

        }
    ]);

}).call(this);