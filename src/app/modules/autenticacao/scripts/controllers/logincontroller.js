﻿(function() {
    'use strict';

    angular.module('portalUsuarioApp').controllerLazy('LoginController', [
        '$scope', '$location', 'AutenticacaoFactory', 'LoginModalsFactory', 'AutenticacaoService', 'Facebook', '$window', 'UsuarioModalsFactory', 'vcRecaptchaService', '$rootScope', 'RecaptchaService','$compile','$browser',
        function ($scope, $location, AutenticacaoFactory, LoginModalsFactory, AutenticacaoService, Facebook, $window, UsuarioModalsFactory, vcRecaptchaService, $rootScope, RecaptchaService, $compile, $browser) {

            
            var login = this;
            
            login.step = 0;
            login.isvalid = true;
            login.setInvalid = setInvalid;
            login.myPromise = null;
            login.formData = AutenticacaoFactory.bindLoginFormData();
            login.formData.Token = null;
            login.formData.Email = null;
            login.formData.Password = null;
            login.formData.isFacebook = false;
            login.submit = submitLoginFormData;
            login.changeToSignUp = LoginModalsFactory.changeToSignUp;
            login.parametros = LoginModalsFactory.parametros;
            login.toRecoverPassword = toRecoverPassword;
            login.redefinirSenha = redefinirSenha;
            login.openSignUp = LoginModalsFactory.openSignUp;

            login.model = {

                key: config.chavesitegoogle

            };

            var stepEs = UsuarioModalsFactory.getEscopo();

            login.step = stepEs.step;


            login.isNotKarma = true;
            login.msgRetorno = "";

            // Define user empty data :/
            $scope.user = {};

            // Defining user logged status
            $scope.logged = false;

            // And some fancy flags to display messages upon user status change
            $scope.byebye = false;
            $scope.salutation = false;

            var userIsConnected = false;

            $scope.widgetId = null;
            $scope.response = null;

            $scope.setWidgetId = function (widgetId) {
                $scope.widgetId = widgetId;
            };

            $scope.setResponse = function (response) {
                $scope.response = response;
            };

            $scope.cbExpiration = function () {
              
                vcRecaptchaService.reload($scope.widgetId);

                $scope.response = null;
            };

            login.pageLoad = function() {

                login.formData.Email = login.parametros.email !== '' ? login.parametros.email : '';
                login.formData.Password = '';
                
                if (LoginModalsFactory.parametros.esqueciMinhaSenha){
                    login.toRecoverPassword();
                    LoginModalsFactory.parametros.esqueciMinhaSenha = false;
                }
            };

            //recebe o email da Ativação do email (Cadastro de Usuário)


            Facebook.getLoginStatus(function(response) {
                
                if (response.status == 'connected') {
                    userIsConnected = true;
                }
            });

            /**
             * IntentLogin
             */
            $scope.IntentLogin = function() {
                // 
                // if (!userIsConnected) {
                    $scope.loginFacebook();
                // }
                // else {
                //     $scope.me();
                // }
            };

            /**
             * Login
             */
            $scope.loginFacebook = function() {
                
                Facebook.login(function() {
                    // não faz nada, deixa a resposta a cargo da promise
                }, {scope: 'public_profile,email'}).then(function(response) {
                    console.log(response);
                    
                    if (response.status == 'connected') {
                        
                        login.formData.Token = response.authResponse.accessToken;
                        $scope.logged = true;
                        $scope.me();
                    }

                }, function (response) {
                    login.msgRetorno = error;
                });
                //Facebook.login(function (response) {
                //    if (response.status == 'connected') {
                //        $scope.logged = true;
                //        $scope.me();
                //    }

                //});
            };

            /**
             * me 
             */
            $scope.me = function() {
                
                Facebook.api('/me', function() {
                    // não faz nada, deixa a resposta a cargo da promise
                },{ "fields": "email,first_name"}).then(function(response) {
                    //login.formData.Token = response.id;
                    login.formData.isFacebook = true;
                    login.formData.Email = response.email;
                    login.formData.Password = "";
                    submitLoginFormData();
                });
                //Facebook.api('/me', function (response) {
                //    login.formData.Token = response.id;
                //    login.formData.Email = "";
                //    login.formData.Password = "";
                //    submitLoginFormData();
                //});
            };

            function setInvalid() {

                login.validarRecaptcha();

            }

            function redefinirSenha(params) {
                
                
                if (login.formData.newPwd !== login.formData.confirmPwd) {
                    login.msgRetorno = "Nova senha e confirmação de senha não conferem! Por favor, digite as senhas novamente.";
                    return false;
                }

                login.myPromise = AutenticacaoService.redefinirSenha(login.formData.Email, login.formData.newPwd);

                login.myPromise.then(function(response) {
                    
                    login.formData.Password = login.formData.newPwd;
                    submitLoginFormData();
                    login.formData.newPwd = "";
                    login.formData.confirmPwd = "";
                    login.formData.Email = "";
                    login.formData.Password = "";
                    login.formData.Token = "";
                    login.formData.remember = "";
                    login.parametros.email = "";
                    UsuarioModalsFactory.closeForgetPwd();
                }, function(error) {
                    login.msgRetorno = error;
                });
            }

            function toRecoverPassword() {
                login.msgRetorno = null;
                login.formData.user = login.formData.Email;
                login.step = 1;
            }

            function submitLoginFormData() {
                
                if(login.formData.Password)
                {
                   login.formData.isFacebook = false; 
                   login.formData.Token = null;
                }
                
                login.myPromise = AutenticacaoService.autenticar(
                    login.formData.Email,
                    login.formData.Password,
                    login.formData.Token,
                    login.formData.remember,
                    null,
                    login.formData.isFacebook);

                login.myPromise.then(function(response) {
                    
                    AutenticacaoService.setNomeUsuarioLogado();
                    login.step = 1; // logado
                    if (login.isNotKarma) {
                        $location.search('t', null);
                        $location.search('p', null);
                        $location.search('rs', null);
                        if (!login.parametros.reload) {
                            if (login.parametros.urlRetorno) {

                                if (login.parametros.urlRetornoParam)
                                    $location.search('p', login.parametros.urlRetornoParam);

                                if (login.parametros.d)
                                    $location.search('d', login.parametros.d);

                                $location.path(login.parametros.urlRetorno);

                                login.parametros.urlRetorno = null;
                            } else {
                                
                                $location.path("/beneficiario/dashboard");
                            }
                        } else {
                            $window.location.reload();
                            LoginModalsFactory.parametros.reload = false;
                        }
                        login.close();
                    }
                }, function (error) {
            
                    if(error.status == 400)
                    {
                        if(error.data.message === "E-mail inválido.")
                        {
                            login.reenviarEmail(); 
                        }
                        else{
                            login.msgRetorno = error.data.message;
                        }
                    }
                    else
                    {
                        login.msgRetorno = "Não foi possível executar a operação, por favor tente novamente mais tarde.";
                    }

                });
            }

            login.close = function() {
                //$scope.modal.dismiss();
                LoginModalsFactory.closeLogin();
            };

            login.reenviarEmail = function() {

                login.myPromise = AutenticacaoService.reenviarEmail(login.formData.Email);

                login.myPromise.then(function(response) {
                    if (response.success) {

                        login.formData.user = login.formData.Email;
                        login.step = 4;
                    }
                }, function(error) {
                    login.msgRetorno = error;
                    //alert("Erro ao processar a requisição, por favor tente mais tarde.");

                });
            };

            login.reenviarSenha = function() {
                login.myPromise = AutenticacaoService.reenviarSenha(login.formData.user);

                login.myPromise.then(function(response) {

                    if (response.success) {
                        login.step = 2;
                    } else {
                        login.mensagem = "Este e-mail não está cadastrado, para realizar seu cadastro <a ng-click='login.openCadastro()'>clique aqui.</a>";
                        login.error = true;
                        vcRecaptchaService.reload($scope.widgetId);
                    }
                }, function(error) {
                    login.msgRetorno = error;
                    vcRecaptchaService.reload($scope.widgetId);
                    //alert("Erro ao processar a requisição, por favor tente mais tarde.");

                });
            };

            login.openCadastro = function() {
                LoginModalsFactory.closeLogin();
                LoginModalsFactory.openSignUp();
            };

            login.validarRecaptcha = function () {

                login.msgRetorno = null;

                var valorRetorno = vcRecaptchaService.getResponse($scope.widgetId);
                
                if (valorRetorno === "" || valorRetorno === undefined) {
                    login.msgRetorno = "Preencha o desafio do Não sou robô";

                } else {
                    $(".loading-mask-position").css("top", document.body.scrollTop);
                    $rootScope.bodyClass = "menu-opened";
                    login.myPromise = RecaptchaService.verificarRecaptchaService(valorRetorno);
                    login.myPromise.then(function (response) {
                            
                        if (response.success) {
                            
                            login.reenviarSenha();
                            login.isvalid = false;

                        }
                        else {

                            login.msgRetorno = "Preencha o desafio do Não sou robô, corretamente";
                            vcRecaptchaService.reload($scope.widgetId);
                        }

                            $rootScope.bodyClass = "";

                        },
                       function (error) {
                           
                           login.msgRetorno = "Preencha o desafio do Não sou robô, corretamente";
                           vcRecaptchaService.reload($scope.widgetId);

                           $rootScope.bodyClass = "";

                       });

                }
            };

        }
    ]);

})();
