(function() {
  'use strict';

  angular.module('portalUsuarioApp').factory('AutenticacaoFactory', [
    function() {

      // set variables
      var loginFormData = {};
      resetLoginFormData();

      // set service
      var service = {
        bindLoginFormData: bindLoginFormData,
        submitLoginFormData: submitLoginFormData,
      };

      return service;

      // public
      // used to bind loginFormData to controller
      function bindLoginFormData() {
        return loginFormData;
      }

      // public
      // used to submit signUpFormData
      function submitLoginFormData() {
        console.log(loginFormData);
      }

      // private
      // reset login form data
      function resetLoginFormData() {
        loginFormData.user = '';
        loginFormData.password = '';
        loginFormData.remember = false;
      }
    }
  ]);

})();
