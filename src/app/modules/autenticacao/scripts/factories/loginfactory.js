(function() {
  angular.module('portalUsuarioApp').factoryLazy('LoginResource', [
    '$resource', function($resource) {
      return $resource(config.urls.loginAPI, null, {
        'update': {
          method: 'PUT'
        }
      });
    }
  ]);

}).call(this);
