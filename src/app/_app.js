
/**
 * Definicao da Aplicacao
 */
var portalUsuarioApp = initModule("portalUsuarioApp", [
    "facebook",
    "ngRoute",
    "ngResource",
    "ngDialog",
    "ngAnimate",
    "base64",
    "ngCookies",
    "ngFileUpload",
    "toaster",
    "angular.filter",
    "ui.bootstrap",
    "datetime",
    "datePicker",
    "ui.utils.masks",
    "cgBusy",
    "angularUtils.directives.dirPagination",
    "vcRecaptcha",
    "angulartics",
    "angulartics.google.analytics",

    "angulartics.google.tagmanager",
    "slick",
    "duScroll",
    "720kb.socialshare",
    "angular-google-adsense",
    "updateMeta"
],
    "templates/home.htm",
    true
);


function initModule(moduleName, dependencies, homeUrl, isRoot) {
    console.log(homeUrl);
    try {
        angular.module("angular-google-adsense");
    }
    catch (err) {
        dependencies.splice(dependencies.indexOf("angular-google-adsense"), 1);
    }

    var app = angular.module(moduleName, dependencies);

    app.factory('OauthService', ['cookieService', '$location', '$q', '$rootScope', '$cookies','$browser',
        function OauthService(cookieService, $location, $q, $rootScope, $cookies, $browser) {
            var token = null;
            var idUsuario = null;

            var setToken = function setToken(someToken) {

                token = someToken.AccessToken;
                idUsuario = someToken.IdUsuario;
            };

            var getToken = function getToken() {
                return token;
            };

            var redirecionarParaLogin = function () {
                window.location.href = $browser.baseHref();
            };

            var request = function request(config_request) {
                var token = cookieService.obterValorCookieAutenticacao("Edenred");

                var excecaoLumis = "@@excecaoLumis";

                if (config_request.url.indexOf(excecaoLumis) > -1) {
                    return config_request;
                }


                var config_urls = null;

                try {
                    config_urls = config;
                }
                catch (err) { }

                if (token && config_urls) {
                    var promise = null;
                    var relogioAgora = moment().format("DD/MM/YYYY HH:mm:ss");
                    var retornoComparacao = compare(relogioAgora, token.expires_in);
                    if (retornoComparacao === 1) {

                        $.ajax({
                            type: "POST",
                            url: config_urls.urls.loginAPI,
                            async: false,
                            data: {
                                Email: null,
                                Password: null,
                                Token: null,
                                origemWeb: true,
                                RefreshToken: token.refresh_token
                            },
                            success: function (response) {
                                var at = JSON.parse(response);
                                var segundos = moment().add(at.expires_in, 'seconds');
                                var horaFinal = moment(segundos, "DD/MM/YYYY").format("DD/MM/YYYY HH:mm:ss");

                                var oauth = {
                                    access_token: at.access_token,
                                    expires_in: horaFinal,
                                    refresh_token: at.refresh_token
                                };
                                $cookies.remove("Edenred");
                                cookieService.criarCookieAutenticacao(oauth, false);
                                at.expires_in = horaFinal;
                                //                            token = at;
                                //config_request.headers.Authorization = "Bearer " + at.access_token;

                            },
                            error: function (response) {
                                console.log(response);
                            }
                        });

                    } else {
                        
                        config_request.headers.Authorization = "Bearer " + token.access_token;

                    }
                }

                return config_request;
            };

            var responseError = function responseError(response) {

                if (response.status === -1) {
                    redirecionarParaLogin();
                }
                return $q.reject(response);
            };


            return {
                setToken: setToken,
                getToken: getToken,
                request: request,
                responseError: responseError
            };

        }]);

    app.config(["$httpProvider", function ($httpProvider) {
        $httpProvider.interceptors.push('OauthService');
        $httpProvider.defaults.withCredentials = false;
        delete $httpProvider.defaults.headers.common["X-Requested-With"];
    }]);

    app.config(["$routeProvider", "$httpProvider", "$controllerProvider", "$compileProvider", "$filterProvider", "$provide", "$injector", '$locationProvider',
        function ($routeProvider, $httpProvider, $controllerProvider, $compileProvider, $filterProvider, $provide, $injector, $locationProvider) {
            app.controllerProvider = $controllerProvider;
            app.compileProvider = $compileProvider;
            app.routeProvider = $routeProvider;
            app.httpProvider = $httpProvider;
            app.filterProvider = $filterProvider;
            app.provide = $provide;
            app.injector = $injector;
            app.locationProvider = $locationProvider;

            app.controllerLazy = function (controller, controllerDefinition) {
                return app.controllerProvider.register(controller, controllerDefinition);
            };

            app.factoryLazy = function (service, serviceDefinition) {
                return app.provide.factory(service, serviceDefinition);
            };

            app.serviceLazy = function (service, serviceDefinition) {
                return app.provide.service(service, serviceDefinition);
            };

            app.directiveLazy = function (name, directiveFactory) {
                return app.compileProvider.directive(name, directiveFactory);
            };

            $httpProvider.defaults.withCredentials = false;

            $locationProvider.html5Mode({ enabled: true });

            //$locationProvider.hashPrefix('!');

            $routeProvider
                .when("/", { templateUrl: homeUrl })
                .when("/:modulo", {
                    templateUrl: function (urlattr) {
                        console.log("ROTA PARA MODULO (NAO ROOT): app/modules/" + urlattr.modulo + "/" + urlattr.modulo + ".htm");

                        return "app/modules/" + urlattr.modulo + "/index.htm";
                    }
                })
                // MAPEAMENTO PARA MODULOS
                .when("/:modulo/p/:parametros*", {
                    templateUrl: function (urlattr) {
                        console.log("ROTA PARA MODULO (NAO ROOT) COM PARAMETRO: app/modules/" + urlattr.modulo + "/" + urlattr.modulo + ".htm");

                        return "app/modules/" + urlattr.modulo + "/index.htm";
                    }
                })
                // MAPEAMENTO PARA PAGINAS DOS MODULOS (COM PARAMETROS)
                .when("/:modulo/:funcionalidade/p/:parametros*", {
                    templateUrl: function (urlattr) {
                        console.log("ROTA PARA PAGINA 1: app/modules/" + urlattr.modulo + "/" + urlattr.funcionalidade + "/" + urlattr.funcionalidade + ".htm");

                        return "app/modules/" + urlattr.modulo + "/" + urlattr.funcionalidade + "/index.htm";
                    }
                })
                // MAPEAMENTO PARA PAGINAS DOS MODULOS (COM PARAMETROS)
                .when("/:modulo/:funcionalidade/:sub*/:page/p/:parametros*", {
                    templateUrl: function (urlattr) {
                        console.log("ROTA PARA PAGINA 2: app/modules/" + urlattr.modulo + "/" + urlattr.funcionalidade + "/" + urlattr.sub + "/" + urlattr.page + "/" + urlattr.page + ".htm");

                        return "app/modules/" + urlattr.modulo + "/" + urlattr.funcionalidade + "/" + urlattr.sub + "/" + urlattr.page + "/" + urlattr.page + ".htm";
                    }
                })
                // MAPEAMENTO PARA PAGINAS DOS MODULOS
                .when("/:modulo/:funcionalidade", {
                    templateUrl: function (urlattr) {
                        console.log("ROTA PARA PAGINA 3: app/modules/" + urlattr.modulo + "/" + urlattr.funcionalidade + "/" + urlattr.funcionalidade + ".htm");

                        return "app/modules/" + urlattr.modulo + "/" + urlattr.funcionalidade + "/index.htm";
                    }
                })
                .when("/:modulo/:funcionalidade/:page", {
                    templateUrl: function (urlattr) {
                        console.log("ROTA PARA PAGINA 4: app/modules/" + urlattr.modulo + "/" + urlattr.funcionalidade + "/" + urlattr.page + "/index.htm");

                        return "app/modules/" + urlattr.modulo + "/" + urlattr.funcionalidade + "/" + "/index.htm";
                    }
                })
                .when("/:modulo/:funcionalidade/:sub*/:page", {
                    templateUrl: function (urlattr) {
                        console.log("ROTA PARA PAGINA 5: app/modules/" + urlattr.modulo + "/" + urlattr.funcionalidade + "/" + urlattr.sub + "/" + urlattr.page + "/" + urlattr.page + ".htm");

                        return "app/modules/" + urlattr.modulo + "/" + urlattr.funcionalidade + "/" + urlattr.sub + "/" + urlattr.page + "/" + urlattr.page + ".htm";
                    }
                })
                


                .when("/souticket", { templateUrl: "souticket/index.htm" })
                .when("/souticket/:modulo", {
                    templateUrl: function (urlattr) {
                        console.log("ROTA PARA MODULO (NAO ROOT): souticket/app/modules/" + urlattr.modulo + "/" + urlattr.modulo + ".htm");

                        return "souticket/app/modules/" + urlattr.modulo + "/index.htm";
                    }
                })
                // MAPEAMENTO PARA MODULOS
                .when("/souticket/:modulo/p/:parametros*", {
                    templateUrl: function (urlattr) {
                        console.log("ROTA PARA MODULO (NAO ROOT) COM PARAMETRO: souticket/app/modules/" + urlattr.modulo + "/" + urlattr.modulo + ".htm");

                        return "souticket/app/modules/" + urlattr.modulo + "/index.htm";
                    }
                })
                // MAPEAMENTO PARA PAGINAS DOS MODULOS (COM PARAMETROS)
                .when("/souticket/:modulo/:funcionalidade/p/:parametros*", {
                    templateUrl: function (urlattr) {
                        console.log("ROTA PARA PAGINA 1: souticket/app/modules/" + urlattr.modulo + "/" + urlattr.funcionalidade + "/" + urlattr.funcionalidade + ".htm");

                        return "souticket/app/modules/" + urlattr.modulo + "/" + urlattr.funcionalidade + "/index.htm";
                    }
                })
                // MAPEAMENTO PARA PAGINAS DOS MODULOS (COM PARAMETROS)
                .when("/souticket/:modulo/:funcionalidade/:sub*/:page/p/:parametros*", {
                    templateUrl: function (urlattr) {
                        console.log("ROTA PARA PAGINA 2: souticket/app/modules/" + urlattr.modulo + "/" + urlattr.funcionalidade + "/" + urlattr.sub + "/" + urlattr.page + "/" + urlattr.page + ".htm");

                        return "souticket/app/modules/" + urlattr.modulo + "/" + urlattr.funcionalidade + "/" + urlattr.sub + "/" + urlattr.page + "/" + urlattr.page + ".htm";
                    }
                })
                // MAPEAMENTO PARA PAGINAS DOS MODULOS
                .when("/souticket/:modulo/:funcionalidade", {
                    templateUrl: function (urlattr) {
                        console.log("ROTA PARA PAGINA 3: souticket/app/modules/" + urlattr.modulo + "/" + urlattr.funcionalidade + "/" + urlattr.funcionalidade + ".htm");

                        return "souticket/app/modules/" + urlattr.modulo + "/" + urlattr.funcionalidade + "/index.htm";
                    }
                })
                .when("/souticket/:modulo/:funcionalidade/:page", {
                    templateUrl: function (urlattr) {
                        console.log("ROTA PARA PAGINA 4: souticket/app/modules/" + urlattr.modulo + "/" + urlattr.funcionalidade + "/" + urlattr.page + "/index.htm");

                        return "souticket/app/modules/" + urlattr.modulo + "/" + urlattr.funcionalidade + "/" + "/index.htm";
                    }
                })
                .when("/souticket/:modulo/:funcionalidade/:sub*/:page", {
                    templateUrl: function (urlattr) {
                        console.log("ROTA PARA PAGINA 5: souticket/app/modules/" + urlattr.modulo + "/" + urlattr.funcionalidade + "/" + urlattr.sub + "/" + urlattr.page + "/" + urlattr.page + ".htm");

                        return "souticket/app/modules/" + urlattr.modulo + "/" + urlattr.funcionalidade + "/" + urlattr.sub + "/" + urlattr.page + "/" + urlattr.page + ".htm";
                    }
                });
        }
    ]);

    app.config([
        'FacebookProvider',
        function (FacebookProvider) {

            var myAppId = '@@facebookId';

            FacebookProvider.init(myAppId);
        }
    ]);

    app.filter(['startFrom', function () {
        return function (input, start) {
            start = +start; //parse to int
            return input.slice(start);
        };
    }]);

    app.filter('iif', function () {
        return function (input, trueValue, falseValue) {
            return input ? trueValue : falseValue;
        };
    });

    return app;
}

function compare(dateTimeA, dateTimeB) {
    var momentA = moment(dateTimeA, "DD/MM/YYYY HH:mm:ss");
    var momentB = moment(dateTimeB, "DD/MM/YYYY HH:mm:ss");
    if (momentA > momentB) return 1;
    else if (momentA < momentB) return -1;
    else return 0;
}

$.urlParam = function (name) {

    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results)
        return results[1];
    else
        return null;
};