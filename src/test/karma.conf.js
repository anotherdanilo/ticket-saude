// Karma configuration
// Generated on Mon May 02 2016 14:25:09 GMT-0300 (Hora oficial do Brasil)

module.exports = function(config) {
  config.set({

    // base path that will be used to resolve all patterns (eg. files, exclude)
    basePath: '',


    // frameworks to use
    // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
    frameworks: ['mocha', 'chai'],


    // list of files / patterns to load in the browser
    files: [
      '../assets/components/jquery-2.1.4/jquery.min.js',
	  '../assets/components/angular-1.4.7/angular.min.js',
      '../assets/components/angular-route-1.4.7/angular-route.min.js',
      '../assets/components/angular-resource-1.4.7/angular-resource.min.js',
      '../assets/components/angular-touch-1.5.0/angular-touch.min.js',
      '../assets/components/angular-cookies-1.4.7/angular-cookies.min.js',
      '../assets/components/angular-animate-1.4.7/angular-animate.min.js',
      '../assets/components/angular-i18n-1.5.3/angular-locale_pt-br.js',
      '../assets/components/jquery-mask-plugin-1.13.4/jquery.mask.min.js',
      '../assets/components/angularjs-toaster-1.1.0/toaster.min.js',
      '../assets/components/bootstrap-3.3.5/js/bootstrap.min.js',
      '../assets/components/angular-ui-bootstrap-1.2.5/ui-bootstrap-tpls.min.js',
      '../assets/components/ng-file-upload-11.2.3/ng-file-upload.min.js',
      '../assets/components/js-xlsx-0.8.0/xlsx.min.js',
      '../assets/components/js-xls-0.7.5/xls.min.js',
      '../assets/components/es6-promise-3.0.2/promise.min.js',
      '../assets/components/file-saver/FileSaver.min.js',
      '../assets/components/ng-dialog-0.5.4/js/ngDialog.min.js',
      '../assets/components/angular-filter-0.5.8/angular-filter.min.js',
      '../assets/components/angular-datetime-2.2.0/datetime.js',
      '../assets/components/angular-base64-1.5.0/angular-base64.min.js',
      '../assets/components/datejs/date.js',
      '../assets/components/d3-3.5.16/d3.min.js',
      '../assets/components/moment-2.12.0/moment.min.js',
      '../assets/components/moment-2.12.0/moment-with-locales.js',
      '../assets/components/angular-datepicker/angular-datepicker.min.js',
      '../assets/components/jslinq/JSLINQ.js',
      '../assets/components/angular-facebook-0.2.3/angular-facebook.js',
      //'../assets/components/angular-busy-4.1.3/angular-busy.js',
      //'..///maps.googleapis.com/maps/api/js?sensor=false',
      //'../assets/components/angular-utils-pagination/dirPagination.js',
      //'../assets/components/angulartics/angulartics.min.js',
      //'../assets/components/angulartics-google-analytics/angulartics-ga.js',
      //'../assets/components/angulartics-google-tag-manager/angulartics-google-tag-manager.js',
      //'../assets/components/slick-carousel-1.5.9/slick.min.js',
      //'../assets/components/angular-slick-0.2.0/slick.min.js',
      //'../assets/components/angular-scroll/angular-scroll.min.js',

      //Test-Specific frameworks
      '../assets/components/angular-mocks-1.4.7/angular-mocks.js',
		
		  'app.js',
          '../app/config.js',
		
          '../app/modules/shared/**/*.js',
          '../app/modules/autenticacao/**/*.js',
		  '../app/modules/beneficiario/**/*.js',
          '../app/modules/beneficio-club/**/*.js',
		
      //Test specs
		  'spec/modules/**/*spec.js',
          //'spec/modules/shared/controllers/homecontrollerspec.js',
          //'spec/modules/autenticacao/services/autenticacaoservicespec.js',
          //'spec/modules/autenticacao/**/*spec.js',
          //'spec/modules/beneficiario/**/*spec.js',
          //'spec/modules/beneficio-club/**/*spec.js',
          //'spec/modules/rede-credenciada/**/*spec.js',
          //'spec/modules/shared/controllers/*spec.js',
    ],


    // list of files to exclude
    exclude: [
        //'../app/modules/autenticacao/scripts/factories/*.js',
        '../app/modules/beneficiario/scripts/factories/ajudafactory.js',
        '../app/modules/beneficiario/scripts/factories/atendimentomodalsfactory.js',
        '../app/modules/beneficio-club/scripts/factories/beneficioclubdetalhefactory.js',
        '../app/modules/beneficiario/scripts/factories/beneficioclubdetalhefactory.js',
        '../app/modules/shared/scripts/factories/*.js',
        '../app/modules/beneficiario/scripts/directives/*.js',
		'../app/modules/shared/scripts/controllers/logincontroller.js',

        '../app/modules/beneficiario/scripts/factories/automodalsfactory.js',
        '../app/modules/beneficiario/scripts/factories/cadastrofactory.js',
        '../app/modules/beneficiario/scripts/factories/cartaofactory.js',
        '../app/modules/beneficiario/scripts/factories/cartaosenhafactory.js',
        '../app/modules/beneficiario/scripts/factories/editarusuariofactory.js',
        '../app/modules/beneficiario/scripts/factories/graficoconsumofactory.js',
        '../app/modules/beneficiario/scripts/factories/redecredenciadafactory.js',
        '../app/modules/beneficiario/scripts/factories/redecredenciadamodalsfactory.js',
        '../app/modules/beneficiario/scripts/factories/beneficioclubfactory.js',
        '../app/modules/beneficiario/scripts/factories/beneficiomodalsfactory.js',

        '../app/modules/beneficiario/scripts/services/testeservice.js',
        
    ],


    // preprocess matching files before serving them to the browser
    // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
    preprocessors: {
     '../app/**/*.js': ['coverage']
    },


    // test results reporter to use
    // possible values: 'dots', 'progress'
    // available reporters: https://npmjs.org/browse/keyword/karma-reporter
    reporters: ['spec', 'coverage'],
    
    // coverageReporter: {
    //   type : 'html',
    //   dir : 'coverage/',
    //   includeAllSources: true
    // },
    
    coverageReporter: {
      reporters: [
        {type: 'html', dir: 'coverage/'},
        {type: 'text-summary'},
      ]
    },



    // web server port
    port: 9876,


    // enable / disable colors in the output (reporters and logs)
    colors: true,


    // level of logging
    // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_ERROR,


    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: true,


    // start these browsers
    // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
    browsers: [
		//'Chrome'
		'PhantomJS'
    ],


    // Continuous Integration mode
    // if true, Karma captures browsers, runs the tests and exits
    singleRun: false,

    // Concurrency level
    // how many browser should be started simultaneous
    //concurrency: Infinity,
    
  })
}
