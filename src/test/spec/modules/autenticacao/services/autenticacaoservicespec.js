"use strict";
describe('shared module', function() {
    describe('autenticacaoService', function() {

        var provide, $cookies, $httpBackend, $q, service, deferred, service, $scope;
     
        describe('autenticar', function() {
            beforeEach(function() {
                module('portalUsuarioApp', function($provide) {
                $provide.value("cookieService", {
                        criarCookieAutenticacao: function() {
                        },
                        criarCookieUsuario: function() {
                        }
                    });
                });

                inject(function(_$httpBackend_, AutenticacaoService, _$cookies_) {
                    $cookies = _$cookies_;
                    service = AutenticacaoService;
                    $httpBackend = _$httpBackend_;
                });
            });
            
            it('Deve autenticar com sucesso', function() {
                $httpBackend
                    .expectPOST('https://api.ticket.com.br/mobile_qa/usuarios/v3/autenticar', {
                        Email: 'g@g.com',
                        Password: '123',
                        Token: null,
                        origemWeb: true,
                        RefreshToken: null
                    })
                    .respond(200, {
                        "access_token":"81a9f0eda5e910ee97f565618d63b0c3",
                        "expires_in":"20/09/2016 10:30:53",
                        "refresh_token":"80208962e069fa1345786b72145e59e1"	
                    });


                $httpBackend
                    .expectGET('https://api.ticket.com.br/mobile_qa/usuarios/v3', {
                        "Accept":"application/json, text/plain, */*"
                        })
                    .respond({"value": {
                        "id":"092380E1C3FB614EC5F53A7E4F6E7F48",
                        "email":"guilherme.passarelli@edenred.com",
                        "name":"Guilherme",
                        "lastName":null,
                        "cpf":"17148665809",
                        "cellPhone":"11992986900",
                        "device":null,
                        "hasPicture":false,
                        "nascimento":"1981-03-07T03:00:00",
                        "matricula":"553384",
                        "aceitoMarketing":true,
                        "version":null,
                        "identificadorAparelho":null,
                        "numCelValidado":false,
                        "cdValidacaoCelular":null,
                        "sexo":"M",
                        "foto":null,
                        "endereco":{"bairro":"erro bairro","cep":"04534040","logradouro":"erro busca cep","numero":"99","cidade":"erro","complemento":"","uf":"UF","links":[], "origemWeb":false},"preferencias":[3,5],"checkboxPreferencia":[{"id":1,"name":"Bem-estar e Saúde","value":false,"links":[],"origemWeb":false},{"id":2,"name":"Cultura e Lazer","value":false,"links":[],"origemWeb":false},{"id":3,"name":"Cursos","value":true,"links":[],"origemWeb":false},{"id":4,"name":"Esportes e Aventura","value":false,"links":[],"origemWeb":false},{"id":5,"name":"Gastronomia","value":true,"links":[],"origemWeb":false},{"id":6,"name":"Hotéis e Viagens","value":false,"links":[],"origemWeb":false},{"id":7,"name":"Produtos","value":false,"links":[],"origemWeb":false},{"id":8,"name":"Serviços","value":false,"links":[],"origemWeb":false}],"links":[{"rel":"self","href":"http://apiticketmobile.lanet.accorservices.net/TicketMobile.API/usuarios/v3/092380E1C3FB614EC5F53A7E4F6E7F48?controller=Usuarios"},{"rel":"cards","href":"http://apiticketmobile.lanet.accorservices.net/TicketMobile.API/usuarios/v3/092380E1C3FB614EC5F53A7E4F6E7F48/cartoes"},{"rel":"favorites","href":"http://apiticketmobile.lanet.accorservices.net/TicketMobile.API/api/usuarios/092380E1C3FB614EC5F53A7E4F6E7F48/favoritos"}],"origemWeb":false,"manterconectado":false	
                    }});
                    
                var succeeded = false;
                service.autenticar('g@g.com', '123', null, false, null).then(function() {
                    succeeded = true;
                });

                $httpBackend.flush();
                expect(succeeded).to.equal(true);
            });
            
            it('Deve retornar mensagem caso apresente erro na busca dos dados do usuario', function() {
                $httpBackend
                    .expectPOST('https://api.ticket.com.br/mobile_qa/usuarios/v3/autenticar', {
                        Email: 'g@g.com',
                        Password: '123',
                        Token: null,
                        origemWeb: true,
                        RefreshToken: null
                    })
                    .respond(200, {
                        "access_token":"81a9f0eda5e910ee97f565618d63b0c3",
                        "expires_in":"20/09/2016 10:30:53",
                        "refresh_token":"80208962e069fa1345786b72145e59e1"	
                    });

                $httpBackend
                    .expectGET('https://api.ticket.com.br/mobile_qa/usuarios/v3', {
                        "Accept":"application/json, text/plain, */*"
                        })
                    .respond(500);
                    
                var succeeded = false;
                service.autenticar('g@g.com', '123', null, false, null).then(function() {
                    succeeded = true;
                },function(error) {
                    expect(succeeded).to.equal(false);
                    expect(error).to.equal("Não foi possível obter os dados do usuário");
                });

                $httpBackend.flush();
            });
            
            it('Deve retornar mensagem caso apresente erro na autenticacao do usuario', function() {
                $httpBackend
                    .expectPOST('https://api.ticket.com.br/mobile_qa/usuarios/v3/autenticar', {
                        Email: 'g@g.com',
                        Password: '123',
                        Token: null,
                        origemWeb: true,
                        RefreshToken: null
                    })
                    .respond(500, "Usuário e/ou senha inválidos.");
                    
                var succeeded = false;
                
                service.autenticar('g@g.com', '123', null, false, null).then(function() {
                    succeeded = true;
                },function(error) {
                    
                    expect(succeeded).to.equal(false);
                    expect(error.data).to.equal("Usuário e/ou senha inválidos.");
                });

                $httpBackend.flush();
            });
        });
        
        describe('setNomeUsuarioLogado-Sucesso', function() {
            beforeEach(function() {
                module('portalUsuarioApp', function($provide) {
                $provide.value("cookieService", {
                        obterValorCookieAutenticacao: function() {
                             var oauth = {
                                nome: "Teste Moq",
                                id: "123432",
                                email: "teste.autenticao.moq@karma.com"
                            }
                            return oauth;
                        },
                        obterValorCookieUsuario: function() {
                            var usuario = {
                                name: "Teste Moq",
                                id: "123432",
                                email: "teste.autenticao.moq@karma.com"
                            }
                            return usuario;
                        }
                    });
                });
                inject(function(_$httpBackend_,AutenticacaoService, _$cookies_) {
                    $cookies = _$cookies_;
                    service = AutenticacaoService;
                    $httpBackend = _$httpBackend_;
                });
            });
            it('Deve setar e obter o nome do usuario com sucesso', function() {
                service.setNomeUsuarioLogado();
                var usuario = service.obterUsuarioLogado();
                expect(usuario).to.have.property('nome','Teste Moq');
            });
        });
        
        describe('setNomeUsuarioLogado-Erro', function() {
            beforeEach(module('portalUsuarioApp', function($provide) {
            $provide.value("cookieService", {
                        obterValorCookieAutenticacao: function() {
                        },
                        obterValorCookieUsuario: function() {
                        }
                    });
            }));
            beforeEach(function() {
                module('portalUsuarioApp');

                inject(function(_$httpBackend_, AutenticacaoService, _$cookies_) {
                    $cookies = _$cookies_;
                    service = AutenticacaoService;
                    $httpBackend = _$httpBackend_;
                });
            });
            it('Não Deve setar o nome do usuario caso não exista o cookie', function() {
                var retorno = service.setNomeUsuarioLogado();
                var usuario = service.obterUsuarioLogado();
                expect(retorno).to.equal(null);
                expect(usuario).to.have.property('nome','');
                expect(usuario).to.have.property('isLogged',false);
            });
        });
        
    });
})
