﻿"use strict";
describe('autenticacao module', function() {
    describe('loginController', function() {

        var $controller, $scope, $q, $facebook, $autenticacaoFactory, $loginModalFactory;
        var ctrl, deferred, deferredRecaptcha;

        beforeEach(function() {
            module('portalUsuarioApp');

            inject(function(_$rootScope_, _$controller_, _Facebook_, _$q_) {

                $controller = _$controller_;
                $scope = _$rootScope_;
                $autenticacaoFactory = {
                    bindLoginFormData: function() {
                        var loginFormData = {};
                        return loginFormData;
                    }
                };
                $loginModalFactory = {
                    changeToSignUp: function() {
                    },
                    parametros: function()
                    {
                    },
                    openSignUp: function () 
                    {
                    }
                };
                $facebook = _Facebook_;
                $q = _$q_;

                deferred = $q.defer();
                deferredRecaptcha = $q.defer();

            });
        });

        describe('pageLoad', function() {

            beforeEach(function() {
                ctrl = $controller('LoginController', {
                    $scope: $scope,
                    AutenticacaoFactory: $autenticacaoFactory,
                    LoginModalsFactory: $loginModalFactory,

                    AutenticacaoService: {
                        autenticar: function() {
                            return deferred.promise;
                        },
                        setNomeUsuarioLogado: function() {
                        }
                    },

                    Facebook: $facebook,

                    RecaptchaService: {
                        verificarRecaptchaService: function() {

                        }
                    },

                    vcRecaptchaService: {
                        getResponse: function() {

                        },
                        reload: function() {

                        }
                    }
                });

                ctrl.isNotKarma = false;

            });

            it('Ao carregar a tela e for origem de ativação deve receber email', function() {

                ctrl.parametros = {
                    email: "gustavo@gustavo.com"
                };

                ctrl.pageLoad();

                expect(ctrl.formData.Email).to.equal("gustavo@gustavo.com");

            });

            it('Ao carregar a tela e for origem de esqueci minha senha deve exibir de recuperar senha (step 1)', function() {

                ctrl.parametros = {
                    email: "gustavo@gustavo.com"
                };

                $loginModalFactory.parametros.esqueciMinhaSenha = true;

                ctrl.pageLoad();

                expect(ctrl.step).to.equal(1);
                expect($loginModalFactory.parametros.esqueciMinhaSenha).to.equal(false);
            });
        });

        describe('submit', function() {

            beforeEach(function() {
                ctrl = $controller('LoginController', {
                    $scope: $scope,
                    AutenticacaoFactory: $autenticacaoFactory,
                    LoginModalsFactory: $loginModalFactory,

                    AutenticacaoService: {
                        autenticar: function() {
                            return deferred.promise;
                        },
                        setNomeUsuarioLogado: function() {

                        }
                    },
                    Facebook: $facebook,

                    RecaptchaService: {
                        verificarRecaptchaService: function() {

                        }
                    },

                    vcRecaptchaService: {
                        getResponse: function() {

                        },
                        reload: function() {

                        }
                    }
                });

                ctrl.isNotKarma = false;

            });

            it('Deve exibir alterar o passo para LOGADO ( 1 ) se o login for realizada com sucesso', function() {

                ctrl.submit();

                deferred.resolve({
                    status: 200,
                    success: true
                });

                $scope.$apply();

                expect(ctrl.step).to.equal(1);

            });

            it('Deve exibir mensagem de erro se o login não for realizada com sucesso', function() {
                
                ctrl.submit();

                deferred.reject({
                    data: "Usuário ou senha inválidos."
                });

                $scope.$apply();

                expect(ctrl.msgRetorno).to.equal("Usuário ou senha inválidos.");

            });

        });

        describe('loginFacebook', function() {

            beforeEach(function() {
                ctrl = $controller('LoginController', {
                    $scope: $scope,
                    AutenticacaoFactory: $autenticacaoFactory,
                    LoginModalsFactory: $loginModalFactory,

                    AutenticacaoService: {
                        autenticar: function() {
                            return deferred.promise;
                        },
                        setNomeUsuarioLogado: function() {

                        }
                    },

                    Facebook: {
                        login: function() {
                            return deferred.promise;
                        },

                        getLoginStatus: function() {

                            return deferred.promise;
                        },

                        api: function() {

                            return deferred.promise;

                        }
                    },

                    RecaptchaService: {
                        verificarRecaptchaService: function() {

                        }
                    },

                    vcRecaptchaService: {
                        getResponse: function() {

                        },
                        reload: function() {

                        }
                    }
                });

                ctrl.isNotKarma = false;

            });

            it('Deve exibir o status de logado (LOGGED) após o teste de Facebook ser realizado com sucesso.', function() {

                $scope.loginFacebook();

                deferred.resolve({
                    status: 'connected',
                    id: '123',
                    authResponse: {
                        accessToken: "123"
                    }
                });

                $scope.$apply();

                expect($scope.logged).to.equal(true);
                expect(ctrl.formData.Token).to.equal('123');

            });

        });

        describe('reenviarEmail', function() {

            beforeEach(function() {
                ctrl = $controller('LoginController', {
                    $scope: $scope,
                    AutenticacaoFactory: $autenticacaoFactory,
                    LoginModalsFactory: $loginModalFactory,

                    AutenticacaoService: {
                        autenticar: function() {
                            return deferred.promise;
                        },
                        setNomeUsuarioLogado: function() {
                        },
                        reenviarEmail: function() {
                            return deferred.promise;
                        },
                    },
                    Facebook: $facebook,

                    RecaptchaService: {
                        verificarRecaptchaService: function() {

                        }
                    },

                    vcRecaptchaService: {
                        getResponse: function() {

                        },
                        reload: function() {

                        }
                    }
                });

                ctrl.isNotKarma = false;

            });

            it('Deve exibir comunicado de reenvio do email (passo 2) se o mesmo for realizado com sucesso', function() {

                ctrl.reenviarEmail();

                deferred.resolve({
                    success: true
                });

                $scope.$apply();

                expect(ctrl.step).to.equal(4);
            });
        });

        describe('reenviarSenha', function() {

            beforeEach(function() {
                ctrl = $controller('LoginController', {
                    $scope: $scope,
                    AutenticacaoFactory: $autenticacaoFactory,
                    LoginModalsFactory: $loginModalFactory,

                    AutenticacaoService: {
                        autenticar: function() {
                            return deferred.promise;
                        },
                        setNomeUsuarioLogado: function() {
                        },
                        reenviarEmail: function() {
                            return deferred.promise;
                        },
                    },
                    Facebook: $facebook,

                    RecaptchaService: {
                        verificarRecaptchaService: function() {

                            return deferred.promise;
                        }
                    },

                    vcRecaptchaService: {
                        getResponse: function() {
                            return "";
                        },
                        reload: function() {

                        }
                    }
                });

                ctrl.isNotKarma = false;

            });

            it('Deve exibir mensagem caso não for preenchido o desafio do recaptcha', function() {

                ctrl.validarRecaptcha();

                expect(ctrl.msgRetorno).to.equal("Preencha o desafio do Não sou robô");

            });
        });

        describe('reenviarSenha', function() {

            beforeEach(function() {
                ctrl = $controller('LoginController', {
                    $scope: $scope,
                    AutenticacaoFactory: $autenticacaoFactory,
                    LoginModalsFactory: $loginModalFactory,

                    AutenticacaoService: {
                        autenticar: function() {
                            return deferred.promise;
                        },
                        setNomeUsuarioLogado: function() {
                        },
                        reenviarEmail: function() {
                            return deferred.promise;
                        },
                        reenviarSenha: function() {
                            return deferred.promise;
                        },
                    },
                    Facebook: $facebook,

                    RecaptchaService: {
                        verificarRecaptchaService: function() {

                            return deferred.promise;
                        }
                    },

                    vcRecaptchaService: {
                        getResponse: function() {
                            return "valorrecaptcha";
                        },
                        reload: function() {

                        }
                    }
                });

                ctrl.isNotKarma = false;

            });

            it('Deve reenviar a senha e exibir mensagem (step 2) de sucesso caso a operação seja realizada com sucesso', function() {

                ctrl.validarRecaptcha();

                deferred.resolve({
                    success: true
                });

                $scope.$apply();

                expect(ctrl.step).to.equal(2);

            });
        });


    });
});
