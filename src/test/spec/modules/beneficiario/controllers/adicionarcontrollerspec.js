"use strict";
describe('beneficiario module', function() {
    describe('adicionarController', function() {
        var $controller, $scope, $q;
        var ctrl, deferred, deferredObterCartao;

        beforeEach(function() {
            module('portalUsuarioApp');

            inject(function(_$rootScope_, _$controller_, _$q_) {
                $controller = _$controller_;
                $scope = _$rootScope_;
                $scope.$parent = {};
                $q = _$q_;
                deferred = $q.defer();
                deferredObterCartao = $q.defer();
            });
        });

        describe('cadastrarCartao', function() {
            beforeEach(function() {
                ctrl = $controller('AdicionarController', {
                    $scope: $scope,
                    AutoModalsFactory: {
                        closeAdd: function(cartao) {
                            return false;
                        }
                    },
                    CartaoService: {
                        cadastrarCartao: function(cartao) {
                            return deferred.promise;
                        },
                        obterCartaoPorIdService: function(idCartao) {
                            return deferredObterCartao.promise;
                        }
                    },
                    AutenticacaoService: {
                        obterUsuarioLogado: function(cartao) {
                            return deferred.promise;
                        }
                    },
                    AlertaFactory:{
                        openAlerta: function(textBotaoOk, callback){
                            return false;
                        }
                    }
                });

                ctrl.parentDashboard = {
                    carregarCartoes: function() {
                    }
                };

                ctrl.parent = {
                    dismiss: function() {
                    }
                };

                $scope.carregarCartoes = function() {

                };
            });

            it('Cartao deve ser cadastrado com sucesso quando o tipo for tt', function() {


                ctrl.cardtype = "tt";
                ctrl.cartao = {
                    CPF: "362.264.308-00"
                };

                ctrl.cadastrarCartao();

                deferred.resolve({
                    success: true,
                    value: { id: 1 }
                });

                deferredObterCartao.resolve({
                    success: true,
                    value: { balance: { number: "362.264.308-00" } }
                });

                $scope.$apply();

                expect(ctrl.cartao.Number).to.equal("362.264.308-00");
                expect(ctrl.step).to.equal(1);

            });

            it('Deve cadastrar o cartão com sucesso quando o tipo for diferente de tt', function() {

                ctrl.cartao = {
                    Number: "6033.4245.7720.6006"
                }

                ctrl.cadastrarCartao();

                deferred.resolve({ success: true, value: { id: 1 } });
                $scope.$apply();

                expect(ctrl.step).to.equal(0);
            });

            it('Deve exibir mensagem de erro se o cadastro do cartão não for realizado com sucesso', function() {

                ctrl.cartao = {
                    Number: "6033"
                }

                //ctrl.cadastrarCartao();

                try {
                    ctrl.cadastrarCartao();
                    deferred.resolve({ success: false, message: "Cartão Inválido" });
                    $scope.$apply();
                }
                catch (err) {
                    expect(err).to.equal("Cartão Inválido");
                }



                //expect(ctrl.cadastrarCartao()).to.throw("Cartão Inválido");
            });

            it('Deve exibir mensagem de erro se o cadastro do cartão não for realizado com sucesso (HTTP 500)', function() {

                ctrl.cartao = {
                    Number: "6033.4245.7720.6006"
                }

                ctrl.cadastrarCartao();

                deferred.reject();
                $scope.$apply();

                expect(ctrl.step).to.equal(3);
            });
        });
    });
});