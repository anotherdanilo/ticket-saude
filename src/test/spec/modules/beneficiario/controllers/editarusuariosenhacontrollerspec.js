﻿"use strict";
describe('beneficiario module', function () {
    describe('editarUsuarioSenhaController', function () {

        var $controller, $scope, $q, $usuarioModalsFactory;
        var ctrl, deferred;

        beforeEach(function () {
            module('portalUsuarioApp');

            inject(function (_$rootScope_, _$controller_, _$q_, _UsuarioModalsFactory_) {

                $controller = _$controller_;
                $scope = _$rootScope_;
                $q = _$q_;
                $usuarioModalsFactory = _UsuarioModalsFactory_;
                deferred = $q.defer();

            });
        });


        describe('toSuccess', function () {
            beforeEach(function () {
                ctrl = $controller('EditarUsuarioSenhaController', {
                    $scope: $scope,
                    UsuarioModalsFactory: $usuarioModalsFactory,
                    UsuarioService: {
                        alterarCadastroUsuario: function () {
                            return deferred.promise;
                        }
                    },
                    AutenticacaoService: {
                        obterUsuarioLogado: function () {
                            return {
                                id:1
                            }
                        }
                    }
                });

                ctrl.formData = {
                    newPwd: "123",
                    confirmPwd:"123"
                }
            });

            it('Deve exibir o passo de Sucesso (step 1) caso a alteração da senha seja executada com sucesso', function () {

                ctrl.toSuccess();

                deferred.resolve({
                    success: true
                });

                $scope.$apply();

                expect(ctrl.step).to.equal(1);

            });

            it('Deve exibir a mensagem de Erro caso a alteração da senha não seja executada com sucesso', function () {

                ctrl.toSuccess();

                deferred.reject({
                    success: true
                });

                $scope.$apply();

                expect(ctrl.error).to.equal(true);

            });

            it('Deve exibir a mensagem de erro caso a Senha e Confirmação de Senha não sejam iguais', function () {

                ctrl.formData = {
                    newPwd: "123",
                    confirmPwd: "456"
                }

                ctrl.toSuccess();

                expect(ctrl.mensagem).to.equal("Nova senha e confirmação de senha não conferem! Por favor, digite as senhas novamente.");

            });

        });

    });
});
