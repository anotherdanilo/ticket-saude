﻿"use strict";
describe('beneficiario module', function() {
    describe('selecionarCartaoController', function() {

        var $controller, $scope, $q, $reemissaoFactory, $bloqueioFactory, $desbloqueioFactory, $atendimentoModalsFactory;
        var ctrl, deferred;

        beforeEach(function() {
            module('portalUsuarioApp');

            inject(function(_$rootScope_, _$controller_, _$q_, _ReemissaoFactory_, _BloqueioFactory_, _DesbloqueioFactory_) {

                $controller = _$controller_;
                $scope = _$rootScope_;
                $q = _$q_;
                $reemissaoFactory = _ReemissaoFactory_;
                $bloqueioFactory = _BloqueioFactory_;
                $desbloqueioFactory = _DesbloqueioFactory_;
                //$atendimentoModalsFactory = _AtendimentoModalsFactory_;
                deferred = $q.defer();

            });
        });


        describe('carregarCartoes', function() {
            beforeEach(function() {
                ctrl = $controller('SelecionarCartaoController', {
                    $scope: $scope,
                    ReemissaoFactory: $reemissaoFactory,
                    BloqueioFactory: $bloqueioFactory,
                    DesbloqueioFactory: $desbloqueioFactory,
                    AtendimentoModalsFactory: {
                        bindServiceData: function() {
                            var serviceData = {};
                            return serviceData;
                        }
                    },
                    
                    AutenticacaoService: {
                        obterUsuarioLogado: function() {
                            return {
                                id: 1
                            }
                        }
                    },
                    CartaoService: {
                        carregarCartoesPorUsuarioService: function() {
                            return deferred.promise;
                        }
                    },

                    AutoModalsFactory: {

                        openBlock: function () {

                        },
                        openUnblock: function () {

                        },
                        openChangePwd: function () {

                        },
                        openReissue: function () {

                        }
                    },
                    
                });


            });


            it('Bloqueio: Deve exibir a lista com 2 (dois) cartões bloqueados.', function() {

                ctrl.serviceData.service = "block";

                ctrl.carregarCartoes();

                deferred.resolve({
                    success: true,
                    value:
                    [
                        {
                            "valid": true,
                            "balance": { "id": 460475, "code": "0", "bin": "TAE", "nickName": "3085 1210 1107 6003", "number": "3085121011076003", "average": 40.23136363636364, "dailyAverage": 47, "dateNextDeposit": null, "deposit": 885.09, "date": "09/09/2016 00:00:00", "value": "705", "dateParsed": "2016-09-09T00:00:00", "valueParsed": 705, "situacao": 1, "validado": false, "favorito": null, "dataCriacao": "2016-08-12T16:57:25" },
                            "scheduling": [],
                            "release": [{ "date": "05/09/2016 16:59:02", "value": "89,0", "dateParsed": "2016-09-05T16:59:02", "valueParsed": 89, "description": "COMPRA - NOVASOC COMERCIAL LTDA" }, { "date": "31/08/2016 11:35:54", "value": "91,09", "dateParsed": "2016-08-31T11:35:54", "valueParsed": 91.09, "description": "COMPRA - NOVASOC COMERCIAL LTDA" }, { "date": "30/08/2016 00:00:00", "value": "180,0", "dateParsed": "2016-08-30T00:00:00", "valueParsed": 180, "description": "DISPONIB. DE CREDITO" }, { "date": "12/08/2016 17:34:44", "value": "60,0", "dateParsed": "2016-08-12T17:34:44", "valueParsed": 60, "description": "COMPRA - NOVASOC COMERCIAL LTDA" }, { "date": "03/08/2016 11:29:35", "value": "90,0", "dateParsed": "2016-08-03T11:29:35", "valueParsed": 90, "description": "COMPRA - NOVASOC COMERCIAL LTDA" }, { "date": "01/08/2016 12:09:17", "value": "30,0", "dateParsed": "2016-08-01T12:09:17", "valueParsed": 30, "description": "COMPRA - POSTO TOCANTINS LTD" }, { "date": "29/07/2016 00:00:00", "value": "180,0", "dateParsed": "2016-07-29T00:00:00", "valueParsed": 180, "description": "DISPONIB. DE CREDITO" }, { "date": "11/07/2016 14:08:32", "value": "36,0", "dateParsed": "2016-07-11T14:08:32", "valueParsed": 36, "description": "COMPRA - POSTO TOCANTINS LTD" }],
                            "ttCard": []
                        },
                        {
                            "valid": false,
                            "balance": { "id": 460490, "code": null, "bin": "TAE", "nickName": "6026 5124 5597 5129", "number": "6026512455975129", "average": 0, "dailyAverage": 0, "dateNextDeposit": null, "deposit": 0, "date": null, "value": null, "dateParsed": "0001-01-01T00:00:00", "valueParsed": 0, "situacao": 1, "validado": false, "favorito": null, "dataCriacao": "2016-08-25T14:50:18" },
                            "scheduling": [],
                            "release": [{ "date": "24/08/2016 00:00:00", "value": "100", "dateParsed": "2016-08-24T00:00:00", "valueParsed": 100, "description": "COMPRAS - SUPERMERCADO SOUZA" }],
                            "ttCard": []
                        }
                    ]

                });


                $scope.$apply();

                console.log(ctrl.listaCartoes.length);

                expect(ctrl.listaCartoes.length).to.equal(2);

            });

            it('Desbloqueio: Deve exibir a lista com 2 (dois) cartões desbloqueados.', function() {

                ctrl.serviceData.service = "unblock";

                ctrl.carregarCartoes();

                deferred.resolve({
                    success: true,
                    value:
                    [
                        {
                            "valid": true,
                            "balance": { "id": 460475, "code": "0", "bin": "TAE", "nickName": "3085 1210 1107 6003", "number": "3085121011076003", "average": 40.23136363636364, "dailyAverage": 47, "dateNextDeposit": null, "deposit": 885.09, "date": "09/09/2016 00:00:00", "value": "705", "dateParsed": "2016-09-09T00:00:00", "valueParsed": 705, "situacao": 3, "validado": false, "favorito": null, "dataCriacao": "2016-08-12T16:57:25" },
                            "scheduling": [],
                            "release": [{ "date": "05/09/2016 16:59:02", "value": "89,0", "dateParsed": "2016-09-05T16:59:02", "valueParsed": 89, "description": "COMPRA - NOVASOC COMERCIAL LTDA" }, { "date": "31/08/2016 11:35:54", "value": "91,09", "dateParsed": "2016-08-31T11:35:54", "valueParsed": 91.09, "description": "COMPRA - NOVASOC COMERCIAL LTDA" }, { "date": "30/08/2016 00:00:00", "value": "180,0", "dateParsed": "2016-08-30T00:00:00", "valueParsed": 180, "description": "DISPONIB. DE CREDITO" }, { "date": "12/08/2016 17:34:44", "value": "60,0", "dateParsed": "2016-08-12T17:34:44", "valueParsed": 60, "description": "COMPRA - NOVASOC COMERCIAL LTDA" }, { "date": "03/08/2016 11:29:35", "value": "90,0", "dateParsed": "2016-08-03T11:29:35", "valueParsed": 90, "description": "COMPRA - NOVASOC COMERCIAL LTDA" }, { "date": "01/08/2016 12:09:17", "value": "30,0", "dateParsed": "2016-08-01T12:09:17", "valueParsed": 30, "description": "COMPRA - POSTO TOCANTINS LTD" }, { "date": "29/07/2016 00:00:00", "value": "180,0", "dateParsed": "2016-07-29T00:00:00", "valueParsed": 180, "description": "DISPONIB. DE CREDITO" }, { "date": "11/07/2016 14:08:32", "value": "36,0", "dateParsed": "2016-07-11T14:08:32", "valueParsed": 36, "description": "COMPRA - POSTO TOCANTINS LTD" }],
                            "ttCard": []
                        },
                        {
                            "valid": false,
                            "balance": { "id": 460490, "code": null, "bin": "TAE", "nickName": "6026 5124 5597 5129", "number": "6026512455975129", "average": 0, "dailyAverage": 0, "dateNextDeposit": null, "deposit": 0, "date": null, "value": null, "dateParsed": "0001-01-01T00:00:00", "valueParsed": 0, "situacao": 3, "validado": false, "favorito": null, "dataCriacao": "2016-08-25T14:50:18" },
                            "scheduling": [],
                            "release": [{ "date": "24/08/2016 00:00:00", "value": "100", "dateParsed": "2016-08-24T00:00:00", "valueParsed": 100, "description": "COMPRAS - SUPERMERCADO SOUZA" }],
                            "ttCard": []
                        }
                    ]

                });


                $scope.$apply();

                console.log(ctrl.listaCartoes.length);

                expect(ctrl.listaCartoes.length).to.equal(2);

            });


        });

    });
});
