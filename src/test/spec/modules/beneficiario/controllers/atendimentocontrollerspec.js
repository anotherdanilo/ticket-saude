"use strict";
describe('beneficiario module', function() {
    describe('atendimentoController', function() {
        var $controller, $scope, $q;
        var ctrl, deferred;

        beforeEach(function() {
            module('portalUsuarioApp');

            inject(function(_$rootScope_, _$controller_, _$q_) {
                $controller = _$controller_;
                $scope = _$rootScope_;
                $scope.$parent = {};
                $q = _$q_;
                deferred = $q.defer();
            });
        });

        describe('toHelp', function() {
            beforeEach(function() {
                ctrl = $controller('AtendimentoController', {
                    $scope: $scope,
                    HomeFactory: {
                        parametros: {
                            baixaApp: false
                        }
                    },
                    AtendimentoModalsFactory: {
                        openSelect: function(cartao) {
                            return false;
                        },
                        openTel: function(cartao) {
                            return false;
                        },
                        openMsg: function(cartao) {
                            return false;
                        },
                    },

                    AutenticacaoService: {
                        autenticar: function(login, senha, token, manterconectado, refreshtoken) {
                            return deferred.promise;
                        },
                        obterUsuarioLogado: function() {
                            var usuario = {
                                nome: "Usuario Karma",
                                isLogged: true,
                                id: "A04E55BC116B6772A3598F03EEC14038",
                                email: "usuariokarma@karma.com.br"
                            };
                            return usuario;
                        }
                    },

                });
            });

            it('Deve redirecionar para a pagina de ajuda', function() {
                ctrl.toHelp();
            });
        });
    });
});