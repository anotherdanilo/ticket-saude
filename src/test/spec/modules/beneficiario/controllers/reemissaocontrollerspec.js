"use strict";
describe('beneficiario module', function(){
	describe('remissaoController', function(){
		
		var $controller, $scope, $q, $reemissaoFactory;
		var ctrl, deferred;

		beforeEach(function(){
			module('portalUsuarioApp');
			
			inject(function(_$rootScope_, _$controller_, _$q_, _ReemissaoFactory_){
			
				$controller = _$controller_;
				$scope = _$rootScope_;
				$q = _$q_;
				$reemissaoFactory = _ReemissaoFactory_;

				deferred = $q.defer();

			});
		});

		describe('pageLoad', function(){

			beforeEach(function() { 
				ctrl = $controller('ReemissaoController', {
					$scope: $scope,
					ReemissaoFactory: $reemissaoFactory,
                    AutenticacaoService: {
                        obterUsuarioLogado: function() {
                            return deferred.promise;
                        }
                    },

                    AutoModalsFactory: {
                        openBlock: function () {

                        },
                        openUnblock: function () {

                        },
                        openChangePwd: function () {

                        },
                        openReissue: function () {

                        }
                    }
				});
			});

			it('Deve exibir informações do cartão a ser reemitido com validação positiva', function(){

				$scope.detalheCartao = {
					id:1,
					number: '1234234534564567',
					bin: 'TAE',
					apelido: 'Cartao 1',
					favorito: true,
					situacao: 1,
					validado: true,
					value: 10
				};

				ctrl.pageLoad();

				expect(ctrl.textoConfirmacaoReemissao).to.equal('Deseja reemitir este cartão?');

			});

			it('Deve exibir informações do cartão a ser reemitido com validação negativa', function(){

			    $scope.detalheCartao = {
					id:1,
					number: '1234234534564567',
					bin: 'TAE',
					apelido: 'Cartao 1',
					favorito: true,
					situacao: 1,
					validado: false,
					value: 10
				};

				ctrl.pageLoad();

				expect(ctrl.textoConfirmacaoReemissao).to.equal('Deseja reemitir seu cartão?');

			});
		});
		
		describe('reemitirCartao', function(){

			beforeEach(function() { 
				ctrl = $controller('ReemissaoController', {
					$scope: $scope,
					ReemissaoFactory: $reemissaoFactory,
					CartaoService: {
						autoServicoCartao: function (idUsuario, cartao) {
							return deferred.promise;
						}
					},
                    AutenticacaoService: {
                        obterUsuarioLogado: function() {
                            return deferred.promise;
                        }
                    },

                    AutoModalsFactory: {
                        openBlock: function () {

                        },
                        openUnblock: function () {

                        },
                        openChangePwd: function () {

                        },
                        openReissue: function () {

                        }
                    }
				});

				ctrl.cartao = {
				    id: 1
				};

			});

			it('Deve exibir mensagem de sucesso (passo 2) se a reemissão do cartão for realizada com sucesso', function() {

			    ctrl.reemitirCartao();

				deferred.resolve({
			        success: true,
				 	value: {
			         	protocolo:102030
			         }
		       	});

		       	$scope.$apply();

				expect(ctrl.step).to.equal(2);
				expect(ctrl.protocolo).to.equal(102030);
			});

			
			it('Deve exibir mensagem de ERRO se a reemissão do cartão não for realizada com sucesso', function(){
		
				ctrl.reemitirCartao();

				deferred.reject({
					status: 400,
					data:{message: 'erro ao reemitir o cartão'}
		       	});

		       	$scope.$apply();

				expect(ctrl.step).to.equal(3);
				expect(ctrl.mensagem).to.equal('erro ao reemitir o cartão');
				
			});

			it('Deve exibir mensagem de erro (passo 3) se a reemissão do cartão não for realizada devido a erro 500', function(){

				ctrl.reemitirCartao();

				deferred.reject({
					status: 500,
					data:{message: 'erro ao reemitir o cartão'}
		       	});

		       	$scope.$apply();

				expect(ctrl.step).to.equal(3);
				expect(ctrl.mensagem).to.equal('Ops! Ocorreu um erro ao tentar reemitir este cartão, por favor, tente novamente mais tarde!');
				
			});
		});
		
	});
});