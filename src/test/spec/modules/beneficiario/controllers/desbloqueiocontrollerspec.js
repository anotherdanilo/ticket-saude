"use strict";
describe('beneficiario module', function() {
    describe('desbloqueioController', function() {

        var $controller, $scope, $q, $desbloqueioFactory;
        var ctrl, deferred;

        beforeEach(function() {
            module('portalUsuarioApp');

            inject(function(_$rootScope_, _$controller_, _$q_, _DesbloqueioFactory_) {

                $controller = _$controller_;
                $scope = _$rootScope_;
                $q = _$q_;
                $desbloqueioFactory = _DesbloqueioFactory_;

                deferred = $q.defer();

            });
        });

        describe('pageLoad', function() {

            beforeEach(function() {
                ctrl = $controller('DesbloqueioController', {
                    $scope: $scope,
                    DesbloqueioFactory: $desbloqueioFactory,
                    AutenticacaoService: {
                        obterUsuarioLogado: function() {
                            return deferred.promise;
                        }
                    },

                    AutoModalsFactory: {
                        openBlock: function () {

                        },
                        openUnblock: function () {

                        },
                        openChangePwd: function () {

                        },
                        openReissue: function () {

                        }
                    }
                });
            });

            it('Deve exibir informações do cartão a ser desbloqueado com validação positiva', function() {

                $scope.detalheCartao = {
                    id: 1,
                    number: '1234234534564567',
                    bin: 'TAE',
                    apelido: 'Cartao 1',
                    favorito: true,
                    situacao: 1,
                    validado: true,
                    value: 10
                };

                ctrl.pageLoad();

                expect(ctrl.textoConfirmacaoDesbloqueio).to.equal('Deseja desbloquear este cartão?');

            });

            it('Deve exibir informações do cartão a ser bloqueado com validação negativa', function() {

                $scope.detalheCartao = {
                    id: 1,
                    number: '1234234534564567',
                    bin: 'TAE',
                    apelido: 'Cartao 1',
                    favorito: true,
                    situacao: 1,
                    validado: false,
                    value: 10
                };

                ctrl.pageLoad();

                expect(ctrl.textoConfirmacaoDesbloqueio).to.equal('Deseja desbloquear seu cartão?');

            });


        });

        describe('desbloquearCartao', function() {

            beforeEach(function() {
                ctrl = $controller('DesbloqueioController', {
                    $scope: $scope,
                    DesbloqueioFactory: $desbloqueioFactory,
                    CartaoService: {
                        autoServicoCartao: function(idUsuario, cartao) {
                            return deferred.promise;
                        }
                    },
                    AutenticacaoService: {
                        obterUsuarioLogado: function() {
                            return deferred.promise;
                        }
                    },

                    AutoModalsFactory: {
                        openBlock: function () {

                        },
                        openUnblock: function () {

                        },
                        openChangePwd: function () {

                        },
                        openReissue: function () {

                        }
                    }
                });

                ctrl.cartao =
                    {
                        id: 1
                    };
            });

            it('Deve exibir mensagem de sucesso (passo 1) se o desbloqueio do cartão for realizada com sucesso', function() {

                ctrl.desbloquearCartao();

                deferred.resolve({
                    success: true,
                    value: {
                        protocolo: 102030
                    }
                });

                $scope.$apply();

                expect(ctrl.step).to.equal(1);

            });

            it('Deve exibir mensagem de ERRO se o desbloqueio do cartão não for realizada com sucesso', function() {

                ctrl.desbloquearCartao();

                deferred.reject({
                    status: 400,
                    data: { message: 'erro ao desbloquear o cartão' }
                });

                $scope.$apply();

                expect(ctrl.mensagemShow).to.equal(true);
                expect(ctrl.mensagem).to.equal('erro ao desbloquear o cartão');

            });

            it('Deve exibir mensagem de erro (passo 2) se o desbloqueio do cartão não for realizada devido a erro 500', function() {

                ctrl.desbloquearCartao();

                deferred.reject({
                    status: 500,
                    data: { message: 'erro ao desbloquear o cartão' }
                });

                $scope.$apply();

                expect(ctrl.step).to.equal(2);

            });

        });

    });
});
