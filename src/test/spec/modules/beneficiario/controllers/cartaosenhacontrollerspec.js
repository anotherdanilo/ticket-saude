﻿"use strict";
describe('beneficiario module', function () {
    describe('cartaoSenhaController', function () {
        var $controller, $scope, $q;
        var ctrl, deferred;

        beforeEach(function () {
            module('portalUsuarioApp');

            inject(function (_$rootScope_, _$controller_, _$q_) {
                $controller = _$controller_;
                $scope = _$rootScope_;
                $scope.$parent = {};
                $q = _$q_;
                deferred = $q.defer();
            });
        });

        describe('load', function () {
            beforeEach(function () {
                ctrl = $controller('CartaoSenhaController', {
                    $scope: $scope,
                    CartaoSenhaFactory: {
                        bindCartaoSenhaFormData: function () {

                        },
                        submitCartaoSenhaFormData: function () {

                        },
                    },
                    AutoModalsFactory: {
                        closeChangePwd: function() {
                            
                        }
                    }
                });

            });

            it('Deve exibir na tela informações para confirmação do Token enviado ao celular', function () {

                ctrl.toToken();

                expect(ctrl.step).to.equal(1);

            });

            it('Deve exibir na tela informações para confirmar Senha, Nova Senha e Confirmação de senha', function () {

                ctrl.toNewPwd();

                expect(ctrl.step).to.equal(2);

            });

            it('Deve exibir na tela informações de senha alterada com sucesso', function () {

                ctrl.toSuccess();

                expect(ctrl.step).to.equal(3);

            });

        });
    });
});