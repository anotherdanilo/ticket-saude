"use strict";
describe('beneficiario module', function(){
	describe('edicaoController', function(){
		
		var $controller, $scope, $q;
		var ctrl, deferred;

		beforeEach(function(){
			module('portalUsuarioApp');
			
			inject(function(_$rootScope_, _$controller_, _$q_){
			
				$controller = _$controller_;
				$scope = _$rootScope_;
				$q = _$q_;
				
				deferred = $q.defer();
			});
		});

		describe('carregarCartao', function(){
			
			beforeEach(function() { 
				ctrl = $controller('EdicaoController', {
					$scope: $scope,
                    AutenticacaoService: {
                        obterUsuarioLogado: function() {
                            return deferred.promise;
                        }
                    }
				});
			});
			
			it('Deve exibir informações do cartão a ser alterado/excluido', function(){

				$scope.detalheCartao = {
					id:1,
					number: '1234234534564567',
					bin: 'TAE',
					apelido: 'Cartao 1',
					favorito: true,
					situacao: 1,
					validado: false,
					value: 10
				};

				ctrl.carregarCartao();
				
				expect(ctrl.cardLast).to.equal('4567');
			});
		});
		
		describe('alterarCartao', function(){
			
			beforeEach(function() { 
				ctrl = $controller('EdicaoController', {
					$scope: $scope,
					CartaoService: {
						alterarCartaoService: function (cartao) {
							return deferred.promise;
						}
					},
                    AutenticacaoService: {
                        obterUsuarioLogado: function() {
                            return deferred.promise;
                        }
                    }
				});

				$scope.carregarCartoes = function () {

				};
                
			});
			
			it('Deve exibir mensagem de sucesso (passo 3) se alteração do apelido do cartão for realizada com sucesso', function () {

			    ctrl.alterarCartao();
				
				deferred.resolve({ success: true});
				$scope.$apply();
				
				expect(ctrl.step).to.equal(3);
			});

			it('Deve exibir mensagem de erro (passo 2) se alteração do apelido do cartão não for realizada com sucesso', function(){
				
				ctrl.alterarCartao();
				
				deferred.resolve({ success: false});
				$scope.$apply();
				
				expect(ctrl.step).to.equal(2);
			});

			it('Deve exibir mensagem de erro (passo 2) se alteração do apelido do cartão não for realizada com sucesso(HTTP 500)', function(){
				
				ctrl.alterarCartao();
				
				deferred.reject();
				$scope.$apply();
				
				expect(ctrl.step).to.equal(2);
			});
		});
		
		describe('excluirCartao', function(){
			
			beforeEach(function() { 
				ctrl = $controller('EdicaoController', {
					$scope: $scope,
					CartaoService: {
						excluirCartaoService: function (idUsuario, cartao) {
							return deferred.promise;
						}
					},
                    AutenticacaoService: {
                        obterUsuarioLogado: function() {
                            return deferred.promise;
                        }
                    }
				});

				$scope.carregarCartoes = function () {

				};

			});
			
			it('Deve exibir mensagem de sucesso (passo 3) se cartão for excluído', function(){

				ctrl.excluirCartao();

				deferred.resolve({ data: {success: true} });
				$scope.$apply();
				
				expect(ctrl.MensagemAlterarOuExcluir).to.equal(2);
				expect(ctrl.step).to.equal(3);
			});

			it('Deve exibir mensagem de erro (passo 2) se cartão não for excluído', function(){

				ctrl.excluirCartao();
				
				deferred.resolve({ data: {success: false} });
				$scope.$apply();
				
				expect(ctrl.MensagemAlterarOuExcluir).to.equal(1);
				expect(ctrl.step).to.equal(2);
			});

			it('Deve exibir mensagem de erro (passo 2) se cartão não for excluído (HTTP 500)', function(){
				
				ctrl.excluirCartao();
				
				deferred.reject();
				$scope.$apply();
				
				expect(ctrl.MensagemAlterarOuExcluir).to.equal(1);
				expect(ctrl.step).to.equal(2);
			});
		});
	});
});