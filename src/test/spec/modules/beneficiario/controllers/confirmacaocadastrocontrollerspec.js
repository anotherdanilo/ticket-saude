"use strict";
describe('beneficiario module', function() {
    describe('confirmacaoCadastroController', function() {
        var $controller, $scope, $q;
        var ctrl, deferred;

        beforeEach(function() {
            module('portalUsuarioApp');

            inject(function(_$rootScope_, _$controller_) {
                $controller = _$controller_;
                $scope = _$rootScope_;
                // $q = _$q_;
                // deferred = $q.defer();
            });
        });

        beforeEach(function() {
            ctrl = $controller('ConfirmacaoCadastroController', {
                $scope: $scope,
                CadastroFactory: {
                    getSentEmail: function(cartao) { }
                },
            });
        });

        describe('toEmailConfirm', function() {

            it('Deve mudar o step da tela para confirmação do cadastro do usuário', function() {
                ctrl.toEmailConfirm();
                //$scope.$apply();
                expect(ctrl.step).to.equal(1);
            });
        });

        describe('toChangeEmail', function() {
            
            it('Deve confirmar o reenvio do email quando não houver alteração no endereço de email', function() {
                ctrl.changeEmail = false;
                
                ctrl.toChangeEmail();
                //$scope.$apply();
                expect(ctrl.changeEmailBtn).to.equal("Cancelar alteração");
                expect(ctrl.sendEmailBtn).to.equal("Salvar alteração e Reenviar");
            });

        });
    });
});