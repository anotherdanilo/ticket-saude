"use strict";
describe('beneficiario module', function(){
	describe('dashboardController', function(){

		var $controller, $scope, $q, $autenticacaoService, $uibModal;
		var ctrl, deferred;

		beforeEach(function(){
			module('portalUsuarioApp');
			
			inject(function(_$rootScope_, _$controller_, _$q_, _$uibModal_){
			    
				$controller = _$controller_;
				$scope = _$rootScope_;
				$q = _$q_;
				$uibModal = _$uibModal_;
				deferred = $q.defer();

			});
		});

		describe('favoritarCartao', function(){

			beforeEach(function() { 
				ctrl = $controller('DashboardController', {
					$scope: $scope,
                    
                    CartaoService: {
						favoritarCartao: function (idUsuario, cartao) {
							return deferred.promise;
						}
					},
					
                    CartaoFactory: {
                        setDataLineChart: function () { },
                        setDataDonutChart: function () { },
                    },
                    
                    AutenticacaoService: {
                        obterUsuarioLogado: function() {
                            return deferred.promise;
                        }
                    },

                    AutoModalsFactory: {
                        openBlock: function () {

                        },
                        openUnblock: function () {

                        },
                        openChangePwd: function () {

                        },
                        openReissue: function () {

                        }
                    }
				});
				
				ctrl.isNotKarma = false;
				
			});

			it('Deve exibir mensagem de SUCESSO se o favoritar do cartão for realizada com sucesso', function(){
				
				var usuario = '123456789';
			
				var cartao = {
					id:1,
					number: '1234234534564567',
					bin: 'TAE',
					apelido: 'Cartao 1',
					favorito: true,
					situacao: 1,
					validado: true,
					value: 10
				};

		       	ctrl.favoritarCartao(usuario, cartao);

		       	deferred.resolve({
					success: true
		       	});

		       	$scope.$apply();

				expect($scope.titulo).to.equal('Sucesso');
				
			});
			
			it('Deve exibir mensagem de ERRO se o favoritar do cartão não for realizado com sucesso', function(){
				
				var usuario = '123456789';
			
				var cartao = {
					id:1,
					number: '1234234534564567',
					bin: 'TAE',
					apelido: 'Cartao 1',
					favorito: true,
					situacao: 1,
					validado: true,
					value: 10
				};

		       	ctrl.favoritarCartao(usuario, cartao);

		       	deferred.reject(400,{
					success: false
		       	});

		       	$scope.$apply();

				expect($scope.titulo).to.equal('Ops!');
				
			});

		});	

	});
});
