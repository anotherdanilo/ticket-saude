﻿"use strict";
describe('beneficiario module', function() {
    describe('editarUsuarioController', function() {
        //, $cadastroFactory,  $facebook; $sce,
        var $controller, $scope, $q, $usuarioModalsFactory, $alertaFactory;
        var ctrl, deferred;

        beforeEach(function() {
            module('portalUsuarioApp');

            inject(function(_$rootScope_, _$controller_, _UsuarioModalsFactory_, _AlertaFactory_, _$q_) {

                $controller = _$controller_;
                $scope = _$rootScope_;
                $q = _$q_;
                $usuarioModalsFactory = _UsuarioModalsFactory_;
                $alertaFactory = _AlertaFactory_;
                deferred = $q.defer();

            });
        });

        describe('carregarDadosUsuario', function() {

            beforeEach(function() {
                ctrl = $controller('EditarUsuarioController', {
                    $scope: $scope,

                    UsuarioService: {
                        carregarUsuarioPorId: function(usuario) {
                            return deferred.promise;
                        }
                    },

                    AutenticacaoService: {
                        autenticar: function(login, senha, token, manterconectado, refreshtoken) {
                            return deferred.promise;
                        },
                        obterUsuarioLogado: function() {
                            var usuario = {
                                nome: "Usuario Karma",
                                isLogged: true,
                                id: "A04E55BC116B6772A3598F03EEC14038",
                                email: "usuariokarma@karma.com.br"
                            };
                            return usuario;
                        }
                    },

                });

            });

            it('Deve carregar dados do usuário com sucesso', function() {

                ctrl.carregarDadosUsuario();

                deferred.resolve({
                    success: true,
                    value: {
                        id: 1,
                        name: 'Teste1',
                        lastName: 'da Silva',
                        endereco: {
                            cep: '0123',
                            logradouro: 'Rua 1',
                            numero: 123,
                            complemento: null,
                            bairro: 'bairro1',
                            cidade: 'cidade1',
                            uf: 'SP',
                        },
                        nascimento: '2016-01-01'
                    }
                });

                $scope.$apply();

                expect(ctrl.usuario.id).to.equal(1);

            });

        });

        describe('alterarDadosusuarioCadastrado', function() {

            beforeEach(function() {
                ctrl = $controller('EditarUsuarioController', {
                    $scope: $scope,
                    UsuarioModalsFactory: $usuarioModalsFactory,
                    AlertaFactory: $alertaFactory,

                    UsuarioService: {
                        alterarCadastroUsuario: function(usuario) {
                            return deferred.promise;
                        }
                    },

                    AutenticacaoService: {
                        obterUsuarioLogado: function() {

                            return {
                                nome: 'usuario logado',
                                id: 1,
                                isLogged: true
                            };
                        }
                    }
                });

                ctrl.openAlerta = function() {

                }

            });

            it('Deve exibir mensagem positiva caso alteração seja realizada com sucesso', function() {

                ctrl.usuario = {
                    nome: 'Teste1',
                    cpf: '123',
                    cellPhone: '123',
                    matricula: null,
                    email: 'a@a.com',
                    formData: {
                        birthDateDay: '1',
                        birthDateMonth: '1',
                        birthDateYear: '2015'
                    },
                    sexo: 'M',
                    endereco: {
                        cep: '0123',
                        logradouro: 'Rua 1',
                        numero: 123,
                        complemento: null,
                        bairro: 'bairro1',
                        cidade: 'cidade1',
                        uf: 'SP',
                    },
                    aceitoMarketing: true,
                    interests: []
                };

                ctrl.alterarDadosusuarioCadastrado();

                deferred.resolve({
                    success: true,
                    value: { id: 1 }
                });

                $scope.$apply();

                expect($alertaFactory.parametros.mensagem).to.equal("Seus dados foram atualizados com sucesso!");

            });

            it('Deve exibir mensagem negativa caso alteração não seja realizada com sucesso', function() {

                ctrl.usuario = {
                    nome: 'Teste1',
                    cpf: '123',
                    cellPhone: '123',
                    matricula: null,
                    email: 'a@a.com',
                    formData: {
                        birthDateDay: '1',
                        birthDateMonth: '1',
                        birthDateYear: '2015'
                    },
                    sexo: 'M',
                    endereco: {
                        cep: '0123',
                        logradouro: 'Rua 1',
                        numero: 123,
                        complemento: null,
                        bairro: 'bairro1',
                        cidade: 'cidade1',
                        uf: 'SP',
                    },
                    aceitoMarketing: true,
                    interests: []
                };

                ctrl.alterarDadosusuarioCadastrado();

                deferred.reject({
                    success: false
                });

                $scope.$apply();

                expect($alertaFactory.parametros.titulo).to.equal("Erro");
                expect($alertaFactory.parametros.icone).to.equal("icon-error");

            });

        });

        describe('obterCep', function() {

            beforeEach(function() {
                ctrl = $controller('EditarUsuarioController', {
                    $scope: $scope,
                    CadastroFactory: $cadastroFactory,
                    LoginModalsFactory: $loginModalFactory,

                    UsuarioService: {
                        cadastrarUsuario: function(usuario) {
                            return deferred.promise;
                        }
                    },

                    AutenticacaoService: {
                        autenticar: function(login, senha, token, manterconectado, refreshtoken) {
                            return deferred.promise;
                        },
                        obterUsuarioLogado: function() {
                            var usuario = {
                                nome: "Usuario Karma",
                                isLogged: true,
                                id: "A04E55BC116B6772A3598F03EEC14038",
                                email: "usuariokarma@karma.com.br"
                            };
                            return usuario;
                        }
                    },

                    $sce: $sce,
                    Facebook: $facebook
                });

            });

        });




    });
});
