"use strict";
describe('beneficiario module', function(){
	describe('bloqueioController', function(){

		var $controller, $scope, $q, $bloqueioFactory, $autenticacaoService;
		var ctrl, deferred;

		beforeEach(function(){
			module('portalUsuarioApp');
			
			inject(function(_$rootScope_, _$controller_, _$q_, _BloqueioFactory_){
			    
				$controller = _$controller_;
				$scope = _$rootScope_;
				$q = _$q_;
				$bloqueioFactory = _BloqueioFactory_;
				deferred = $q.defer();

			});
		});

		
		describe('pageLoad', function(){
			beforeEach(function() { 
				ctrl = $controller('BloqueioController', {
					$scope: $scope,
					BloqueioFactory: $bloqueioFactory,
                    AutenticacaoService: {
                        obterUsuarioLogado: function() {
                            return deferred.promise;
                        }
                    },
                    AutoModalsFactory: {
                        openBlock: function () {

                        },
                        openUnblock: function () {

                        },
                        openChangePwd: function () {

                        },
                        openReissue: function () {

                        }
                    }
				});
			});

			it('Deve exibir informações do cartão a ser bloqueado com validação positiva', function(){
                //console.log("entrou no IT");
				$scope.detalheCartao = {
					id:1,
					number: '1234234534564567',
					bin: 'TAE',
					apelido: 'Cartao 1',
					favorito: true,
					situacao: 1,
					validado: true,
					value: 10
				};

				ctrl.pageLoad();

				expect(ctrl.textoConfirmacaoBloqueio).to.equal('Deseja bloquear este cartão?');

			});

			it('Deve exibir informações do cartão a ser bloqueado com validação negativa', function(){

			    $scope.detalheCartao = {
					id:1,
					number: '1234234534564567',
					bin: 'TAE',
					apelido: 'Cartao 1',
					favorito: true,
					situacao: 1,
					validado: false,
					value: 10
				};

				ctrl.pageLoad();

				expect(ctrl.textoConfirmacaoBloqueio).to.equal('Deseja bloquear seu cartão?');

			});

		});
		
		describe('bloquearCartao', function(){

			beforeEach(function() { 
				ctrl = $controller('BloqueioController', {
					$scope: $scope,
					BloqueioFactory: $bloqueioFactory,
					CartaoService: {
						autoServicoCartao: function (idUsuario, cartao) {
							return deferred.promise;
						}
					},
                    AutenticacaoService: {
                        obterUsuarioLogado: function() {
                            return deferred.promise;
                        }
                    },
                    AutoModalsFactory: {
                        openBlock: function () {

                        },
                        openUnblock: function () {

                        },
                        openChangePwd: function () {

                        },
                        openReissue: function () {

                        }
                    }
				});
                
                ctrl.cartao = 
                {
                    id: 1
                };
                
			});

			it('Deve exibir mensagem de sucesso (passo 1) se o bloqueio do cartão for realizada com sucesso', function(){

		       	ctrl.bloquearCartao();

		       	deferred.resolve({
			         success: true,
			         value: {
			         	protocolo:102030
			         }
		       	});

		       	$scope.$apply();

				expect(ctrl.step).to.equal(1);
				
			});

			
			it('Deve exibir mensagem de ERRO se o bloqueio do cartão não for realizada com sucesso', function(){
		
				ctrl.bloquearCartao();

				deferred.reject({
					status: 400,
					data:{message: 'erro ao bloquear o cartão'}
		       	});

		       	$scope.$apply();

				expect(ctrl.mensagemShow).to.equal(true);
				expect(ctrl.mensagem).to.equal('erro ao bloquear o cartão');
				
			});

			it('Deve exibir mensagem de erro (passo 2) se o bloqueio do cartão não for realizada devido a erro 500', function(){
		
				ctrl.bloquearCartao();

				deferred.reject({
			        status: 500,
					data:{message: 'erro ao bloquear o cartão'}
		       	});

		       	$scope.$apply();

				expect(ctrl.step).to.equal(2);
				
			});

		});	

	});
});
