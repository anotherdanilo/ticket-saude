﻿"use strict";
describe('beneficiario module', function () {
    describe('alertaController', function () {
        var $controller, $scope, $q;
        var ctrl, deferred;

        beforeEach(function () {
            module('portalUsuarioApp');

            inject(function (_$rootScope_, _$controller_, _$q_) {
                $controller = _$controller_;
                $scope = _$rootScope_;
                $scope.$parent = {};
                $q = _$q_;
                deferred = $q.defer();
            });
        });

        describe('load', function () {
            beforeEach(function () {
                ctrl = $controller('AlertaController', {
                    $scope: $scope,
                    AlertaFactory: {
                        parametros: {
                            titulo: 'Titulo 1',
                            mensagem: 'Mensagem 1',
                            icone:'icone-1'
                        },
                        closeAlerta: function() {
                        }
                    }
                });
               
            });

            it('Deve carregar as informações informadas na Factory', function () {

                expect(ctrl.titulo).to.equal("Titulo 1");
                expect(ctrl.mensagem).to.equal('Mensagem 1');
                expect(ctrl.icone).to.equal('icone-1');

            });

        });
    });
});