﻿"use strict";
describe('beneficiario module', function () {
    describe('AtendimentoContatoController', function () {
        var $controller, $scope, $q;
        var ctrl, deferred;

        beforeEach(function () {
            module('portalUsuarioApp');

            inject(function (_$rootScope_, _$controller_, _$q_) {
                $controller = _$controller_;
                $scope = _$rootScope_;
                $scope.$parent = {};
                $q = _$q_;
                deferred = $q.defer();
            });
        });

        describe('load', function () {
            beforeEach(function () {
                ctrl = $controller('AtendimentoContatoController', {
                    $scope: $scope,
                    AtendimentoModalsFactory: {
                        closeTel: function () {
                        },
                        closeMsg: function () {
                        }
                    }
                });

            });

            it('Deve carregar o passo de sucesso (step 1) caso tenha sucesso no contato', function () {

                ctrl.toSuccess();
                expect(ctrl.step).to.equal(1);

            });

            it('Deve carregar o passo de erro (step 2) caso não tenha sucesso no contato', function () {

                ctrl.toError();
                expect(ctrl.step).to.equal(2);

            });

        });
    });
});