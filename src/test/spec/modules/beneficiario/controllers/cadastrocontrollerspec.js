﻿"use strict";
describe('beneficiario module', function() {
    describe('cadastroController', function() {

        var $controller, $scope, $cadastroFactory, $loginModalFactory, $sce, $q, $facebook;
        var ctrl, deferred, deferred1;

        beforeEach(function() {
            module('portalUsuarioApp');

            inject(function(_$rootScope_, _$controller_, _$sce_, _Facebook_, _$q_) {

                $controller = _$controller_;
                $scope = _$rootScope_;
                //$cadastroFactory = _CadastroFactory_;
                //$loginModalFactory = _LoginModalsFactory_;
                $sce = _$sce_;
                $facebook = _Facebook_;
                $q = _$q_;

                deferred = $q.defer();
                deferred1 = $q.defer();

            });
        });

        describe('cadastrarUsuario', function() {

            beforeEach(function() {
                ctrl = $controller('CadastroController', {
                    $scope: $scope,
                    //CadastroFactory: $cadastroFactory,
                    //LoginModalsFactory: $loginModalFactory,

                    UsuarioService: {
                        cadastrarUsuario: function(usuario) {
                            return deferred.promise;
                        }
                    },

                    AutenticacaoService: {
                        autenticar: function(login, senha, token, manterconectado, refreshtoken) {
                            return deferred1.promise;
                        }
                    },

                    CadastroFactory: {
                        resetSignUpFormData: function() {
                            
                        },
                        bindSignUpFormData: function() {
                            var signUpFormData = {};
                            return signUpFormData;
                        }
                    },

                    LoginModalsFactory: {
                        getEscopo: function() {
                            var escopo = {
                                step: 0,
                                objUsuario: null
                            };
                            return escopo;
                        },
                        closeSignUp: function() {
                            return false;
                        }
                    },

                    $sce: $sce,
                    Facebook: $facebook
                });

            });

            it('Deve exibir mensagem de SUCESSO (passo 2) se o cadastro do usuário for realizada com sucesso', function() {

                ctrl.formData.NomeCompleto = "Nome1 Nome2";
                ctrl.formData.Cpf = "268.536.130-82";
                ctrl.formData.Sexo = "M";
                ctrl.formData.Email = "email@email.com.br";
                ctrl.formData.Password = "Ticket2016";
                ctrl.formData.birthDateDay = "01";
                ctrl.formData.birthDateMonth = "01";
                ctrl.formData.birthDateYear = "2015";

                ctrl.cadastrarUsuario(ctrl.formData);

                deferred.resolve({
                    status: 200,
                    success: true
                });

                $scope.$apply();

                expect(ctrl.step).to.equal(2);

            });

            it('Deve exibir mensagem de "CPF já informado" se o CPF informado já estiver cadastrado para outro usuário', function() {

                ctrl.formData.NomeCompleto = "Nome1 Nome2";
                ctrl.formData.Cpf = "268.536.130-82";
                ctrl.formData.Sexo = "M";
                ctrl.formData.Email = "email@email.com.br";
                ctrl.formData.Password = "Ticket2016";
                ctrl.formData.birthDateDay = "01";
                ctrl.formData.birthDateMonth = "01";
                ctrl.formData.birthDateYear = "2015";

                ctrl.cadastrarUsuario(ctrl.formData);

                deferred.reject({
                    status: 400,
                    data: {
                        message: "CPF já informado para outro usuário."
                    }
                });

                $scope.$apply();
                
                expect(ctrl.errorCPF).to.equal(false);
                expect(ctrl.error).to.equal(true);

            });

            it('Deve exibir mensagem de ERRO se o cadastro do usuário não for relizado com sucesso', function() {

                ctrl.formData.NomeCompleto = "Nome1 Nome2";
                ctrl.formData.Cpf = "268.536.130-82";
                ctrl.formData.Sexo = "M";
                ctrl.formData.Email = "email@email.com.br";
                ctrl.formData.Password = "Ticket2016";
                ctrl.formData.birthDateDay = "01";
                ctrl.formData.birthDateMonth = "01";
                ctrl.formData.birthDateYear = "2015";

                ctrl.cadastrarUsuario(ctrl.formData);

                deferred.reject({
                    status: 400,
                    data: {
                        message: "Erro atrelado ao cadastro de usuário"
                    }
                });

                $scope.$apply();

                expect(ctrl.errorCPF).to.equal(false);
                expect(ctrl.error).to.equal(true);
                expect(ctrl.mensagem).to.equal("Erro atrelado ao cadastro de usuário");

            });

        });

        describe('cadastrarUsuario', function() {

            beforeEach(function() {
                ctrl = $controller('CadastroController', {
                    $scope: $scope,
                    CadastroFactory: {
                        resetSignUpFormData: function() {
                            
                        },
                        bindSignUpFormData: function() {
                            var signUpFormData = {};
                            return signUpFormData;
                        }
                    },

                    LoginModalsFactory: {
                        getEscopo: function() {
                            return {
                                objUsuario: {
                                    token: '123',
                                    NomeCompleto: 'Nome 1',
                                    email: 'email@email.com'
                                }
                            }
                        },
                        closeSignUp: function() {
                            return false;
                        }
                    },

                    UsuarioService: {
                        cadastrarUsuario: function(usuario) {
                            return deferred.promise;
                        }
                    },

                    AutenticacaoService: {
                        autenticar: function(login, senha, token, manterconectado, refreshtoken) {
                            return deferred1.promise;
                        }
                    },
                    

                    $sce: $sce,

                    Facebook: {
                        login: function() {

                            return deferred.promise;

                        },

                        getLoginStatus: function() {

                            return deferred.promise;
                        },

                        api: function() {

                            return deferred.promise;

                        }
                    }
                });

            });

            it('Deve exibir o status de logado (LOGGED) após o teste de Facebook ser realizado com sucesso.', function() {
                
                $scope.login();

                deferred1.reject({
                    status: 400
                });

                deferred.resolve({
                    status: 'connected',
                    authResponse: {
                        accessToken: "EAANojuk2TlgBAI6a8XSWHQ0d4EMeUKCRsAnRkn59SDNFGxByV9BioibvHJAdQq8XE9hmk7P6PphcRZAUvOAd7ZAk9Co40ZBBB26GAhgw34qnJ4T1AN50PSV9qD3tnPLTpPHL4WzHnjSzLrFWvxMQ7MlGOZA6VQMx2ktZBEfh3VJL2ks4ZAbIqRgWeZBGA2dCuCPZBWxjEvZCRnVYpwKxJoZBVA"
                    },
                    first_name: 'José',
                    last_name: 'Alves',

                });

                $scope.$apply();

                expect($scope.logged).to.equal(true);
                expect(ctrl.formData.NomeCompleto).to.equal('José Alves');
                expect(ctrl.step).to.equal(1);

            });

        });

    });
});
