"use strict";
describe('shared module', function() {
    describe('cookieService', function() {

        var $q, service, deferred, $cookies, $base64;

        beforeEach(function() {
            module('portalUsuarioApp');

            inject(function(_$cookies_, cookieService, _$q_, _$base64_) {
                $base64 = _$base64_;
                $cookies = _$cookies_;
                service = cookieService;
                $q = _$q_;
                deferred = $q.defer();
            });
            $cookies.remove("Edenred");
            $cookies.remove("EdenredPUA");
        });

        it('Deve criar cookie de autenticação com sucesso', function() {
            var token = { access_token: "eef9c151176b91bd911e1a1a52076ff", expires_in: "19/09/2016 13:09:06", refresh_token: "144bb42356539313b6d4c58fa448f62" };
            service.criarCookieAutenticacao(token);
            var cookie = $cookies.get(config.nomeCookieSSO);
            expect(cookie).not.to.equal(undefined);
            expect(cookie).not.to.equal(null);
        });
        
        it('Deve obter o valor do cookie de autenticação passando o parametro', function() {
            
            var token = { access_token: "eef9c151176b91bd911e1a1a52076ff", expires_in: "19/09/2016 13:09:06", refresh_token: "144bb42356539313b6d4c58fa448f62" };
            service.criarCookieAutenticacao(token);
            var objeto = service.obterValorCookieAutenticacao("Edenred");
            expect(JSON.parse(objeto)).to.have.property('access_token', 'eef9c151176b91bd911e1a1a52076ff');
        });
        
        it('Deve retornar nulo caso não exista o cookie de autenticacao', function() {
            var objeto = service.obterValorCookieAutenticacao("Edenred");
            expect(objeto).to.equal(undefined);
        });
        
         it('Deve obter o valor do cookie de autenticação sem passar o parametro', function() {
            var token = { access_token: "eef9c151176b91bd911e1a1a52076ff", expires_in: "19/09/2016 13:09:06", refresh_token: "144bb42356539313b6d4c58fa448f62" };
            service.criarCookieAutenticacao(token);
            var objeto = service.obterValorCookieAutenticacao();
            expect(JSON.parse(objeto)).to.have.property('access_token', 'eef9c151176b91bd911e1a1a52076ff');
        });
        
        it('Deve criar cookie do usuario sem manter conectado com sucesso', function() {
            var usuario = {"id":"D91B0453D201F698B81D8F4B0D582C89","email":"gu_osbourne@hotmail.com","name":"Gustavo","lastName":"","cpf":"02150254824","cellPhone":"11984863007","device":null,"hasPicture":false,"nascimento":"1988-10-26T00:00:00","matricula":null,"aceitoMarketing":true,"version":null,"identificadorAparelho":null,"numCelValidado":false,"cdValidacaoCelular":null,"sexo":"M","foto":null,"endereco":null,"preferencias":[],"checkboxPreferencia":[{"id":1,"name":"Bem-estar e Saúde","value":false,"links":[],"origemWeb":false},{"id":2,"name":"Cultura e Lazer","value":false,"links":[],"origemWeb":false},{"id":3,"name":"Cursos","value":false,"links":[],"origemWeb":false},{"id":4,"name":"Esportes e Aventura","value":false,"links":[],"origemWeb":false},{"id":5,"name":"Gastronomia","value":false,"links":[],"origemWeb":false},{"id":6,"name":"Hotéis e Viagens","value":false,"links":[],"origemWeb":false},{"id":7,"name":"Produtos","value":false,"links":[],"origemWeb":false},{"id":8,"name":"Serviços","value":false,"links":[],"origemWeb":false}],"origemWeb":false,"manterconectado":false};
            service.criarCookieUsuario(usuario);
            var cookie = $cookies.get(config.nomeCookieUsuario);
            var contemExpires = cookie.indexOf("expires");
            expect(contemExpires).to.equal(-1);
        });
        
         it('Deve criar cookie do usuario mantendo conectado com sucesso', function() {
            var usuario = {"id":"D91B0453D201F698B81D8F4B0D582C89","email":"gu_osbourne@hotmail.com","name":"Gustavo","lastName":"","cpf":"02150254824","cellPhone":"11984863007","device":null,"hasPicture":false,"nascimento":"1988-10-26T00:00:00","matricula":null,"aceitoMarketing":true,"version":null,"identificadorAparelho":null,"numCelValidado":false,"cdValidacaoCelular":null,"sexo":"M","foto":null,"endereco":null,"preferencias":[],"checkboxPreferencia":[{"id":1,"name":"Bem-estar e Saúde","value":false,"links":[],"origemWeb":false},{"id":2,"name":"Cultura e Lazer","value":false,"links":[],"origemWeb":false},{"id":3,"name":"Cursos","value":false,"links":[],"origemWeb":false},{"id":4,"name":"Esportes e Aventura","value":false,"links":[],"origemWeb":false},{"id":5,"name":"Gastronomia","value":false,"links":[],"origemWeb":false},{"id":6,"name":"Hotéis e Viagens","value":false,"links":[],"origemWeb":false},{"id":7,"name":"Produtos","value":false,"links":[],"origemWeb":false},{"id":8,"name":"Serviços","value":false,"links":[],"origemWeb":false}],"origemWeb":false,"manterconectado":true};
            service.criarCookieUsuario(usuario);
            var cookie = $cookies.get(config.nomeCookieUsuario);
            var objeto = JSON.parse($base64.decode(cookie));
            expect(objeto).to.have.property('manterconectado', true);
        });
        
         it('Deve obter o valor do cookie do usuario passando o parametro', function() {
           var usuario = {"id":"D91B0453D201F698B81D8F4B0D582C89","email":"gu_osbourne@hotmail.com","name":"Gustavo","lastName":"","cpf":"02150254824","cellPhone":"11984863007","device":null,"hasPicture":false,"nascimento":"1988-10-26T00:00:00","matricula":null,"aceitoMarketing":true,"version":null,"identificadorAparelho":null,"numCelValidado":false,"cdValidacaoCelular":null,"sexo":"M","foto":null,"endereco":null,"preferencias":[],"checkboxPreferencia":[{"id":1,"name":"Bem-estar e Saúde","value":false,"links":[],"origemWeb":false},{"id":2,"name":"Cultura e Lazer","value":false,"links":[],"origemWeb":false},{"id":3,"name":"Cursos","value":false,"links":[],"origemWeb":false},{"id":4,"name":"Esportes e Aventura","value":false,"links":[],"origemWeb":false},{"id":5,"name":"Gastronomia","value":false,"links":[],"origemWeb":false},{"id":6,"name":"Hotéis e Viagens","value":false,"links":[],"origemWeb":false},{"id":7,"name":"Produtos","value":false,"links":[],"origemWeb":false},{"id":8,"name":"Serviços","value":false,"links":[],"origemWeb":false}],"origemWeb":false,"manterconectado":true};
            service.criarCookieUsuario(usuario);
            var objeto = service.obterValorCookieUsuario("EdenredPUA");
            expect(objeto).to.have.property('id', 'D91B0453D201F698B81D8F4B0D582C89');
        });
        
        it('Deve obter o valor do cookie do usuario sem passar o parametro', function() {
           var usuario = {"id":"D91B0453D201F698B81D8F4B0D582C89","email":"gu_osbourne@hotmail.com","name":"Gustavo","lastName":"","cpf":"02150254824","cellPhone":"11984863007","device":null,"hasPicture":false,"nascimento":"1988-10-26T00:00:00","matricula":null,"aceitoMarketing":true,"version":null,"identificadorAparelho":null,"numCelValidado":false,"cdValidacaoCelular":null,"sexo":"M","foto":null,"endereco":null,"preferencias":[],"checkboxPreferencia":[{"id":1,"name":"Bem-estar e Saúde","value":false,"links":[],"origemWeb":false},{"id":2,"name":"Cultura e Lazer","value":false,"links":[],"origemWeb":false},{"id":3,"name":"Cursos","value":false,"links":[],"origemWeb":false},{"id":4,"name":"Esportes e Aventura","value":false,"links":[],"origemWeb":false},{"id":5,"name":"Gastronomia","value":false,"links":[],"origemWeb":false},{"id":6,"name":"Hotéis e Viagens","value":false,"links":[],"origemWeb":false},{"id":7,"name":"Produtos","value":false,"links":[],"origemWeb":false},{"id":8,"name":"Serviços","value":false,"links":[],"origemWeb":false}],"origemWeb":false,"manterconectado":true};
            service.criarCookieUsuario(usuario);
            var objeto = service.obterValorCookieUsuario();
            expect(objeto).to.have.property('id', 'D91B0453D201F698B81D8F4B0D582C89');
        });
        
         it('Deve retornar nulo caso não exista o cookie de usuario', function() {
            var objeto = service.obterValorCookieUsuario();
            expect(objeto).to.equal(undefined);
        });

    });
});

