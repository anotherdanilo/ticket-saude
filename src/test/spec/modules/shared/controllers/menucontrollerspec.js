﻿"use strict";
describe('shared module', function () {
    describe('menuController', function () {

        var $controller, $scope, $q, $loginModalsFactory, $usuarioModalsFactory, $homeFactory, $menuFactory, $window, $location;
        var ctrl, deferred;

        beforeEach(function () {
            module('portalUsuarioApp');

            inject(function (_$rootScope_, _$controller_, _$q_, _UsuarioModalsFactory_, _$window_, _$location_) {

                $controller = _$controller_;
                $scope = _$rootScope_;
                $q = _$q_;
                $usuarioModalsFactory = _UsuarioModalsFactory_;
                $window = _$window_;
                $location = _$location_;

                deferred = $q.defer();

            });
        });


        describe('carregarCartoes', function () {
            beforeEach(function () {
                ctrl = $controller('MenuController', {
                    $scope: $scope,
                    //LoginModalsFactory: $loginModalsFactory,
                    UsuarioModalsFactory: $usuarioModalsFactory,
                    //HomeFactory: $homeFactory,
                    //MenuFactory: $menuFactory,

                    AutenticacaoService: {
                        obterUsuarioLogado: function () {
                            return {
                                id: 1,
                                isLogged: true
                            }
                        }
                    },
                    CartaoService: {
                        carregarCartoesPorUsuarioService: function () {
                            return deferred.promise;
                        }
                    },
                    LoginModalsFactory: {
                        openLogin: function () { },
                        openSignUp: function () { }
                    },
                    HomeFactory: {

                    },
                    MenuFactory: {

                    }
                });


            });


            it('Deve exibir a lista com 2 (dois) cartões.', function () {

                ctrl.carregarCartoes();

                deferred.resolve({
                    success: true,
                    value:
                      [
                          {
                              "valid": true,
                              "balance": { "id": 460475, "code": "0", "bin": "TC", "nickName": "3085 1210 1107 6003", "number": "3085121011076003", "average": 40.23136363636364, "dailyAverage": 47, "dateNextDeposit": null, "deposit": 885.09, "date": "09/09/2016 00:00:00", "value": "705", "dateParsed": "2016-09-09T00:00:00", "valueParsed": 705, "situacao": 1, "validado": false, "favorito": null, "dataCriacao": "2016-08-12T16:57:25" },
                              "scheduling": [],
                              "release": [{ "date": "05/09/2016 16:59:02", "value": "89,0", "dateParsed": "2016-09-05T16:59:02", "valueParsed": 89, "description": "COMPRA - NOVASOC COMERCIAL LTDA" }, { "date": "31/08/2016 11:35:54", "value": "91,09", "dateParsed": "2016-08-31T11:35:54", "valueParsed": 91.09, "description": "COMPRA - NOVASOC COMERCIAL LTDA" }, { "date": "30/08/2016 00:00:00", "value": "180,0", "dateParsed": "2016-08-30T00:00:00", "valueParsed": 180, "description": "DISPONIB. DE CREDITO" }, { "date": "12/08/2016 17:34:44", "value": "60,0", "dateParsed": "2016-08-12T17:34:44", "valueParsed": 60, "description": "COMPRA - NOVASOC COMERCIAL LTDA" }, { "date": "03/08/2016 11:29:35", "value": "90,0", "dateParsed": "2016-08-03T11:29:35", "valueParsed": 90, "description": "COMPRA - NOVASOC COMERCIAL LTDA" }, { "date": "01/08/2016 12:09:17", "value": "30,0", "dateParsed": "2016-08-01T12:09:17", "valueParsed": 30, "description": "COMPRA - POSTO TOCANTINS LTD" }, { "date": "29/07/2016 00:00:00", "value": "180,0", "dateParsed": "2016-07-29T00:00:00", "valueParsed": 180, "description": "DISPONIB. DE CREDITO" }, { "date": "11/07/2016 14:08:32", "value": "36,0", "dateParsed": "2016-07-11T14:08:32", "valueParsed": 36, "description": "COMPRA - POSTO TOCANTINS LTD" }],
                              "ttCard": []
                          },
                          {
                              "valid": false,
                              "balance": { "id": 460490, "code": null, "bin": "TAE", "nickName": "6026 5124 5597 5129", "number": "6026512455975129", "average": 0, "dailyAverage": 0, "dateNextDeposit": null, "deposit": 0, "date": null, "value": null, "dateParsed": "0001-01-01T00:00:00", "valueParsed": 0, "situacao": 1, "validado": false, "favorito": null, "dataCriacao": "2016-08-25T14:50:18" },
                              "scheduling": [],
                              "release": [{ "date": "24/08/2016 00:00:00", "value": "100", "dateParsed": "2016-08-24T00:00:00", "valueParsed": 100, "description": "COMPRAS - SUPERMERCADO SOUZA" }],
                              "ttCard": []
                          }
                      ]

                });


                $scope.$apply();

                console.log(ctrl.listaCartoes.length);

                expect(ctrl.listaCartoes.length).to.equal(2);

            });

            it('Não deve exibir a lista de cartões', function () {

                ctrl.carregarCartoes();

                deferred.reject({
                    success: true,
                    value:
                      [
                          {
                              "valid": true,
                              "balance": { "id": 460475, "code": "0", "bin": "TC", "nickName": "3085 1210 1107 6003", "number": "3085121011076003", "average": 40.23136363636364, "dailyAverage": 47, "dateNextDeposit": null, "deposit": 885.09, "date": "09/09/2016 00:00:00", "value": "705", "dateParsed": "2016-09-09T00:00:00", "valueParsed": 705, "situacao": 1, "validado": false, "favorito": null, "dataCriacao": "2016-08-12T16:57:25" },
                              "scheduling": [],
                              "release": [{ "date": "05/09/2016 16:59:02", "value": "89,0", "dateParsed": "2016-09-05T16:59:02", "valueParsed": 89, "description": "COMPRA - NOVASOC COMERCIAL LTDA" }, { "date": "31/08/2016 11:35:54", "value": "91,09", "dateParsed": "2016-08-31T11:35:54", "valueParsed": 91.09, "description": "COMPRA - NOVASOC COMERCIAL LTDA" }, { "date": "30/08/2016 00:00:00", "value": "180,0", "dateParsed": "2016-08-30T00:00:00", "valueParsed": 180, "description": "DISPONIB. DE CREDITO" }, { "date": "12/08/2016 17:34:44", "value": "60,0", "dateParsed": "2016-08-12T17:34:44", "valueParsed": 60, "description": "COMPRA - NOVASOC COMERCIAL LTDA" }, { "date": "03/08/2016 11:29:35", "value": "90,0", "dateParsed": "2016-08-03T11:29:35", "valueParsed": 90, "description": "COMPRA - NOVASOC COMERCIAL LTDA" }, { "date": "01/08/2016 12:09:17", "value": "30,0", "dateParsed": "2016-08-01T12:09:17", "valueParsed": 30, "description": "COMPRA - POSTO TOCANTINS LTD" }, { "date": "29/07/2016 00:00:00", "value": "180,0", "dateParsed": "2016-07-29T00:00:00", "valueParsed": 180, "description": "DISPONIB. DE CREDITO" }, { "date": "11/07/2016 14:08:32", "value": "36,0", "dateParsed": "2016-07-11T14:08:32", "valueParsed": 36, "description": "COMPRA - POSTO TOCANTINS LTD" }],
                              "ttCard": []
                          },
                          {
                              "valid": false,
                              "balance": { "id": 460490, "code": null, "bin": "TAE", "nickName": "6026 5124 5597 5129", "number": "6026512455975129", "average": 0, "dailyAverage": 0, "dateNextDeposit": null, "deposit": 0, "date": null, "value": null, "dateParsed": "0001-01-01T00:00:00", "valueParsed": 0, "situacao": 1, "validado": false, "favorito": null, "dataCriacao": "2016-08-25T14:50:18" },
                              "scheduling": [],
                              "release": [{ "date": "24/08/2016 00:00:00", "value": "100", "dateParsed": "2016-08-24T00:00:00", "valueParsed": 100, "description": "COMPRAS - SUPERMERCADO SOUZA" }],
                              "ttCard": []
                          }
                      ]

                });


                $scope.$apply();

                console.log(ctrl.listaCartoes.length);

                expect(ctrl.listaCartoes.length).to.equal(0);

            });

        });

        describe('Botões', function () {
            beforeEach(function () {
                ctrl = $controller('MenuController', {
                    $scope: $scope,

                    UsuarioModalsFactory: $usuarioModalsFactory,

                    AutenticacaoService: {
                        obterUsuarioLogado: function () {
                            return {
                                id: 1,
                                isLogged: true
                            }
                        },
                        setNomeUsuarioLogado: function() {
                            
                        }
                    },
                    CartaoService: {
                        carregarCartoesPorUsuarioService: function () {
                            return deferred.promise;
                        }
                    },
                    LoginModalsFactory: {
                        openLogin: function () { },
                        openSignUp: function () { }
                    },
                    HomeFactory: {

                    },
                    MenuFactory: {
                        close: function () { }
                    },

                    $window: $window,
                    $location: $location
                });


            });


            it('Ao clicar no botão "Dashboard", deve redirecionar para a pagina Dashboard', function () {

                ctrl.toDashboard(1);

                expect($location.$$url.indexOf("/beneficiario/dashboard")).to.not.equal(-1);

            });

            it('Ao clicar no botão "Rede Credenciada", deve redirecionar para a pagina Rede Credenciada', function () {

                ctrl.toRedeCredenciada();
                
                expect($location.$$path).to.equal("/rede-credenciada");

            });

            it('Ao clicar no botão "Atendimento Digital", deve redirecionar para a pagina Atendimento Digital', function () {

                ctrl.toAtendimentoDigital();
                
                expect($location.$$path).to.equal("/beneficiario/atendimento");

            });

            it('Ao clicar no botão "Meu Cadastro", deve redirecionar para a pagina Meu Cadastro', function () {

                ctrl.toMeuCadastro();

                expect($location.$$path).to.equal("/beneficiario/usuario/editarusuario");

            });

            it('Ao clicar no botão "Beneficio Club", deve redirecionar para a pagina Beneficio Club', function () {

                ctrl.toBeneficioClub();

                expect($location.$$url).to.equal("/beneficio-club");

            });

            it('Ao clicar no botão "Consulta Saldo", deve redirecionar para a pagina Consulta Saldo', function () {

                ctrl.toConsultaSaldo();

                expect($location.$$path).to.equal("/consulta-saldo");

            });

            // it('Ao clicar no botão "Home", deve redirecionar para a pagina Home', function () {

            //     ctrl.toHome();

            //     expect($window.location.href.indexOf("/")).to.not.equal(-1);

            // });

            it('Ao clicar no botão "Sair", deve redirecionar para a pagina Sair', function () {

                ctrl.sair();

                expect(ctrl.isOpen).to.equal(false);

            });
            
        });


    });
});
