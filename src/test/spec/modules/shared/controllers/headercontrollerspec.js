﻿"use strict";
describe('shared module', function () {
    describe('headerController', function () {

        var $controller, $scope, $q, $usuarioModalsFactory, $alertaFactory, $window, $location;
        var ctrl, deferred;

        beforeEach(function () {
            module('portalUsuarioApp');

            inject(function (_$rootScope_, _$controller_, _$q_, _UsuarioModalsFactory_, _AlertaFactory_, _$window_, _$location_) {

                $controller = _$controller_;
                $scope = _$rootScope_;
                $q = _$q_;
                $usuarioModalsFactory = _UsuarioModalsFactory_;
                $alertaFactory = _AlertaFactory_;
                $window = _$window_;
                $location = _$location_;

                deferred = $q.defer();

            });
        });


        describe('Botoes', function () {
            beforeEach(function () {
                ctrl = $controller('HeaderController', {
                    $scope: $scope,
                    LoginModalsFactory: {
                        
                    },
                    MenuFactory: {
                        
                    },
                    UsuarioModalsFactory: $usuarioModalsFactory,
                    AlertaFactory: $alertaFactory,
                    
                    AutenticacaoService: {
                        obterUsuarioLogado: function () {
                            return {
                                id: 1,
                                isLogged: true
                            }
                        }
                    },
                    CartaoService: {
                        carregarCartoesPorUsuarioService: function () {
                            return deferred.promise;
                        }
                    },
                    
                    HomeFactory: {
                        
                    }
                });

                // $window = $window;
                // $location = $location;

                // $location.search('t', '123');
            });


            // it('Ao clicar no botão "Home" deve mudar o MenuSelecionado para zero (0).', function () {

            //     ctrl.home();

            //     expect($scope.menuSelecionado).to.equal(0);

            // });

            it('Ao clicar no botão "Rede Credenciada" deve mudar o MenuSelecionado para zero (1).', function () {

                ctrl.toRedeCredenciada();

                expect($scope.menuSelecionado).to.equal(1);

            });

            it('Ao clicar no botão "Beneficio Club" deve mudar o MenuSelecionado para zero (2).', function () {

                ctrl.beneficioClub();

                expect($scope.menuSelecionado).to.equal(2);

            });

            it('Ao clicar no botão "Auto Atendimento" deve mudar o MenuSelecionado para zero (3).', function () {

                ctrl.toAtendimento();

                expect($scope.menuSelecionado).to.equal(3);

            });

            it('Ao clicar no botão "Consulta Saldo" deve mudar o MenuSelecionado para zero (4).', function () {

                ctrl.toConsultaSaldo();

                expect($scope.menuSelecionado).to.equal(4);

            });
        });
    });
});
