﻿"use strict";
describe('shared module', function () {
    describe('homeController', function () {

        var $controller, $scope, $beneficioClubFactory, $q, $facebook;
        var ctrl, deferred, deferred1;

        beforeEach(function () {
            module('portalUsuarioApp');

            inject(function (_$rootScope_, _$controller_, _BeneficioClubFactory_, _Facebook_, _$q_) {

                $controller = _$controller_;
                $scope = _$rootScope_;
                $beneficioClubFactory = _BeneficioClubFactory_;
                $facebook = _Facebook_;
                $q = _$q_;

                deferred = $q.defer();
                deferred1 = $q.defer();

            });
        });

        describe('pageLoad', function () {

            beforeEach(function () {
                ctrl = $controller('HomeController', {
                    $scope: $scope,

                    HomeFactory: {
                        parametros: {
                            baixaApp: false
                        }
                    },


                    BeneficioClubFactory: $beneficioClubFactory,

                    LoginModalsFactory: {

                    },

                    AutenticacaoService: {
                        autenticar: function () {
                            return deferred.promise;
                        },
                        setNomeUsuarioLogado: function () {
                        },
                        obterUsuarioLogado: function () {
                        }
                    },


                    Facebook: {
                        login: function () {
                            return deferred.promise;
                        },

                        getLoginStatus: function () {

                            return deferred.promise;
                        },

                        api: function () {

                            return deferred.promise;

                        }
                    },

                    UsuarioService: {
                        obterUsuarioPorToken: function() {
                            return deferred1.promise;
                        }
                    }


                });

                ctrl.isNotKarma = false;

            });

            it('Deve exibir o status de logado (LOGGED) após o teste de Facebook ser realizado com sucesso.', function () {

                $scope.login();

                deferred.resolve({
                    status: 'connected',
                    id: '123',
                    authResponse: {
                        accessToken: "123"
                    }
                });

                deferred1.resolve({});

                $scope.$apply();

                expect($scope.logged).to.equal(true);

            });
        });

        //describe('submit', function () {

        //    beforeEach(function () {
        //        ctrl = $controller('LoginController', {
        //            $scope: $scope,
        //            AutenticacaoFactory: $autenticacaoFactory,
        //            LoginModalsFactory: $loginModalFactory,

        //            AutenticacaoService: {
        //                autenticar: function () {
        //                    return deferred.promise;
        //                },
        //                setNomeUsuarioLogado: function () {

        //                }
        //            },
        //            Facebook: $facebook,

        //            RecaptchaService: {
        //                verificarRecaptchaService: function () {

        //                }
        //            },

        //            vcRecaptchaService: {
        //                getResponse: function () {

        //                },
        //                reload: function () {

        //                }
        //            }
        //        });

        //        ctrl.isNotKarma = false;

        //    });

        //    //it('Deve exibir alterar o passo para LOGADO ( 1 ) se o login for realizada com sucesso', function() {

        //    //    ctrl.submit();

        //    //    deferred.resolve({
        //    //        status: 200,
        //    //        success: true
        //    //    });

        //    //    $scope.$apply();

        //    //    expect(ctrl.step).to.equal(1);

        //    //});

        //    //it('Deve exibir mensagem de erro se o login não for realizada com sucesso', function() {
        //    //    ctrl.submit();

        //    //    deferred.reject("Usuário, senha ou token inválidos.");

        //    //    $scope.$apply();

        //    //    expect(ctrl.msgRetorno).to.equal("Usuário, senha ou token inválidos.");

        //    //});

        //});

        //describe('loginFacebook', function () {

        //    beforeEach(function () {
        //        ctrl = $controller('LoginController', {
        //            $scope: $scope,
        //            AutenticacaoFactory: $autenticacaoFactory,
        //            LoginModalsFactory: $loginModalFactory,

        //            AutenticacaoService: {
        //                autenticar: function () {
        //                    return deferred.promise;
        //                },
        //                setNomeUsuarioLogado: function () {

        //                }
        //            },

        //            Facebook: {
        //                login: function () {
        //                    return deferred.promise;
        //                },

        //                getLoginStatus: function () {

        //                    return deferred.promise;
        //                },

        //                api: function () {

        //                    return deferred.promise;

        //                }
        //            },

        //            RecaptchaService: {
        //                verificarRecaptchaService: function () {

        //                }
        //            },

        //            vcRecaptchaService: {
        //                getResponse: function () {

        //                },
        //                reload: function () {

        //                }
        //            }
        //        });

        //        ctrl.isNotKarma = false;

        //    });

        //    //it('Deve exibir o status de logado (LOGGED) após o teste de Facebook ser realizado com sucesso.', function() {

        //    //    $scope.loginFacebook();

        //    //    deferred.resolve({
        //    //        status: 'connected',
        //    //        id: '123'
        //    //    });

        //    //    $scope.$apply();

        //    //    expect($scope.logged).to.equal(true);
        //    //    expect(ctrl.formData.Token).to.equal('123');

        //    //});

        //});

        //describe('reenviarEmail', function () {

        //    beforeEach(function () {
        //        ctrl = $controller('LoginController', {
        //            $scope: $scope,
        //            AutenticacaoFactory: $autenticacaoFactory,
        //            LoginModalsFactory: $loginModalFactory,

        //            AutenticacaoService: {
        //                autenticar: function () {
        //                    return deferred.promise;
        //                },
        //                setNomeUsuarioLogado: function () {
        //                },
        //                reenviarEmail: function () {
        //                    return deferred.promise;
        //                },
        //            },
        //            Facebook: $facebook,

        //            RecaptchaService: {
        //                verificarRecaptchaService: function () {

        //                }
        //            },

        //            vcRecaptchaService: {
        //                getResponse: function () {

        //                },
        //                reload: function () {

        //                }
        //            }
        //        });

        //        ctrl.isNotKarma = false;

        //    });

        //    //it('Deve exibir comunicado de reenvio do email (passo 2) se o mesmo for realizado com sucesso', function() {

        //    //    ctrl.reenviarEmail();

        //    //    deferred.resolve({
        //    //        success: true
        //    //    });

        //    //    $scope.$apply();

        //    //    expect(ctrl.step).to.equal(2);
        //    //});
        //});

    });
});
