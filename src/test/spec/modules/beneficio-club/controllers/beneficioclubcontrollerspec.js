﻿"use strict";
describe('beneficio-club module', function () {
    describe('beneficioClubController', function () {

        var $controller, $scope, $q, $beneficioClubFactory, $beneficioModalsFactory, $filter, $sce, $location, $route;
        var ctrl, deferred;

        beforeEach(function () {
            module('portalUsuarioApp');

            inject(function (_BeneficioClubFactory_, _BeneficioModalsFactory_, _$rootScope_, _$controller_, _$q_, _$filter_, _$sce_, _$location_, _$route_) {
                $controller = _$controller_;
                $scope = _$rootScope_;
                $q = _$q_;
                $beneficioClubFactory = _BeneficioClubFactory_,
                $beneficioModalsFactory = _BeneficioModalsFactory_,
                $filter = _$filter_,
                $sce = _$sce_,
                $location = _$location_,
                $route = _$route_,
                deferred = $q.defer();

            });
        });


        describe('carregarCategorias', function () {

            beforeEach(function () {
                ctrl = $controller('BeneficioClubController', {
                    BeneficioClubFactory: $beneficioClubFactory,
                    BeneficioModalsFactory: $beneficioModalsFactory,
                    $location: $location,
                    $scope: $scope,
                    BeneficioService: {
                        listarCategorias: function() {
                            return deferred.promise;
                        },
                        listarBanners: function() {
                            return deferred.promise;
                        }
                    },
                    $sce: $sce,
                    $route: $route
                });

                $route.current = {
                    params: {
                        
                    }
                };

            });

            it('Deve exibir a lista de categorias populada se a consulta da API for realizada com sucesso.', function () {
                
                ctrl.carregarCategorias();

                deferred.resolve({
                    data: {
                        category: [
                            {
                                "id": "2C9E141A409D12140140A0987ADD0AB3",
                                "name": "37 anos da Ticket",
                                "color": null,
                                "subCategory": null
                            },
                            {
                                "id": "2C9E141A409D12140140A0987AD10AAA",
                                "name": "Bem-estar e Saúde",
                                "color": "#e7297b",
                                "subCategory": null
                            },
                            {
                                "id": "2C9E141A409D12140140A0987AD70AAF",
                                "name": "Black Friday",
                                "color": null,
                                "subCategory": null
                            }
                        ]
                    }

                });

                $scope.$apply();
                
                expect(ctrl.filterData.categorias.length).not.to.equal(0);

            });

            it('Deve exibir a lista de categorias vazia se a consulta da API for não realizada com sucesso.', function () {

                ctrl.carregarCategorias();

                deferred.reject({
                    status: 400
                });

                $scope.$apply();

                expect(ctrl.filterData.categorias.length).to.equal(0);

            });

        });

        describe('pageLoad', function () {

            beforeEach(function () {
                ctrl = $controller('BeneficioClubController', {
                    BeneficioClubFactory: $beneficioClubFactory,
                    BeneficioModalsFactory: $beneficioModalsFactory,
                    $location: $location,
                    $scope: $scope,
                    BeneficioService: {
                        listarPromocoes: function () {
                            return deferred.promise;
                        },
                        listarCategorias: function () {
                            return deferred.promise;
                        },
                        listarBanners: function () {
                            return deferred.promise;
                        }
                    },
                    $sce: $sce,
                    $route: $route
                });

                $route.current = {
                    params: {
                        
                    }
                };
            });

            it('Deve exibir a lista de ofertas e Quantidade de Filtros no primeiro acesso se a consulta for realizada com sucesso.', function () {

                ctrl.pageLoad();

                deferred.resolve({
                    data: {
                        "offers": [
                            {
                                "sites": null,
                                "category": [
                                    {
                                        "id": "2C9E141A409D12140140A0987ACB0AA7",
                                        "name": "Gastronomia",
                                        "color": "#00B5BE",
                                        "subCategory": null
                                    }
                                ],
                                "contentLocaleId": "2C9E141940D01D1F014105EC806E0A9D",
                                "contentVersionId": "2C9E141940D01D1F014105EC806E0A9E",
                                "stateId": "approved",
                                "id": "2C9E141940D01D1F014105EC1E2309B0",
                                "logo": null,
                                "title": "Original Burger Campo Belo: 30% OFF nos melhores restaurantes da cidade, sem restrição e com total discrição.",
                                "titleUrl": "Original Burger Campo Belo",
                                "introduction": "<p>Aproveite os benefícios dessa parceria entre o Grubster e o Benefício Club onde o desconto de 30% é aplicado em todo o valor da mesa, considerando a bebida e a comida.</p><p>Você escolhe o restaurante e faz sua reserva pelo site com até 30 minutos de antecedência e ganha também 30% OFF na taxa de reserva, pagando apenas R$7,00.</p><p>Ao chegar no restaurante, é só se apresentar mencionando a reserva em seu nome e, ao pedir a conta, verifique se o desconto foi aplicado corretamente.</p>",
                                "typeOffer": 0,
                                "website": "http://www.grubster.com.br/restaurantes/original-burger-campo-belo",
                                "content": null,
                                "image1": null,
                                "image2": null,
                                "image3": null,
                                "rules": null,
                                "countView": 2,
                                "idImport": "2934",
                                "logo1": "https://d2rlg75vf4lcam.cloudfront.net/uploads/restaurant/logomark/2934/logo-pronto.jpg",
                                "image4": "https://d2rlg75vf4lcam.cloudfront.net/uploads/restaurant_picture/picture/19408/saveme_prato-3.jpg",
                                "image5": null,
                                "activeVoucher": false,
                                "titleVoucher": null,
                                "contentVoucher": null,
                                "action": null,
                                "friendlyPath": "/bc/ofertas/original-burger-campo-belo.htm",
                                "publishStartDateTime": 1378783887383,
                                "publishEndDateTime": null
                            },
                            {
                                "sites": null,
                                "category": [
                                    {
                                        "id": "2C9E141A409D12140140A0987ACB0AA7",
                                        "name": "Gastronomia",
                                        "color": "#00B5BE",
                                        "subCategory": null
                                    }
                                ],
                                "contentLocaleId": "2C9E141940D01D1F0140E70637313881",
                                "contentVersionId": "2C9E141940D01D1F0140E70637323882",
                                "stateId": "approved",
                                "id": "2C9E141940D01D1F0140E705B7073796",
                                "logo": null,
                                "title": "Original Burger Alto de PInheiros: 30% OFF nos melhores restaurantes da cidade, sem restrição e com total discrição.",
                                "titleUrl": "Original Burger Alto de PInheiros",
                                "introduction": "<p>Aproveite os benefícios dessa parceria entre o Grubster e o Benefício Club onde o desconto de 30% é aplicado em todo o valor da mesa, considerando a bebida e a comida.</p><p>Você escolhe o restaurante e faz sua reserva pelo site com até 30 minutos de antecedência e ganha também 30% OFF na taxa de reserva, pagando apenas R$7,00.</p><p>Ao chegar no restaurante, é só se apresentar mencionando a reserva em seu nome e, ao pedir a conta, verifique se o desconto foi aplicado corretamente.</p>",
                                "typeOffer": 0,
                                "website": "http://www.grubster.com.br/restaurantes/original-burger-alto-de-pinheiros",
                                "content": null,
                                "image1": null,
                                "image2": null,
                                "image3": null,
                                "rules": null,
                                "countView": 0,
                                "idImport": "2935",
                                "logo1": "https://d2rlg75vf4lcam.cloudfront.net/uploads/restaurant/logomark/2935/logo-pronto.jpg",
                                "image4": "https://d2rlg75vf4lcam.cloudfront.net/uploads/restaurant_picture/picture/19402/saveme_prato-2.jpg",
                                "image5": "https://d2rlg75vf4lcam.cloudfront.net/uploads/restaurant_picture/picture/19400/saveme_ambiente-1.jpg",
                                "activeVoucher": false,
                                "titleVoucher": null,
                                "contentVoucher": null,
                                "action": null,
                                "friendlyPath": "/bc/ofertas/original-burger-alto-de-pinheiros.htm",
                                "publishStartDateTime": 1378265478790,
                                "publishEndDateTime": null
                            }
                        ]
                    }

                });

                $scope.$apply();

                expect(ctrl.filterData.ofertas.length).not.to.equal(0);
                expect(ctrl.filterData.contadores.todos).not.to.equal(0);

            });

            it('Deve exibir a lista de ofertas se a consulta for realizada com sucesso.', function () {

                ctrl.primeiroCarremento = false;

                ctrl.pageLoad();

                deferred.resolve({
                    data: {
                        "offers": [
                            {
                                "sites": null,
                                "category": [
                                    {
                                        "id": "2C9E141A409D12140140A0987ACB0AA7",
                                        "name": "Gastronomia",
                                        "color": "#00B5BE",
                                        "subCategory": null
                                    }
                                ],
                                "contentLocaleId": "2C9E141940D01D1F014105EC806E0A9D",
                                "contentVersionId": "2C9E141940D01D1F014105EC806E0A9E",
                                "stateId": "approved",
                                "id": "2C9E141940D01D1F014105EC1E2309B0",
                                "logo": null,
                                "title": "Original Burger Campo Belo: 30% OFF nos melhores restaurantes da cidade, sem restrição e com total discrição.",
                                "titleUrl": "Original Burger Campo Belo",
                                "introduction": "<p>Aproveite os benefícios dessa parceria entre o Grubster e o Benefício Club onde o desconto de 30% é aplicado em todo o valor da mesa, considerando a bebida e a comida.</p><p>Você escolhe o restaurante e faz sua reserva pelo site com até 30 minutos de antecedência e ganha também 30% OFF na taxa de reserva, pagando apenas R$7,00.</p><p>Ao chegar no restaurante, é só se apresentar mencionando a reserva em seu nome e, ao pedir a conta, verifique se o desconto foi aplicado corretamente.</p>",
                                "typeOffer": 0,
                                "website": "http://www.grubster.com.br/restaurantes/original-burger-campo-belo",
                                "content": null,
                                "image1": null,
                                "image2": null,
                                "image3": null,
                                "rules": null,
                                "countView": 2,
                                "idImport": "2934",
                                "logo1": "https://d2rlg75vf4lcam.cloudfront.net/uploads/restaurant/logomark/2934/logo-pronto.jpg",
                                "image4": "https://d2rlg75vf4lcam.cloudfront.net/uploads/restaurant_picture/picture/19408/saveme_prato-3.jpg",
                                "image5": null,
                                "activeVoucher": false,
                                "titleVoucher": null,
                                "contentVoucher": null,
                                "action": null,
                                "friendlyPath": "/bc/ofertas/original-burger-campo-belo.htm",
                                "publishStartDateTime": 1378783887383,
                                "publishEndDateTime": null
                            },
                            {
                                "sites": null,
                                "category": [
                                    {
                                        "id": "2C9E141A409D12140140A0987ACB0AA7",
                                        "name": "Gastronomia",
                                        "color": "#00B5BE",
                                        "subCategory": null
                                    }
                                ],
                                "contentLocaleId": "2C9E141940D01D1F0140E70637313881",
                                "contentVersionId": "2C9E141940D01D1F0140E70637323882",
                                "stateId": "approved",
                                "id": "2C9E141940D01D1F0140E705B7073796",
                                "logo": null,
                                "title": "Original Burger Alto de PInheiros: 30% OFF nos melhores restaurantes da cidade, sem restrição e com total discrição.",
                                "titleUrl": "Original Burger Alto de PInheiros",
                                "introduction": "<p>Aproveite os benefícios dessa parceria entre o Grubster e o Benefício Club onde o desconto de 30% é aplicado em todo o valor da mesa, considerando a bebida e a comida.</p><p>Você escolhe o restaurante e faz sua reserva pelo site com até 30 minutos de antecedência e ganha também 30% OFF na taxa de reserva, pagando apenas R$7,00.</p><p>Ao chegar no restaurante, é só se apresentar mencionando a reserva em seu nome e, ao pedir a conta, verifique se o desconto foi aplicado corretamente.</p>",
                                "typeOffer": 0,
                                "website": "http://www.grubster.com.br/restaurantes/original-burger-alto-de-pinheiros",
                                "content": null,
                                "image1": null,
                                "image2": null,
                                "image3": null,
                                "rules": null,
                                "countView": 0,
                                "idImport": "2935",
                                "logo1": "https://d2rlg75vf4lcam.cloudfront.net/uploads/restaurant/logomark/2935/logo-pronto.jpg",
                                "image4": "https://d2rlg75vf4lcam.cloudfront.net/uploads/restaurant_picture/picture/19402/saveme_prato-2.jpg",
                                "image5": "https://d2rlg75vf4lcam.cloudfront.net/uploads/restaurant_picture/picture/19400/saveme_ambiente-1.jpg",
                                "activeVoucher": false,
                                "titleVoucher": null,
                                "contentVoucher": null,
                                "action": null,
                                "friendlyPath": "/bc/ofertas/original-burger-alto-de-pinheiros.htm",
                                "publishStartDateTime": 1378265478790,
                                "publishEndDateTime": null
                            }
                        ]
                    }

                });

                $scope.$apply();

                expect(ctrl.filterData.ofertas.length).not.to.equal(0);

            });

            it('Deve exibir a lista de ofertas vazia se a consulta não for realizada com sucesso.', function () {

                ctrl.pageLoad();

                deferred.reject({
                    status: 400
                });

                $scope.$apply();

                expect(ctrl.filterData.ofertas.length).to.equal(0);

            });

        });

        describe('Filtros', function () {

            beforeEach(function () {
                ctrl = $controller('BeneficioClubController', {
                    BeneficioClubFactory: $beneficioClubFactory,
                    BeneficioModalsFactory: $beneficioModalsFactory,
                    $location: $location,
                    $scope: $scope,
                    BeneficioService: {
                        listarPromocoes: function () {
                            return deferred.promise;
                        },
                        listarCategorias: function () {
                            return deferred.promise;
                        },
                        listarBanners: function () {
                            return deferred.promise;
                        }
                    },
                    $sce: $sce,
                });

                ctrl.pageLoad = function() {
                    
                }
            });

            it('Deve filtrar a lista de ofertas pela categoria "Gastronomia".', function () {

                ctrl.filtrar('Gastronomia');

                expect(ctrl.filterData.filtros.category).to.equal('Gastronomia');

            });

            it('Deve ordenar a lista de ofertas por ofertas Mais vistos (1).', function () {

                ctrl.ordernar('1');

                expect(ctrl.filterData.filtros.countView).to.equal('1');

            });

            it('Deve filtar a lista de ofertas pelo tipo Oferta "Online".', function () {

                ctrl.tipoOferta('online');

                expect(ctrl.filterData.filtros.type).to.equal('online');

            });

        });

    });
});
