﻿"use strict";
describe('beneficio-club module', function () {
    describe('BeneficioClubDenunciaController', function () {

        var $controller, $scope, $q, $beneficioModalsFactory;
        var ctrl, deferred;

        beforeEach(function () {
            module('portalUsuarioApp');

            inject(function (_BeneficioModalsFactory_, _$rootScope_, _$controller_, _$q_) {
                $controller = _$controller_;
                $scope = _$rootScope_;
                $q = _$q_;
                $beneficioModalsFactory = _BeneficioModalsFactory_,
                deferred = $q.defer();

            });
        });


        describe('enviar', function () {

            beforeEach(function () {
                ctrl = $controller('BeneficioClubDenunciaController', {
                    BeneficioModalsFactory: $beneficioModalsFactory,
                    $scope: $scope,
                    BeneficioService: {
                        denunciarOferta: function () {
                            return deferred.promise;
                        }
                    }
                });

             

            });

            it('Deve enviar a denuncia e exibir a mensagem de sucesso (passo 1) se a Denuncia foi enviada com sucesso.', function () {

                $scope.offer = {
                    parceiro: '',
                    oferta: '',
                    partners: '',
                    offer: '',
                    register: '',
                    registerName: '',
                    registerEmail: ''
                };

                ctrl.enviar();

                deferred.resolve();

                $scope.$apply();

                expect(ctrl.step).to.equal(1);

            });

            it('Deve exibir a mensagem de erro (passo 2) caso a mensagem de Denuncia não seja enviada com sucesso.', function () {

                $scope.offer = {
                    parceiro: '',
                    oferta: '',
                    partners: '',
                    offer: '',
                    register: '',
                    registerName: '',
                    registerEmail: ''
                };

                ctrl.enviar();

                deferred.reject();

                $scope.$apply();

                expect(ctrl.step).to.equal(2);

            });

        });

        describe('pageLoad', function () {

            beforeEach(function () {
                ctrl = $controller('BeneficioClubDenunciaController', {
                    BeneficioModalsFactory: $beneficioModalsFactory,
                    $scope: $scope
                });

            });

            it('Deve exibir o parceiro e descrição da oferta se a Denuncia foi carregada com sucesso.', function () {

                $scope.offer = {
                    parceiro: 'Parceiro Teste',
                    oferta: 'Oferta teste',
                    partners: '',
                    offer: '',
                    register: '',
                    registerName: '',
                    registerEmail: ''
                };

                ctrl.pageLoad();

                expect(ctrl.parceiro).to.equal('Parceiro Teste');
                expect(ctrl.oferta).to.equal('Oferta teste');

            });

        });

    });
});
