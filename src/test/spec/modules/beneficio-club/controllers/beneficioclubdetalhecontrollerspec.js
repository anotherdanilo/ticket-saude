﻿"use strict";
describe('beneficio-club module', function() {
    describe('beneficioClubDetalheController', function() {

        var $controller, $scope, $q, $beneficioClubDetalheFactory, $loginModalsFactory, $filter;
        var ctrl, deferred;

        beforeEach(function() {
            module('portalUsuarioApp');

            inject(function(_$rootScope_, _$controller_, _$q_) {
                $controller = _$controller_;
                $scope = _$rootScope_;
                $q = _$q_;
                //$beneficioClubDetalheFactory = _BeneficioClubDetalheFactory_,
                //$loginModalsFactory = _LoginModalsFactory_,
                deferred = $q.defer();
            });
        });


        describe('pageLoad', function() {

            beforeEach(function() {
                ctrl = $controller('BeneficioClubDetalheController', {
                    //BeneficioClubDetalheFactory: $beneficioClubDetalheFactory,
                    //LoginModalsFactory: $loginModalsFactory,
                    $scope: $scope,

                    BeneficioService: {
                        listarPromocoes: function() {
                            return deferred.promise;
                        },
                        States: function() {
                            return deferred.promise;
                        }
                    },

                    AutenticacaoService: {
                        obterUsuarioLogado: function() {
                            var usuario = { nome: "", isLogged: false, id: 0 };
                            return usuario;
                        }
                    },

                    BeneficioClubDetalheFactory: {
                        bindOfferData: function() {
                            var offerData = {};
                            return offerData;
                        }
                    },

                    LoginModalsFactory: {
                        getEscopo: function() {
                            return {
                                objUsuario: {
                                    token: '123',
                                    NomeCompleto: 'Nome 1',
                                    email: 'email@email.com'
                                }
                            }
                        }
                    },

                    Socialshare: {
                        share: function () {

                        }
                    }

                });
            });

            it('Deve exibir a oferta detalhada se a consulta for realizada com sucesso.', function() {

                ctrl.pageLoad();

                deferred.resolve({
                    data: {
                        "offers": [
                            {
                                "sites": null,
                                "category": [
                                    {
                                        "id": "2C9E141A409D12140140A0987ACB0AA7",
                                        "name": "Gastronomia",
                                        "color": "#00B5BE",
                                        "subCategory": null
                                    }
                                ],
                                "contentLocaleId": "2C9E141940D01D1F014105EC806E0A9D",
                                "contentVersionId": "2C9E141940D01D1F014105EC806E0A9E",
                                "stateId": "approved",
                                "id": "2C9E141940D01D1F014105EC1E2309B0",
                                "logo": null,
                                "title": "Original Burger Campo Belo: 30% OFF nos melhores restaurantes da cidade, sem restrição e com total discrição.",
                                "titleUrl": "Original Burger Campo Belo",
                                "introduction": "<p>Aproveite os benefícios dessa parceria entre o Grubster e o Benefício Club onde o desconto de 30% é aplicado em todo o valor da mesa, considerando a bebida e a comida.</p><p>Você escolhe o restaurante e faz sua reserva pelo site com até 30 minutos de antecedência e ganha também 30% OFF na taxa de reserva, pagando apenas R$7,00.</p><p>Ao chegar no restaurante, é só se apresentar mencionando a reserva em seu nome e, ao pedir a conta, verifique se o desconto foi aplicado corretamente.</p>",
                                "typeOffer": 0,
                                "website": "http://www.grubster.com.br/restaurantes/original-burger-campo-belo",
                                "content": null,
                                "image1": null,
                                "image2": null,
                                "image3": null,
                                "rules": null,
                                "countView": 2,
                                "idImport": "2934",
                                "logo1": "https://d2rlg75vf4lcam.cloudfront.net/uploads/restaurant/logomark/2934/logo-pronto.jpg",
                                "image4": "https://d2rlg75vf4lcam.cloudfront.net/uploads/restaurant_picture/picture/19408/saveme_prato-3.jpg",
                                "image5": null,
                                "activeVoucher": false,
                                "titleVoucher": null,
                                "contentVoucher": null,
                                "action": null,
                                "friendlyPath": "/bc/ofertas/original-burger-campo-belo.htm",
                                "publishStartDateTime": 1378783887383,
                                "publishEndDateTime": null
                            },
                        ]
                    }

                });

                $scope.$apply();

                expect(ctrl.item.id).to.equal("2C9E141940D01D1F014105EC1E2309B0");

            });

            it('Não deve exibir a oferta detalhada se a consulta não for realizada com sucesso.', function() {

                ctrl.pageLoad();

                deferred.reject({
                    status: 400
                });

                $scope.$apply();

                expect(ctrl.item).to.equal(null);

            });

            it('Deve exibir a oferta detalhada com imagens internas.', function() {

                ctrl.pageLoad();

                deferred.resolve({
                    data: {
                        "offers": [
                            {
                                "sites": null,
                                "category": [
                                    {
                                        "id": "2C9E141A409D12140140A0987AD40AAC",
                                        "name": "Produtos",
                                        "color": "#00FF00",
                                        "qtd": null,
                                        "subCategory": null
                                    }
                                ],
                                "partners": {
                                    "id": "2C9E141A409D12140140A0987CC10AF6",
                                    "dateCad": 1377083882360,
                                    "cnpj": "07.130.025/0001-59",
                                    "corporate": "DIGIBRAS INDUSTRIA DO BRASIL S/A",
                                    "fantasy": "CCE",
                                    "image": "files/2C9E1D2F389F952A0138A4CE586029A9/CCE.jpg",
                                    "urlImage": "files/2C9E1D2F389F952A0138A4CE586029A9/CCE.jpg",
                                    "name1": "RAFAEL GODOI",
                                    "email1": null,
                                    "phone1": null,
                                    "cellular1": null,
                                    "name2": null,
                                    "email2": null,
                                    "phone2": null,
                                    "cellular2": null,
                                    "is_client": false,
                                    "is_establishment": null,
                                    "address": []
                                },
                                "contentLocaleId": "2C9E141940A094AC0140A098E538023D",
                                "contentVersionId": "2C9E141940A094AC0140A098E539023E",
                                "stateId": "approved",
                                "id": "2C9E141A409D12140140A09887090D2E",
                                "logo": null,
                                "title": "Usuários Ticket têm 25% de desconto em produtos CCE!",
                                "titleUrl": "CCE",
                                "introduction": "Associados Benefício Club podem adquirir produtos da CCE com até 25% de desconto + Frete Grátis + 5% de desconto no boleto bancário à vista.",
                                "typeOffer": 1,
                                "website": "http://www.cceshop.com.br/beneficioclub",
                                "content": "<p>Associados Benef&iacute;cio Club podem adquirir produtos da CCE com at&eacute; 25% de desconto + Frete Gr&aacute;tis + 5% de desconto no boleto banc&aacute;rio &agrave; vista.<br />\n&nbsp;<br />\n&nbsp;<br />\nPara adquirir o desconto, basta acessar o site da promo&ccedil;&atilde;o.<br />\n&nbsp;<br />\n&nbsp;<br />\nNo site da promo&ccedil;&atilde;o, voc&ecirc; poder&aacute; encontrar produtos, como: Desktops, Notebooks, Celulares, TV&acute;s de LCD e muito mais<br />\nTV de 32&quot; CCE<br />\nDe: R$1.299,00<br />\nPor: R$969,00<br />\nClique e acesse a p&aacute;gina do produto - <a href=\"https://www.cceshop.com.br/produto/272/stile-d32led\">https://www.cceshop.com.br/produto/272/stile-d32led</a></p>",
                                "image1": "files/2C9E14193804E0840138051A4DE97607/produtos-1.png",
                                "image2": "files/2C9E14193804E0840138051A4DE97607/produtos-2.png",
                                "image3": "files/2C9E14193804E0840138051A4DE97607/produtos-3.png",
                                "rules": "<p>Promo&ccedil;&atilde;o n&atilde;o cumulativa com as demais promo&ccedil;&otilde;es vigentes. Promo&ccedil;&atilde;o exclusiva e intransfer&iacute;vel para usu&aacute;rios associados Benef&iacute;cio Club. Produtos com descontos de at&eacute; 25%. Verificar bandeiras de cart&atilde;o de cr&eacute;dito dispon&iacute;veis para parcelamento ou boleto banc&aacute;rio para pagamento &aacute; vista. Prazo de entrega informado no final da compra. Para d&uacute;vidas, cr&iacute;ticas ou sugest&otilde;es entre em contato pelo e-mail <a href=\"mailto:cceshop@cce.com.br\">cceshop@cce.com.br</a> <a href=\"mailto:cceshop@cce.com.br\">cceshop@cce.com.br</a> informando o seu CPF e n&uacute;mero de pedido. Frete sob consulta.</p>",
                                "countView": 1408,
                                "idImport": null,
                                "logo1": null,
                                "image4": "files/2C9E14193804E0840138051A4DE97607/produtos-4.png",
                                "image5": "files/2C9E14193804E0840138051A4DE97607/produtos-5.png",
                                "activeVoucher": false,
                                "titleVoucher": null,
                                "contentVoucher": null,
                                "action": null,
                                "friendlyPath": "/bc/ofertas/cce.htm",
                                "publishStartDateTime": "18/07/2012 18:35",
                                "publishEndDateTime": null
                            }
                        ]
                    }

                });

                $scope.$apply();

                expect(ctrl.offerData.imagens[4].src).to.equal(config.baseUrlImagemPortalWeb + "files/2C9E14193804E0840138051A4DE97607/produtos-1.png");
                expect(ctrl.offerData.imagens[3].src).to.equal(config.baseUrlImagemPortalWeb + "files/2C9E14193804E0840138051A4DE97607/produtos-2.png");
                expect(ctrl.offerData.imagens[2].src).to.equal(config.baseUrlImagemPortalWeb + "files/2C9E14193804E0840138051A4DE97607/produtos-3.png");
                expect(ctrl.offerData.imagens[1].src).to.equal(config.baseUrlImagemPortalWeb + "files/2C9E14193804E0840138051A4DE97607/produtos-4.png");
                expect(ctrl.offerData.imagens[0].src).to.equal(config.baseUrlImagemPortalWeb + "files/2C9E14193804E0840138051A4DE97607/produtos-5.png");

            });
        });

        describe('ofertasRelacionadas', function() {

            beforeEach(function() {
                ctrl = $controller('BeneficioClubDetalheController', {
                    //BeneficioClubDetalheFactory: $beneficioClubDetalheFactory,
                    //LoginModalsFactory: $loginModalsFactory,
                    $scope: $scope,

                    BeneficioService: {
                        listarPromocoes: function() {
                            return deferred.promise;
                        },
                        States: function() {
                            return deferred.promise;
                        }
                    },

                    AutenticacaoService: {
                        obterUsuarioLogado: function() {
                            var usuario = { nome: "", isLogged: false, id: 0 };
                            return usuario;
                        }
                    },

                    BeneficioClubDetalheFactory: {
                        bindOfferData: function() {
                            var offerData = {};
                            return offerData;
                        }
                    },

                    LoginModalsFactory: {
                        getEscopo: function() {
                            return {
                                objUsuario: {
                                    token: '123',
                                    NomeCompleto: 'Nome 1',
                                    email: 'email@email.com'
                                }
                            }
                        }
                    },

                    Socialshare: {
                        share: function () {

                        }
                    }

                });
            });

            it('Deve exibir as ofertas relacionadas se a consulta na API for realizada com sucesso.', function() {

                ctrl.filtros = {
                    id: 0
                };

                ctrl.ofertasRelacionadas();

                deferred.resolve({
                    data: {
                        "offers": [
                            {
                                "sites": null,
                                "category": [
                                    {
                                        "id": "2C9E141A409D12140140A0987AD70AAF",
                                    }
                                ],
                                "partners": {
                                    "id": "2C9E141A409D12140140A09880AA0CCE",
                                },
                                "id": "2C9E15F8554FEB0B015550B6056A0E5F",
                            },
                            {
                                "sites": null,
                                "category": [
                                    {
                                        "id": "2C9E141A409D12140140A0987ACB0AA7",

                                    }
                                ],
                                "partners": {
                                    "id": "2C9E1419409D3D8101409D45CE29177A",
                                    "address": [
                                        {
                                            "id": "2C9E141A40D02812014105EC800C0014",
                                        }
                                    ]
                                },
                                "id": "2C9E141940D01D1F014105EC1E2309B0",
                            }
                        ]
                    }

                });

                $scope.$apply();

                expect(ctrl.ofertas.length).to.equal(2);

            });

            it('Não deve exibir as ofertas relacionadas se a consulta na API não for realizada com sucesso.', function() {

                ctrl.filtros = {
                    id: 0
                };

                ctrl.ofertasRelacionadas();

                deferred.reject();

                $scope.$apply();

                expect(ctrl.ofertas).to.equal(undefined);

            });

        });

        describe('funções de Apoio', function() {

            beforeEach(function() {
                ctrl = $controller('BeneficioClubDetalheController', {
                    //BeneficioClubDetalheFactory: $beneficioClubDetalheFactory,
                    //LoginModalsFactory: $loginModalsFactory,
                    $scope: $scope,

                    BeneficioService: {
                        listarPromocoes: function() {
                            return deferred.promise;
                        },
                        States: function() {
                            return deferred.promise;
                        }
                    },

                    AutenticacaoService: {
                        obterUsuarioLogado: function() {
                            var usuario = { nome: "", isLogged: false, id: 0 };
                            return usuario;
                        }
                    },

                    BeneficioClubDetalheFactory: {
                        bindOfferData: function() {
                            var offerData = {};
                            return offerData;
                        }
                    },

                    LoginModalsFactory: {
                        getEscopo: function() {
                            return {
                                objUsuario: {
                                    token: '123',
                                    NomeCompleto: 'Nome 1',
                                    email: 'email@email.com'
                                }
                            }
                        }
                    },

                    Socialshare: {
                        share: function() {
                            
                        }
                    }

                });
                
                ctrl.item = { 
                    title: "teste karma",
                    introduction: "teste karma",
                    logoPrincipal: "https://testekarma.com.br/imagem.jpg"
                }
            });

            it('Deve abrir o Facebook.', function() {

                ctrl.facebook();

            });

            it('Deve abrir o Twiter.', function() {

                ctrl.twiter();

            });

            it('Deve abrir o GooglePlus.', function() {

                ctrl.google();

            });

            it('Deve abrir o Pinterest.', function() {

                ctrl.pinterest();

            });

        });

        describe('FiltroUnidades', function() {

            beforeEach(function() {
                ctrl = $controller('BeneficioClubDetalheController', {
                    //BeneficioClubDetalheFactory: $beneficioClubDetalheFactory,
                    //LoginModalsFactory: $loginModalsFactory,
                    $scope: $scope,

                    BeneficioService: {
                        listarPromocoes: function() {
                            return deferred.promise;
                        },
                        States: function() {
                            return deferred.promise;
                        }
                    },

                    AutenticacaoService: {
                        obterUsuarioLogado: function() {
                            var usuario = { nome: "", isLogged: false, id: 0 };
                            return usuario;
                        }
                    },

                    BeneficioClubDetalheFactory: {
                        bindOfferData: function() {
                            var offerData = {};
                            return offerData;
                        }
                    },

                    LoginModalsFactory: {
                        getEscopo: function() {
                            return {
                                objUsuario: {
                                    token: '123',
                                    NomeCompleto: 'Nome 1',
                                    email: 'email@email.com'
                                }
                            }
                        }
                    },

                    Socialshare: {
                        share: function () {

                        }
                    }
                });
            });

            it('Deve exibir a lista de cidades após o estado SP selecionado.', function() {

                ctrl.resultadoGeral = [
                    {
                        "state": "RJ",
                        "city": [
                            {
                                "city": "RIO DE JANEIRO",
                                "neighborhood": [
                                    {
                                        "neighborhood": "CENTRO"
                                    }
                                ]
                            }
                        ],
                        "address": [
                            {
                                "id": "2C9E141A409D12140140A0987DFB0B7E",
                                "partners": "2C9E141A409D12140140A0987DF70B7D",
                                "cep": "04534-002",
                                "address": "R JOAQUIM FLORIANO",
                                "number": "466",
                                "complement": null,
                                "neighborhood": "ITAIM BIBI",
                                "city": "SAO PAULO",
                                "state": "SP",
                                "lat": null,
                                "lng": null,
                                "phone1": "(11)2387-5139  ",
                                "phone2": "(11)2386-4080  ",
                                "action": null
                            },
                            {
                                "id": "2C9E141A409D12140140A0987DFC0B7F",
                                "partners": "2C9E141A409D12140140A0987DF70B7D",
                                "cep": "20031-144",
                                "address": "R MEXICO",
                                "number": "21",
                                "complement": null,
                                "neighborhood": "CENTRO",
                                "city": "RIO DE JANEIRO",
                                "state": "RJ",
                                "lat": null,
                                "lng": null,
                                "phone1": "(21)2524-7788  ",
                                "phone2": null,
                                "action": null
                            }
                        ]
                    },
                    {
                        "state": "SP",
                        "city": [
                            {
                                "city": "SAO PAULO",
                                "neighborhood": [
                                    {
                                        "neighborhood": "ITAIM BIBI"
                                    }
                                ]
                            }
                        ],
                        "address": [
                            {
                                "id": "2C9E141A409D12140140A0987DFB0B7E",
                                "partners": "2C9E141A409D12140140A0987DF70B7D",
                                "cep": "04534-002",
                                "address": "R JOAQUIM FLORIANO",
                                "number": "466",
                                "complement": null,
                                "neighborhood": "ITAIM BIBI",
                                "city": "SAO PAULO",
                                "state": "SP",
                                "lat": null,
                                "lng": null,
                                "phone1": "(11)2387-5139  ",
                                "phone2": "(11)2386-4080  ",
                                "action": null
                            },
                            {
                                "id": "2C9E141A409D12140140A0987DFC0B7F",
                                "partners": "2C9E141A409D12140140A0987DF70B7D",
                                "cep": "20031-144",
                                "address": "R MEXICO",
                                "number": "21",
                                "complement": null,
                                "neighborhood": "CENTRO",
                                "city": "RIO DE JANEIRO",
                                "state": "RJ",
                                "lat": null,
                                "lng": null,
                                "phone1": "(21)2524-7788  ",
                                "phone2": null,
                                "action": null
                            }
                        ]
                    }
                ];

                ctrl.estadoSelecionado = 'SP';

                ctrl.FiltroUnidades(1);

                $scope.$apply();

                expect(ctrl.cidades.length).to.equal(1);

            });

            it('Deve exibir a lista de bairros após o estado SP e cidade São Paulo selecionados.', function() {

                ctrl.cidades = [
                    {

                        "city": "SAO PAULO",
                        "neighborhood": [
                            {
                                "neighborhood": "ITAIM BIBI"
                            }
                        ]
                    }

                ];

                ctrl.estadoSelecionado = 'SP';
                ctrl.cidadeSelecionada = 'SAO PAULO';

                ctrl.FiltroUnidades(2);

                expect(ctrl.bairros.length).to.equal(1);

            });

        });

    });

});
