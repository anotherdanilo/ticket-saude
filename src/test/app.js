var portalUsuarioApp = initModule("portalUsuarioApp", [
    "facebook",
 	"ngRoute",
	"ngResource",
	"ngDialog",
	"ngAnimate",
	"base64",
	"ngCookies",
	"ngFileUpload",
	"toaster",
	"angular.filter",
  	"ui.bootstrap",
	"datetime",
  	"datePicker",
    //"ui.utils.masks",
    //"cgBusy",
    //"angularUtils.directives.dirPagination",
    //"vcRecaptcha",
    //"angulartics",
    //"angulartics.google.analytics",


    //"angulartics.google.tagmanager",
    //"slick",
    //"duScroll"
 	]);

function initModule(moduleName, dependencies) {
 	var app = angular.module(moduleName, dependencies);

	app.controllerLazy = app.controller;
	app.factoryLazy = app.factory;
	app.serviceLazy = app.service;
	app.directiveLazy = app.directive;

	return app;
}

//@ sourceURL=apps.js
