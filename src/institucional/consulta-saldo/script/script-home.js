/*
* Ticket - Edenred Brasil
*
* Este javascript contém todas as funções e
* eventos referentes à home
* 
* @section Javascript
*/

/* VARIÁVEIS GLOBAIS
******************************************************************** */
var larguraJanela = $(window).width(),
    alturaMenuTopo = $('.navbar').outerHeight(),
    alturaFooter = $('.footer').outerHeight(),
    alturaAreaLogin = $('.area-login-topo-f').outerHeight(),
    alturaContainerPagina = $('.containerPagina').innerHeight(),
    alturaJanela = $(window).innerHeight(),
    alturaAtual = alturaContainerPagina / 4,
    isHome = true,
    isMobile = false,
    position = 0,
    isScrolling = true,
    ie,
    deviceAgent = navigator.userAgent.toLowerCase(),
    agentID = deviceAgent.match(/(ipad)/),
    alturaBanner = 400;

/* EVENTO DISPARADO QUANDO A TELA É REDIMENSIONADA
******************************************************************** */
$(window).on('resize', function () {
    alturaJanela = $(window).height();
    larguraJanela = $(window).width();

    ajustaBanner();
    
    // Verifica se é dispositivo móvel
    if (isGadget()) {
        $('.contentBoxCinzaHome').css({ 'width': '100%' });
        $('.footerHome').css('width', larguraJanela + 'px');
    }
    else {
        $('.pagina').each(function () {
            atualizaCem($(this));
        });
        $('.containerPagina').removeClass('containerIpad');
    }
});

$(window).load(function () {
    ajustaBanner();
});

/* EVENTO DISPARADO QUANDO A PÁGINA É EXIBIDA
******************************************************************** */
$(function () {
    // Verifica se o navegador é Internet Explorer 8
    if (ie != 8){
        window.addEventListener("orientationchange", function () {
            if (window.orientation === 90 || window.orientation === -90) {
                atualizaIpad();
            }
            if (window.orientation === 0 || window.orientation === 180) {
                atualizaMobile();
            }
        }, false);
    }

    // Remove a classe containerIpad
    $('.containerPagina').removeClass('containerIpad');

    // Verifica se é desktop
    if (!isGadget()) {
        animaSlideHome();
        $('.containerPagina').removeClass('containerIpad');
        $('.btnServicos').dropdownHover({ delay: 0, instantlyCloseOthers: true });

        $('.dropdown').hover(
            function () {
                var menuSize = $('.navbar-default').outerHeight();
                $('.pagina').css({ 'margin-top': menuSize + alturaAreaLogin });
            },
            function () {
                $('.pagina').css({ 'margin-top': '150px' });
            }
        );
    }

    // Verifica se está acessando por dispositivo móvel
    if (isGadget()) {
        $('.btnSobeDesce').hide();
        atualizaIpad();
        atualizaMobile();
    }

    // Verifica se a janela é menor que 768px
    if (larguraJanela < 768) {
        isMobile = true;
    }

    // Esconde o botão de subir os slides
    $('#sobe').hide();

    // Passa para o slide de cima
    $("#sobe").click(function () {
        desceSobe('sobe');
    });

    // Passa para o slide de baixo
    $("#desce").click(function () {
        desceSobe('desce');
    });

    // Vai direto para o slide de rede credenciada
    var urlSite = window.location.href;
    if (urlSite.search('credenciadas') > 0) {
        paginaAtualHome(-200);
    }

    // Exibe o telefone ao clicar no botão
    $('#btnVerTelefone').click(function () {
        $(this).hide();
        $('.telefoneOculto').show();
    });

    // Lista estabelecimentos credenciadas
    $('#estabelecimentos').change(function () {
        $(this).parent().find('.texto-input-f').htm($(this).val());
    });

    // Mapa de credenciadas
    $('.containerOculto').hide();
    $('.buscaHome[data-pagina-busca="2"]').hide();

    // Mostra/esconde mapa de credenciadas
    $('.ocultaBusca, .containerOculto').click(function () {
        if ($(this).hasClass('clicado')) {
            $('.buscaHome[data-pagina-busca="2"], .containerBuscaIndica').show();
            if (isGadget() === false) {
                $('.containerOculto').css({ 'left': '410px' });
            } else {
                $('.containerOculto').css({ 'left': 'initial', 'right': '5%' });
            }
            $(this).removeClass('clicado');
        } else {
            $('.buscaHome[data-pagina-busca="2"], .containerBuscaIndica').hide();
            $('.containerOculto').css({ 'left': '20px' });
            $(this).addClass('clicado');
        }
    });

    // Botão busca de credenciada
    $('#btnBuscarLocal').click(function () {
        $('.buscaHome[data-pagina-busca="1"]').hide();
        $('.buscaHome[data-pagina-busca="2"], .containerBuscaIndica').show();
        $('.overlaymap').hide();
        $('.containerOculto').show();
    });

    // Fazer nova busca de credenciada
    $('#btnNovaBusca').click(function () {
        $('.buscaHome[data-pagina-busca="2"]').hide();
        $('.buscaHome[data-pagina-busca="1"], .containerBuscaIndica').show();
        $('.overlaymap').show();
        $('.containerOculto').hide();
        $('.buscaPorCartao .panel').removeClass('active');
        $('.buscaPorCartao .panel input[type=checkbox]').prop('checked', false);
        $('.ocultaBusca').removeClass('clicado');
    });

    // Formulários
    $(".checkCartao .panel input[type=checkbox]").on("click", function (e) {
        if ($(this).is(":checked"))
            $(this).parent().parent().addClass("active");
        else
            $(this).parent().parent().removeClass("active");
    });

    $(".buscaPorCartao .panel input[type=checkbox]").on("click", function (e) {
        if ($(this).is(":checked"))
            $(this).parent().parent().addClass("active");
        else
            $(this).parent().parent().removeClass("active");
    });

    $('.simulacaoCartao').slideDown();
});

/*
* Função que faz scroll até o slide indicado
* - @param posicao : int : Posição do slide que deve ser exibido
*/
function paginaAtualHome(posicao) {
    position = posicao;
    desceSobe('desce');
    scrollContainer();
}

/*
* Função que posiciona o menu
*/
function aplicaMenuFalso() {
    var menuPagina = $('.menuServicos').clone();

    if ($('.menuFake').is(':empty')) {
        $('.menuFake').htm(menuPagina);
        $('.menuFake').find('.btn-group-f').removeClass('dropup');
        $('.menuFake .menuServicos').css({ 'position': 'relative', 'top': '0px', 'z-index': '999' });
        $('<div class="bracket-f bracket-f-menu"></div>').insertAfter('.menuFake .menuServicos');
    }
	console.log(position);
    if (position === 0) {
        $('.menuFake').hide();
		$('.area-login-topo-f').show();
    }
    else {
        $('.menuFake').show();
		$('.area-login-topo-f').hide();
    }

    Cufon.replace('.dropdown-servicos', { hover: true, fontFamily: 'Novel Sans Pro' });
    Cufon.refresh();
    $('.btnServicos').dropdownHover({ delay: 0, instantlyCloseOthers: true });
}

/* PAGINACAO DA HOME */
function animaSlideHome() {
    // Atualiza altura da primeira pagina
    if (!isGadget()) {
        atualizaCem($('.pagina1'));
    }
    enterTop();

    // Anima slide ao fazer scroll
    if (ie === 8) {
        window.attachEvent('onmousewheel', handleMouseWheel);
    }
    else {
        window.addEventListener('DOMMouseScroll', handleMouseWheel, false);
        window.addEventListener('mousewheel', handleMouseWheel, false);
    }

    function handleMouseWheel(event) {
        if (isGadget()) return;

        if (event.detail > 0 || event.wheelDelta < 0) {
            desceSobe('desce');
        } else if (event.detail < 0 || event.wheelDelta > 0) {
            desceSobe('sobe');
        }
    }

    document.onkeydown = checkKey;

    // Anima slide ao clicar nas setas sobe/desce
    function checkKey(e) {
        e = e || window.event;
        if (isGadget() === true) return;

        if (e.keyCode == '38') {
            desceSobe('sobe');
        }
        else if (e.keyCode == '40') {
            desceSobe('desce');
        }
    }
}

/*
* Funções que fazem scroll na página
*/
function enterTop() {
    isScrolling = true;

    setTimeout(function () {
        isHome = false;
        isScrolling = false;
    }, 1000);
}

function closeTop() {
    isScrolling = true;

    setTimeout(function () {
        isHome = true;
        isScrolling = false;
    }, 1000);
}

/*
* Faz scroll na página
*/
function scrollContainer() {
    var topo = 0;
    $(".containerPagina").addClass("scroll");
    newPosition = String(position) + "%";

    $(".pagina").each(
        function () {
            topo = $(this).css('top');
        }
    );

    $(".pagina").css({ 'top': (topo - 20) + '%' });

    isScrolling = true;
    $(".containerPagina").css("top", newPosition);

    setTimeout(function () {
        aplicaMenuFalso();
    }, 0);

    setTimeout(function () {
        $(".containerPagina").removeClass("scroll");
    }, 700);

    setTimeout(function () {
        isScrolling = false;
    }, 700);
    console.log('fim');
}

/*
* Atualiza altura visível da página
*/
function atualizaCem(pagina) {
    var prc, novaAltura = 0,
        proximaAltura = 0;
    paginaAtual = pagina.data('pagina');

    // Altura atual sem contar o menu topo
    if (paginaAtual === 1) {
        novaAltura = alturaJanela - alturaMenuTopo - alturaAreaLogin;
    } else if (paginaAtual === 2) {
        novaAltura = alturaJanela - alturaMenuTopo - alturaAreaLogin;
    } else if (paginaAtual == 3) {
        novaAltura = alturaJanela - alturaMenuTopo - alturaAreaLogin;
    } else if (paginaAtual === 4) {
        novaAltura = alturaJanela - alturaMenuTopo - alturaAreaLogin - alturaFooter;
    }

    if (isGadget() === false) {
        pagina.css({ 'height': novaAltura + 'px' });
    }
}

/*
* Corrige topo da pagina
*/
function alteraTopo(posicaoY) {
    if (posicaoY < '0') {
        setTimeout(function () {
            //$('.area-login-topo-f').hide();
            $('.pagina').css({ 'margin-top': '150px' });
            $('#sobe').show();
        }, 200);
    }
    else if (posicaoY == '0') {
        setTimeout(function () {
            //$('.area-login-topo-f').show();
            $('.pagina').css({ 'margin-top': '150px' });

            $('#sobe').hide();
        }, 200);
    }

    if (posicaoY == '-300') {
        setTimeout(function () {
            $('#desce').hide();
        }, 500);
    } else {
        setTimeout(function () {
            $('#desce').show();
        }, 500);
    }
}

/*
* Desce e sobe das pagina
*/
function desceSobe(direcao) {
    if (direcao === 'sobe') {
        if (position < 0 && isScrolling == false && isHome == false) {
            position = position + 100;
            if (position != -300) {
                $('.footerHome').animate({ bottom: '-' + $('.footerHome').outerHeight() + 'px' }, 500, function () { $('.footerHome').hide(); });
            }
            scrollContainer();
        }
        if (position == 0 && isScrolling == false && isHome == false) {
            closeTop();
        }
    }

    if (direcao === 'desce') {
        if (position > -300 && isScrolling == false && isHome == false) {
            position = position - 100;
            if (position == -300) {
                $('.footerHome').show().animate({ bottom: '0' }, 700);
            }
            scrollContainer();
        }
        if (position == 0 && isScrolling == false && isHome == true) {
            enterTop();
        }
    }

    alteraTopo(position);

    $('.pagina').each(function () {
        atualizaCem($(this));
    });
}

/* 
* Versão mobile
*/
function atualizaMobile() {
    $('.containerPagina').removeClass('containerIpad');
    Cufon.replace('.containerFormProp p', { hover: true, fontFamily: 'Novel Sans Pro' });
    Cufon.refresh();
}

/*
* Versão iPad
*/
function atualizaIpad() {
    $('.containerPagina').addClass('containerIpad');
    $('.containerIpad .btnSobeDesce').hide();

    $(window).on('resize load', function () {
        $('.pagina2').css('height', '25%');
        if ($(window).width() > 768) {
            $('.pagina2 .checkCartao .panel label').css('height', '120px');
        }
    });

    $(window).on('scroll', function () {
        if ($(window).width() > 768 && $(window).scrollTop() > 1800) {
            $('.footerHome').addClass('footeriPad');
        } else {
            $('.footerHome').removeClass('footeriPad');
        }
    });
}

function ajustaBanner() {
    if ($(window).width() < 1024) {
        $('.pagina1').height(alturaBanner);
        var banner = 1000;
        // Verifica qual o menor item dos banners
        $('.banners li').each(function () {
            if ($(this).find('img').length > 0) {
                if (alturaBanner < banner)
                    alturaBanner = $(this).find('img').height();
            } else {
                if (alturaBanner < banner)
                    alturaBanner = $(this).find('video').height();
            }
        });

        $('.bracket-f-bottom').css('bottom', $('.bracket-f-bottom').height() / 2 - $('.bracket-f-bottom').height());

        $('.pagina1').height(alturaBanner);
    }
}

/*
* API do Google Mapas
*/
function initialize() {
    var mapOptions = {
        zoom: 15,
        center: new google.maps.LatLng(-23.571608, -46.641344),
        disableDefaultUI: true
    }
    var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

    var image = 'images/google-maps-pin.png';
    var myLatLng = new google.maps.LatLng(-23.571608, -46.641344);

    var marker = new google.maps.Marker({
        position: myLatLng,
        map: map,
        icon: image,
        title: 'Foster Agência Digital'
    });
}

// Inicializa a API
google.maps.event.addDomListener(window, 'load', initialize);