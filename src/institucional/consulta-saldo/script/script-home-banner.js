﻿/*
* Ticket - Edenred Brasil
*
* Este javascript contém todas as funções e
* eventos referentes ao banner da home
* 
* @section Javascript
*/

/* VARIÁVEIS GLOBAIS
******************************************************************** */
var qtdBanners = ($('.banners li').length - 1),
    marcadoAnterior = $('.bannerBtn span:eq(0)'),
    indice = 0,
    bannerAnterior = 0,
    intervalo = 0,
    lado = 2,
    podeClicar = true;

/* EVENTO DISPARADO ASSIM QUE A PÁGINA É EXIBIDA
******************************************************************** */
$(function () {
    // Verifica os atributos data- dos elementos e escreve caso seja imagem ou vídeo
    $('.banners li').each(function (e) {
        $(this).htm(initBanner($(this).attr('data-mp4'), $(this).attr('data-webm'), $(this).attr('data-img'), $(this).attr('data-link'), $(this).attr('data-img-mob')));
    });
    
    // Assim que o vídeo for carregado, a função que troca os banners automaticamente é chamada
    if ($('.banners li').eq(0).find('video').length > 0) {
        $('.banners li').eq(0).find('video')[0].addEventListener('loadeddata', function () {
            iniciarSlidesAuto();
        }, false);
    }

    // Caso a tela possua menos de 480px como não há vídeos, não espera carregar
    if ($(window).width() < 480) {
        iniciarSlidesAuto();
    }

    // Vai para o próximo banner ao clicar na seta
    $('.proximo').click(function () {
        irBannerEsq();
    });

    // Vai para o banner anterior ao clicar na seta
    $('.anterior').click(function () {
        irBannerDir();
    });

    // Vai para o banner equivalente ao clicar nos bullets
    $('.bannerBtn span').click(function () {
        if (!podeClicar)
            return;

        indice = $('.bannerBtn span').index(this);

        marcarBanner(this);
        marcadoAnterior = this;

        trocarBanner();
        iniciarSlidesAuto();
    });

    // Troca o banner ao fazer o efeito de swipe
    $('.banners').on('swipeleft', function () {
        irBannerEsq();
    });
    $('.banners').on('swiperight', function () {
        irBannerDir();
    });
});

/* EVENTO DISPARADO ASSIM QUE A PÁGINA É CARREGADA
******************************************************************** */
$(window).load(function () {
    // Caso a tela seja menor que 640px ajusta a altura da imagem
    if ($(window).width() <= 640) {
        $('.banners li').css('min-height', $('.banners li').find('.img-banner').height() + 'px');
    }
});

/*
* Função que troca os banners automaticamente
*/
function iniciarSlidesAuto() {
    lado = 2;
    clearInterval(intervalo);
    
    intervalo = setInterval(function () {
        if (indice >= qtdBanners)
            indice = -1;

        indice++;
        marcarBanner($('.bannerBtn span:eq(' + indice + ')'));
        marcadoAnterior = $('.bannerBtn span:eq(' + indice + ')');
        trocarBanner();

    }, 9000);
}

/*
* Função muda a cor do bullet
*/
function marcarBanner(obj) {
    if (marcadoAnterior != null)
        $(marcadoAnterior).toggleClass('bannerBtnMarcado');

    $(obj).toggleClass('bannerBtnMarcado');
}
marcadoAnterior.addClass('bannerBtnMarcado');

/*
* Função que troca o banner com base na variável indice
*/
function trocarBanner() {
    if (indice == bannerAnterior)
        return

    var left = '101%';
    if (lado == 2)
        left = '101%';
    else
        left = '-101%';

    $('.banners li').eq(indice).css({ 'left': left });
    $('.banners li').eq(indice).animate({ 'left': 0, 'z-index': 50 }, 800, function () {
        $('.banners li').each(function () {
            if ($(this).index() != indice)
                $('.banners li').eq($(this).index()).css({ 'left': left, 'z-index': 100 });
        });

        var v_atual = $('.banners li:eq(' + indice + ')').find('video')[0];

        if (v_atual != undefined) {
            v_atual.play();
        }

        bannerAnterior = indice;
        podeClicar = true;
    });
    podeClicar = false;
}

/*
* Movimento para LÁ: >>>>>>>>
*/
function irBannerEsq() {
    if (!podeClicar)
        return;

    if (indice >= qtdBanners)
        indice = -1;

    indice++;
    marcarBanner($('.bannerBtn span:eq(' + indice + ')'));
    marcadoAnterior = $('.bannerBtn span:eq(' + indice + ')');

    lado = 2;
    trocarBanner();
    iniciarSlidesAuto();
}

/*
* Movimento para CÁ: <<<<<<<<
*/
function irBannerDir() {
    if (!podeClicar)
        return;

    if (indice <= 0)
        indice = qtdBanners + 1;

    indice--;
    marcarBanner($('.bannerBtn span:eq(' + indice + ')'));
    marcadoAnterior = $('.bannerBtn span:eq(' + indice + ')');

    lado = 1;
    trocarBanner();
    iniciarSlidesAuto();
}

/*
 * Função para escrever os vídeos nas LI's dos banners.
 * - @param _mp4 : diretório video MP4
 * - @param _webm : diretório video webm
 * - @param _poster : diretório da imagem da capa
 * - @param _link_url : Link do banner
 * - @param _posterMobile : diretório da imagem da capa (mobile)
*/
function initBanner(_mp4, _webm, _poster, _link_url, _posterMobile) {
    var _video = '', _img = '', _btn_saiba_mais = '';
    var _html_desktop = '', _html_mobile = '', isMobileBanner = $(window).width() <= 640;

    // Monta a tag do vídeo
    _video += '<video autoplay poster="' + _poster + '">';
    _video += '<source src="' + _webm + '" type="video/webm">';
    _video += '<source src="' + _mp4 + '" type="video/mp4">';
    _video += '</video>';

    // Monta o botão "saiba mais"
    _btn_saiba_mais += (_link_url != '') ? '<a href="' + _link_url + '" class="btn-home"><img src="../images/btn-banner-home.png" alt="" /></a>' : '';

    // Monta a imagem do banner (decide se versão desktop ou mobile)
    _img = isMobileBanner ? '<img src="' + _posterMobile + '" style="width:100%;" />' : '<img src="' + _poster + '" style="width:100%;" />';

    // Monta o HTML para versão desktop
    if (_mp4 != "" || _webm != "")
        _html_desktop = ($(window).width() < 768) ? _img : _video;
    else
        _html_desktop = (_link_url != '') ? '<img src="' + _poster + '" class="img-banner" />' : '<img src="' + _poster + '" class="img-banner" />';

    // Monta o HTML para versão mobile
    _html_mobile = (_link_url != '') ? '<a href="' + _link_url + '">' + _img + '</a>' : _img;

    return isMobileBanner ? _html_mobile : _html_desktop + _btn_saiba_mais;
}