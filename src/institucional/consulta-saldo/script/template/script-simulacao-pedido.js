/*
* Ticket - Edenred Brasil
*
* Este javascript cont�m todas as fun��es e
* eventos referentes ao template espec�fico de Simula��o e Pedido
* 
* @section Javascript
*/

/* DECLARA��O DOS OBJETOS */
var formSimulaCAD01 = $('#formSimulaCAD01'),
    formSimulaCAD02 = $('#formSimulaCAD02'),
    formSimulaCAD03 = $('#formSimulaCAD03'),
    formSimulaCAD04 = $('#formSimulaCAD04'),
    formModalContato = $('#formModalContato'),
    formModalEditar = $('#formModalEditar'),
    formNaoCadastrado = $('#formNaoCadastrado'),
    formCadastrado = $('#formCadastrado'),
    formCadastroEmpresa = $('#formCadastroEmpresa'),

    /* BOTOES PASSOS */
    btnContinuarEtapa2 = $('.btnContinuarEtapa2'),
    btnContinuarEtapa3 = $('.btnContinuarEtapa3'),
    btnContinuarEtapa4 = $('.btnContinuarEtapa4'),
    btnContinuarEtapa5 = $('.btnContinuarEtapa5'),
    btnContinuarEtapa6 = $('.btnContinuarEtapa6'),

    /* SUBPASSOS */
    btnContinuarSub2NaoCad = $('.btnContinuarSub2NaoCad'),
    btnContinuarSub2Cad = $('.btnContinuarSub2Cad'),
    btnContinuarSub3 = $('.btnContinuarSub3'),
    btnCadastrarEmpresa = $('.btnCadastrarEmpresa'),

    /* BOTOES MODAIS */
    btnTermoAlimentacaoPAT = $('.btnTermoAlimentacaoPAT'),
    btnTermoRestaurantePAT = $('.btnTermoRestaurantePAT'),
    btnEnviaModalContato = $('.btnEnviaModalContato'),
    btnEnviaModalEditar = $('.btnEnviaModalEditar'),
    modalValorMedio1 = $('.modalValorMedio1'),
    modalValorMedio2 = $('.modalValorMedio2'),
    modalEscolhaTermo = $('.modalEscolhaTermo'),
    modalTermoServico = $('.modalTermoServico'),

    /* BOTOES PASSOS */
    passo1 = $('.bannerSimulacaoPedido .passo1'),
    passo2 = $('.bannerSimulacaoPedido .passo2'),
    passo3 = $('.bannerSimulacaoPedido .passo3'),
    passo4 = $('.bannerSimulacaoPedido .passo4'),
    passo5 = $('.bannerSimulacaoPedido .passo5'),
    chkTermo = $('.chkTermo');

var array_global = [];

$(function () {
    /* Fun��o que trata eventos
    * @param el: Elemento que receber� o evento
    * @param event: Evento a ser tratado
    * @param fn: Fun��o a ser executada durante o evento
    */
    var bind = function (el, event, fn) {
        if (el.addEventListener) {
            el.addEventListener(event, fn, false);
        } else {
            el.attachEvent('on' + event, fn);
        }
    };
    
    // Vari�veis que representam o progresso do usu�rio na simula��o
    var to = 15;
    var containerProgress = document.getElementById('progressPedido'), timer, progress = 0;
    var el = $('.progressoPedido').eq(0);
    var valor = el.children('div');

    /*
    * Fun��o que atualiza o progresso na tela
    * @param to: porcentagem do progresso
    */
    var progressTo = function (to) {
        if (to > parseInt(valor.css('width')) * 100 / parseInt(el.css('width'))) {
            valor.animate({
                'width': to + '%'
            }, 500);
        } else {
            valor.css('width', to + '%');
        }
    };

    /*
    * Fun��o que mostra cada um dos passos na tela
    * @param p: elemento do passo a ser exibido
    * @param passo: n�mero do passo a ser exibido
    */
    var progressoF = function (p, passo) {
        if (p.hasClass('passoAtual') && to === progress) {
            return false;
        }
        else {
            p.addClass('passoAtual');
            exibePasso($('.contentTR'), passo);
            progressTo(to);
        }
    }

    $('.contentTR .containerPasso').hide();
    $('.contentTR .containerSubPasso').hide();
    modalValorMedio2.hide();
    $('.itemSimulacao').hide();

    // Exibe o primeiro passo 
    progressoF(passo1, 1);

    // Exibe o primeiro passo
    passo1.click(function () {
        $('.passos').find('a').removeClass('passoAtual');
        passo1.addClass('passoAtual');
        to = 15;
        progressoF(passo1, 1);
    });

    // Exibe o segundo passo
    btnContinuarEtapa2.click(function () {
        to = 39;
        progressoF(passo2, 2);
    });
    
    // Verifica se o formul�rio foi preenchido e se sim, exibe o terceiro passo
    btnContinuarEtapa3.click(function (e) {
        e.preventDefault();
        to = 62;
        for (n = 0; n < array_global.length; n++) {
            document.getElementsByClassName("item_passo3")[(array_global[n] - 1)].style.display = "block";
        }
        if (formSimulaCAD02.valid() && passo2.hasClass('passoAtual')) {
            progressoF(passo3, 3);
        }
    });

    // Verifica se o formul�rio foi preenchido e se sim, exibe o terceiro passo
    passo3.click(function (e) {
        e.preventDefault();
        to = 62;
        if (formSimulaCAD02.valid() && passo2.hasClass('passoAtual')) {
            progressoF(passo3, 3);
        }
    });

    btnTermoRestaurantePAT.click(function () {
        modalEscolhaTermo.hide();
        modalTermoServico.show();
    });

    btnTermoAlimentacaoPAT.click(function () {
        modalEscolhaTermo.hide();
        modalTermoServico.show();
    });

    btnContinuarEtapa4.click(function () {
        to = 85;
        if (formSimulaCAD03.valid() && passo3.hasClass('passoAtual')) {
            progressoF(passo4, 4);
            exibeSubPasso($('.guardaSubPassos'), 1);
        }
    });
    
    $('.tabelaCustom tbody tr:even').addClass('branco');
    
    $('.cartao-especial').click(function () {
        $('.panel').removeClass('active');

        $(this).addClass('active');
    });

    $('.cartao-normal').click(function () {
        $('.cartao-especial').removeClass('active');
    });

    $('.checkCartao .panel input[type=checkbox]').click(function (e) {
        if ($(this).is(':checked'))
            $(this).parent().parent().addClass('active');
        else
            $(this).parent().parent().removeClass('active');
    });
    
    $('[data-toggle="popover"]').popover({
        trigger: 'hover',
        'placement': 'right'
    });

    $('.modalValorMedio button.btn-success').click(function () {
        modalValorMedio1.hide();
        modalValorMedio2.show();
    });

    $('.modalValorMedio').on('hidden.bs.modal', function () {
        modalValorMedio2.hide();
        modalValorMedio1.show();
    });

    $('.modalTermos').on('show.bs.modal', function (e) {
        modalEscolhaTermo.show();
        modalTermoServico.hide();
    });
    
    formSimulaCAD01.mousemove(function () {
        var qts_panel_ativado = $('#formSimulaCAD01 .active').length;
        if (qts_panel_ativado != 0) {
            $('#formSimulaCAD01 button[type=submit]').removeAttr('disabled');
        } else {
            $('#formSimulaCAD01 button[type=submit]').attr('disabled', 'disabled');
        }
    });

    formSimulaCAD01.find('button[type=submit]').click(function (e) {
        e.preventDefault();
        var qts = $('#formSimulaCAD01 .panel.active input').length;
        var ar = [];

        //Array de todos selecionados
        for (i = 0; i < qts; i++) {
            ar.push($('#formSimulaCAD01 .panel.active input')[i].value);
            array_global = ar;
        }

        for (n = 0; n < ar.length; n++) {
            $('.itemSimulacao' + ar[n]).show();
        }
    });

    formSimulaCAD03.find('.chkTermo').change(function () {
        if ($(this).is(':checked')) {
            $('#formSimulaCAD03 button').removeAttr('disabled');
        } else {
            $('#formSimulaCAD03 button').attr('disabled', 'disabled');
        }
    });

    btnEnviaModalContato.click(function (e) {
        e.preventDefault();
        formModalContato.submit();
    });

    btnEnviaModalEditar.click(function (e) {
        e.preventDefault();
        formModalEditar.submit();
    });
    
    btnContinuarSub2NaoCad.click(function (e) {
        e.preventDefault();
        if(formNaoCadastrado.valid())
            exibeSubPasso($('.guardaSubPassos'), 2);
    });

    btnContinuarSub2Cad.click(function (e) {
        e.preventDefault();
        if (formCadastrado.valid())
            exibeSubPasso($('.guardaSubPassos'), 2);
    });

    btnCadastrarEmpresa.click(function (e) {
        e.preventDefault();
        if (formCadastroEmpresa.valid())
            exibeSubPasso($('.guardaSubPassos'), 3);
    });

    btnContinuarSub3.click(function (e) {
        e.preventDefault();
        if (formCadastroEmpresa.valid())
            exibeSubPasso($('.guardaSubPassos'), 3);
    });

    btnContinuarEtapa5.click(function (e) {
        e.preventDefault();
        if (formSimulaCAD04.valid()) {
            to = 100;
            progressoF(passo5, 5);
            exibeSubPasso($('.ultimoPasso'), 1);
        }
    });    

    btnContinuarEtapa6.click(function (e) {
        e.preventDefault();
        exibeSubPasso($('.ultimoPasso'), 2);
    });

    $("#formSimulaCAD01 .panel").click(function () {
        if ($(this).hasClass("active")) {
            $(this).removeClass("active");
        }
        else {
            $(this).addClass("active");
        }
        return false;
    });

    $("#formSimulaCAD01 .cartao-especial").click(function () {
        $("#formSimulaCAD01 .panel").removeClass("active");
        $(this).addClass("active");
        return false;
    });

    $(".passo1").click(function () {
        $("#formSimulaCAD02 .itemSimulacao").hide();
    });

    $('input[type="radio"]').click(function () {
        if ($(this).attr("value") == "PJ") {
            $("#cadastroJuridica").toggle();
            $("#cadastroFisica").hide();
        }
        if ($(this).attr("value") == "PF") {
            $("#cadastroFisica").toggle();
            $("#cadastroJuridica").hide();
        }
    });
});

/*
* Fun��o que remove um item da listagem de cart�es
* @param item: Item a ser removido
*/
function removeItem(item){
    var resposta = confirm("Tem certeza que deseja remover esse item?");
    if (resposta) {
        item.hide(500);
    } else {
        return;
    }
}