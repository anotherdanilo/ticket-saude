﻿/*
* Ticket - Edenred Brasil
*
* Este javascript contém todas as funções e
* eventos referentes ao template específico da área Canais de Atendimento
* 
* @section Javascript
*/

/*O próprio modal não acompanha o collapse, esta função vai fazer com que o Modal acompanhe o conteúdo do collapse*/
/*template 02*/
var ids_collapse = "#collapseEstabelecimento, #collapseUsuario, #collapseRH, #collapseGestao";
$(ids_collapse).on('shown.bs.collapse', function () {
    $("#modalTelefone .modal-backdrop").css("height", document.body.offsetHeight + "px");
});

$(ids_collapse).on('hidden.bs.collapse', function () {
    $("#modalTelefone .modal-backdrop").css("height", document.body.offsetHeight + "px");
});