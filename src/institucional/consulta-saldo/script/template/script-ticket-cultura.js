﻿/*
* Ticket - Edenred Brasil
*
* Este javascript contém todas as funções e
* eventos referentes ao template específico de Ticket Cultura
* 
* @section Javascript
*/

var height = $(window).height(),
    formTicketCultura01 = $('.simulacaoCartao #formTicketCultura01'),
    formTicketCultura02 = $('.simulacaoCartao #formTicketCultura02')
    btnContinuarEtapa2 = $('.simulacaoCartao #btnContinuarEtapa2')
    btnContinuarEtapa3 = $('.simulacaoCartao #btnContinuarEtapa3'),
    cartaoPassosRadios = $('.simulacaoCartao .thumbnailPassos'),
    voltarEtapas = $('.simulacaoCartao #voltarEtapas'),
    passo1 = $('.simulacaoCartao .passo1'),
    passo2 = $('.simulacaoCartao .passo2'),
    passo3 = $('.simulacaoCartao .passo3'),
    dataF = '';

$('.simulacaoCartao .containerPasso').hide();

var to = 33;
var containerProgressS = document.getElementById('progressSimulacaoAzul'), timer, progress = 0;
var elS = $('.progresso').eq(0);
var valor = elS.children('div');
var cartaoPassos = $.map(cartaoPassosRadios, function (e, i) { return $(e).attr('name'); }).join(' ');

var progressToSimulacao = function (to) {
    if (to > parseInt(valor.css('width')) * 100 / parseInt(elS.css('width'))) {
        valor.animate({
            'width': to + '%'
        }, 500);
    } else {
        valor.css('width', to + '%');
    }
};

var progressoFS = function (p, passo) {
    if (p.hasClass('passoAtual') && to === progress) {
        return false;
    }
    else {
        p.addClass('passoAtual');
        exibePasso($('.simulacaoCartao'), passo);
        progressToSimulacao(to);
    }
}

to = 29;
progressoFS(passo1, 1);
passo1.addClass('passoAtual');

/* IR PASSO 2 */
btnContinuarEtapa2.click(function (e) {
    e.preventDefault();
    if (formTicketCultura01.valid()) {
        to = 50;
        progressoFS(passo2, 2);
    }
});

passo2.click(function (e) {
    e.preventDefault();
    if (formTicketCultura01.valid()) {
        to = 50;
        progressoFS(passo2, 2);
    }
});

/* IR PASSO 3 */
btnContinuarEtapa3.click(function (e) {
    e.preventDefault();
    if (formTicketCultura02.valid()) {
        to = 100;
        progressoFS(passo3, 3);
    }
});

passo3.click(function (e) {
    e.preventDefault();
    if (formTicketCultura02.valid() && passo2.hasClass('passoAtual')) {
        to = 100;
        progressoFS(passo3, 3);
    }
});

/* VOLTAR PARA PASSO 1  */
voltarEtapas.click(function (e) {
    e.preventDefault();
    to = 29;
    $('.simulacaoCartao .passos').find('a').removeClass('passoAtual');
    passo1.addClass('passoAtual');
    progressoFS(passo1, 1);
});