/*
* Ticket - Edenred Brasil
*
* Este javascript contém funções e eventos
* reutilizáveis em mais de um template
* 
* @section Javascript
*/

/* VARIÁVEIS GLOBAIS
******************************************************************************** */
var widthJanela = $(window).width();
var menuMobileFechado = true;

/* EVENTO DISPARADO APÓS O CARREGAMENTO DA PÁGINA
******************************************************************************** */
$(window).load(function () {
    // Cufon - fontes
    Cufon.replace('.banner strong, .list-inline li > label, .containerPasso span, .containerPasso p, .tituloMedio, .labelCufon label', { hover: true, fontFamily: 'Novel Sans Pro Light' });
    Cufon.replace('.banner h1, .banner .conteudoBanner > p, .guardaTitulo p, .tblPAT, .tblPATheader, .item-menu-normal, .tituloNews, .boxContent .texto, .menu-faixa-azul li a span, .textoCufon', { hover: true, fontFamily: 'Novel Sans Pro' });
    Cufon.replace('h2, h3, h4, .btn-lg, .subtitulo, .assistente-f h4, legend, .tabelaCustom th, .containerPassoDesc p, .containerPasso strong, ul.abas > li, .item-menu, .economiaPAT, .labelCufon button[type="submit"], .buttonCufon, .cufonExtraBold, .bigVirtualAssis h5', { hover: true, fontFamily: 'Novel Sans Pro ExtraBold' });

    // Altura dos thumbnails 
    if (widthJanela > 767) {
        var alturaContentBoxes = $(".contentBoxes").height();
        $(".contentBoxes .thumbnail").css("height", (alturaContentBoxes + 30) + "px");
    }

    // Faz scroll na tela com base no hash da URL
    if (location.hash != '') {
        $('a[href=' + location.hash + ']').trigger('click');
    }
});

/* EVENTO DISPARADO APÓS REDIMENSIONAR A PÁGINA
******************************************************************************** */
window.addEventListener("resize", function () {
    // Define a visibilidade para o fundo escurecido do menu
    var mostrarFundoMenu = $(window).width() >= 1024 && $('.navbar-collapse').css('display') == 'block';
    $('.fundo-escuro').css('display', mostrarFundoMenu ? 'none' : 'none');
}, false);

jQuery(window).bind('resize', (function () {
    // Atualiza o tamanho das fonts renderizadas pelo Cufon
    var timer;
    return function () {
        clearTimeout(timer);
        timer = setTimeout(function () {
            Cufon.refresh();
        }, 50);
    };
})());

/* EVENTO DISPARADO AO DESCER/SUBIR A PÁGINA
******************************************************************************** */
$(window).scroll(function () {
    // Ajusta o menu do topo ao tamanho da tela
    var heightScroll = $(window).scrollTop();
    if (heightScroll < 150 && widthJanela < 1024) {
        $('.navbar-collapse').css({ maxHeight: $(window).height() - $('.navbar-header').height() - $('.navbar-toggle-f').height() + "px" });
    }
});

/* EVENTO DISPARADO AO ACESSAR A PÁGINA
******************************************************************************** */
$(function () {
    // Aplica o plugin de paginação das tabelas
    if ($.fn.jPages) {
        $('.containerPaginadorTabela').jPages({
            containerName: '.tableResponsive .paginavel',
            perPage: 1,
            keyBrowse: false,
            scrollBrowse: false
        });
    }

    // Coloca opacidade nos campos
    $('input:radio').css('opacity', 0);

    // Radio button
    $('input:radio').each(function () {
        $("input:radio").change(function () {
            $("input:radio[name=" + $(this).attr('name') + "]").next('span').removeClass('marcado');
            $(this).next('span').addClass('marcado');
        });
    });

    // Preenche descrição upload
    $(".upload").change(function () {
        var arquivo = $(this).val().split('\\').pop();
        $(".descricaoUpload").text(arquivo);
    });

    // Aplica a função do tooltip
    $('[data-toggle="tooltip"]').tooltip();

    // Exibe o tooltip ao clicar no ícone
    $('.ci-tooltip-info').click(function () {
        var item = $(this).parent().find('.tooltipProposta')
        item.fadeIn();
        setTimeout(function () { item.fadeOut(); }, 5000);
    });

    // Exibe abas
    $('.contentBox > .box').siblings().hide();
    $('.contentBox > .box').eq(0).show();
    $('ul.abas > li').click(function () {
        i = $(this).index();
        $('.contentBox').find('.box').eq(i).siblings().hide();
        $('.contentBox').find('.box').eq(i).fadeIn();
        $(this).addClass('selected');
        $(this).siblings().removeClass('selected');
    });

    // Aplica o hover no cufon caso o aparelho seja mobile
    if (widthJanela < 768) {
        Cufon.replace('.submenu-perfil-f', { hover: true, fontFamily: 'Novel Sans Pro' });
    }

    // Aplica o hover no cufon caso o aparelho seja desltop
    if (widthJanela >= 1024) {
        Cufon.replace('.submenu-perfil-f', { hover: true, fontFamily: 'Novel Sans Pro' });
    }

    // Botão toggle
    $('.caret-click-f').on('touchstart click',
        function () {
            $(this).next('.caret-f').toggleClass('caret-aberto-f');
        }
    );

    // Troca valor do input
    $('#form-perfil-usuario-f').change(function () {
        if (this.value) {
            $(this).prev('.texto-input-f').htm(this.value);
        }
    });

    // Função para fazer animate no scrol (links)
    $('.anchor').click(function () {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');

            var diferencaOffset = 100;
            if ($(window).width() <= 764)
                diferencaOffset = 0;

            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top - diferencaOffset
                }, 1000);
                return false;
            }
        }
    });
    
    // Função para fazer animate no scroll (menu com ícones)
    // Necessário o uso da classe "scrollAnimate" para não dar conflito na função de ".anchor"
    $('.menuInterna.scrollAnimate li a, .listaBotoes.scrollAnimate li a').click(function () {
        addScrollAnimate(this);
    });
    $('.menuInterna.scrollAnimateMobile li a, .listaBotoes.scrollAnimateMobile li a').click(function () {
        if (widthJanela < 768) {
            addScrollAnimate(this);
        }
    });

    // Toggle do botão de "Simule a economia"
    $('.btnSimuleE').click(function () {
        $('.simulacaoCartao').slideToggle(500);
        if ($(this).text() == 'SIMULE A ECONOMIA') {
            $(this).toggleClass('btn-warning');
            $(this).text('FECHAR SIMULAÇÃO');
        } else if ($(this).text() == 'FECHAR SIMULAÇÃO') {
            $(this).toggleClass('btn-warning');
            $(this).text('SIMULE A ECONOMIA');
        }
    });

    // Abre a div com a notícia
    $('.paginavel li').click(function () {
        $(this).parents('.contentBoxBege, .contItems').addClass('noPadding-t');
        $(this).parents('.contentBoxBege, .contItems').find('.listagemNoticiaPrincipal, .guardaPaginacaoContainer').hide();
        $(this).parents('.contentBoxBege, .contItems').find('.itemAbre').fadeIn();
    });

    // Fecha a div com a notícia
    $('.btnVoltarNoticia').click(function () {
        $(this).parents('.contentBoxBege, .contItems').removeClass('noPadding-t');
        $(this).parents('.contentBoxBege, .contItems').find('.itemAbre').hide();
        $(this).parents('.contentBoxBege, .contItems').find('.listagemNoticiaPrincipal, .guardaPaginacaoContainer').fadeIn();
    });

    // Aplica o plugin do datepicker
    if ($.fn.daterangepicker) {
        $('.datePicker').daterangepicker({
            timePicker: true,
            timePickerIncrement: 30,
            format: 'DD/MM/YYYY'
        });
        $('.datepicker').prop('readonly', true);
    }

    // Previne que a página dê post nos formulários de busca, onde não há validação
    $('.formBusca').submit(function (e) {
        e.preventDefault();
    });

    // Script que marca os itens de download de poses
    $(".poses .item").click(function (event) {
        $(this).toggleClass('active');
        $(this).find("input[type=checkbox]").prop("checked", $(this).find('input[type=checkbox]').is(':checked') ? false : true);

        return false;
    });

    // Exibe/esconde itens do formulário de login
    $("input[name=perfil]").on("click", function () {
        $('#loginPerfil .elem.usuario').css('display', 'block');
        $('#loginPerfil .elem.usuario .lblSelect').css('display', 'none');

        // Reseta os campos ao trocar de perfil
        trocarTipoUsuario('nenhum');
        $('#loginPerfil .elem.usuario select').css('display', 'none');
        $('#loginPerfil .elem.usuario select').each(function () {
            $(this).prop('selectedIndex', 0);
            $(this).change();
        });

        switch ($(this).val()) {
            case "Empregador/Gestor":
                $('#loginPerfil .elem.usuario .empregador').css('display', 'block');
                $('#loginPerfil .elem.usuario .empregador select').css('display', 'block');
                break;
            case "Comerciante":
                $('#loginPerfil .elem.usuario .comerciante').css('display', 'block');
                $('#loginPerfil .elem.usuario .comerciante select').css('display', 'block');
                break;
        }
    });

    $("#loginPerfil .empregador select").change(function () { trocarCamposLogin($(this).val()); });
    $("#loginPerfil .comerciante select").change(function () { trocarCamposLogin($(this).val()); });

    // Select personalizado
    $('.form-select-f').change(function () {
        if (this.value) {
            $(this).parent().find('.texto-input-f').htm('');
            $(this).parent().find('.texto-input-f').htm($(this).children("option").filter(":selected").text());
            Cufon.replace('.texto-input-f', { hover: true, fontFamily: 'Novel Sans Pro' });
            Cufon.refresh();
        }
    });

    // Script responsável por retirar a animação ao abrir o submenu na versão desktop
    $('.dropdown').on('show.bs.dropdown', function (e) {
        var alturaMenu;
        $(this).find('.dropdown-menu').first().stop(true, true).css('display', 'block');
        //seta dinamicamente a altura
        alturaMenu = $(this).find('.flyout').height();
        $(this).children('.height-submenu').css('height', alturaMenu + 20);
    });
    $('.dropdown').on('hide.bs.dropdown', function (e) {
        $(this).find('.dropdown-menu').first().stop(true, true).css('display', 'none');
        $(this).children('.height-submenu').css('height', 0);
    });

    // Script responsável por abrir o menu na versão para iPad e Mobile
    $('#menuSuperiorMobile').click(function () {
        if (menuMobileFechado) {
            if (widthJanela >= 481) {
                $('.navbar-collapse-f').css('height', parseInt($(window).height() - 100) + 'px');
            }

            $('.fundo-escuro').fadeIn(500);
            $('.navbar-collapse-f').slideDown(500);
            menuMobileFechado = false;
        }
        else {
            $('.navbar-collapse-f').css('height', '100%');

            $('.fundo-escuro').fadeOut(500);
            $('.navbar-collapse-f').slideUp(500);
            menuMobileFechado = true;
        }
    });

    // Script para abrir e fechar o box de login
    $('#abrirLogin').click(function () { $('#loginPerfil').slideToggle(); });

    // Script para abrir e fechar a busca do topo
    var podeMostrar = true;

    // Busca do topo (versão desktop)
    $(".btn-busca-f").click(function () {
        if (podeMostrar) { mostrarBusca(); podeMostrar = false; }
        else { esconderBusca(); podeMostrar = true; }
    });

    // Função para fixar o menu no topo quando scrollar a página
    $(window).scroll(function () {
        $('.navbar-collapse-f').css('maxHeight', '');
        if (widthJanela < 1024) {
            return;
        }

        if ($(window).scrollTop() > $('.navbar-default').outerHeight()) {
            $(".navbar-default").css({ 'position': 'fixed', 'top': '0', 'width': '100%' });
            $("body").css({ 'padding-top': $(".navbar-default").outerHeight() });
        } else if ($(window).scrollTop() < $('.navbar-default').outerHeight()) {
            $(".navbar-default").css({ 'position': 'relative' });
            $("body").css({ 'padding-top': '0' });
        }
    });

    // Faz o conteudo descer ao abrir o Menu Superior
    $('.dropdown').hover(
        function () { $('.ie8 .menuFake').css('height', $(this).find('.dropdown-menu-f').outerHeight() + 'px'); },
        function () { $('.ie8 .menuFake').css('height', '0px'); }
    );

    // Aplica o carousel nos 3 casos possíveis
    carousel($('.carouselAzul'), true, 3, 'icon-vantagens', true, true, [[0, 1], [400, 1], [650, 2], [768, 2], [800, 3]]);
    carousel($('.carouselPadraoNav'), true, 1, 'icon-singleItem', false, true);
    carousel($('.carouselPadrao'), true, 1, 'icon-singleItem', true, true);
    atualizaItens(0);

    // Chama a função que faz a simulação de PAT funcionar
    if ($('#progressSimulacao').length > 0)
        simulaCadastroPAT();

    // Esconde o rodape quando o teclado estiver visivel no tablet (algum campo com focus)
    if (isAndroid()) {
        $('input').on('focus', function () {
            $('.footer').css('display', 'none');
        });
        $('input').blur(function () {
            $('.footer').css('display', 'block');
        });
    }

    // Reposiciona o modal no centro da tela
    $('.modal').on('show.bs.modal', reposition);

    // Exibe o formulário do banner
    $('.btnSimuleCompre, .btnSoliciteContato, .btnFaltouAlgo').click(function () {
        $('.containerSimuleCompre').find('.boxSimulador').stop().slideToggle(300);
    });
    $('.btnSoliciteContato, .btnFaltouAlgo').click(function () {
        $('.containerSoliciteContato').find('.boxSimulador').stop().slideToggle(300);
    });
    $('.btnDiferenciais').click(function () {
        $('.containerSoliciteContato').find('.boxSimulador.diferenciais').stop().slideToggle(300);
    });

    // Define a visibilidade dos campos no formulário de SIMULE E COMPRE conforme o click entre "Ticket Restaurante Eletronico" ou "Voucher"
    $('.boxSimulador .listaForm > div').hide();
    $('.boxSimulador .listaForm > div').eq(0).show();
    $('.boxSimulador .selecionaList li label, .boxSimulador .selecionaList li input').click(function () {
        var indiceAba = $(this).closest('li').index();

        $('.boxSimulador .listaForm > div').eq(indiceAba).show();
        $('.boxSimulador .listaForm > div').eq(indiceAba).siblings().hide();
    });

    // Verifica se o dispositivo não é android e aplica as máscaras
    if (!isAndroid() && $.fn.mask) {
        aplicaMascaras();
    }

    // Faz o efeito com FOCUS e BLUR quando o navegador não tiver suporte para o placeholder
    if (!$.support.placeholder) {
        $("[placeholder]").focus(function () {
            if ($(this).val() == $(this).attr("placeholder"))
                $(this).val("");
        }).blur(function () {
            if ($(this).val() == "")
                $(this).val($(this).attr("placeholder"));
        }).blur();

        $("[placeholder]").parents("form").submit(function () {
            $(this).find('[placeholder]').each(function () {
                if ($(this).val() == $(this).attr("placeholder")) {
                    $(this).val("");
                }
            });
        });
    }

    /* ---------- SOBRE A TICKET ---------- */
    var owl = $("#owl-historias").data('owlCarousel');
    var anteriorQtde;

    $('.itensLinhaTempo li a').click(function () {
        anteriorQtde = $(this).parent().index();
        $(this).parent().addClass('active');
        owl.goTo(anteriorQtde);
        atualizaItens(anteriorQtde);
    });

    $('.enviarProjeto').click(function () {
        $('#projeto').toggleClass('hidden');
    });


    /* ---------- SEJA UM CREDENCIADO ---------- */
    var formCredenciamento = $('.formCredenciamento'),
        formFinalizar = $('.formFinalizar');

    $('#iniciarCredenciamento').on('touchstart click',
        function () {
            if (formCredenciamento.valid()) {
                irParaProximo($(this));
            }
        }
    );

    $('button[data-nextstep="2"]').on('touchstart click',
        function () {
            irParaProximo($(this));
        }
    );

    $('button[data-nextstep="3"]').on('touchstart click',
        function () {
            if ($('#step2').valid()) {
                irParaProximo($(this));
            }
        }
    );

    $('button[data-nextstep="4"]').on('touchstart click',
        function () {
            if ($('#step3').valid()) {
                irParaProximo($(this));
            }
        }
    );

    $('button[data-nextstep="5"]').on('touchstart click',
        function () {
            if ($('#step4').valid()) {
                irParaProximo($(this));
            }
        }
    );

    $('button[data-nextstep="6"]').on('touchstart click',
        function () {
            if ($('#step5').valid()) {
                irParaProximo($(this));
            }
        }
    );

    $('button.voltar').on('touchstart click',
        function () {
            irParaAnterior($(this));
        }
    );

    /* ---------- PAGINAÇÃO ---------- */
    if ($.fn.jPages) {
        // Sobre a ticket
        instanciaPaginadorGlobal(6, 'salaImprensa');

        // Canal de notícias
        $('#informativoPaginador').jPages({
            containerName: '#informativoPaginavel',
            perPage: 4,
            keyBrowse: false,
            scrollBrowse: false
        });

        $('#noticiaPaginador').jPages({
            containerName: '#noticiaPaginavel',
            perPage: 8,
            keyBrowse: false,
            scrollBrowse: false
        });

        $('#rotinaPaginador').jPages({
            containerName: '#rotinaPaginavel',
            perPage: 8,
            keyBrowse: false,
            scrollBrowse: false
        });
    }

    //Alternar de "Download APP" para "Consulta rápida"
    $("#consultaRapida").click(function () {
        //Template-01
        $(".SimuladorHome").show('slow');
        $(".SimuladorHome").removeClass("hidden-xs");
        $(".SimuladorHome").removeClass("hidden-md");
        $(".download-app").fadeOut('slow');

        //Template-19e20
        $("#SaldoLogin #DownloadApp19").fadeOut('slow');
        $("#SaldoLogin .NotDownload-app").show('slow');
        $("#SaldoLogin .NotDownload-app").removeClass("hidden-xs");
        $("#SaldoLogin .NotDownload-app").removeClass("hidden-sm");
        $("#SaldoLogin .bracket-f-temp19-app").addClass("bracket-f-temp19");
        $("#SaldoLogin .bracket-f-temp19").removeClass("bracket-f-temp19-app");        
    });
        
});

/*
* Função para fazer animação no scroll
* @param obj: Objeto que recebeu o click
*/
function addScrollAnimate(obj, destino) {
    if (location.pathname.replace(/^\//, '') == obj.pathname.replace(/^\//, '') && location.hostname == obj.hostname) {
        location.hash = $(obj).attr('href');
        
        var target;
        target = $(obj.hash).parent().parent().find('.tab-content.clearfix');

        var tempo = parseInt(target.offset().top - $(window).scrollTop() - $('.navbar-default').outerHeight()) * 2;
        if (tempo > 1500)
            tempo = 1500;

        var alturaNavbar = (isGadget() && widthJanela < 768) ? 0 : $('.navbar-default').outerHeight();

        $('html,body').animate({
            scrollTop: target.offset().top - alturaNavbar + 1
        }, tempo);
    }
}

/*
* Verifica se o dispositivo do usuário é um dispositivo móvel
*/
function isGadget() {
    return /android|webos|iphone|ipad|ipod|blackberry|iemobile|opera mini/i.test(navigator.userAgent.toLowerCase());
}

/*
* Verifica se o dispositivo do usuário possui o sistema operacional Android
*/
function isAndroid() {
    return /android/i.test(navigator.userAgent.toLowerCase());
}

/* 
* Função que mostra o formulário de busca
*/
function mostrarBusca() {
    $('.form-group-f').find('input').focus();
    $('.form-group-f').removeClass('escondido');
}

/* 
* Função que esconde o formulário de busca
*/
function esconderBusca() {
    $('.form-group-f').addClass('escondido');
}

/*
* Função que verifica se o navegador oferece suporte ao uso de placeholders (Função anônima)
*/
(function ($) {
    $.support.placeholder = ('placeholder' in document.createElement('input'));
})(jQuery);

/*
* Reposiciona o modal padrão do bootstrap no centro da tela
*/
function reposition() {
    var modal = $(this),
        dialog = modal.find('.modal-dialog');
    modal.css('display', 'block');
    dialog.css("margin-top", Math.max(0, ($(window).height() - dialog.height()) / 2));
}

/*
* Função para levar para próxima tela
* @param e: Próximo elemento
*/
function irParaProximo(e) {
    var next = e.attr('data-nextStep');
    if (next != null) {
        $(window).scrollTop(0);
        $('#step' + (next - 1)).hide();
        $('#step' + (next)).show();
    }
}

/*
* Função para levar para tela anterior
* @param e: Elemento anterior
*/
function irParaAnterior(e) {
    var prev = e.attr('data-prevStep');
    if (prev != null) {
        $(window).scrollTop(0);

        $('#step' + (prev * 1 + 1)).hide();
        $('#step' + (prev)).show();
    }
}

/*
* Função que aplica máscaras nos campos conforme classe
*/
function aplicaMascaras() {
    // Telefone
    $('.txtTelefone').mask('(99) 9999-9999', { placeholder: ' ' });
    // Celular
    $('.txtCelular').mask('(99) 9?9999-9999', { placeholder: ' ' });
    // Celular sem DDD
    $('.txtCelularNoDDD').mask('9?9999-9999', { placeholder: ' ' });
    // DDD
    $('.txtDDD').mask("(99)", { placeholder: ' ' });
    // CNPJ
    $('.txtCNPJ').mask('999.999.999/9999-99', { placeholder: ' ' });
    // CPF
    $('.txtCPF').mask('999.999.999-99', { placeholder: ' ' });
    // CPF ou CNPJ
    $('.txtCPFeCNPJ').mask('999.999.999-99?99-99', { placeholder: ' ' });
    // RG
    $('.txtRG').mask('99.999.999-9', { placeholder: ' ' });
    // CEP
    $('.txtCEP').mask('99999-999', { placeholder: ' ' });
    // Ano
    $('.txtAno').mask('99?99', { placeholder: ' ' });
    // Dias úteis
    $('.txtDiasUteis').mask("9?9", { placeholder: " " });
    // Número (Endereço)
    $('.txtNumero').mask('9?99999', { placeholder: ' ' });
    // Data
    $(".txtData").mask("99/99/9999", { placeholder: ' ' });
    // Número do cartão
    $('.txtCartao').mask('9999 9999 9999 9999', { placeholder: ' ' });
    // Intervalo de datas
    $('.txtIntervaloData').mask('99/99/9999 - 99/99/9999', { placeholder: ' ' });
    // Monetário
    if ($.fn.maskMoney) {
        $(".txtValor").maskMoney({ prefix: 'R$ ', allowNegative: true, thousands: '.', decimal: ',', affixesStay: false });
        $(".txtValorNoPrefix").maskMoney({ prefix: '', allowNegative: true, thousands: '.', decimal: ',', affixesStay: false });
    }
}

/*
* Função atualiza os bullets da linha do tempo
*  - @param item : int : Item que deve ser marcado
*/
function atualizaItens(item) {
    $('.itensLinhaTempo li').each(function () {
        if ($(this).index() <= item) {
            $(this).addClass('active');
        } else {
            $(this).removeClass('active');
        }

        if ($(this).index() == item) {
            $('.progressoLinhaTempo div').css('width', ($(this).offset().left - $('.progressoLinhaTempo').offset().left) + 1);
        }
    });
}

/*
* Função que mostra/esconde os campos que são exibidos no login
* - @param cliente : string : Tipo do cliente que foi selcionado pelo usuário
*/
function trocarCamposLogin(cliente) {
    switch (cliente) {
        case "CLIENTE-TICKET":
            trocarTipoUsuario('perfil-cliente-ticket');
            break;
        case "TICKETCAR":
            trocarTipoUsuario('perfil-ticketcar');
            break;
        case "TICKETCARMANUTENCAO":
            trocarTipoUsuario('perfil-ticketcar-manut');
            break;
        case "E-TICKET":
            trocarTipoUsuario('perfil-e-ticket');
            break;
    }
}

/*
* Função que adiciona as classes necessárias para exibir os itens conforme o tipo de cliente
*  - @param classe : string : Classe que deve ser adicionada ao elemento #loginPerfil
*/
function trocarTipoUsuario(classe) {
    $('#loginPerfil').attr('class', '');
    $('#loginPerfil').addClass(classe);
}

/*
* Exibe passo atual
*  - @param pai : obj : Elemento que guarda os itens que serão escondidos/exibidos
*  - @param atual : string : Valor do atributo "data-passo" do item que deve ser exibido
*/
function exibePasso(pai, atual) {
    pai.find('.containerPasso').hide();
    pai.find('.containerPasso[data-passo=' + atual + ']').show();
}

/*
* Exibe subpasso do passo atual
*  - @param pai : obj : Elemento que guarda os itens que serão escondidos/exibidos
*  - @param atual : string : Valor do atributo "data-sub-passo" do item que deve ser exibido
*/
function exibeSubPasso(pai, atual) {
    pai.find('.containerSubPasso').hide();
    pai.find('.containerSubPasso[data-sub-passo=' + atual + ']').show();
}

/*
* Função para validar os campos de formulário
*  - @param idForm : ID do formulário que será aplicado a validação
*  - @param json : Json com as propriedades de validação
*  - @param groups : Json com grupos de campos a serem validados
*/
function validarCampos(formulario, json, groups, submitHandler) {
    $(formulario).validate({
        groups: groups,
        rules: json,
        highlight: function (element) {
            $(element).closest('div').addClass('has-error');
        },
        unhighlight: function (element) {
            $(element).closest('div').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function (error, element) {
            if (element.parent('.input-group').length) {
                element.parent().parent().find('.help-block').remove();
                error.insertAfter(element.parent());
            } else if (element.parent('.checkbox-f').length) {
                element.parent().parent().parent().parent().find('.help-block').remove();
                error.insertAfter(element.parent().parent().parent());
            } else {
                element.parent().find('.help-block').remove();
                error.insertAfter(element);
            }
        },
        submitHandler: submitHandler
    });
}

/*
* Função que aplica carousel
*  - @param carousel : obj: Objeto em que o carousel será aplicado
*  - @param navigation : bool: Define se o carousel terá navegação lateral
*  - @param itens : int: Número de itens que aparecerão por vez
*  - @param icone : string: Classe que define a cor dos ícones da navegação lateral
*  - @param autoPlay : bool: Define se os itens irão mudar automaticamente
*  - @param pagination : bool: Define se haverá navegação em baixo (bullets)
*  - @param itensCustom : json: Objeto que define cada um dos itens do carousel
*/
function carousel(carousel, navigation, itens, icone, autoPlay, pagination, itensCustom) {
    if (carousel.length > 0) {
        carousel.owlCarousel({
            items: itens,
            itemsCustom: itensCustom,
            itemsDesktop: [1000, 1],
            itemsDesktopSmall: [900, 1],
            itemsTablet: [767, 1],
            itemsMobile: false,
            slideSpeed: 1000,
            autoPlay: 10000,
            navigation: navigation,
            pagination: pagination,
            autoPlay: autoPlay,
            afterMove: function (e) {
                // Verifica se existe um carrossel com essa classe, para que não conflite com outros carrosséis da página
                if ($('.guardaLinhaTempo').length > 0) {
                    setTimeout(function () {
                        var atual = Math.ceil(Math.abs(parseInt(e.find('.owl-wrapper').css('left')) / parseInt(e.find('.item').eq(0).css('width')))) - 1;
                        if (atual == -1)
                            atual = 0;
                        $('.itensLinhaTempo li').removeClass('active')
                        $('.itensLinhaTempo li').eq(atual).addClass('active');
                        atualizaItens(atual);
                    }, 1000);
                }
            },
            navigationText: [
                "<span class='icon-chevron-left " + icone + "-left'></span>",
                "<span class='icon-chevron-right " + icone + "-right'></span>"
            ]
        });
    }
}

/*
* Função que faz o formulário de simulação do cadastro PAT funcionar
*/
function simulaCadastroPAT() {
    var height = $(window).height(),
        formSimulaPAT01 = $('.simulacaoCartao #formSimulaPATPasso01'),
        formSimulaPAT02 = $('.simulacaoCartao #formSimulaPATPasso02'),
        formSimulaPAT03 = $('.simulacaoCartao #formSimulaPATPasso03'),
        btnSimulaPAT = $('.simulacaoCartao #btnSimulaPAT'),
        btnContinuarEtapa3 = $('.simulacaoCartao #btnContinuarEtapa3'),
        btnContinuarEtapa4 = $('.simulacaoCartao #btnContinuarEtapa4'),
        cartaoPassosRadios = $('.simulacaoCartao .thumbnailPassos'),
        voltarEtapas = $('.simulacaoCartao #voltarEtapas'),
        passo1 = $('.simulacaoCartao .passo1'),
        passo2 = $('.simulacaoCartao .passo2'),
        passo3 = $('.simulacaoCartao .passo3'),
        passo4 = $('.simulacaoCartao .passo4'),
        dataF = '';

    $('.simulacaoCartao .containerPasso').hide();

    var to = 20;
    var containerProgressS = document.getElementById('progressSimulacao'), timer, progress = 0;
    var elS = $('.progresso').eq(0);
    var valor = elS.children('div');
    var cartaoPassos = $.map(cartaoPassosRadios, function (e, i) { return $(e).attr('name'); }).join(' ');

    var progressToSimulacao = function (to) {
        if (to > parseInt(valor.css('width')) * 100 / parseInt(elS.css('width'))) {
            valor.animate({
                'width': to + '%'
            }, 500);
        } else {
            valor.css('width', to + '%');
        }
    };

    var progressoFS = function (p, passo) {
        if (p.hasClass('passoAtual') && to === progress) {
            return false;
        }
        else {
            p.addClass('passoAtual');
            exibePasso($('.simulacaoCartao'), passo);
            progressToSimulacao(to);
        }
    }

    to = 20;
    progressoFS(passo1, 1);
    passo1.addClass('passoAtual');

    /* IR PASSO 2 */
    cartaoPassosRadios.click(function (e) {
        e.preventDefault();
        to = 50;
        var cartaoClicado = $(this).children().find('.check');
        cartaoClicado.attr('checked', 'checked').button('refresh');
        proximoCartao(cartaoClicado);
        if (formSimulaPAT01.valid()) {
            progressoFS(passo2, 2);
        }
    });

    passo2.click(function (e) {
        e.preventDefault();
        to = 50;
        var cartaoClicado = $(this).children().find('.cartaoPassos');
        cartaoClicado.attr('checked', 'checked').button('refresh');
        proximoCartao(cartaoClicado);
        if (formSimulaPAT01.valid()) {
            progressoFS(passo2, 2);
        }
    });

    /* IR PASSO 3 */
    btnContinuarEtapa3.click(function (e) {
        e.preventDefault();
        to = 80;
        if (formSimulaPAT02.valid()) {
            progressoFS(passo3, 3);
        }
    });

    passo3.click(function (e) {
        e.preventDefault();
        to = 80;
        if (formSimulaPAT02.valid() && passo2.hasClass('passoAtual')) {
            progressoFS(passo3, 3);
        }
    });

    /* IR PASSO 4 */
    btnContinuarEtapa4.click(function (e) {
        e.preventDefault();
        to = 100;
        if (formSimulaPAT03.valid()) {
            progressoFS(passo4, 4);
        }
    });

    passo4.click(function (e) {
        e.preventDefault();
        to = 100;
        if (formSimulaPAT03.valid() && passo3.hasClass('passoAtual')) {
            progressoFS(passo4, 4);
        }
    });

    /* VOLTAR PARA PASSO 1  */
    voltarEtapas.click(function (e) {
        e.preventDefault();
        to = 20;
        $('.simulacaoCartao .passos').find('a').removeClass('passoAtual');
        passo1.addClass('passoAtual');
        progressoFS(passo1, 1);
    });
}

/*
* Função que "salva" o cartão selecionado pelo usuário
*/
function proximoCartao(botao) {
    dataF = botao.data('cartao');
    var novoCartao = 'images/cartoes/cartao-ticket-' + dataF + '-pat.png';
    $('.simulacaoCartao div[data-passo="2"]').find("img").attr("src", novoCartao);
    $('.simulacaoCartao div[data-passo="3"] .imgCartao').find("img").attr("src", novoCartao);
}

/*
* Função para permitir a digitação apenas de numeros
*/
function soNumeros(e) {
    var tecla = (window.event) ? event.keyCode : e.which;

    if ((tecla > 47 && tecla < 58))
        return true;
    else {
        if (tecla == 8 || tecla == 0)
            return true;
        else
            return false;
    }
}



