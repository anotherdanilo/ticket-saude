﻿/*
* Ticket - Edenred Brasil
*
* Este javascript contém todas as chamadas
* ao plugin de validação do site
* 
* @section Javascript
*/

/* Formulário de "Envie uma mensagem" */
validarCampos($('#envieMensagem'), {
    txtNome: {
        minlength: 3,
        maxlength: 100,
        required: true
    },
    txtEmail: {
        minlength: 3,
        maxlength: 100,
        required: true,
        email: true
    },
    txtTelefone: {
        required: true
    },
    txtMensagem: {
        required: true
    }
});

/* Formulário 1 do "Simule e compre" */
validarCampos($('#tre'), {
    nroBeneficiarios: {
        number: true,
        required: true,
        minlength: 1,
        maxlength: 5
    },
    txtDiasUteis: {
        number: true,
        minlength: 1,
        maxlength: 2,
        required: true
    },
    txtValorBeneficio: {
        required: true
    }
});

/* Formulário 2 do "Simule e compre" */
validarCampos($('#voucher'), {
    nroValorFacial: {
        required: true
    },
    nroFolhasBloco: {
        number: true,
        minlength: 1,
        maxlength: 6,
        required: true
    },
    nroQtdBlocos: {
        number: true,
        minlength: 1,
        maxlength: 6,
        required: true
    }
});

/* PAT */
validarCampos($('#formSimulaPATPasso01'), {
    cartaoPassos: {
        required: true
    }
});

validarCampos($('#formSimulaPATPasso02'), {
    nroCartao: {
        required: true,
        number: true
    },
    txtDiasUteis: {
        required: true,
        number: true
    },
    txtValorBeneficio: {
        required: true
    }
});

validarCampos($('#formSimulaPATPasso03'), {
    txtLair: {
        required: true
    }
});




/* Simule c Compre Alimentação Natal */
validarCampos($('#alimentacaoNatal'), {
    txtValorBeneficio: {
        required: true
    },
    nroBeneficiarios: {
        required: true,
        number: true
    }
});

// Formulário "Ticket na Facul" de Sobre a Ticket
validarCampos($('#formTicketFacul'), {
    txtNome: {
        minlength: 3,
        maxlength: 100,
        required: true
    },
    txtEmail: {
        minlength: 3,
        maxlength: 100,
        email: true,
        required: true
    },
    txtTelefone: {
        required: true
    },
    txtCelular: {
        required: true
    },
    txtCEP: {
        required: true
    },
    txtEndereco: {
        required: true
    },
    txtNumero: {
        number: true,
        required: true
    },
    txtComplemento: {
        required: true
    },
    txtDescricaoProjeto: {
        minlength: 3,
        maxlength: 2048,
        required: true
    },
    txtNomeInstituicao: {
        minlength: 3,
        maxlength: 100,
        required: true
    },
    txtCurso: {
        required: true
    },
    txtAnoInicioCurso: {
        number: true,
        minlength: 2,
        maxlength: 4,
        required: true
    },
    txtAnoFimCurso: {
        number: true,
        minlength: 2,
        maxlength: 4,
        required: true
    },
    nroMatricula: {
        required: true
    },
    txtNomeResponsavel: {
        minlength: 3,
        maxlength: 100,
        required: true
    },
    txtCEPInstituicao: {
        required: true
    },
    txtEnderecoInstituicao: {
        required: true
    },
    txtNumeroInstituicao: {
        number: true,
        required: true
    },
    txtComplementoInstituicao: {
        minlength: 3,
        maxlength: 100,
        required: true
    },
    txtDescricaoTema: {
        minlength: 3,
        maxlength: 4000,
        required: true
    },
    txtDescricaoStatusTCC: {
        minlength: 3,
        maxlength: 4000,
        required: true
    },
    txtDescricaoArea: {
        minlength: 3,
        maxlength: 4000,
        required: true
    },
    txtDescricaoNecessidades: {
        minlength: 3,
        maxlength: 4000,
        required: true
    },
    txtDescricaoEdenred: {
        minlength: 3,
        maxlength: 4000,
        required: true
    }
});

// Formulário "Enviar projeto" de Sobre a Ticket
validarCampos($('#ticketCases'), {
    txtInstituicao: {
        minlength: 3,
        maxlength: 100,
        required: true
    },
    txtNome: {
        minlength: 3,
        maxlength: 100,
        required: true
    },
    txtEmail: {
        minlength: 3,
        maxlength: 100,
        email: true,
        required: true
    },
    txtCPF: {
        required: true
    },
    txtTelefone: {
        required: true
    }
});

/* Formulário "Enviar projeto" de Sobre a Ticket */
validarCampos($('#projeto'), {
    txtCelular: {
        required: true
    },
    txtCPFeCNPJ: {
        required: true
    },
    txtRazaoSocial: {
        required: true
    },
    txtNomeResponsavel: {
        minlength: 3,
        maxlength: 100,
        required: true
    },
    txtEmailResponsavel: {
        minlength: 3,
        maxlength: 100,
        email: true,
        required: true
    },
    txtDescricaoProjeto: {
        minlength: 3,
        maxlength: 2048,
        required: true
    },
    tipoProposta: {
        required: true
    }
});

/* Formulários de download de materiais - Sobre a Ticket */
validarCampos($('#downloadLogos'), {
    nomeIndicacao: {
        required: true
    },
    emailIndicacao: {
        required: true,
        email: true
    },
    nomeEmpresaIndicacao: {
        required: true
    }
});

/* Formulários de download de materiais - Sobre a Ticket */
validarCampos($('#downloadCartoes'), {
    nomeIndicacao: {
        required: true
    },
    emailIndicacao: {
        required: true,
        email: true
    },
    nomeEmpresaIndicacao: {
        required: true
    }
});

// Formulário "Simule e compre" de Ticket Car
validarCampos($('#ticketCar'), {
    nroBeneficiarios: {
        number: true,
        required: true
    },

    txtValorBeneficio: {
        required: true
    },
    txtCNPJ: {
        required: true
    }
});

/* Formulário de "Simule um pedido" de Ticket Transporte */
validarCampos($('#simulePedidoTransporte'), {
    ddlLocalidade: {
        required: true
    },
    txtFuncionarios: {
        required: true,
        number: true
    },
    txtCNPJFuncionarios: {
        required: true
    }
});

/* Formulário de Indique sua empresa */
validarCampos($('#indicacao'), {
    txtNome: {
        minlength: 3,
        maxlength: 100,
        required: true
    },
    txtEmail: {
        minlength: 3,
        maxlength: 100,
        email: true,
        required: true
    },
    txtCNPJ: {
        required: true
    },
    txtNomeEmpresa: {
        minlength: 3,
        maxlength: 100,
        required: true
    },
    txtTelefone: {
        required: true
    },
    txtNomeResponsavel: {
        minlength: 3,
        maxlength: 100,
        required: true
    },
    txtEmailResponsavel: {
        minlength: 3,
        maxlength: 100,
        email: true,
        required: true
    }
});

/* Formulário de "Adesivos de sinalização" de Portal do Credenciado */
validarCampos($('#form-adesivos'), {
    txtNome: {
        minlength: 3,
        maxlength: 100,
        required: true
    },
    txtEmail: {
        minlength: 3,
        maxlength: 100,
        email: true,
        required: true
    },
    txtTelefone: {
        required: true
    },
    txtCelular: {
        required: true
    },
    txtCNPJ: {
        required: true
    },
    txtRazaoSocial: {
        required: true
    },
    txtEndereco: {
        required: true
    },
    txtNumero: {
        number: true,
        required: true
    },
    txtBairro: {
        required: true
    },
    selectCidade: {
        required: true
    },
    selectEstado: {
        required: true
    }
});

/* Formulário de "Antecipe seu reembolso" de Portal do Credenciado */
validarCampos($('#envieMensagemR'), {
    txtNome: {
        minlength: 3,
        maxlength: 100,
        required: true
    },
    txtEmail: {
        minlength: 3,
        maxlength: 100,
        email: true,
        required: true
    },
    txtRazaoSocialR: {
        required: true
    },
    txtCNPJR: {
        required: true
    },
    txtTelefone: {
        required: true
    },
    celularReembolso: {
        required: true
    },
    txtMensagem: {
        required: true
    }
});

/* Formulário de "Inicie seu credeciamento" de Seja um credenciado */
validarCampos($('.formCredenciamento'), {
    txtCNPJ: {
        required: true,
        minlength: 19,
        maxlength: 19
    },
    email: {
        required: true,
        email: true
    }
});

/* Formulário de "Inicie seu credeciamento passo 2" de Seja um credenciado */
validarCampos($('#step2'), {
    codAtv: {
        required: true
    },
    razaoSocial: {
        required: true
    },
    nomeFantasia: {
        required: true
    },
    inscricaoEstadual: {
        required: true
    },
    nEstabelecimentoRede: {
        required: true
    },
    telEstabelecimento: {
        required: true
    },
    cep: {
        required: true
    },
    endereco: {
        required: true
    },
    numero: {
        required: true
    },
    complemento: {
        required: true
    },
    bairro: {
        required: true
    },
    cidade: {
        required: true
    },
    uf: {
        required: true,
        minlength: 2,
        maxlength: 2
    }
});

/* Formulário de "Inicie seu credeciamento passo 3" de Seja um credenciado */
validarCampos($('#step3'), {
    nLugares: {
        required: true
    },
    nRefeicoes: {
        required: true
    },
    areaAtendimento: {
        required: true
    }
});

/* Formulário de "Inicie seu credeciamento passo 4" de Seja um credenciado */
validarCampos($('#step4'), {
    nomeCompleto: {
        required: true
    },
    cpf: {
        required: true
    },
    rg: {
        required: true
    },
    email: {
        required: true,
        email: true
    },
    dataNascimento: {
        required: true,
        date: true
    },
    cargo: {
        required: true
    },
    nomeCompleto2: {
        required: true
    },
    cpf2: {
        required: true
    },
    rg2: {
        required: true
    },
    email2: {
        required: true,
        email: true
    },
    dataNascimento2: {
        required: true,
        date: true
    },
    cargo2: {
        required: true
    },
    nomeCompletoInterlocutor: {
        required: true
    },
    emailInterlocutor: {
        required: true,
        email: true
    },
    CargoInterlocutor: {
        required: true
    },
    DDDtelefone: {
        required: true,
        minlength: 4,
        minlength: 4
    },
    DDDcelular: {
        required: true,
        minlength: 4,
        minlength: 4
    }
});

/* Formulário de "Inicie seu credeciamento passo 5" de Seja um credenciado */
validarCampos($('#step5'), {
    agencia: {
        required: true
    },
    cc: {
        required: true
    },
    banco: {
        required: true
    }
});

/* Formulário de "Consultar saldo" */
validarCampos($('#consultarSaldo'), {
    nroCartaoTicket: {
        required: true
    },
    txtCaptcha: {
        required: true
    }
});

/* Formulário de "Consultar saldo" de ticket transporte */
validarCampos($('#ticketTransporte'), {
    txtCPF: {
        required: true
    }
});

/* Formulário de busca de "Consulta de saldo" */
validarCampos($('#consulta'), {
    busca: {
        required: true
    }
});

/* Formulário do "Solicite um contato" */
validarCampos($('#solicite'), {
    txtNome: {
        minlength: 3,
        maxlength: 100,
        required: true
    },
    txtEmail: {
        minlength: 3,
        maxlength: 100,
        required: true,
        email: true
    },
    txtTelefone: {
        required: true
    },
    txtEmpresa: {
        required: true
    },
    txtMensagem: {
        required: true
    }
});

/* Formulário "Simule a economia" de cadastro do PAT */
// Formulário 1
validarCampos($('#formSimulaPAT01'), {}, {});

// Formulário 2
validarCampos($('#formSimulaPAT02'), {
    nroCartao: {
        required: false,
        number: true,
    },
    txtDiasUteis: {
        required: true,
        number: true
    },
    txtValorBeneficio: {
        required: true
    }
});

// Formulário 3
validarCampos($('#formSimulaPAT03'), {
    txtLair: {
        required: true
    }
});

/* Formulário "Simule e compre" de Ticket Alimentação Natal */
validarCampos($("#ticketNatal"), {
    txtValorBeneficio: {
        required: true
    },
    nroBeneficiarios: {
        required: true
    }
});

// Formulário "Download de materiais" de Sobre a Ticket
validarCampos($('.formDownload'), {
    nomeIndicacao: {
        minlength: 3,
        maxlength: 100,
        required: true
    },
    emailIndicacao: {
        minlength: 3,
        maxlength: 100,
        email: true,
        required: true
    },
    nomeEmpresaIndicacao: {
        minlength: 3,
        maxlength: 100,
        required: true
    }
});

// Formulário "Simule um pedido voucher" de Ticket Cultura
validarCampos($("#voucherCultura"), {
    nroEmissao: {
        number: true,
        required: true
    },
    txtDiasUteisVoucher: {
        number: true,
        minlength: 1,
        maxlength: 2,
        required: true
    },
    txtValorBeneficioVoucher: {
        required: true
    }
});

/* Formulário "Calcular economia" de Ticket Cultura */
// Passo 1
validarCampos($('#formTicketCultura01'), {
    txtFaixaSalarial: {
        required: true
    },
    nroCartao: {
        required: true,
        number: true
    }
});

// Passo 2
validarCampos($('#formTicketCultura02'), {
    txtLair: {
        required: true,
    }
});

/* Formulário de "Inicie seu credeciamento último passo" de Seja um credenciado */
validarCampos($('.formFinalizar'), {
    txtCNPJ: {
        required: true,
        minlength: 19,
        maxlength: 19
    }
});

/* Formulário de "Simulação de pedido" de Fazer Simulação */
validarCampos($('#formSimulaCAD01'), {}, {}, {}, function () { progressoF(passo2, 2); });

validarCampos($('#formSimulaCAD02'), {
    nroCartaoRestaurante: {
        required: true,
        number: true
    },
    nroCartaoRestaurante: {
        required: true,
        number: true
    },
    nroCartaoAlimentacao: {
        required: true,
        number: true
    },
    nroBeneficiariosNatal: {
        required: true,
        number: true
    },
    nroCartaoCultura: {
        required: true,
        number: true
    },
    nroCartao: {
        required: true,
        number: true
    },
    txtDiasUteis: {
        required: true,
        number: true
    },
    txtValorBeneficio: {
        required: true
    },
    nroFolhasBloco: {
        number: true,
        minlength: 1,
        maxlength: 6,
        required: true
    },
    nroQtdBlocos: {
        number: true,
        minlength: 1,
        maxlength: 6,
        required: true
    },
    txtValorBeneficioRestaurante: {
        required: true
    },
    txtValorBeneficioVoucher: {
        required: true
    },
    txtValorBeneficioAlimentacao: {
        required: true
    },
    txtValorBeneficioNatal: {
        required: true
    },
    txtLocalidade: {
        required: true
    },
    nroBeneficiarios: {
        required: true
    },
    txtValorBeneficioCultura: {
        required: true
    },
    txtValorBeneficioCar: {
        required: true
    }
}, {});


validarCampos($('#formSimulaCAD03'), {}, {}, function () { progressoF(passo4, 4); });


validarCampos($('#formModalContato'), {
    txtNome: {
        minlength: 3,
        maxlength: 100,
        required: true
    },
    txtEmail: {
        minlength: 3,
        maxlength: 100,
        email: true,
        required: true
    },
    txtCNPJ: {
        required: true
    },
    txtTelefone: {
        required: true
    }
}, {}, function () { btnEnviaModalContato.attr('data-dismiss', 'modal'); });


validarCampos($('#formModalEditar'), {
    nroCartao: {
        required: true,
        number: true
    },
    txtDiasUteis: {
        required: true,
        number: true
    },
    txtValorBeneficio: {
        required: true

    }
}, {}, function () { btnEnviaModalEditar.attr('data-dismiss', 'modal'); });


validarCampos($('#formNaoCadastrado'), {
    txtCNPJ: {
        required: true
    },
    txtCPF: {
        required: true
    },
    txtEmail: {
        minlength: 3,
        maxlength: 100,
        email: true,
        required: true
    }
});


validarCampos($('#formCadastrado'), {
    txtSenha: {
        minlength: 3,
        maxlength: 100,
        required: true
    },
    txtEmail: {
        minlength: 3,
        maxlength: 100,
        email: true,
        required: true
    }
});


validarCampos($('#formCadastroEmpresa'), {
    txtRazaoSocial: {
        required: true
    },
    txtCNPJ: {
        required: true
    }
}, {}, function () { exibeSubPasso(3); });


validarCampos($('#formSimulaCAD04'), {
    txtRazaoSocial: {
        required: true
    },
    txtInscricaoEstadual: {
        number: true,
        required:
        {
            depends: function () {
                var isento0 = $("#IEisento-0:checked").val();
                if (isento0 == 'Isento') {
                    return false;
                }
                else {
                    return true;
                }
            }
        }

    },
    txtInscricaoMunicipal: {
        number: true,
        required:
        {
            depends: function () {
                var isento0 = $("#IMisento-0:checked").val();
                if (isento0 == 'Isento') {
                    return false;
                }
                else {
                    return true;
                }
            }
        }
    },
    txtCEP: {
        required: true
    },
    txtEndereco: {
        required: true
    },
    txtNumero: {
        required: true,
        number: true
    },
    txtNome: {
        minlength: 3,
        maxlength: 100,
        required: true
    },
    txtEmail: {
        minlength: 3,
        maxlength: 100,
        email: true,
        required: true
    },
    txtTelefone: {
        required: true
    },
    txtCPF: {
        required: true
    },
    txtRG: {
        required: true
    },
    sexo: {
        required: true
    },
    dataNascimento: {
        required: true,
        date: true
    },
    senhaCAD: 'required',
    confirmaSenha: {
        equalTo: '#senhaCAD'
    }
});

/* Formulário de "Download de poses" de Sala de imprensa sobre Eva */
validarCampos($('#formPoses'), {
    txtNome: {
        minlength: 3,
        maxlength: 100,
        required: true
    },
    txtEmail: {
        minlength: 3,
        maxlength: 100,
        required: true,
        email: true
    },
    txtTelefone: {
        required: true
    },
    txtVeiculo: {
        required: true
    },
    txtCargo: {
        required: true,
        minlength: 3,
        maxlength: 100
    },
    txtEmpresa: {
        required: true,
        minlength: 3,
        maxlength: 100
    }
});

validarCampos($('#formLogos'), {
    txtNomeLogo: {
        minlength: 3,
        maxlength: 100,
        required: true
    },
    txtEmailLogo: {
        minlength: 3,
        maxlength: 100,
        required: true,
        email: true
    },
    txtTelefoneLogo: {
        required: true
    },
    txtVeiculoLogo: {
        required: true
    },
    txtCargoLogo: {
        minlength: 3,
        maxlength: 100,
        required: true
    },
    txtEmpresaLogo: {
        minlength: 3,
        maxlength: 100,
        required: true
    }
});

validarCampos($('#formPecas'), {
    txtNomePeca: {
        minlength: 3,
        maxlength: 100,
        required: true
    },
    txtEmailPeca: {
        minlength: 3,
        maxlength: 100,
        required: true,
        email: true
    },
    txtTelefonePeca: {
        required: true
    },
    txtVeiculoPeca: {
        required: true
    },
    txtCargoPeca: {
        minlength: 3,
        maxlength: 100,
        required: true
    },
    txtEmpresaPeca: {
        minlength: 3,
        maxlength: 100,
        required: true
    }
});



/* Formulário de "Diferenciais na gestão" de Ticket Transporte */
validarCampos($('#diferenciaisGestao'), {
    txtNome: {
        minlength: 3,
        maxlength: 100,
        required: true
    },
    txtEmail: {
        minlength: 3,
        maxlength: 100,
        email: true,
        required: true
    },
    txtTelefone: {
        required: true
    },
    txtCNPJ: {
        required: true
    }
});

/* Formulário de "Proposta" da home */
validarCampos($('#formPropostaHome'), {
    txtNome: {
        minlength: 3,
        maxlength: 100,
        required: true
    },
    txtEmail: {
        minlength: 3,
        maxlength: 100,
        required: true,
        email: true
    },
    txtTelefone: {
        required: true
    },
    txtCNPJ: {
        required: true
    }
});


/*Validação somente de dois campos, Nome e Email*/
validarCampos($('#envieEmail'), {
    txtNome: {
        minlength: 3,
        maxlength: 100,
        required: true
    },
    txtEmail: {
        minlength: 3,
        maxlength: 100,
        required: true,
        email: true
    }
});