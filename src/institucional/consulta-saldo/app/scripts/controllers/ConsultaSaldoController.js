(function() {
    'use strict';
    angular.module('portalUsuarioApp').controller('ConsultaSaldoController', [
        '$scope', '$http',
        function($scope, $http) {
            var main = this;
            main.bodyClass = 'menu-opened';

            $scope.items = [{ html: "<span>1</span>" }, { html: "<h3>some text</h3>" }];

            $scope.teste = "Teste";

            //Simple GET request example:
            // $http({
            //     method: 'GET',
            //     url: 'http://localhost/superbc/#/'
            // }).then(function successCallback(response) {
            //     // this callback will be called asynchronously
            //     // when the response is available
            // }, function errorCallback(response) {
            //     // called asynchronously if an error occurs
            //     // or server returns response with an error status.
            // });

        }
    ]);

}).call(this);
